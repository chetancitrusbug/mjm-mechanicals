<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobInspectionForm extends Model
{
    //
    use SoftDeletes;

    public $table='job_inspection_form';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','job_id','user_id','date','customer_name','address','phone','email','system_age','stay_cool_enough','has_been_cycling','temperature_consistent','have_any_weird_smells','increase_electric_bill','ac_hp_1','brand_1','model_1','serial_1','ac_hp_2','brand_2','model_2','serial_2','ac_hp_3','brand_3','model_3','serial_3','filter_size_1','quantity_1','accessories_1','filter_size_2','quantity_2','accessories_2','filter_size_3','quantity_3','accessories_3','seer','stages','fan_type','cap','disconnect','refrigerant','blower_amps_r','blower_amps_a','blower_capacitor_r','blower_capacitor_a','copressor_amps_r','copressor_amps_a','fan_motor_r','fan_motor_a','outdoor_capacitor_r','outdoor_capacitor_a','sub_cooling_r','sub_cooling_a','psi_low_side','psi_high_side','low_line_temp','high_line_temp','indoor_outdoor_db','indoor_outdoor_wb','condenser_coil','evaporator_coil','drain_line','refrigerant_leak','ref_add','ref_add_qty','ref_add_lbs','ref_removed','ref_removed_qty','ref_removed_lbs','notes','tech','occupant','bottom_date','status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function getDateAttribute($value)
    {
        if($value != ""){
            return \Carbon\Carbon::parse($value)->format('d-m-Y');
        }
        return $value;
    }

    public function setDateAttribute($value)
    {
        if($value != ""){
            $this->attributes['date'] = \Carbon\Carbon::parse($value)->format('Y-m-d');
        }
    }

    public function getBottomDateAttribute($value)
    {
        if($value != ""){
            return \Carbon\Carbon::parse($value)->format('d-m-Y');
        }
        return $value;
    }

    public function setBottomDateAttribute($value)
    {
        if($value != ""){
            $this->attributes['bottom_date'] = \Carbon\Carbon::parse($value)->format('Y-m-d');
        }
    }
}
