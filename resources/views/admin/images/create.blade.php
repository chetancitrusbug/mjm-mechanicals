@extends('layouts.backend')


@section('title',trans('image.add_image'))



@section('pageTitle')
    <i class="icon-tint"></i>

    <span>@lang('image.add_new_image')</span>


    @endsection


@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('image.add_new_image')</div>
                    <div class="panel-body">
                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('image.back')</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/images', 'class' => 'form-horizontal','id'=>'formimage','enctype'=>'multipart/form-data']) !!}

                        @include ('admin.images.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
@endsection

