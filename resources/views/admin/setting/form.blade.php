<div class="row">
    <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">
            <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
                {!! Form::label('email', 'Email *', ['class' => 'col-sm-4 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('email', null, ['class' => 'form-control']) !!} {!! $errors->first('email', '
                    <p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div> 
    </div>
   
    <div class="col-sm-12 floatLeft">
        <div class="col-sm-6">  
            <div class="form-group">
                <div class="col-sm-offset-8 col-sm-4">
                    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary','id'=>'setting-submit']) !!}
                </div>
            </div>
        </div>
    </div> 
</div>                   