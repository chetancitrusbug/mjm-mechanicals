@extends('layouts.backend')


@section('title',trans('jobcode.add_jobcode'))



@section('pageTitle')
    <i class="icon-tint"></i>

    <span>@lang('jobcode.add_new_jobcode')</span>


    @endsection


@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">@lang('jobcode.add_new_jobcode')</div>
                    <div class="panel-body">
                        <a href="{{ URL::previous() }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>  @lang('document.back')</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        {!! Form::open(['url' => '/admin/jobcode', 'class' => 'form-horizontal','id'=>'formjobcode','enctype'=>'multipart/form-data']) !!}

                        @include ('admin.jobcode.form')

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
@endsection

