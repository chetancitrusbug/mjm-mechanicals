<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use App\ActionLog;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');

        //$this->middleware('session_user');

    }


    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            if(!Auth::user()->hasRole('SU', true) && !Auth::user()->hasRole('SA', true))
            {
                $this->guard()->logout();
                $request->session()->invalidate();

                return $this->sendFailedLoginResponse($request);
            }
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }


    // public function postLogin(Request $request)
    // {
    //     $this->validate($request, [
    //         'email' => 'required|email', 'password' => 'required',
    //     ]);

    //     $throttles = $this->isUsingThrottlesLoginsTrait();

    //     if ($throttles && $this->hasTooManyLoginAttempts($request)) {
    //         return $this->sendLockoutResponse($request);
    //     }

    //     $credentials = $request->only('email', 'password');

    //     if (Auth::attempt($credentials,true))
    //     {
    //         Auth::user()->time= diffInMinutes( Carbon::now());
    //         Auth::user()->save();
    //         return redirect()->intended('dashboard');
    //     }


    //     if ($throttles) {
    //         $this->incrementLoginAttempts($request);
    //     }

    //     return redirect($this->loginPath())
    //         ->withInput($request->only($this->loginUsername(), 'remember'))
    //         ->withErrors([
    //             $this->loginUsername() => $this->getFailedLoginMessage(),
    //         ]);

    // }


}
