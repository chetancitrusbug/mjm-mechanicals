@extends('layouts.backend')
@section('title',trans('job.view_form'))
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">Invoice  </div>
    {!! Form::model($jobInvoiceForm, [ 'method' => 'PATCH', 'url' => ['/admin/jobInvoice', $jobInvoiceForm->job_main_invoice_id],
    'class' => 'form-horizontal','id'=>'jobInvoice', 'enctype'=>'multipart/form-data' ]) !!}
    <a href="{{ url('/admin/job/' . $job->id . '/edit') }}" title="Back to Forms list"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true" ></i>Back</button></a>
        {!! Form::hidden('job_id', $job->id, ['class' => 'form-control input-100']) !!}
        <div class="panel-body">

            <div class="invoicebox-top1 clearfix">
            <div class="row">
                <div class="col-md-4 col-sm-3 clearfix">
                    <div class="boxvcolor01 clearfix">
                        <h1>TO REORDER THESE FORMS</h1>
                        <h1 class="small-h1">CALL TOLL FREE <a href="tel:1800-633-2950">1(800)633-2950</a></h1>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 clearfix">
                    <div class="boxvcolor01 clearfix">
                        <p class="bigtxt-1">ALABAMA BUSINESS FORMS, INC. </p>
                        <p>HEATING AND AIR CONDITIONING, DIV. </p>
                        <p>725 NORTH MEMORIAL PARKWAY </p>
                        <p>HUNTSVILLE, ALABAMA 35801 </p>
                        <p><a href="//www.alabamabusinessforms.com">www.alabamabusinessforms.com</a> </p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-3 clearfix">
                    <div class="boxvcolor01 clearfix">
                        <h4>IN ALABAMA <a href="tel:256-534-8038">(256) 534-8038</a></h4>
                    </div>
                </div>
            </div>

            </div>

            <hr>

            <div class="grid-box-1 colbox-in01 clearfix">

            <div class="row">

                <div class="col-md-3 col-sm-4 clearfix">
                    <img src="{!! asset('assets/images/carrier-logo-full.png') !!}" class="img-responsive carrier-logo01">
                </div>

                <div class="col-md-6 col-sm-4 clearfix">
                    <div class="box-in01 text-center">
                        <h3>MJM MECHANICAL LLC</h3>
                        <p><span class="span-block">921 E. DUPONT RD. PMB 801</span><span class="span-block">FORT WAYNE, IN 46825</span> <span class="span-block">(260) 740-1509</span> </p>
                    </div>
                </div>

                <div class="col-md-3 col-sm-4 clearfix">
                    <p class="height-auto">Date:</p>
                    {!! Form::text('main_date', null, ['class' => 'form-control input-100 main_date']) !!}
                    <p class="height-auto">DATE ORDERED:</p>
                    {!! Form::text('date_ordered', null, ['class' => 'form-control input-100 date_ordered']) !!}
                </div>
            </div>

            <div class="row">
                <div class="invoice-box1 clearfix">
                    <div class="col-md-5 col-sm-4 clearfix">
                        <p class="height-auto">NAME:</p>
                        {!! Form::text('name', null, ['class' => 'form-control input-100']) !!}
                    </div>
                    <div class="col-md-4 col-sm-4 clearfix">
                        <p class="height-auto">Email:</p>
                        {!! Form::text('email', null, ['class' => 'form-control input-100']) !!}
                    </div>
                    <div class="col-md-3 col-sm-4 clearfix">
                        <p class="height-auto">DATE SCHEDULED:</p>
                        {!! Form::text('date_scheduled', null, ['class' => 'form-control input-100 date_scheduled']) !!}
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="invoice-box2 clearfix">
                    <div class="col-md-9 col-sm-9 clearfix">
                        <p class="height-auto">STREET:</p>
                        {!! Form::text('street', null, ['class' => 'form-control input-100']) !!}
                    </div>
                    <div class="col-md-3 col-sm-3 clearfix">
                        <p class="height-auto">PHONE:</p>
                        {!! Form::text('phone', null, ['class' => 'form-control input-100']) !!}
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="invoice-box3 clearfix">
                    <div class="col-md-4 col-sm-3 clearfix">
                        <p class="height-auto">CITY:</p>
                        {!! Form::text('city', null, ['class' => 'form-control input-100']) !!}
                    </div>
                    <div class="col-md-3 col-sm-3 clearfix">
                        <p class="height-auto">STATE :</p>
                        {!! Form::text('state', null, ['class' => 'form-control input-100']) !!}
                    </div>
                    <div class="col-md-2 col-sm-3 clearfix">
                        <p class="height-auto">ZIP:</p>
                        {!! Form::text('zip', null, ['class' => 'form-control input-100']) !!}
                    </div>
                    <div class="col-md-3 col-sm-3 clearfix">
                        <p class="height-auto">WK. OR CELL:</p>
                        {!! Form::text('wk_or_cell', null, ['class' => 'form-control input-100']) !!}
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="invoice-box4 clearfix">
                    <div class="col-md-9 col-sm-12 clearfix">

                        <div class="col-md-12 col-sm-12 clearfix">
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th class="col-md-3 text-center">MAKE</th>
                                        <th class="col-md-3 text-center">MODEL </th>
                                        <th class="col-md-3 text-center">SERIAL NUMBER </th>
                                    </tr>
                                    </thead>
                                    <tbody class="text-center">
                                    <tr>
                                            <td>{!! Form::text('make', null, ['class' => 'form-control input-box-1']) !!}</td>
                                            <td>{!! Form::text('model', null, ['class' => 'form-control input-box-1']) !!}</td>
                                            <td>{!! Form::text('serial_number', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>

                        <div class="col-md-12 col-sm-12 clearfix">
                        <div class="row">
                            <p class="height-auto">JOB LOCATION :</p>
                            {!! Form::text('job_location', null, ['class' => 'form-control input-100']) !!}
                        </div>
                        </div>


                    </div>
                    <div class="col-md-3 col-sm-12 mt-m10 clearfix">
                        <div class="checklist01 clearfix">{!! Form::checkbox('warranty', null , null, ['class'=>'form-radio']) !!}<span class="pl-10">WARRANTY</span></div>
                        <div class="checklist01 clearfix">{!! Form::checkbox('contract', null , null, ['class'=>'form-radio']) !!}<span class="pl-10">CONTRACT </span></div>
                        <div class="checklist01 clearfix">{!! Form::checkbox('service_contract', null , null, ['class'=>'form-radio']) !!}<span class="pl-10">SERVICE CONTRACT </span></div>
                        <div class="checklist01 clearfix">{!! Form::checkbox('normal', null , null, ['class'=>'form-radio']) !!}<span class="pl-10">NORMAL </span></div>
                        <div class="checklist01 clearfix">{!! Form::checkbox('res', null , null, ['class'=>'form-radio']) !!}<span class="pl-10">RES</span></div>
                        <div class="checklist01 clearfix">{!! Form::checkbox('comm', null , null, ['class'=>'form-radio']) !!}<span class="pl-10">COMM. </span></div>
                    </div>

                </div>
            </div>

            <div class="row mt-10">
                <div class="col-md-9 col-sm-12 clearfix">

                    <div class="table-responsive">
                        <table class="table table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th class="col-md-9 text-center">DESCRIPTION OF WORK </th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <tr>
                                <td>{!! Form::text('desc_of_work_1', null, ['class' => 'form-control input-box-1','placeholder'=>'ORIGINAL COMPLAINT']) !!}</td>
                            </tr>
                            <tr>
                                <td>{!! Form::text('desc_of_work_2', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                            <tr>
                                <td>{!! Form::text('desc_of_work_3', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                            <tr>
                                <td>{!! Form::text('desc_of_work_4', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                            <tr>
                                <td>{!! Form::text('desc_of_work_5', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                        </tbody>
                        </table>
                    </div>

                    <div class="col-md-7 col-sm-12 clearfix">
                        <div class="row">
                        <div class="table-responsive vertical-align-center vertical-align-center2">
                            <table class="table table-bordered table-responsive">
                                <tbody class="text-center">
                                    <tr>
                                    <td rowspan="3" class="text-uppecase color-b01"> <span class="span-block">L</span><span class="span-block">a</span><span class="span-block">b</span><span class="span-block">o</span><span class="span-block">r</span> </td>
                                    </tr>
                                    <tr>
                                    <td class="td-nospace">
                                        <p class="display-inline"><span class="span-p10 middle50">TECH <br>#1</span> </p>
                                        <p class="display-inline"><span class="span-p10">HRS. @ </span>{!! Form::text('tech_1_regular_hrs', null, ['class' => 'form-control auto-input1']) !!}</p>
                                        <p class="display-inline"><span class="span-p10">/HR.=</span><span class="abs-top5">REGULAR</span>{!! Form::text('tech_1_regular_hr', null, ['class' => 'form-control auto-input1']) !!}</p>
                                    </td>
                                    </tr>
                                    <tr>
                                    <td class="td-nospace">
                                        <p class="display-inline"><span class="span-p10 middle50">TECH <br>#2</span> </p>
                                        <p class="display-inline"><span class="span-p10">HRS. @ </span>{!! Form::text('tech_2_regular_hrs', null, ['class' => 'form-control auto-input1']) !!}</p>
                                        <p class="display-inline"><span class="span-p10">/HR.=</span><span class="abs-top5">REGULAR</span>{!! Form::text('tech_2_regular_hr', null, ['class' => 'form-control auto-input1']) !!}</p>
                                    </td>
                                    </tr>


                                </tbody>
                            </table>
                        </div>
                        </div>
                    </div>

                    <div class="col-md-5 col-sm-12 clearfix">
                        <div class="row">
                        <div class="table-responsive vertical-align-center vertical-align-center2 vertical-align-center3">
                            <table class="table table-bordered table-responsive">
                                <tbody class="text-center">
                                    <tr>
                                    <td rowspan="3" class="text-uppecase color-b01 f10"><span class="span-block">O</span><span class="span-block">V</span><span class="span-block">E</span><span class="span-block">R</span><span class="span-block">T</span><span class="span-block">I</span><span class="span-block">M</span><span class="span-block">E</span> </td>
                                    </tr>
                                    <tr>
                                    <td class="td-nospace">
                                        <p class="display-inline"><span class="span-p10">HRS. @ </span>{!! Form::text('tech_1_overtime_hrs', null, ['class' => 'form-control auto-input1']) !!}</p>
                                        <p class="display-inline"><span class="span-p10">/HR.=</span><span class="abs-top5">OVERTIME</span>{!! Form::text('tech_1_overtime_hr', null, ['class' => 'form-control auto-input1']) !!}</p>
                                    </td>
                                    </tr>
                                    <tr>
                                    <td class="td-nospace">
                                        <p class="display-inline"><span class="span-p10">HRS. @ </span>{!! Form::text('tech_2_overtime_hrs', null, ['class' => 'form-control auto-input1']) !!}</p>
                                        <p class="display-inline"><span class="span-p10">/HR.=</span><span class="abs-top5">OVERTIME</span>{!! Form::text('tech_2_overtime_hr', null, ['class' => 'form-control auto-input1']) !!}</p>
                                    </td>
                                    </tr>


                                </tbody>
                            </table>
                        </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-3 col-sm-12 clearfix">
                    <div class="table-responsive">
                        <table class="table table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th class="col-md-3 text-center" colspan="2">SERVICE </th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <tr>
                                <td class="col-md-9">{!! Form::text('service_1', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td class="col-md-3">{!! Form::text('service_2', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                            <tr>
                                <td>{!! Form::text('service_3', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('service_4', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                            <tr>
                                <td>{!! Form::text('service_5', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('service_6', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                            <tr>
                                <td>{!! Form::text('service_7', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('service_8', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                            <tr>
                                <td>{!! Form::text('service_9', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('service_10', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                            <tr>
                                <td>{!! Form::text('service_11', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td></td>
                            </tr>
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="invoice-box7 clearfix">

                    <div class="col-md-4 col-sm-4 clearfix">
                        <p class="height-auto">TECHNICIAN SIGNATURE:</p>
                        {!! Form::text('technician_signature', null, ['class' => 'form-control input-100']) !!}
                    </div>
                    <div class="col-md-4 col-sm-4 clearfix">
                        <p class="height-auto">CERT. #:</p>
                        {!! Form::text('cert', null, ['class' => 'form-control input-100']) !!}
                    </div>
                    <div class="col-md-4 col-sm-4 clearfix">
                        <p class="height-auto">TOTAL OTHER CHARGES :</p>
                        {!! Form::text('service_total_other_charges', null, ['class' => 'form-control input-100']) !!}
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-md-8 col-sm-7 clearfix">
                    <div class="invoice-box8 clearfix">
                        <h5 class="blueh51">TERMS: DUE UPON COMPLETION</h5>
                        <p class="pblue">I HAVE THE AUTHORITY TO ORDER THE ABOVE WORK AND DO SO ORDER AS OUTLINED ABOVE. IT IS AGREED THAT THE SELLER WILL RETAIN TITLE TO ANY EQUIPMENT OR MATERIAL FURNISHED UNTIL FINAL & COMPLETE PAYMENT IS MADE. AND IF SETTLEMENT IS NOT MADE AS AGREED, THE SELLER SHALL HAVE THE RIGHT TO REMOVE SAME AND THE SELLER WILL BE HELD HARMLESS FOR ANY DAMAGES RESULTING FROM THE REMOVAL THEREOF.</p>
                        <div class="col-md-12 col-sm-12 clearfix">
                        {!! Form::text('authorized_signature', null, ['class' => 'form-control input-100 mb-5']) !!}
                        <p class="height-auto text-center">AUTHORIZED SIGNATURE </p>
                        </div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-5 clearfix">
                    <div class="table-responsive vertical-align-center">
                        <table class="table table-bordered table-responsive">

                        <tbody class="text-center">

                            <tr>
                                <td class="col-md-3 color-td1"><span>SUB TOTAL</span></td>
                                <td class="col-md-6">{!! Form::text('sub_total', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td class="col-md-3"><input type="text" class="form-control input-box-1" disabled></td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control input-box-1" disabled></td>
                                <td><input type="text" class="form-control input-box-1" disabled></td>
                                <td><input type="text" class="form-control input-box-1" disabled></td>
                            </tr>
                            <tr>
                                <td class="col-md-3"><span>TRIP CHARGE</span></td>
                                <td class="col-md-6"><input type="text" class="form-control input-box-1" disabled></td>
                                <td class="col-md-3">{!! Form::text('trip_charge', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                            <tr>
                                <td class="col-md-3"><span>TAX</span></td>
                                <td class="col-md-6"><input type="text" class="form-control input-box-1" disabled></td>
                                <td class="col-md-3">{!! Form::text('tax', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                            <tr>
                                <td><input type="text" class="form-control input-box-1" disabled></td>
                                <td><input type="text" class="form-control input-box-1" disabled></td>
                                <td><input type="text" class="form-control input-box-1" disabled></td>
                            </tr>
                            <tr>
                                <td class="col-md-3 color-td2"><span class="span-block">TOTAL</span><span class="span-block">AMOUNT</span><span class="span-block"> DUE </span></td>
                                <td class="col-md-6"><input type="text" class="form-control input-box-1" disabled></td>
                                <td class="col-md-3">{!! Form::text('total_amount_due', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>

                        </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="row">

                <p class="p-15">ABOVE ORDERED WORK HAS BEEN COMPLETED AND I ACKNOWLEDGE RECEIPT OF MY COPY. </p>
                <div class="col-md-8 col-sm-8 clearfix">
                    <p class="height-auto"></p>
                    {!! Form::text('above_ordered_work_has_been_completed', null, ['class' => 'form-control input-100 mt-024']) !!}
                </div>
                <div class="col-md-4 col-sm-4 clearfix">
                    <p class="height-auto">Date:</p>
                    {!! Form::text('date', null, ['class' => 'form-control input-100 invoice_date']) !!}
                </div>

            </div>

            </div>

            <hr>

            <div class="grid-box-2 colbox-in02 clearfix">

            <div class="col-md-4 col-sm-12 mb-padding0 clearfix">
                <div class="box-left box-checklist1 clearfix">
                    <h3 class="h3-title1">CHECK LIST</h3>
                    <div class="rowbox1 clearfix">
                        <div class="checklist01 clearfix">{!! Form::checkbox('compressor', null , null, ['class'=>'form-radio']) !!}<span class="pl-10 f-600">COMPRESSOR</span></div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('suction', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">SUCTION </p><p class="fleft-02 clearfix">{!! Form::text('suction_value', null, ['class' => 'form-control input-100 ']) !!}<span class="block-span1">PSI</span> </p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('head', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">HEAD </p><p class="fleft-02 clearfix">{!! Form::text('head_value', null, ['class' => 'form-control input-100 ']) !!}<span class="block-span1">PSI</span> </p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('volts', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">VOLTS </p><p class="fleft-02 clearfix">{!! Form::text('volts_value', null, ['class' => 'form-control input-100 ']) !!}<span class="block-span1">AMPS</span> </p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('electrical_connections_compressor', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">ELECTRICAL CONNECTIONS</p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('contractor_tight_clean', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600">
                            <p class="fleft-01">CONTACTOR TIGHT & CLEAN </p>
                            {{-- <p class="fleft-02 clearfix">
                                <label class="radio-inline"><input type="radio" value="" name="checkbox">Y</label>
                                <label class="radio-inline"><input type="radio" value="" name="checkbox">N</label>
                            </p> --}}
                        </span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('oil_level_condition', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">OIL LEVEL & CONDITION</p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('burn_out', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">BURN OUT</p></span>
                        </div>

                    </div>

                    <div class="rowbox1 clearfix">
                        <div class="checklist01 clearfix">{!! Form::checkbox('condenser_coil', null , null, ['class'=>'form-radio']) !!}<span class="pl-10 f-600">CONDENSER COIL </span></div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('clean_coil_check_fin_cond', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">CLEAN COIL & CHECK FIN COND.</p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('ent_lvg', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600 blk-span"> <p class="fleft-01">ENT </p><p class="fleft-02 clearfix">{!! Form::text('ent', null, ['class' => 'form-control input-100px input-70px ']) !!}<span class="block-span1">°F</span> </p></span>
                        <span class="pl-10 f-600 blk-span"> <p class="fleft-01">LVG </p><p class="fleft-02 clearfix">{!! Form::text('lvg', null, ['class' => 'form-control input-100px input-70px ']) !!}<span class="block-span1">°F</span> </p></span>
                        </div>
                    </div>

                    <div class="rowbox1 clearfix">
                        <div class="checklist01 clearfix">{!! Form::checkbox('refrigerant', null , null, ['class'=>'form-radio']) !!}<span class="pl-10 f-600">REFRIGERANT</span></div>
                        <div class="checklist02 checklist02-50 clearfix">
                        {!! Form::checkbox('leak', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">LEAK</p></span>
                        </div>
                        <div class="checklist02 checklist02-50 clearfix">
                        {!! Form::checkbox('charge', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">CHARGE</p></span>
                        </div>
                    </div>

                    <div class="rowbox1 clearfix">
                        <div class="checklist01 clearfix">{!! Form::checkbox('fan_and_motor', null , null, ['class'=>'form-radio']) !!}<span class="pl-10 f-600">FAN AND MOTOR </span></div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('volts_amps', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">VOLTS </p><p class="fleft-02 clearfix">{!! Form::text('fan_and_motor_volts_value', null, ['class' => 'form-control input-100px']) !!}<span class="block-span1">AMPS</span> {!! Form::text('fan_and_motor_amps_value', null, ['class' => 'form-control input-100px']) !!}</p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('electrical_connections_fan_and_motor', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">ELECTRICAL CONNECTIONS</p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('contacts_tight_clean', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">CONTACTS TIGHT & CLEAN</p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('fan_pulleys', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">FAN PULLEYS (ADJUST BELT) </p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('check_lube_bearings_motor', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">CHECK, LUBE BEARINGS & MOTOR</p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('cfm', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">CFM </p><p class="fleft-02 clearfix">{!! Form::text('cfm_value', null, ['class' => 'form-control input-100 ']) !!}</p></span>
                        </div>


                    </div>

                    <div class="rowbox1 clearfix">
                        <div class="checklist01 clearfix">{!! Form::checkbox('evaporator_coil', null , null, ['class'=>'form-radio']) !!}<span class="pl-10 f-600">EVAPORATOR COIL </span></div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('clean_coil_check_fin', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">CLEAN COIL & CHECK FIN</p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('ent_db_lvg_db', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600 blk-span"> <p class="fleft-01">ENT DB </p><p class="fleft-02 clearfix">{!! Form::text('ent_db', null, ['class' => 'form-control input-100  input-70px  ']) !!}<span class="block-span1">°F</span> </p></span>
                        <span class="pl-10 f-600 blk-span"> <p class="fleft-01">LVG DB </p><p class="fleft-02 clearfix">{!! Form::text('lvg_db', null, ['class' => 'form-control input-100  input-70px  ']) !!}<span class="block-span1">°F</span> </p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('ent_wb_lvg_wb', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600 blk-span"> <p class="fleft-01">ENT WB </p><p class="fleft-02 clearfix">{!! Form::text('ent_wb', null, ['class' => 'form-control input-100  input-70px  ']) !!}<span class="block-span1">°F</span> </p></span>
                        <span class="pl-10 f-600 blk-span"> <p class="fleft-01">LVG WB </p><p class="fleft-02 clearfix">{!! Form::text('lvg_wb', null, ['class' => 'form-control input-100  input-70px  ']) !!}<span class="block-span1">°F</span> </p></span>
                        </div>

                    </div>

                    <div class="rowbox1 clearfix">
                        <div class="checklist01 clearfix">{!! Form::checkbox('condensate_areas', null , null, ['class'=>'form-radio']) !!}<span class="pl-10 f-600">CONDENSATE AREAS </span></div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('inspect_clean_drain_pan', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">INSPECT & CLEAN DRAIN PAN</p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('inspect_clean_drain', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">INSPECT & CLEAN DRAIN</p></span>
                        </div>

                    </div>

                    <div class="rowbox1 clearfix">
                        <div class="checklist01 clearfix">{!! Form::checkbox('air_filters', null , null, ['class'=>'form-radio']) !!}<span class="pl-10 f-600">AIR FILTERS </span></div>
                        <div class="checklist02 checklist02-50 clearfix">
                        {!! Form::checkbox('cleaned', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">CLEANED </p></span>
                        </div>
                        <div class="checklist02 checklist02-50 clearfix">
                        {!! Form::checkbox('replaced', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">REPLACED </p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('filter_size', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">FILTER SIZE </p><p class="fleft-02 clearfix">{!! Form::text('filter_size_value', null, ['class' => 'form-control input-100 ']) !!}</p></span>
                        </div>


                    </div>

                    <div class="rowbox1 clearfix">
                        <div class="checklist01 clearfix">{!! Form::checkbox('heating_assy', null , null, ['class'=>'form-radio']) !!}<span class="pl-10 f-600">HEATING ASSY. </span></div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('burner_heat_exchanger', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">BURNER & HEAT EXCHANGER </p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('fuel_supply_pressure', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">FUEL SUPPLY & PRESSURE </p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('pilot_assembly', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">PILOT ASSEMBLY  </p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('flame_adjustment', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">FLAME ADJUSTMENT</p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('primary_relay_flue', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">PRIMARY RELAY & FLUE </p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('fan_limit_switch_oper', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">FAN & LIMIT SWITCH OPER.  </p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('blower_assembly', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">BLOWER ASSEMBLY </p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('rv_valve', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">RV VALVE</p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('strip_heat', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">STRIP HEAT</p></span>
                        </div>
                        <div class="checklist02 clearfix">
                        {!! Form::checkbox('defrost_cycle', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">DEFROST CYCLE</p></span>
                        </div>

                    </div>

                    <div class="rowbox1 clearfix">
                        <div class="checklist01 clearfix">{!! Form::checkbox('electrical_compts', null , null, ['class'=>'form-radio']) !!}<span class="pl-10 f-600">ELECTRICAL COMP'TS. </span></div>
                        <div class="checklist02 checklist02-50 clearfix">
                        {!! Form::checkbox('relays', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">RELAYS </p></span>
                        </div>
                        <div class="checklist02 checklist02-50 clearfix">
                        {!! Form::checkbox('contactors', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">CONTACTORS  </p></span>
                        </div>
                        <div class="checklist02 checklist02-50 clearfix">
                        {!! Form::checkbox('overload', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">OVERLOAD </p></span>
                        </div>
                        <div class="checklist02 checklist02-50 clearfix">
                        {!! Form::checkbox('press_switch', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">PRESS. SWITCH</p></span>
                        </div>

                    </div>

                    <div class="rowbox1 clearfix">
                        <div class="checklist01 clearfix">{!! Form::checkbox('thermostat', null , null, ['class'=>'form-radio']) !!}<span class="pl-10 f-600">THERMOSTAT </span></div>
                        <div class="checklist02 checklist02-50 clearfix">
                        {!! Form::checkbox('ok_thermostat', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">О.К. </p></span>
                        </div>
                        <div class="checklist02 checklist02-50 clearfix">
                        {!! Form::checkbox('replace', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">REPLACE  </p></span>
                        </div>
                        <div class="checklist02 checklist02-50 clearfix">
                        {!! Form::checkbox('relocate', null , null, ['class'=>'form-radio']) !!}
                        <span class="pl-10 f-600"> <p class="fleft-01">RELOCATE </p></span>
                        </div>
                    </div>

                </div>

                <div class="box-left box-inputlist2 line-box01 clearfix">
                    <h4 class="h4-title1">TRAVEL TIME </h4>

                    <div class="boxa-input01 clearfix">
                        <div class="blk-div1 clearfix">
                        <p class="height-auto">TIME ARRIVED </p>
                        {!! Form::text('time_arrived', null, ['class' => 'form-control input-100']) !!}
                        </div>
                        <div class="blk-div1 clearfix">
                        <p class="height-auto">TIME DEPARTED</p>
                        {!! Form::text('time_departed', null, ['class' => 'form-control input-100']) !!}
                        </div>
                        <div class="blk-div1 clearfix">
                        <p class="height-auto">TRAVEL TIME </p>
                        {!! Form::text('travel_time', null, ['class' => 'form-control input-100']) !!}
                        </div>
                    </div>
                </div>

                <div class="box-left box-inputlist2 line-box01 clearfix">
                    <h4 class="h4-title1">MILEAGE </h4>

                    <div class="boxa-input01 clearfix">
                        <div class="blk-div1 clearfix">
                        <p class="height-auto">ENDING </p>
                        {!! Form::text('ending', null, ['class' => 'form-control input-100']) !!}
                        </div>
                        <div class="blk-div1 clearfix">
                        <p class="height-auto">START </p>
                        {!! Form::text('start', null, ['class' => 'form-control input-100']) !!}
                        </div>
                        <div class="blk-div1 clearfix">
                        <p class="height-auto">TOTAL MILES </p>
                        {!! Form::text('total_miles', null, ['class' => 'form-control input-100']) !!}
                        </div>
                        <div class="blk-div1 clearfix">
                        <p class="display-inline50 display-inline501">{!! Form::text('total_miles_x1', null, ['class' => 'form-control auto-input1','placeholder' => 'X']) !!}</p>
                        <p class="display-inline50 display-inline502"><span class="span-p10">/HR. =</span>{!! Form::text('trip_miles_hr', null, ['class' => 'form-control auto-input1']) !!}</p>
                        </div>
                        <div class="blk-div1 clearfix">
                            <p class="display-inline50 display-inline501">{!! Form::text('total_miles_x2', null, ['class' => 'form-control auto-input1','placeholder' => 'X']) !!}</p>
                        <p class="display-inline50 display-inline502"><span class="span-p10">/MI. =</span>{!! Form::text('total_miles_mi', null, ['class' => 'form-control auto-input1']) !!}</p>
                        </div>
                        <div class="blk-div1 clearfix">
                        <p class="height-auto">TRIP CHARGES </p>
                        {!! Form::text('totalcharges', null, ['class' => 'form-control input-100']) !!}
                        </div>
                    </div>
                </div>



            </div>
            <div class="col-md-8 col-sm-12 mb-padding0 clearfix">
                <div class="row">
                    <div class="invoice-box4 invoice-box-r2 clearfix">
                        <div class="col-md-12 col-sm-12 clearfix">
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th class="col-md-2 text-center">QUANTITY</th>
                                        <th class="col-md-5 text-center">ITEM OR PART DESCRIPTION </th>
                                        <th class="col-md-2 text-center">PRICE </th>
                                        <th class="col-md-3 text-center color-b02">AMOUNT </th>
                                    </tr>
                                    </thead>
                                    <tbody class="text-center">

                                    <tr>
                                        <td>{!! Form::text('quantity_1', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('item_or_part_description_1', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('price_1', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('amount_1', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>{!! Form::text('quantity_2', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('item_or_part_description_2', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('price_2', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('amount_2', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>{!! Form::text('quantity_3', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('item_or_part_description_3', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('price_3', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('amount_3', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>{!! Form::text('quantity_4', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('item_or_part_description_4', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('price_4', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('amount_4', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>{!! Form::text('quantity_5', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('item_or_part_description_5', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('price_5', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('amount_5', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>{!! Form::text('quantity_6', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('item_or_part_description_6', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('price_6', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('amount_6', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>{!! Form::text('quantity_7', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('item_or_part_description_7', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('price_7', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('amount_7', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>{!! Form::text('quantity_8', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('item_or_part_description_8', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('price_8', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('amount_8', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>{!! Form::text('quantity_9', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('item_or_part_description_9', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('price_9', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('amount_9', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>{!! Form::text('quantity_10', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('item_or_part_description_10', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('price_10', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('amount_10', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>{!! Form::text('quantity_11', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('item_or_part_description_11', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('price_11', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('amount_11', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>{!! Form::text('quantity_12', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('item_or_part_description_12', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('price_12', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('amount_12', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>{!! Form::text('quantity_13', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('item_or_part_description_13', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('price_13', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('amount_13', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>{!! Form::text('quantity_14', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('item_or_part_description_14', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('price_14', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('amount_14', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>{!! Form::text('quantity_15', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('item_or_part_description_15', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('price_15', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('amount_15', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>{!! Form::text('quantity_16', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('item_or_part_description_16', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('price_16', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('amount_16', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>{!! Form::text('quantity_17', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('item_or_part_description_17', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('price_17', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('amount_17', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>{!! Form::text('quantity_18', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('item_or_part_description_18', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('price_18', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('amount_18', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>{!! Form::text('quantity_19', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('item_or_part_description_19', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('price_19', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td>{!! Form::text('amount_19', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    <tr>
                                        <td>{!! Form::text('total_parts', null, ['class' => 'form-control input-box-1']) !!}</td>
                                        <td class="text-rightside"><p>TOTAL PARTS</p></td>
                                        <td><input type="text" class="form-control input-box-1" disabled></td>
                                        <td><input type="text" class="form-control input-box-1" disabled></td>
                                    </tr>


                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="invoice-box-r3 clearfix">

                        <div class="col-md-6 col-sm-6 clearfix">
                        <div class="row">
                            <div class="box-content-1 clearfix">
                                <h3>PARTS WARRANTY </h3>
                                <p>All parts as recorded are warranted as per manufacturer specifications. </p>
                                <h3>LABOR GUARANTY</h3>
                                <p>The labor charge as recorded here relative to the equipment serviced as noted, is guaranteed for a period of 30 days. </p>
                                <p>We do not, of course, guaranty other parts than those</p>
                                <p>we supply. If repairs later become necessary due to other defective parts, they will be charged separately.</p>
                            </div>
                        </div>
                        </div>
                        <div class="col-md-6 col-sm-6 clearfix">

                        <div class="table-responsive">
                            <table class="table table-bordered table-responsive">
                                <thead>
                                    <tr>
                                    <th class="text-center th-title" colspan="2">CHARGES FROM BELOW </th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                    <tr>
                                    <td><p class="p015">WRITE OR CODE</p></td>
                                    <td><p class="p015">AMOUNT</p></td>
                                    </tr>
                                    <tr>
                                    <td>{!! Form::text('write_or_code_1', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    <td>{!! Form::text('charges_from_amount_1', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    <tr>
                                    <td>{!! Form::text('write_or_code_2', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    <td>{!! Form::text('charges_from_amount_2', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    <tr>
                                    <td>{!! Form::text('write_or_code_3', null, ['class' => 'form-control input-box-1','placeholder'=>'REFER MACH. USAGE']) !!}</td>
                                    <td>{!! Form::text('charges_from_amount_3', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>
                                    <tr>
                                    <td><p class="p015 blue-bg1"><span class="span-block">TOTAL</span> OTHER CHARGES </p></td>
                                    <td>{!! Form::text('total_other_charges', null, ['class' => 'form-control input-box-1']) !!}</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>



                        </div>

                    </div>

                    <div class="invoice-box-r4 clearfix">
                        <div class="col-md-12 col-sm-12 clearfix">
                        <div class="row">
                            <h4 class="h4-title2">ENVIRONMENT CHECK LIST  </h4>

                            <div class="col-md-7 col-sm-12 clearfix">
                                <div class="row">
                                    <div class="table-responsive vertical-align-center vertical-align-center2">
                                    <table class="table table-bordered table-responsive">
                                        <tbody class="text-center">
                                            <tr>
                                                <td rowspan="10" class="text-uppecase color-b01 color-b03 vm-center"> <span class="span-block">R</span><span class="span-block">E</span><span class="span-block">F</span><span class="span-block">R</span><span class="span-block">I</span><span class="span-block">G</span><span class="span-block">E</span><span class="span-block">R</span><span class="span-block">A</span><span class="span-block">N</span><span class="span-block">T</span> </td>
                                            </tr>

                                            <tr>
                                                <td class="td-nospace">
                                                <p class="display-10">CHRG.<br> CODE</p>
                                                <p class="display-60">TYPE</p>
                                                <p class="display-30">SYSTEM</p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td-nospace">
                                                <p class="display-10"></p>
                                                <p class="display-60">
                                                    <span class="span-p10">REFRIG. </span>{!! Form::text('refrig', null, ['class' => 'form-control auto-input1 w-120px']) !!}
                                                </p>
                                                <p class="display-30">
                                                    <span class="span-p10">QTY. </span>{!! Form::text('refrig_qty', null, ['class' => 'form-control auto-input1 w-70px']) !!}
                                                </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td-nospace">
                                                <div class="display-10"><span class="roundnumber">1</span></div>
                                                <div class="display-60">
                                                    <span class="span-p10">RECOVERED? </span>
                                                    <div class="checklist01 checkblock01  clearfix">{!! Form::radio('recovered', '1', null, ['class' => '']) !!}<span class="pl-10 f-600">YES</span></div>
                                                    <div class="checklist01 checkblock01  clearfix">{!! Form::radio('recovered', '0', null, ['class' => '']) !!}<span class="pl-10 f-600">NO</span></div>
                                                </div>
                                                <div class="display-30">
                                                    <span class="span-p10">QTY. </span>{!! Form::text('recovered_qty', null, ['class' => 'form-control auto-input1 w-70px']) !!}
                                                </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td-nospace">
                                                <div class="display-10"><span class="roundnumber">2</span></div>
                                                <div class="display-60">
                                                    <span class="span-p10">RECYCLED? </span>
                                                    <div class="checklist01 checkblock01  clearfix">{!! Form::radio('recycled', '1', null, ['class' => '']) !!}<span class="pl-10 f-600">YES</span></div>
                                                    <div class="checklist01 checkblock01  clearfix">{!! Form::radio('recycled', '0', null, ['class' => '']) !!}<span class="pl-10 f-600">NO</span></div>
                                                </div>
                                                <div class="display-30">
                                                    <span class="span-p10">QTY. </span>{!! Form::text('recycled_qty', null, ['class' => 'form-control auto-input1 w-70px']) !!}
                                                </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td-nospace">
                                                <div class="display-10"><span class="roundnumber">3</span></div>
                                                <div class="display-60">
                                                    <span class="span-p10">RECLAIMED? </span>
                                                    <div class="checklist01 checkblock01  clearfix">{!! Form::radio('reclaimed', '1', null, ['class' => '']) !!}<span class="pl-10 f-600">YES</span></div>
                                                    <div class="checklist01 checkblock01  clearfix">{!! Form::radio('reclaimed', '0', null, ['class' => '']) !!}<span class="pl-10 f-600">NO</span></div>
                                                </div>
                                                <div class="display-30">
                                                    <span class="span-p10">QTY. </span>{!! Form::text('reclaimed_qty', null, ['class' => 'form-control auto-input1 w-70px']) !!}
                                                </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td-nospace">
                                                <div class="display-10"> &nbsp;</div>
                                                <div class="display-60">
                                                    <span class="span-p10">RETURNED TO THIS SYSTEM? </span>
                                                    <div class="checklist01 checkblock01  clearfix">{!! Form::radio('returned_to_this_system', '1', null, ['class' => '']) !!}<span class="pl-10 f-600">YES</span></div>
                                                    <div class="checklist01 checkblock01  clearfix">{!! Form::radio('returned_to_this_system', '0', null, ['class' => '']) !!}<span class="pl-10 f-600">NO</span></div>
                                                </div>
                                                <div class="display-30">
                                                    <span class="span-p10">QTY. </span>{!! Form::text('returned_to_this_system_qty', null, ['class' => 'form-control auto-input1 w-70px']) !!}
                                                </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td-nospace">
                                                <p class="display-10"><span class="roundnumber">4</span></p>
                                                <p class="display-60">
                                                    <span class="span-p10">DISPOSAL </span>{!! Form::text('disposal_1', null, ['class' => 'form-control auto-input1 w-120px']) !!}
                                                </p>
                                                <p class="display-30">
                                                    <span class="span-p10">QTY. </span>{!! Form::text('disposal_qty', null, ['class' => 'form-control auto-input1 w-70px']) !!}
                                                </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td-nospace">
                                                <div class="display-10"> &nbsp;</div>
                                                <div class="display-60">
                                                    <span class="span-p10">NON USEABLE </span>
                                                    <div class="checklist01 checkblock01  clearfix">{!! Form::radio('non_useable', '1', null, ['class' => '']) !!}<span class="pl-10 f-600">YES</span></div>
                                                    <div class="checklist01 checkblock01  clearfix">{!! Form::radio('non_useable', '0', null, ['class' => '']) !!}<span class="pl-10 f-600">NO</span></div>
                                                </div>
                                                <div class="display-30">
                                                    <span class="span-p10">QTY. </span>{!! Form::text('non_useable_qty', null, ['class' => 'form-control auto-input1 w-70px']) !!}
                                                </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td-nospace paddingb15">
                                                <div class="display-10"><span class="roundnumber">5</span></div>
                                                <div class="display-60">
                                                    <span class="span-p10">DISPOSAL</span>
                                                </div>
                                                <div class="display-30">
                                                </div>
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-5 col-sm-12 clearfix">
                                <div class="row">

                                    <div class="table-responsive vertical-align-center vertical-align-center2">
                                    <table class="table table-bordered table-responsive">
                                        <tbody class="text-center">
                                            <tr>
                                                <td rowspan="10" class="text-uppecase color-b01 color-b03 vm-center"> <span class="span-block">E</span><span class="span-block">Q</span><span class="span-block">U</span><span class="span-block">I</span><span class="span-block">P</span><span class="span-block">M</span><span class="span-block">E</span><span class="span-block">N</span><span class="span-block">T</span></td>
                                            </tr>

                                            <tr>
                                                <td class="td-nospace">
                                                <div class="display-60 display-100">
                                                    <span class="span-p10">CHANGED OUT( OR REPLACED )? </span>
                                                    <div class="checklist01 checkblock01  clearfix">{!! Form::radio('changed_out', '1', null, ['class' => '']) !!}<span class="pl-10 f-600">YES</span></div>
                                                    <div class="checklist01 checkblock01  clearfix">{!! Form::radio('changed_out', '0', null, ['class' => '']) !!}<span class="pl-10 f-600">NO</span></div>
                                                </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td-nospace">
                                                <div class="display-60 display-100">
                                                    <span class="span-p10">DIS-MANTLED?</span>
                                                    <div class="checklist01 checkblock01  clearfix">{!! Form::radio('dis_mantled', '1', null, ['class' => '']) !!}<span class="pl-10 f-600">YES</span></div>
                                                    <div class="checklist01 checkblock01  clearfix">{!! Form::radio('dis_mantled', '0', null, ['class' => '']) !!}<span class="pl-10 f-600">NO</span></div>
                                                </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="td-nospace">
                                                <div class="display-10"><span class="roundnumber">6</span></div>
                                                <p class="display-90">
                                                    <span class="span-p10">REFRIGERANT DISPOSAL</span>{!! Form::text('refrigerant_disposal', null, ['class' => 'form-control auto-input1 w-120px']) !!}
                                                </p>
                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                    </div>

                                    <div class="col-md-12 col-sm-12 clearfix">
                                    <p class="height-auto">OUR PERSONNEL RECOMMEND:</p>
                                    {!! Form::text('our_personnel_recommend', null, ['class' => 'form-control input-100']) !!}
                                    </div>

                                    <div class="owner-div1 clearfix">

                                    <h4 class="h4-title2">OWNER's INITILS </h4>

                                    <div class="a-div1 clearfix">
                                        <div class="col-50per">
                                            <p>ACCEPTED</p>
                                            {!! Form::radio('accepted', '1', null, ['class' => 'form-radio']) !!}
                                        </div>
                                        <div class="col-50per">
                                            <p>DECLINED</p>
                                            {!! Form::radio('declined', '1', null, ['class' => 'form-radio']) !!}
                                        </div>
                                    </div>

                                    </div>

                                </div>
                            </div>

                        </div>
                        </div>
                    </div>

                </div>
            </div>

            </div>
            <div class="grid-box-1 clearfix">
                <div class=" pull-left">
                    {!! Form::submit((isset($jobInvoiceForm) & $jobInvoiceForm != null)? 'Update' : 'Add', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
@push('script-head')
    <script type="text/javascript">
        $(function () {
            $('.main_date,.invoice_date,.date_ordered,.date_scheduled').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        })

    </script>

@endpush