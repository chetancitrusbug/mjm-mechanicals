@extends('layouts.backend')

@section('title',trans('builder.builders'))
@section('pageTitle',trans('builder.builders'))

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('builder.builders')
                                                  </div>

                               </div>
                <div class="box-content ">


                    <div class="row">
                        <div class="col-md-6">
                                <a href="{{ url('/admin/builder/create') }}" class="btn btn-success btn-sm"
                                   title="Add New BUilder">
                                    <i class="fa fa-plus" aria-hidden="true"></i> @lang('builder.add_new_builder')
                                </a>

                        </div>

                        <div class="col-md-6">
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/builder', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
        
                            {!! Form::close() !!}
                            </div>
                        </div>
                    


                    <div class="table-responsive">
                        <table class="table table-borderless" id="builder-table">
                            <thead>
                            <tr>
                               <!-- <th data-priority="1">@lang('builder.id')</th> -->                        
                                <th data-priority="3">@lang('builder.name')</th>  
                                <th data-priority="3">@lang('builder.email')</th>  
                                <th data-priority="7">@lang('builder.status')</th>
                                <th data-priority="8">@lang('builder.actions')</th>                        
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('script-head')
<script>
var url ="{{ url('/admin/builder/') }}";
           
        datatable = $('#builder-table').DataTable({
           // "order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
             ajax: {
                    url: '{!! route('BuilderControllerBuilderData') !!}',
                    type: "get", // method , by default get
                    
                },
                columns: [
                   // { data: 'id', name: 'Id',"searchable": false },
                    { data: 'name',name:'builder.name',"searchable" : true}, 
                    { data: 'email',name:'builder.email',"searchable" : true},       
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                            if(o.status=='blocked')
                            status = "<a href='"+url+"/"+o.id+"?status=blocked' data-id="+o.id+" title='blocked'><button class='btn btn-default btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>@lang('builder.blocked')</button></a>";
                            else if(o.status == 'inactive')
                            
                            status = '<a href="'+url+'/'+o.id+'?status=inactive" title="inactive"><button class="btn btn-success btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>@lang("builder.inactive")</button></a>';
                            else
                            status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('builder.active')</button></a>";
                            
                            return status;
                                            
                        }

                    }, 
					{ 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";
                           
                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>@lang('builder.edit')</button></a>&nbsp;";

                                d = "<a href='javascript:void(0);' class='btn btn-primary btn-xs'  ><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash-o' aria-hidden='true'></i> @lang('builder.delete')</button></a>&nbsp;";
                               
                            var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i> @lang('builder.view')</button></a>&nbsp;";   
                            
                            
                            return v+e+d;
                        }
                    }
                    
                ]
        });
    
    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');
		
		var url ="{{ url('/admin/builder/') }}";
        url = url + "/" + id;
		//alert(url);
        var r = confirm("Are you sure you want to delete Builder ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });

</script>
@endpush
