<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangesColumnTypeInJobRepairTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::statement("ALTER TABLE `job_system_repair_replace_form` CHANGE `airflow` `airflow` VARCHAR(11) NULL DEFAULT NULL, CHANGE `noise` `noise` VARCHAR(11) NULL DEFAULT NULL, CHANGE `refrigerants` `refrigerants` VARCHAR(11) NULL DEFAULT NULL, CHANGE `cycling` `cycling` VARCHAR(11) NULL DEFAULT NULL, CHANGE `comfort` `comfort` VARCHAR(11) NULL DEFAULT NULL, CHANGE `remodeled` `remodeled` VARCHAR(11) NULL DEFAULT NULL, CHANGE `building_codes` `building_codes` VARCHAR(11) NULL DEFAULT NULL, CHANGE `venting` `venting` VARCHAR(11) NULL DEFAULT NULL, CHANGE `fuel_changes` `fuel_changes` VARCHAR(11) NULL DEFAULT NULL, CHANGE `safety_issues` `safety_issues` VARCHAR(11) NULL DEFAULT NULL, CHANGE `repair_history` `repair_history` VARCHAR(11) NULL DEFAULT NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::statement("ALTER TABLE `job_system_repair_replace_form` CHANGE `airflow` `airflow` VARCHAR(11) NULL DEFAULT NULL, CHANGE `noise` `noise` VARCHAR(11) NULL DEFAULT NULL, CHANGE `refrigerants` `refrigerants` VARCHAR(11) NULL DEFAULT NULL, CHANGE `cycling` `cycling` VARCHAR(11) NULL DEFAULT NULL, CHANGE `comfort` `comfort` VARCHAR(11) NULL DEFAULT NULL, CHANGE `remodeled` `remodeled` VARCHAR(11) NULL DEFAULT NULL, CHANGE `building_codes` `building_codes` VARCHAR(11) NULL DEFAULT NULL, CHANGE `venting` `venting` VARCHAR(11) NULL DEFAULT NULL, CHANGE `fuel_changes` `fuel_changes` VARCHAR(11) NULL DEFAULT NULL, CHANGE `safety_issues` `safety_issues` VARCHAR(11) NULL DEFAULT NULL, CHANGE `repair_history` `repair_history` VARCHAR(11) NULL DEFAULT NULL");
    }
}
