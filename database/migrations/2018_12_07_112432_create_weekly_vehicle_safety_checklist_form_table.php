<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeeklyVehicleSafetyChecklistFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_weekly_vehicle_safety_checklist_form', function (Blueprint $table) {
           $table->increments('id');
            $table->integer('job_id');
            $table->integer('user_id');
            $table->date('date')->nullable();
            $table->string('vehicle_unit_number')->nullable();
            $table->string('license_number')->nullable();
            $table->string('mileage')->nullable();
            $table->string('driver')->nullable();
            $table->string('year')->nullable();
            $table->string('make')->nullable();
            $table->string('model')->nullable();
            $table->string('head')->nullable();
            $table->string('back_up')->nullable();
            $table->string('parking')->nullable();
            $table->string('cargo')->nullable();
            $table->string('tail')->nullable();
            $table->string('flashers')->nullable();
            $table->string('directional')->nullable();
            $table->string('PSI')->nullable();
            $table->string('front_left')->nullable();
            $table->string('front_right')->nullable();
            $table->string('rear_left')->nullable();
            $table->string('rear_right')->nullable();
            $table->string('conventional_spare')->nullable();
            $table->text('tires_note')->nullable();
            $table->text('brakes_note')->nullable();
            $table->string('check_brake_pedal')->nullable();
            $table->text('comments')->nullable();
            $table->string('check_brake_fluid')->nullable();
            $table->string('windshield_wipers')->nullable();
            $table->string('paint_overall_condition')->nullable();
            $table->string('glass_overall_condition')->nullable();
            $table->string('ladder_Rack')->nullable();
            $table->string('engine')->nullable();
            $table->text('note_apparent_leakage')->nullable();
            $table->string('engine_oil')->nullable();
            $table->string('oil_condition')->nullable();
            $table->string('mileage_of_last_oil_change')->nullable();
            $table->string('windshield_washer_fluid')->nullable();
            $table->string('transmission_fluid_condition')->nullable();
            $table->string('transmission_color')->nullable();
            $table->string('power_steering_fluid')->nullable();
            $table->string('status')->default('active')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_weekly_vehicle_safety_checklist_form');

    }
}
