
<div class="form-group{{ $errors->has('title') ? ' has-error' : ''}}">
    {!! Form::label('title', trans('image.title'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', trans('image.description'), ['class' => 'col-md-4 control-label']) !!}
     <div class="col-md-6">
     {!! Form::textarea('description', null, ['class' => 'form-control','size' => '30x5']) !!}
    {!! $errors->first('description', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>
@if(isset($image) && $image->image_url)


<div class="form-group{{ $errors->has('image_url') ? ' has-error' : ''}}">
   {!! Form::label('image_url', trans('image.changeimage'),['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::file('image_url', null, ['class' => 'form-control']) !!}
            {!! $errors->first('image_url', '<p class="help-block with-errors">:message</p>') !!}
    </div>
	<img src={{ $image->image_full_url }} class="product_thumb">
</div>
@else
<div class="form-group{{ $errors->has('image_url') ? ' has-error' : ''}}">
   {!! Form::label('image_url', trans('image.addimage'),['class' => 'col-md-4 control-label required']) !!}
    <div class="col-md-6">
        {!! Form::file('image_url', null, ['class' => 'form-control']) !!}
            {!! $errors->first('image_url', '<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>

@endif

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : trans('image.create'), ['class' => 'btn btn-primary']) !!}
    </div>
</div>

