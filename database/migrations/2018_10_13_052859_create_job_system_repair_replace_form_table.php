<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobSystemRepairReplaceFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_system_repair_replace_form', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_id');
            $table->integer('user_id');
            $table->text('age_equipment_cooling_and_heat')->nullable();
            $table->text('age_equipment_furnace_and_boilers')->nullable();
            $table->text('age_equipment_packaged_units')->nullable();
            $table->text('estimate_cost_cooling_and_heat')->nullable();
            $table->text('estimate_cost_furnace_and_boilers')->nullable();
            $table->text('estimate_cost_packaged_units')->nullable();
            $table->text('repairs_under_warranty_cooling_and_heat')->nullable();
            $table->text('repairs_under_warranty_furnace_and_boilers')->nullable();
            $table->text('repairs_under_warranty_packaged_units')->nullable();
            $table->text('seer_of_equipment_cooling_and_heat')->nullable();
            $table->text('seer_of_equipment_packaged_units')->nullable();
            $table->text('type_of_refrigerant_cooling_and_heat')->nullable();
            $table->text('type_of_refrigerant_packaged_units')->nullable();
            $table->text('afue_furnace_and_boilers')->nullable();
            $table->text('afue_packaged_units')->nullable();
            $table->text('outdoor_equipment_cooling_and_heat')->nullable();
            $table->text('outdoor_equipment_packaged_units')->nullable();
            $table->text('indoor_equipment_cooling_and_heat')->nullable();
            $table->text('indoor_equipment_furnace_and_boilers')->nullable();
            $table->text('owner_expects_cooling_and_heat')->nullable();
            $table->text('owner_expects_furnace_and_boilers')->nullable();
            $table->text('owner_expects_packaged_units')->nullable();
            $table->integer('airflow')->nullable();
            $table->integer('noise')->nullable();
            $table->integer('refrigerants')->nullable();
            $table->integer('cycling')->nullable();
            $table->integer('comfort')->nullable();
            $table->integer('remodeled')->nullable();
            $table->integer('building_codes')->nullable();
            $table->integer('venting')->nullable();
            $table->integer('fuel_changes')->nullable();
            $table->integer('safety_issues')->nullable();
            $table->integer('repair_history')->nullable();
            $table->date('date')->nullable();
            $table->string('customer')->nullable();
            $table->string('technician')->nullable();
            $table->string('status')->default('active')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_system_repair_replace_form');
    }
}
