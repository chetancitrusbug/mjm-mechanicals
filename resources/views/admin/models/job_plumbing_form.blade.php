<div class="panel panel-default">
    <div class="panel-heading">Final Plumbing Check List</div>
    <div class="panel-body">
    <a href="#" title="Back to Forms list"><button type="button" class=" backToForm btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true" ></i>Back</button></a>
        {!! Form::hidden('job_id', $job->id, ['class' => 'form-control input-100']) !!}
            <div class="grid-box-1 clearfix">
            <div class="col-md-4 col-sm-4 pull-right clearfix">
                <p class="height-auto">Date:</p>
                {!! Form::text('date', null, ['class' => 'form-control input-100 plumbing_date']) !!}
            </div>
            </div>

            <div class="grid-box-1 box-HVAC-checklist box-final-plumbing clearfix">
            <div class="col-md-12 col-sm-12 clearfix">
                <div class="row">

                        <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('plumbing1', null , null, []) !!}ALL FIXTURES HAVE BEEN CHECKED FOR HOT AND COLD WATER </label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('plumbing2', null , null, []) !!}HOT ON LEFT, COLD ON RIGHT ON ALL FIXTURES  </label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('plumbing3', null , null, []) !!}TOILETS ARE SOLID AND DO NOT ROCK  </label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('plumbing4', null , null, []) !!}TOILETS HAVE SUFFICIENT WAX TO SEAL DRAIN </label>
                    </div>

                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('plumbing5', null , null, []) !!}TOILETS HAVE BEEN FLUSHED MINIMUM OF 5 TIMES EACH TO TEST FOR LEAKS </label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('plumbing6', null , null, []) !!}KITHCEN SINK IS SEALED TO COUNTER TOP, PUTTY FOR S.S. SINKS / CAULK FOR COMPOSIT SINKS</label>
                    </div>

                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('plumbing7', null , null, []) !!}DISHWASHER DRAIN IS LOOPED UP IN CABINET AND STRAPPED </label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('plumbing8', null , null, []) !!}KNOCK OUT PLUG REMOVED FROM DISPOSAL FOR DISHWASHER DRAIN </label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('plumbing9', null , null, []) !!}WHIRLPOOL TUB IS PLUGGED IN AND PUMP TESTED</label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('plumbing10', null , null, []) !!}TUBS AND TUBISHOWERS HAVE BEEN FILLED AND TESTED TO OVERFLOW </label>
                    </div>

                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('plumbing11', null , null, []) !!}OUTSIDE FAUCETS ARE SCREWED TO THE HOUSE </label>
                    </div>

                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('plumbing12', null , null, []) !!}FLOOR DRAINS, STAND PIPES AND WASHER DRAINS HAVE BEEN FILLED WITH WATER </label>
                    </div>

                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('plumbing13', null , null, []) !!}ALL "FUTURE" DRAINS HAVE BEEN CAPPED, ie: future bar sink, future bath  </label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('plumbing14', null , null, []) !!}SUMP PIT IS CLEAN AND FREE OF DEBRIS  </label>
                    </div>

                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('plumbing15', null , null, []) !!}SEWAGE PUMP AND ALARM PLUGGED IN  </label>
                    </div>

                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('plumbing16', null , null, []) !!}SUMP PUMP OPERATION TESTED </label>
                    </div>

                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('plumbing17', null , null, []) !!}ALL FIXTURES AND DRAINS HAVE BEEN DOUBLE CHECKED FOR LEAKS </label>
                    </div>

                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('plumbing18', null , null, []) !!}FINAL INSPECTION HAS BEEN CALLED IN </label>
                    </div>


                </div>
            </div>
        </div><!-- End of box-div -->
        <div class="grid-box-1 clearfix">
            <div class=" pull-left">
                {!! Form::submit((isset($jobPlumbingForm) & $jobPlumbingForm != null)? 'Update' : 'Add', ['class' => 'btn btn-primary'])
                !!}
            </div>
        </div>
    </div>
</div>
@push('script-head')
    <script type="text/javascript">
        $(function () {
            $('.plumbing_date').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        })

    </script>

@endpush