<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Jobassignemployee;
class Job extends Model
{
    use SoftDeletes;
   /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'job';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'title', 'description', 'duedate','client_id','notes','deleted_at','job_status','end_time', 'start_time','job_number'];

    public function clientName(){
        return $this->hasOne('App\Client','id','client_id')->select('clients.id as id','clients.name as name');
    }
    
    public function job(){
        return $this->hasMany('App\Job','job_id');
    }
	public function job_employee(){
        return $this->hasMany('App\Jobassignemployee','job_id','id')->leftjoin('users', 'jobassignemployee.job_employee_id', '=', 'users.id')
        ->select('*');
    }
}
