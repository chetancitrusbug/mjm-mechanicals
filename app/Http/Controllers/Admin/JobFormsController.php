<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Job;
use App\Folder;
use App\Video;
use App\Foldervideo;
use App\Jobtask;
use App\Folderdocument;
use App\Client;
use App\Document;
use App\Jobassignemployee;
use Session;
use App\User;
use App\Jobcard;
use App\Builder;
use App\AssignjobBuilder;
use App\Foldernotes;
use App\JobGeothermalForm;
use App\JobFurnaceForm;
use App\JobInspectionForm;
use App\JobChecklistForm;
use App\JobMaintenanceForm;
use App\JobSystemRepairForm;
use App\JobPlumbingForm;
use App\JobInvoice;
use App\JobInvoiceOtherField;
use App\JobWeeklyVehicleSafetyChecklistForm;
use App\JobProjrctWorksheet1;
use App\JobProjrctWorksheet2;
use App\JobProjrctWorksheet3;
use App\JobProjrctWorksheet4;
use App\Jobcode;
use DB;
use App\JobImage;
use App\JobImageFolder;
use App\Taskfolder;

use App\Forms;
use App\JobForms;

class JobFormsController extends Controller
{
    public function addJobGeothermalForm(Request $request){

        $rules = array(
            'job_id'=>'required|integer',
        );
        if($request->install_date != null)
        {
            $rules['install_date'] = 'required|date|date_format:Y-m-d';
        }
        if($request->desuperheater != null)
        {
            $rules['desuperheater'] = 'required|in:Y,N';
        }
        $this->validate($request, $rules, []);

        $job_id = $request->job_id;
        $requestData = $request->except('_token','job_name');
        $requestData['created_by'] = '';
        $data = JobGeothermalForm::create($requestData);

        $forms= Forms::where('form_code', 'geo_thermal')->first();
        if(isset($forms->id))
        {
            $formdata['job_id'] = $job_id;
            $formdata['job_form_id'] = $data->id;
            $formdata['form_id'] = $forms->id;
            JobForms::create($formdata);
        }
        Session::flash('flash_message','Job Form Added Successfully!');
        return redirect('admin/job/'.$request->job_id.'/edit');
    }

    public function editJobGeothermalForm(Request $request,$id){

        $rules = array();
        if ($request->install_date != null) {
            $rules['install_date'] = 'required|date|date_format:Y-m-d';
        }
        if ($request->desuperheater != null) {
            $rules['desuperheater'] = 'required|in:Y,N';
        }
        $this->validate($request, $rules, []);

        $jobGeothermalForm = JobGeothermalForm::where('id', $request->id)->where('status', 'active')->first();
        $requestData = $request->except('_token', 'job_name','job_id');
        $data = $jobGeothermalForm->update($requestData);

        Session::flash('flash_message', 'Job Form Updated Successfully!');
        return redirect('admin/job/' . $request->job_id . '/edit');
    }

    public function addJobFurnaceForm(Request $request){
        $rules = array(
            'job_id'=>'required|integer',
            'date'=>'nullable|date|date_format:d-m-Y',
            'email'=>'nullable|email',
            'system_age'=>'nullable|integer',
            'staying_warm_enough'=>'nullable|in:y,n',
            'temrature_consistent'=>'nullable|in:y,n',
            'dry_inside_home'=>'nullable|in:y,n',
            'have_any_weird_smells'=>'nullable|in:y,n',
            'accessories_1'=>'nullable|in:humidifier,condensate_pump,uv_light',
            'accessories_2'=>'nullable|in:electronic_air_cleaner,carbon_monoxide_detector',
            'accessories_3'=>'nullable|in:gps',
            'gas_type'=>'nullable|in:natural,propane,oil,none',
            'blower'=>'nullable|in:psc,x13,ecm',
            'ignition'=>'nullable|in:spark,standing_pilot,hsi',
            'stages'=>'nullable|in:1,2,3,mod',
            'draft_meter'=>'nullable|in:none,std,ecm',
            'efficiency'=>'nullable|in:electric,80+,92+,95+,98+',
            'temp_humidity'=>'nullable|in:indoor,oa,rh',
            'tstat_type'=>'nullable|in:mechanical,digital,programmable,communicating',
            // 'motor_bearings'=>'nullable|in:good,tight,need_replaced',
            // 'blower_wheel'=>'nullable|in:clean,need_cleaned,out_of_balance',
            // 'heat_exchanger'=>'nullable|in:ok,sign_of_strees,cracked',
            // 'burners'=>'nullable|in:clean,rusty,replace',
            // 'ignitor'=>'nullable|in:ok,replace',
            // 'limits'=>'nullable|in:pass,fail',
            'bottom_date'=>'nullable|date|date_format:d-m-Y',
        );

        $this->validate($request, $rules, []);

        $job_id = $request->job_id;
        $requestData = $request->except('_token','job_name');
        $requestData['created_by'] = '';

        $data = JobFurnaceForm::create($requestData);
        $forms = Forms::where('form_code', 'HVAC_inspection_furnace')->first();

        if(isset($forms->id))
        {
            $formdata['job_id'] = $job_id;
            $formdata['job_form_id'] = $data->id;
            $formdata['form_id'] = $forms->id;
            JobForms::create($formdata);
        }
        Session::flash('flash_message','Job Form Added Successfully!');
        return redirect('admin/job/'.$request->job_id.'/edit');
    }

    public function editJobFurnaceForm(Request $request,$id){

        $rules = array(
            'date'=>'nullable|date|date_format:d-m-Y',
            'email'=>'nullable|email',
            'system_age'=>'nullable|integer',
            'staying_warm_enough'=>'nullable|in:y,n',
            'temrature_consistent'=>'nullable|in:y,n',
            'dry_inside_home'=>'nullable|in:y,n',
            'have_any_weird_smells'=>'nullable|in:y,n',
            'accessories_1'=>'nullable|in:humidifier,condensate_pump,uv_light',
            'accessories_2'=>'nullable|in:electronic_air_cleaner,carbon_monoxide_detector',
            'accessories_3'=>'nullable|in:gps',
            'gas_type'=>'nullable|in:natural,propane,oil,none',
            'blower'=>'nullable|in:psc,x13,ecm',
            'ignition'=>'nullable|in:spark,standing_pilot,hsi',
            'stages'=>'nullable|in:1,2,3,mod',
            'draft_meter'=>'nullable|in:none,std,ecm',
            'efficiency'=>'nullable|in:electric,80+,92+,95+,98+',
            'temp_humidity'=>'nullable|in:indoor,oa,rh',
            'tstat_type'=>'nullable|in:mechanical,digital,programmable,communicating',
            // 'motor_bearings'=>'nullable|in:good,tight,need_replaced',
            // 'blower_wheel'=>'nullable|in:clean,need_cleaned,out_of_balance',
            // 'heat_exchanger'=>'nullable|in:ok,sign_of_strees,cracked',
            // 'burners'=>'nullable|in:clean,rusty,replace',
            // 'ignitor'=>'nullable|in:ok,replace',
            // 'limits'=>'nullable|in:pass,fail',
            'bottom_date'=>'nullable|date|date_format:d-m-Y',
        );

        $this->validate($request, $rules, []);

        $jobFurnaceForm = JobFurnaceForm::where('id', $request->id)->where('status', 'active')->first();
        $requestData = $request->except('_token', 'job_name','job_id');
        $requestData['blower_amps_r'] = isset($requestData['blower_amps_r'])?$requestData['blower_amps_r']:'';
        $requestData['blower_amps_a'] = isset($requestData['blower_amps_a'])?$requestData['blower_amps_a']:'';
        $requestData['blower_capacitor_r'] = isset($requestData['blower_capacitor_r'])?$requestData['blower_capacitor_r']:'';
        $requestData['blower_capacitor_a'] = isset($requestData['blower_capacitor_a'])?$requestData['blower_capacitor_a']:'';
        $requestData['gas_pressure_r'] = isset($requestData['gas_pressure_r'])?$requestData['gas_pressure_r']:'';
        $requestData['gas_pressure_a'] = isset($requestData['gas_pressure_a'])?$requestData['gas_pressure_a']:'';
        $requestData['draft_motor_amps_r'] = isset($requestData['draft_motor_amps_r'])?$requestData['draft_motor_amps_r']:'';
        $requestData['draft_motor_amps_a'] = isset($requestData['draft_motor_amps_a'])?$requestData['draft_motor_amps_a']:'';
        $requestData['draft_motor_capacitor_r'] = isset($requestData['draft_motor_capacitor_r'])?$requestData['draft_motor_capacitor_r']:'';
        $requestData['draft_motor_capacitor_a'] = isset($requestData['draft_motor_capacitor_a'])?$requestData['draft_motor_capacitor_a']:'';
        $requestData['ignitor_ohms_r'] = isset($requestData['ignitor_ohms_r'])?$requestData['ignitor_ohms_r']:'';
        $requestData['ignitor_ohms_a'] = isset($requestData['ignitor_ohms_a'])?$requestData['ignitor_ohms_a']:'';
        $requestData['temprature_rise_rated'] = isset($requestData['temprature_rise_rated'])?$requestData['temprature_rise_rated']:'';
        $requestData['temprature_rise_actual'] = isset($requestData['temprature_rise_actual'])?$requestData['temprature_rise_actual']:'';
        $requestData['temprature_rise_degrees'] = isset($requestData['temprature_rise_degrees'])?$requestData['temprature_rise_degrees']:'';

        $data = $jobFurnaceForm->update($requestData);

        Session::flash('flash_message', 'Job Form Updated Successfully!');
        return redirect('admin/job/' . $request->job_id . '/edit');
    }

    public function addJobInspectionForm(Request $request){
        $rules = array(
            'job_id'=>'required|integer',
            'date'=>'nullable|date|date_format:d-m-Y',
            'email'=>'nullable|email',
            'system_age'=>'nullable|integer',
            'stay_cool_enough'=>'nullable|in:y,n',
            'has_been_cycling'=>'nullable|in:y,n',
            'temperature_consistent'=>'nullable|in:y,n',
            'have_any_weird_smells'=>'nullable|in:y,n',
            'increase_electric_bill'=>'nullable|in:y,n',
            'accessories_1'=>'nullable|in:humidifier,condensate_pump,uv_light',
            'accessories_2'=>'nullable|in:electronic_air_cleaner,carbon_monoxide_detector',
            'accessories_3'=>'nullable|in:gps',
            'seer'=>'nullable|in:6,7,8,10,12,13,15+',
            'stages'=>'nullable|in:1,2,vs,mod',
            'fan_type'=>'nullable|in:psc,ecm',
            'cap'=>'nullable|in:run,start',
            'disconnect'=>'nullable|in:y,n',
            'refrigerant'=>'nullable|in:r22,r410a',
            // 'condenser_coil'=>'nullable|in:clean,needs_cleaned,cleaned',
            // 'evaporator_coil'=>'nullable|in:clean,needs_cleaned,cleaned',
            // 'drain_line'=>'nullable|in:clean,needs_cleaned,cleaned',
            // 'refrigerant_leak'=>'nullable|in:y,n',
            // 'ref_add'=>'nullable|in:y,n',
            // 'ref_removed'=>'nullable|in:y,n',
            'bottom_date'=>'nullable|date|date_format:d-m-Y',
        );

        $this->validate($request, $rules, []);

        $job_id = $request->job_id;
        $requestData = $request->except('_token','job_name');
        $requestData['created_by'] = '';

        $data = jobInspectionForm::create($requestData);
        $forms = Forms::where('form_code', 'HVAC_inspection')->first();

        if(isset($forms->id))
        {
            $formdata['job_id'] = $job_id;
            $formdata['job_form_id'] = $data->id;
            $formdata['form_id'] = $forms->id;
            JobForms::create($formdata);
        }
        Session::flash('flash_message','Job Form Added Successfully!');
        return redirect('admin/job/'.$request->job_id.'/edit');
    }

    public function editJobInspectionForm(Request $request,$id){

        $rules = array(
            'date'=>'nullable|date|date_format:d-m-Y',
            'email'=>'nullable|email',
            'system_age'=>'nullable|integer',
            'stay_cool_enough'=>'nullable|in:y,n',
            'has_been_cycling'=>'nullable|in:y,n',
            'temperature_consistent'=>'nullable|in:y,n',
            'have_any_weird_smells'=>'nullable|in:y,n',
            'increase_electric_bill'=>'nullable|in:y,n',
            'accessories_1'=>'nullable|in:humidifier,condensate_pump,uv_light',
            'accessories_2'=>'nullable|in:electronic_air_cleaner,carbon_monoxide_detector',
            'accessories_3'=>'nullable|in:gps',
            'seer'=>'nullable|in:6,7,8,10,12,13,15+',
            'stages'=>'nullable|in:1,2,vs,mod',
            'fan_type'=>'nullable|in:psc,ecm',
            'cap'=>'nullable|in:run,start',
            'disconnect'=>'nullable|in:y,n',
            'refrigerant'=>'nullable|in:r22,r410a',
            // 'condenser_coil'=>'nullable|in:clean,needs_cleaned,cleaned',
            // 'evaporator_coil'=>'nullable|in:clean,needs_cleaned,cleaned',
            // 'drain_line'=>'nullable|in:clean,needs_cleaned,cleaned',
            // 'refrigerant_leak'=>'nullable|in:y,n',
            // 'ref_add'=>'nullable|in:y,n',
            // 'ref_removed'=>'nullable|in:y,n',
            'bottom_date'=>'nullable|date|date_format:d-m-Y',
        );

        $this->validate($request, $rules, []);

        $jobInspectionForm = jobInspectionForm::where('id', $request->id)->where('status', 'active')->first();
        $requestData = $request->except('_token', 'job_name','job_id');
        $requestData['blower_amps_r'] = isset($requestData['blower_amps_r'])?$requestData['blower_amps_r']:'';
        $requestData['blower_amps_a'] = isset($requestData['blower_amps_a'])?$requestData['blower_amps_a']:'';
        $requestData['blower_capacitor_r'] = isset($requestData['blower_capacitor_r'])?$requestData['blower_capacitor_r']:'';
        $requestData['blower_capacitor_a'] = isset($requestData['blower_capacitor_a'])?$requestData['blower_capacitor_a']:'';
        $requestData['copressor_amps_r'] = isset($requestData['copressor_amps_r'])?$requestData['copressor_amps_r']:'';
        $requestData['copressor_amps_a'] = isset($requestData['copressor_amps_a'])?$requestData['copressor_amps_a']:'';
        $requestData['fan_motor_r'] = isset($requestData['fan_motor_r'])?$requestData['fan_motor_r']:'';
        $requestData['fan_motor_a'] = isset($requestData['fan_motor_a'])?$requestData['fan_motor_a']:'';
        $requestData['outdoor_capacitor_r'] = isset($requestData['outdoor_capacitor_r'])?$requestData['outdoor_capacitor_r']:'';
        $requestData['outdoor_capacitor_a'] = isset($requestData['outdoor_capacitor_a'])?$requestData['outdoor_capacitor_a']:'';
        $requestData['sub_cooling_r'] = isset($requestData['sub_cooling_r'])?$requestData['sub_cooling_r']:'';
        $requestData['sub_cooling_a'] = isset($requestData['sub_cooling_a'])?$requestData['sub_cooling_a']:'';

        $data = $jobInspectionForm->update($requestData);

        Session::flash('flash_message', 'Job Form Updated Successfully!');
        return redirect('admin/job/' . $request->job_id . '/edit');
    }

    public function addJobChecklistForm(Request $request){
        $rules = array(
            'job_id'=>'required|integer',
        );

        $this->validate($request, $rules, []);

        $job_id = $request->job_id;
        $requestData = $request->except('_token','job_name');
        $requestData['created_by'] = '';

        $data = JobChecklistForm::create($requestData);
        $forms = Forms::where('form_code', 'home_final_checklist')->first();

            if(isset($forms->id))
            {
                $formdata['job_id'] = $job_id;
                $formdata['job_form_id'] = $data->id;
                $formdata['form_id'] = $forms->id;
                JobForms::create($formdata);
            }
        Session::flash('flash_message','Job Form Added Successfully!');
        return redirect('admin/job/'.$request->job_id.'/edit');
    }

    public function editJobChecklistForm(Request $request,$id){

        $jobInspectionForm = JobChecklistForm::where('id', $request->id)->where('status', 'active')->first();
        $requestData = $request->except('_token', 'job_name','job_id');
        $requestData['all_register_open'] = isset($requestData['all_register_open'])?$requestData['all_register_open']:0;
        $requestData['install_toe_kicks'] = isset($requestData['install_toe_kicks'])?$requestData['install_toe_kicks']:0;
        $requestData['install_thermostat'] = isset($requestData['install_thermostat'])?$requestData['install_thermostat']:0;
        $requestData['furnace_cleaned'] = isset($requestData['furnace_cleaned'])?$requestData['furnace_cleaned']:0;
        $requestData['place_new_filter'] = isset($requestData['place_new_filter'])?$requestData['place_new_filter']:0;
        $requestData['set_ac_and_level'] = isset($requestData['set_ac_and_level'])?$requestData['set_ac_and_level']:0;
        $requestData['weigh_in_charge'] = isset($requestData['weigh_in_charge'])?$requestData['weigh_in_charge']:0;
        $requestData['install_start_assist'] = isset($requestData['install_start_assist'])?$requestData['install_start_assist']:0;
        $requestData['verify_fireplace_valve'] = isset($requestData['verify_fireplace_valve'])?$requestData['verify_fireplace_valve']:0;
        $requestData['startup_furnace'] = isset($requestData['startup_furnace'])?$requestData['startup_furnace']:0;
        $requestData['stickers_on_equipment'] = isset($requestData['stickers_on_equipment'])?$requestData['stickers_on_equipment']:0;
        $requestData['drain_installed'] = isset($requestData['drain_installed'])?$requestData['drain_installed']:0;
        $requestData['flue_pipes'] = isset($requestData['flue_pipes'])?$requestData['flue_pipes']:0;
        $requestData['stat_wire_size'] = isset($requestData['stat_wire_size'])?$requestData['stat_wire_size']:0;
        $requestData['humidifier'] = isset($requestData['humidifier'])?$requestData['humidifier']:0;
        $requestData['fresh_air_damper'] = isset($requestData['fresh_air_damper'])?$requestData['fresh_air_damper']:0;
        $requestData['ac_disconnect_installed'] = isset($requestData['ac_disconnect_installed'])?$requestData['ac_disconnect_installed']:0;

        $data = $jobInspectionForm->update($requestData);

        Session::flash('flash_message', 'Job Form Updated Successfully!');
        return redirect('admin/job/' . $request->job_id . '/edit');
    }

    public function addJobMaintenanceForm(Request $request){
        $rules = array(
            'job_id'=>'required|integer',
        );

        $this->validate($request, $rules, []);

        $job_id = $request->job_id;
        $requestData = $request->except('_token','job_name');
        $requestData['created_by'] = '';

        $data = JobMaintenanceForm::create($requestData);
        $forms = Forms::where('form_code', 'residential_maintenance')->first();

        if(isset($forms->id))
        {
            $formdata['job_id'] = $job_id;
            $formdata['job_form_id'] = $data->id;
            $formdata['form_id'] = $forms->id;
            JobForms::create($formdata);
        }
        Session::flash('flash_message','Job Form Added Successfully!');
        return redirect('admin/job/'.$request->job_id.'/edit');
    }

    public function editJobMaintenanceForm(Request $request,$id){

        $jobInspectionForm = JobMaintenanceForm::where('id', $request->id)->where('status', 'active')->first();
        $requestData = $request->except('_token', 'job_name','job_id');

        $data = $jobInspectionForm->update($requestData);

        Session::flash('flash_message', 'Job Form Updated Successfully!');
        return redirect('admin/job/' . $request->job_id . '/edit');
    }

    public function addJobSystemRepairForm(Request $request){
        $rules = array(
            'job_id'=>'required|integer',
            'date'=>'nullable|date|date_format:d-m-Y',
        );

        $this->validate($request, $rules, []);

        $job_id = $request->job_id;
        $requestData = $request->except('_token','job_name');
        $requestData['created_by'] = '';

        $data = JobSystemRepairForm::create($requestData);
        $forms = Forms::where('form_code', 'indoor_comfort_system_repair')->first();

                if(isset($forms->id))
                {
                    $formdata['job_id'] = $job_id;
                    $formdata['job_form_id'] = $data->id;
                    $formdata['form_id'] = $forms->id;
                    JobForms::create($formdata);
                }
        Session::flash('flash_message','Job Form Added Successfully!');
        return redirect('admin/job/'.$request->job_id.'/edit');
    }

    public function editJobSystemRepairForm(Request $request,$id){
        $rules = array(
            'date'=>'nullable|date|date_format:d-m-Y',
        );

        $this->validate($request, $rules, []);

        $jobRepairForm = JobSystemRepairForm::where('id', $request->id)->where('status', 'active')->first();
        $requestData = $request->except('_token', 'job_name','job_id');

        $data = $jobRepairForm->update($requestData);

        Session::flash('flash_message', 'Job Form Updated Successfully!');
        return redirect('admin/job/' . $request->job_id . '/edit');
    }

    public function addJobPlumbingForm(Request $request){
        $rules = array(
            'job_id'=>'required|integer',
            'date'=>'nullable|date|date_format:Y-m-d'
        );

        $this->validate($request, $rules, []);

        $job_id = $request->job_id;
        $requestData = $request->except('_token');
        $requestData['user_id'] = \Auth::user()->id;

        for ($i=1; $i <=18 ; $i++) {
            $requestData['plumbing'.$i] = ((isset($requestData['plumbing'.$i]) && $requestData['plumbing'.$i] == 'on') ? 1 : 0);
        }
        $data = JobPlumbingForm::create($requestData);
        $forms = Forms::where('form_code', 'plumbing')->first();

                if(isset($forms->id))
                {
                    $formdata['job_id'] = $job_id;
                    $formdata['job_form_id'] = $data->id;
                    $formdata['form_id'] = $forms->id;
                    JobForms::create($formdata);
                }
        Session::flash('flash_message','Job Form Added Successfully!');
        return redirect('admin/job/'.$request->job_id.'/edit');

    }
    public function editJobPlumbingForm(Request $request,$id){
        $rules = array(
            'date'=>'nullable|date|date_format:Y-m-d',
        );

        $this->validate($request, $rules, []);

        $fromdata = JobPlumbingForm::where('id', $request->id)->where('status', 'active')->first();
        $requestData = $request->except('_token','job_id');
        $requestData['user_id'] = \Auth::user()->id;
        for ($i=1; $i <=18 ; $i++) {
            $requestData['plumbing'.$i] = ((isset($requestData['plumbing'.$i]) && $requestData['plumbing'.$i] == 'on') ? 1 : 0);
        }
        $data = $fromdata->update($requestData);

        Session::flash('flash_message', 'Job Form Updated Successfully!');
        return redirect('admin/job/' . $request->job_id . '/edit');
    }

    public function addJobWeeklyVehicleSafetyChecklistForm(Request $request){
        $rules = array(
            'job_id'=>'required|integer',
            'date'=>'nullable|date|date_format:Y-m-d'
        );

        $this->validate($request, $rules, []);

        $job_id = $request->job_id;
        $requestData = $request->except('_token');
        $requestData['user_id'] = \Auth::user()->id;

        $requestData['PSI'] = implode(",",$requestData['PSI']);

        $data = JobWeeklyVehicleSafetyChecklistForm::create($requestData);
        $forms = Forms::where('form_code', 'weekly_vehicle_safety')->first();

                if(isset($forms->id))
                {
                    $formdata['job_id'] = $job_id;
                    $formdata['job_form_id'] = $data->id;
                    $formdata['form_id'] = $forms->id;
                    JobForms::create($formdata);
                }
        Session::flash('flash_message','Job Form Added Successfully!');
        return redirect('admin/job/'.$request->job_id.'/edit');

    }
    public function editJobWeeklyVehicleSafetyChecklistForm(Request $request,$id){
        $rules = array(
            'date'=>'nullable|date|date_format:Y-m-d',
        );

        $this->validate($request, $rules, []);

        $fromdata = JobWeeklyVehicleSafetyChecklistForm::where('id', $request->id)->where('status', 'active')->first();
        $requestData = $request->except('_token','job_id');
        $requestData['user_id'] = \Auth::user()->id;

        $requestData['PSI'] = implode(",",$requestData['PSI']);

        $data = $fromdata->update($requestData);

        Session::flash('flash_message', 'Job Form Updated Successfully!');
        return redirect('admin/job/' . $request->job_id . '/edit');
    }

    public function addJobInvoiceForm(Request $request){
        $rules = array(
            'job_id'=>'required|integer',
            'date'=>'nullable|date|date_format:Y-m-d',
            'main_date'=>'nullable|date|date_format:Y-m-d',
            'date_ordered'=>'nullable|date|date_format:Y-m-d',
            'date_scheduled'=>'nullable|date|date_format:Y-m-d'
        );
        $this->validate($request, $rules, []);

        $job_id = $request->job_id;
        $requestData = $request->except('_token');
        $requestData['user_id'] = \Auth::user()->id;


        foreach ($requestData as $key => $value) {
            if (isset($requestData[$key]) && $requestData[$key] == 'on') {
                $requestData[$key] = 1;
            }
            elseif (isset($requestData[$key]) && $requestData[$key] == 'off') {
                $requestData[$key] = 0;
            }
            else {
                $requestData[$key] = $value;
            }
        }
        $data = JobInvoice::create($requestData);
        $otherdata = $requestData;
         $forms = Forms::where('form_code', 'invoice')->first();

                if(isset($forms->id))
                {
                    $formdata['job_id'] = $job_id;
                    $formdata['job_form_id'] = $data->id;
                    $formdata['form_id'] = $forms->id;
                    JobForms::create($formdata);
                }
        $otherdata['job_invoice_id'] = $data->id;
        $data = JobInvoiceOtherField::create($otherdata);

        Session::flash('flash_message','Job Form Added Successfully!');
        return redirect('admin/job/'.$request->job_id.'/edit');

    }
    public function editJobInvoiceForm(Request $request,$id){
        $rules = array(
            'date'=>'nullable|date|date_format:Y-m-d',
            'main_date'=>'nullable|date|date_format:Y-m-d',
            'date_ordered'=>'nullable|date|date_format:Y-m-d',
            'date_scheduled'=>'nullable|date|date_format:Y-m-d'
        );

        $this->validate($request, $rules, []);

        $fromdata = JobInvoice::where('id', $request->id)->where('status', 'active')->first();

        $jobOtherForm = JobInvoiceOtherField::where('job_invoice_id', $id)->where('status', 'active')->first();
        $requestData = $request->except('user_id', 'api_token');
        foreach ($requestData as $key => $value) {
            if (isset($requestData[$key]) && $requestData[$key] == 'on') {
                $requestData[$key] = 1;
            }
            elseif (isset($requestData[$key]) && $requestData[$key] == 'off') {
                $requestData[$key] = 0;
            }
            else {
                $requestData[$key] = $value;
            }
        }
         $data = $fromdata->update($requestData);
        if($jobOtherForm == null)
        {
            $requestData['job_invoice_id'] = $id;
            $data = JobInvoiceOtherField::create($requestData);
        }
        else {
            $data = $jobOtherForm->update($requestData);
        }

        Session::flash('flash_message', 'Job Form Updated Successfully!');
        return redirect('admin/job/' . $request->job_id . '/edit');
    }

    public function addJobProjectWorksheetForm(Request $request){

        $rules = array(
            'job_id'=>'required|integer',
            'sold_date'=>'nullable|date|date_format:Y-m-d',
            'install_date'=>'nullable|date|date_format:Y-m-d',
            'quoted_by'=>'nullable|date|date_format:Y-m-d',
            'apt_date'=>'nullable|date|date_format:Y-m-d',
            'client_information_date'=>'nullable|date|date_format:Y-m-d',
            'client_information_meter_set_date'=>'nullable|date|date_format:Y-m-d',
            'permits_date'=>'nullable|date|date_format:Y-m-d',
            'permit_order_date_gas_piping'=>'nullable|date|date_format:Y-m-d',
            'permit_order_date_mechanical'=>'nullable|date|date_format:Y-m-d',
            'permit_order_date_plumbing'=>'nullable|date|date_format:Y-m-d',
            'permit_order_date_electrical'=>'nullable|date|date_format:Y-m-d',
            'permit_order_date_boiler'=>'nullable|date|date_format:Y-m-d',
            'time'=>'nullable|date_format:H:i:s'
        );

        $this->validate($request, $rules, []);
        $job_id = $request->job_id;
        $requestData = $request->except('_token');
        $requestData['user_id'] = \Auth::user()->id;

        $data = JobProjrctWorksheet1::create($requestData);
        $forms = Forms::where('form_code', 'project_worksheet')->first();

                if(isset($forms->id))
                {
                    $formdata['job_id'] = $job_id;
                    $formdata['job_form_id'] = $data->id;
                    $formdata['form_id'] = $forms->id;
                    JobForms::create($formdata);
                }
        $otherdata = $requestData;
        $otherdata['job_project_worksheet_form_1_id'] = $data->id;
        $otherformdata = JobProjrctWorksheet2::create($otherdata);
        $otherformdata = JobProjrctWorksheet3::create($otherdata);
        $otherformdata = JobProjrctWorksheet4::create($otherdata);

        Session::flash('flash_message','Job Form Added Successfully!');
        return redirect('admin/job/'.$request->job_id.'/edit');

    }
    public function editJobProjectWorksheetForm(Request $request,$id){
        $rules = array(
            'sold_date'=>'nullable|date|date_format:Y-m-d',
            'install_date'=>'nullable|date|date_format:Y-m-d',
            'quoted_by'=>'nullable|date|date_format:Y-m-d',
            'apt_date'=>'nullable|date|date_format:Y-m-d',
            'client_information_date'=>'nullable|date|date_format:Y-m-d',
            'client_information_meter_set_date'=>'nullable|date|date_format:Y-m-d',
            'permits_date'=>'nullable|date|date_format:Y-m-d',
            'permit_order_date_gas_piping'=>'nullable|date|date_format:Y-m-d',
            'permit_order_date_mechanical'=>'nullable|date|date_format:Y-m-d',
            'permit_order_date_plumbing'=>'nullable|date|date_format:Y-m-d',
            'permit_order_date_electrical'=>'nullable|date|date_format:Y-m-d',
            'permit_order_date_boiler'=>'nullable|date|date_format:Y-m-d',
            'time'=>'nullable|date_format:H:i:s'
        );

        $this->validate($request, $rules, []);

        $fromdata = JobProjrctWorksheet1::where('id', $request->id)->where('status', 'active')->first();


        $requestData = $request->except('user_id', 'api_token','_method','_token');
        $allRequestDataArr = array_chunk($requestData, 10, true);

        foreach ($allRequestDataArr as $key => $value) {
            $value['job_project_worksheet_form_1_id'] = $id;
            $value['_method'] = $request->_method;
            $value['_token'] = $request->_token;

            $jobProjrctWorksheet2 = JobProjrctWorksheet2::where('job_project_worksheet_form_1_id', $id)->first();
            $jobProjrctWorksheet3 = JobProjrctWorksheet3::where('job_project_worksheet_form_1_id', $id)->first();
            $jobProjrctWorksheet4 = JobProjrctWorksheet4::where('job_project_worksheet_form_1_id', $id)->first();

            $data = $fromdata->update($value);

            if($jobProjrctWorksheet2 == null)
            {
                $data = JobProjrctWorksheet2::create($value);
            }
            else {
                $data = $jobProjrctWorksheet2->update($value);
            }

            if($jobProjrctWorksheet3 == null)
            {
                $data = JobProjrctWorksheet3::create($value);
            }
            else {
                $data = $jobProjrctWorksheet3->update($value);
            }

            if($jobProjrctWorksheet4 == null)
            {
                $data = JobProjrctWorksheet4::create($value);
            }
            else {
                $data = $jobProjrctWorksheet4->update($value);
            }
        }


        Session::flash('flash_message', 'Job Form Updated Successfully!');
        return redirect('admin/job/' . $request->job_id . '/edit');
    }


    public function viewForm(Request $request,$job_id,$form_code,$id){

        $status = true;
        $job_id = $request->job_id;
        $form= Forms::where('form_code',$form_code)->first();
        $job= Job::where('job.id',$job_id)->first();

        if(!$form){
            $message = 'No form Found';
            $status = false;

        }else if(!$job){
            $message = 'No Job Found';
            $status = false;

        }


        if($status){
            if ($form_code == 'geo_thermal') {
                $jobGeothermalForm = JobGeothermalForm::where('id', $id)->first();
                return view('admin.jobsforms.geo_thermal_form', compact('jobGeothermalForm','job'));
            }
            else if($form_code == 'HVAC_inspection_furnace') {
                $jobFurnaceForm = JobFurnaceForm::where('id', $id)->first();
                return view('admin.jobsforms.job_furnace_forms', compact('jobFurnaceForm','job'));
            }
            else if($form_code == 'HVAC_inspection') {
                $jobInspectionForm = JobInspectionForm::where('id', $id)->first();
                return view('admin.jobsforms.job_inspection_form', compact('jobInspectionForm','job'));

            }
            else if($form_code == 'home_final_checklist') {
                $jobChecklistForm = JobChecklistForm::where('id', $id)->first();
                return view('admin.jobsforms.job_checklist_form', compact('jobChecklistForm','job'));
            }
            else if($form_code == 'residential_maintenance') {
                $jobMaintenanceForm = JobMaintenanceForm::where('id', $id)->first();
                return view('admin.jobsforms.job_maintenance_form', compact('jobMaintenanceForm','job'));

            }
            else if($form_code == 'indoor_comfort_system_repair') {
                $jobSystemRepairForm = JobSystemRepairForm::where('id', $id)->first();
                return view('admin.jobsforms.job_repair_form', compact('jobSystemRepairForm','job'));
            }
            else if($form_code == 'plumbing') {
                $jobPlumbingForm = JobPlumbingForm::where('id', $id)->first();
                //dd($jobPlumbingForm);
                return view('admin.jobsforms.job_plumbing_form', compact('jobPlumbingForm','job'));
            }
            else if($form_code == 'weekly_vehicle_safety') {
                $jobWeeklyVehicleSafetyChecklistForm = JobWeeklyVehicleSafetyChecklistForm::where('id', $id)->first();
                if($jobWeeklyVehicleSafetyChecklistForm != null && $jobWeeklyVehicleSafetyChecklistForm['PSI'] != null)
                {
                    $jobWeeklyVehicleSafetyChecklistForm['PSI'] = explode(",",$jobWeeklyVehicleSafetyChecklistForm['PSI']);
                }
                return view('admin.jobsforms.job_weekly_vehicle_safety_checklist_form', compact('jobWeeklyVehicleSafetyChecklistForm','job'));

            }
            else if($form_code == 'project_worksheet') {
                $jobProjectWorksheetForm = JobProjrctWorksheet1::select('job_project_worksheet_form_1.*', 'job_project_worksheet_form_2.*', 'job_project_worksheet_form_3.*', 'job_project_worksheet_form_4.*', 'job.title as job_name', 'job_project_worksheet_form_1.id as job_project_worksheet_id')->join('job', 'job_project_worksheet_form_1.job_id', 'job.id')->leftJoin('job_project_worksheet_form_2', 'job_project_worksheet_form_2.job_project_worksheet_form_1_id', 'job_project_worksheet_form_1.id')->leftJoin('job_project_worksheet_form_3', 'job_project_worksheet_form_3.job_project_worksheet_form_1_id', 'job_project_worksheet_form_1.id')->leftJoin('job_project_worksheet_form_4', 'job_project_worksheet_form_4.job_project_worksheet_form_1_id', 'job_project_worksheet_form_1.id')->where('job_project_worksheet_form_1.id', $id)->first();
                return view('admin.jobsforms.job_project_worksheet', compact('jobProjectWorksheetForm','job'));

            }
            else if($form_code == 'invoice') {
                $jobInvoiceForm = JobInvoice::select('job_invoice_form.*', 'job_invoice_other_fields_form.*', 'job.title as job_name', 'job_invoice_form.id as job_main_invoice_id')->join('job', 'job_invoice_form.job_id', 'job.id')->leftJoin('job_invoice_other_fields_form', 'job_invoice_other_fields_form.job_invoice_id', 'job_invoice_form.id')->where('job_invoice_form.id', $id)->first();
                return view('admin.jobsforms.job_invoice_form', compact('jobInvoiceForm','job'));
            }
        }
        if (isset($message)) {
           Session::flash('flash_message', $message);
        }

        return redirect('admin/job/' . $request->job_id . '/edit');
    }

}
