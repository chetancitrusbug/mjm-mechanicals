<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlumbingFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_plumbing_form', function (Blueprint $table) {
           $table->increments('id');
            $table->integer('job_id');
            $table->integer('user_id');
            $table->date('date')->nullable();
            $table->string('plumbing1')->nullable();
            $table->string('plumbing2')->nullable();
            $table->string('plumbing3')->nullable();
            $table->string('plumbing4')->nullable();
            $table->string('plumbing5')->nullable();
            $table->string('plumbing6')->nullable();
            $table->string('plumbing7')->nullable();
            $table->string('plumbing8')->nullable();
            $table->string('plumbing9')->nullable();
            $table->string('plumbing10')->nullable();
            $table->string('plumbing11')->nullable();
            $table->string('plumbing12')->nullable();
            $table->string('plumbing13')->nullable();
            $table->string('plumbing14')->nullable();
            $table->string('plumbing15')->nullable();
            $table->string('plumbing16')->nullable();
            $table->string('plumbing17')->nullable();
            $table->string('plumbing18')->nullable();
            $table->string('status')->default('active')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_plumbing_form');

    }
}
