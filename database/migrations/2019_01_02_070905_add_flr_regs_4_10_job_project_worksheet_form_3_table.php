<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFlrRegs410JobProjectWorksheetForm3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_project_worksheet_form_3', function (Blueprint $table) {
            $table->string('flr_regs_4_10_del',50)->nullable()->after('flr_regs_2_1_4_12_del');
            $table->string('flr_regs_4_10_usd',50)->nullable()->after('flr_regs_2_1_4_12_usd');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job_project_worksheet_form_3', function (Blueprint $table) {
            $table->dropColumn('flr_regs_4_10_usd');
            $table->dropColumn('flr_regs_4_10_del');
        });
    }
}
