<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobInvoiceOtherFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_invoice_other_fields_form', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_invoice_id');
            $table->string('item_or_part_description_1')->nullable();
            $table->string('item_or_part_description_2')->nullable();
            $table->string('item_or_part_description_3')->nullable();
            $table->string('item_or_part_description_4')->nullable();
            $table->string('item_or_part_description_5')->nullable();
            $table->string('item_or_part_description_6')->nullable();
            $table->string('item_or_part_description_7')->nullable();
            $table->string('item_or_part_description_8')->nullable();
            $table->string('item_or_part_description_9')->nullable();
            $table->string('item_or_part_description_10')->nullable();
            $table->string('item_or_part_description_11')->nullable();
            $table->string('item_or_part_description_12')->nullable();
            $table->string('item_or_part_description_13')->nullable();
            $table->string('item_or_part_description_14')->nullable();
            $table->string('item_or_part_description_15')->nullable();
            $table->string('item_or_part_description_16')->nullable();
            $table->double('price_1', 0, 2)->nullable();
            $table->double('price_2', 0, 2)->nullable();
            $table->double('price_3', 0, 2)->nullable();
            $table->double('price_4', 0, 2)->nullable();
            $table->double('price_5', 0, 2)->nullable();
            $table->double('price_6', 0, 2)->nullable();
            $table->double('price_7', 0, 2)->nullable();
            $table->double('price_8', 0, 2)->nullable();
            $table->double('price_9', 0, 2)->nullable();
            $table->double('price_10', 0, 2)->nullable();
            $table->double('price_11', 0, 2)->nullable();
            $table->double('price_12', 0, 2)->nullable();
            $table->double('price_13', 0, 2)->nullable();
            $table->double('price_14', 0, 2)->nullable();
            $table->double('price_15', 0, 2)->nullable();
            $table->double('price_16', 0, 2)->nullable();
            $table->double('amount_1', 0, 2)->nullable();
            $table->double('amount_2', 0, 2)->nullable();
            $table->double('amount_3', 0, 2)->nullable();
            $table->double('amount_4', 0, 2)->nullable();
            $table->double('amount_5', 0, 2)->nullable();
            $table->double('amount_6', 0, 2)->nullable();
            $table->double('amount_7', 0, 2)->nullable();
            $table->double('amount_8', 0, 2)->nullable();
            $table->double('amount_9', 0, 2)->nullable();
            $table->double('amount_10', 0, 2)->nullable();
            $table->double('amount_11', 0, 2)->nullable();
            $table->double('amount_12', 0, 2)->nullable();
            $table->double('amount_13', 0, 2)->nullable();
            $table->double('amount_14', 0, 2)->nullable();
            $table->double('amount_15', 0, 2)->nullable();
            $table->double('amount_16', 0, 2)->nullable();
            $table->string('total_parts')->nullable();
            $table->string('write_or_code_1')->nullable();
            $table->string('write_or_code_2')->nullable();
            $table->string('write_or_code_3')->nullable();
            $table->string('charges_from_amount_1')->nullable();
            $table->string('charges_from_amount_2')->nullable();
            $table->string('charges_from_amount_3')->nullable();
            $table->string('total_other_charges')->nullable();
            $table->string('refrig')->nullable();
            $table->string('refrig_qty')->nullable();
            $table->tinyInteger('recovered')->nullable();
            $table->tinyInteger('recycled')->nullable();
            $table->tinyInteger('reclaimed')->nullable();
            $table->tinyInteger('returned_to_this_system')->nullable();
            $table->string('disposal_1')->nullable();
            $table->tinyInteger('non_useable')->nullable();
            $table->string('disposal_2')->nullable();
            $table->string('recovered_qty')->nullable();
            $table->string('recycled_qty')->nullable();
            $table->string('reclaimed_qty')->nullable();
            $table->string('returned_to_this_system_qty')->nullable();
            $table->string('non_useable_qty')->nullable();
            $table->tinyInteger('changed_out')->nullable();
            $table->tinyInteger('dis_mantled')->nullable();
            $table->string('refrigerant_disposal')->nullable();
            $table->string('our_personnel_recommend')->nullable();
            $table->tinyInteger('accepted')->nullable();
            $table->tinyInteger('declined')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('street')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->string('make')->nullable();
            $table->string('model')->nullable();
            $table->string('serial_number')->nullable();
            $table->text('job_location')->nullable();
            $table->text('desc_of_work_1')->nullable();
            $table->text('desc_of_work_2')->nullable();
            $table->text('desc_of_work_3')->nullable();
            $table->text('desc_of_work_4')->nullable();
            $table->text('desc_of_work_5')->nullable();
            $table->text('desc_of_work_6')->nullable();
            $table->text('desc_of_work_7')->nullable();
            $table->text('desc_of_work_8')->nullable();
            $table->text('desc_of_work_9')->nullable();
            $table->string('tech_1_regular_hrs')->nullable();
            $table->string('tech_1_regular_hr')->nullable();
            $table->string('tech_2_regular_hrs')->nullable();
            $table->string('tech_2_regular_hr')->nullable();
            $table->string('tech_1_overtime_hrs')->nullable();
            $table->string('tech_1_overtime_hr')->nullable();
            $table->string('tech_2_overtime_hrs')->nullable();
            $table->string('tech_2_overtime_hr')->nullable();
            $table->string('technician_signature')->nullable();
            $table->string('cert')->nullable();
            $table->string('authorized_signature')->nullable();
            $table->string('above_ordered_work_has_been_completed')->nullable();
            $table->date('main_date')->nullable();
            $table->date('date_ordered')->nullable();
            $table->date('date_scheduled')->nullable();
            $table->string('phone')->nullable();
            $table->string('wk_or_cell')->nullable();
            $table->tinyInteger('warranty')->nullable();
            $table->tinyInteger('contract')->nullable();
            $table->tinyInteger('service_contract')->nullable();
            $table->tinyInteger('normal')->nullable();
            $table->tinyInteger('res')->nullable();
            $table->tinyInteger('comm')->nullable();
            $table->double('service_1', 0, 2)->nullable();
            $table->double('service_2', 0, 2)->nullable();
            $table->double('service_3', 0, 2)->nullable();
            $table->double('service_4', 0, 2)->nullable();
            $table->double('service_5', 0, 2)->nullable();
            $table->double('service_6', 0, 2)->nullable();
            $table->double('service_7', 0, 2)->nullable();
            $table->double('service_8', 0, 2)->nullable();
            $table->double('service_9', 0, 2)->nullable();
            $table->double('service_10', 0, 2)->nullable();
            $table->double('service_11', 0, 2)->nullable();
            $table->string('service_total_other_charges')->nullable();
            $table->string('sub_total')->nullable();
            $table->string('trip_charge')->nullable();
            $table->string('tax')->nullable();
            $table->string('total_amount_due')->nullable();
            $table->date('date')->nullable();
            $table->string('status')->default('active')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('job_invoice_other_fields_form');

    }
}
