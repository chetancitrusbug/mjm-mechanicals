<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class JobPlumbingForm extends Model
{
    use SoftDeletes;

    public $table='job_plumbing_form';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','job_id','user_id','date','plumbing1','plumbing2','plumbing3','plumbing4','plumbing5','plumbing6','plumbing7','plumbing8','plumbing9','plumbing10','plumbing11','plumbing12','plumbing13','plumbing14','plumbing15','plumbing16','plumbing17','plumbing18','status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    // public function getDateAttribute($value)
    // {
    //     if($value != ""){
    //         return \Carbon\Carbon::parse($value)->format('d-m-Y');
    //     }
    //     return $value;
    // }

    // public function setDateAttribute($value)
    // {
    //     if($value != ""){
    //         $this->attributes['date'] = \Carbon\Carbon::parse($value)->format('Y-m-d');
    //     }
    // }
}
