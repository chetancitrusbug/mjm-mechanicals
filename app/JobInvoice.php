<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobInvoice extends Model
{

    use SoftDeletes;

    public $table='job_invoice_form';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','job_id', 'user_id', 'compressor', 'suction', 'suction_value', 'head', 'head_value', 'volts', 'volts_value', 'electrical_connections_compressor', 'contractor_tight_clean', 'oil_level_condition', 'burn_out', 'condenser_coil', 'clean_coil_check_fin_cond', 'ent', 'lvg', 'refrigerant', 'leak', 'charge', 'fan_and_motor', 'volts_amps', 'fan_and_motor_volts_value', 'fan_and_motor_amps_value', 'electrical_connections_fan_and_motor', 'contacts_tight_clean', 'fan_pulleys', 'check_lube_bearings_motor', 'cfm', 'cfm_value', 'evaporator_coil', 'clean_coil_check_fin', 'ent_db_lvg_db', 'ent_db', 'lvg_db', 'ent_wb_lvg_wb', 'ent_wb', 'lvg_wb', 'condensate_areas', 'inspect_clean_drain_pan', 'inspect_clean_drain', 'air_filters', 'cleaned', 'replaced', 'filter_size_value', 'heating_assy', 'burner_heat_exchanger', 'fuel_supply_pressure', 'pilot_assembly', 'flame_adjustment', 'primary_relay_flue', 'fan_limit_switch_oper', 'blower_assembly', 'rv_valve', 'strip_heat', 'defrost_cycle', 'electrical_compts', 'relays', 'contactors', 'overload', 'press_switch', 'thermostat', 'ok_thermostat', 'replace', 'relocate', 'time_arrived', 'time_departed', 'travel_time', 'ending', 'start', 'total_miles', 'total_miles_x1', 'total_miles_x2', 'total_miles_mi', 'trip_miles_hr', 'totalcharges', 'quantity_1', 'quantity_2', 'quantity_3', 'quantity_4', 'quantity_5', 'quantity_6', 'quantity_7', 'quantity_8', 'quantity_9', 'quantity_10', 'quantity_11', 'quantity_12', 'quantity_13', 'quantity_14', 'quantity_15', 'quantity_16', 'status','ent_lvg','filter_size','disposal_qty'];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
