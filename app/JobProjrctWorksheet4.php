<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobProjrctWorksheet4 extends Model
{
    use SoftDeletes;

    public $table='job_project_worksheet_form_4';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'job_project_worksheet_form_1_id', '3_b_vent_del', '3_90_elbows_del', '3_45_elbows_del', '3_5_length_del', '3_3_length_del', '3_2_length_del', '3_1_length_del', '3_1_adjustable_del', '3_tee_del', '3_wyes_del', '3_increasers_del', '3_4_12_flash_del', '3_5_12_flash_del', '3_6_12_flash_del', '3_7_12_flash_del', '3_reducer_del', '3_cap_del', '3_collar_del', '4_b_vent_del', '4_90_elbows_del', '4_45_elbows_del', '4_5_length_del', '4_3_length_del', '4_2_length_del', '4_1_length_del', '4_1_adjustable_del', '4_tee_del', '4_wyes_del', '4_increasers_del', '4_4_12_flash_del', '4_5_12_flash_del', '4_6_12_flash_del', '4_7_12_flash_del', '4_reducer_del', '4_cap_del', '4_collar_del', '5_b_vent_del', '5_90_elbows_del', '5_45_elbows_del', '5_5_length_del', '5_3_length_del', '5_2_length_del', '5_1_length_del', '5_1_adjustable_del', '5_tee_del', '5_wyes_del', '5_increasers_del', '5_4_12_flash_del', '5_5_12_flash_del', '5_6_12_flash_del', '5_7_12_flash_del', '5_reducer_del', '5_cap_del', '5_collar_del', '6_b_vent_del', '6_90_elbows_del', '6_45_elbows_del', '6_5_length_del', '6_3_length_del', '6_2_length_del', '6_1_length_del', '6_1_adjustable_del', '6_tee_del', '6_wyes_del', '6_increasers_del', '6_4_12_flash_del', '6_5_12_flash_del', '6_6_12_flash_del', '6_7_12_flash_del', '6_reducer_del', '6_cap_del', '6_collar_del', '3_b_vent_usd', '3_90_elbows_usd', '3_45_elbows_usd', '3_5_length_usd', '3_3_length_usd', '3_2_length_usd', '3_1_length_usd', '3_1_adjustable_usd', '3_tee_usd', '3_wyes_usd', '3_increasers_usd', '3_4_12_flash_usd', '3_5_12_flash_usd', '3_6_12_flash_usd', '3_7_12_flash_usd', '3_reducer_usd', '3_cap_usd', '3_collar_usd', '4_b_vent_usd', '4_90_elbows_usd', '4_45_elbows_usd', '4_5_length_usd', '4_3_length_usd', '4_2_length_usd', '4_1_length_usd', '4_1_adjustable_usd', '4_tee_usd', '4_wyes_usd', '4_increasers_usd', '4_4_12_flash_usd', '4_5_12_flash_usd', '4_6_12_flash_usd', '4_7_12_flash_usd', '4_reducer_usd', '4_cap_usd', '4_collar_usd', '5_b_vent_usd', '5_90_elbows_usd', '5_45_elbows_usd', '5_5_length_usd', '5_3_length_usd', '5_2_length_usd', '5_1_length_usd', '5_1_adjustable_usd', '5_tee_usd', '5_wyes_usd', '5_increasers_usd', '5_4_12_flash_usd', '5_5_12_flash_usd', '5_6_12_flash_usd', '5_7_12_flash_usd', '5_reducer_usd','5_cap_usd','5_collar_usd','6_b_vent_usd','6_90_elbows_usd','6_45_elbows_usd','6_5_length_usd','6_3_length_usd','6_2_length_usd','6_1_length_usd','6_1_adjustable_usd','6_tee_usd','6_wyes_usd','6_increasers_usd','6_4_12_flash_usd','6_5_12_flash_usd','6_6_12_flash_usd','6_7_12_flash_usd','6_reducer_usd','6_cap_usd','6_collar_usd','furnace_type_up_flow','furnace_type_counter_flow','furnace_type_condensing','furnace_type_horizontal','furnace_type_notes','equipment_furnace','equipment_make','equipment_model','equipment_serial_1','equipment_eac_filter','equipment_eac_filte_size','equipment_thermostat','equipment_thermostat_make','equipment_thermostat_model','equipment_cond_unit','equipment_cond_unit_make','equipment_cond_unit_model','equipment_serial_2','equipment_evap_coil','equipment_evap_coi_make','equipment_evap_coi_model','hot_water_tank_size','hot_water_tank_type_standard_atmosphere','hot_water_tank_type_direct_vent','hot_water_tank_type_induced_vent','hot_water_tank_stand_required','hot_water_tank_expansion_tank','hot_water_tank_prv','hot_water_tank_t_p_drain','hot_water_tank_run_to_outside','hot_water_tank_floor','hot_water_tank_exist','hot_water_tank_pan','water_htr_make','serial_3_make','fireplace_make','rooftop_unit_make','serial_4_make','wall_heater_make','other_make','water_htr_model','serial_3_model','fireplace_model','rooftop_unit_model','serial_4_model','wall_heater_model','other_model','status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
