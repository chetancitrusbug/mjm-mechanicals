<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobFurnaceFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_furnace_form', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_id');
            $table->integer('user_id');
            $table->date('date')->nullable();
            $table->string('customer_name')->nullable();
            $table->text('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('system_age')->nullable();
            $table->enum('staying_warm_enough',['y','n'])->nullable();
            $table->enum('temrature_consistent',['y','n'])->nullable();
            $table->enum('dry_inside_home',['y','n'])->nullable();
            $table->enum('have_any_weird_smells',['y','n'])->nullable();
            $table->string('furnace_boiler_1')->nullable();
            $table->string('brand_1')->nullable();
            $table->string('model_1')->nullable();
            $table->string('serial_1')->nullable();
            $table->string('furnace_boiler_2')->nullable();
            $table->string('brand_2')->nullable();
            $table->string('model_2')->nullable();
            $table->string('serial_2')->nullable();
            $table->string('furnace_boiler_3')->nullable();
            $table->string('brand_3')->nullable();
            $table->string('model_3')->nullable();
            $table->string('serial_3')->nullable();
            $table->string('filter_size_1')->nullable();
            $table->string('quantity_1')->nullable();
            $table->string('accessories_1')->nullable();
            $table->string('filter_size_2')->nullable();
            $table->string('quantity_2')->nullable();
            $table->string('accessories_2')->nullable();
            $table->string('filter_size_3')->nullable();
            $table->string('quantity_3')->nullable();
            $table->string('accessories_3')->nullable();
            $table->string('gas_type')->nullable();
            $table->string('blower')->nullable();
            $table->string('ignition')->nullable();
            $table->string('stages')->nullable();
            $table->string('draft_meter')->nullable();
            $table->string('efficiency')->nullable();
            $table->string('blower_amps_r')->nullable();
            $table->string('blower_amps_a')->nullable();
            $table->string('blower_capacitor_r')->nullable();
            $table->string('blower_capacitor_a')->nullable();
            $table->string('draft_motor_amps_r')->nullable();
            $table->string('draft_motor_amps_a')->nullable();
            $table->string('draft_motor_capacitor_r')->nullable();
            $table->string('draft_motor_capacitor_a')->nullable();
            $table->string('ignitor_ohms_r')->nullable();
            $table->string('ignitor_ohms_a')->nullable();
            $table->string('temprature_rise_rated')->nullable();
            $table->string('temprature_rise_actual')->nullable();
            $table->string('temprature_rise_degrees')->nullable();
            $table->string('flame_signal_ua')->nullable();
            $table->string('gas_pressure_r')->nullable();
            $table->string('gas_pressure_a')->nullable();
            $table->string('thermostat_setpoint')->nullable();
            $table->string('temp_humidity')->nullable();
            $table->string('tstat_type')->nullable();
            $table->string('motor_bearings')->nullable();
            $table->string('blower_wheel')->nullable();
            $table->string('heat_exchanger')->nullable();
            $table->string('burners')->nullable();
            $table->string('ignitor')->nullable();
            $table->string('limits')->nullable();
            $table->text('notes')->nullable();
            $table->string('tech')->nullable();
            $table->string('occupant')->nullable();
            $table->string('bottom_date')->nullable();
            $table->string('status')->default('active')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_furnace_form');
    }
}
