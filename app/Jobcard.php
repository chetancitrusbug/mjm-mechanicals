<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobcard extends Model
{
     /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'jobcard';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'job_id', 'job_title', 'total_amount','job_card_signature_image'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_by','updated_at','created_at','deleted_at'
    ];

    public function jobcarddetailList(){
            return $this->hasMany
            ('App\Jobcarddetail','job_card_id');
    }
    public function jobcarduserName(){
            return $this->hasOne
            ('App\user','id','user_id');
    }
	 public function jobdetailList(){
            return $this->hasMany
            ('App\Jobcarddetail','job_card_id','id')->select('*','jobcarddetail.id as jobdetail_id');
    }
   
}
