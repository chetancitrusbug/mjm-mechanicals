@extends('layouts.backend')
@section('title',trans('job.view_form'))
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">Weekly Vehicle Safety Checklist  </div>
    {!! Form::model($jobWeeklyVehicleSafetyChecklistForm, [ 'method' => 'PATCH', 'url' => ['/admin/jobWeeklyVehicleSafetyChecklistForm',
    $jobWeeklyVehicleSafetyChecklistForm->id], 'class' => 'form-horizontal','id'=>'jobWeeklyVehicleSafetyChecklistForm', 'enctype'=>'multipart/form-data'
    ]) !!}
    <div class="panel-body">
    <a href="{{ url('/admin/job/' . $job->id . '/edit') }}" title="Back to Forms list"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true" ></i>Back</button></a>
        {!! Form::hidden('job_id', $job->id, ['class' => 'form-control input-100']) !!}

        <div class="grid-box-1 colbox-v001 clearfix">
            <p>The vehicle inspection report is completed on a weekly basis</p>
        </div>

        <div class="grid-box-1 colbox-v002 clearfix">

            <div class="row">
                <div class="col-md-3 col-sm-4 clearfix">
                    <p class="height-auto">Date:</p>
                    {!! Form::text('date', null, ['class' => 'form-control input-100 vehicle_date']) !!}
                </div>
                <div class="col-md-3 col-sm-4 clearfix">
                    <p class="height-auto">Vehicle unit number:</p>
                    {!! Form::text('vehicle_unit_number', null, ['class' => 'form-control input-100']) !!}
                </div>
                <div class="col-md-3 col-sm-4 clearfix">
                    <p class="height-auto"> License number:</p>
                    {!! Form::text('license_number', null, ['class' => 'form-control input-100']) !!}
                </div>
                <div class="col-md-3 col-sm-4 clearfix">
                    <p class="height-auto"> Mileage:</p>
                    {!! Form::text('mileage', null, ['class' => 'form-control input-100']) !!}
                </div>
                <div class="col-md-12 col-sm-8 clearfix">
                    <p class="height-auto"> Driver:</p>
                    {!! Form::text('driver', null, ['class' => 'form-control input-100']) !!}
                </div>
                <div class="col-md-4 col-sm-4 clearfix">
                    <p class="height-auto">Year:</p>
                    {!! Form::text('year', null, ['class' => 'form-control input-100']) !!}
                </div>
                <div class="col-md-4 col-sm-4 clearfix">
                    <p class="height-auto">Make:</p>
                    {!! Form::text('make', null, ['class' => 'form-control input-100']) !!}
                </div>
                <div class="col-md-4 col-sm-4 clearfix">
                    <p class="height-auto"> Model:</p>
                    {!! Form::text('model', null, ['class' => 'form-control input-100']) !!}
                </div>
            </div>

        </div>

            <div class="grid-box-1 colbox-v003 clearfix">
            <div class="col-md-12 col-sm-12 clearfix">
                <div class="row">

                    <div class="col-md-12 col-sm-12 clearfix">
                        <h3>INSPECT AND CIRCLE ONE</h3>
                    </div>
                    <div class="col-md-6 col-sm-12 p0 clearfix">

                        <div class="gridbox-v6 gridbox-v6-1 clearfix">
                            <h4>LIGHTS</h4>
                            <div class="box-vs1 clearfix">

                                    <div class="grid-box-1 table-01 clearfix">

                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <td class="col-md-5 p-12">Head: </td>
                                                        <td class="col-md-7 text-center">
                                                            <label class="radio-inline">{!! Form::radio('head', 'ok', null, ['class' => '']) !!}OK </label>
                                                            <span class="space-line">/</span>
                                                            <label class="radio-inline">{!! Form::radio('head', 'out', null, ['class' => '']) !!}Out</label></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="col-md-5 p-12">Back-up: </td>
                                                        <td class="col-md-7 text-center">
                                                            <label class="radio-inline">{!! Form::radio('back_up', 'ok', null, ['class' => '']) !!}OK </label>
                                                            <span class="space-line">/</span>
                                                            <label class="radio-inline">{!! Form::radio('back_up', 'out', null, ['class' => '']) !!}Out</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="col-md-5 p-12">Parking: </td>
                                                        <td class="col-md-7 text-center">
                                                            <label class="radio-inline">{!! Form::radio('parking', 'ok', null, ['class' => '']) !!}OK </label>
                                                            <span class="space-line">/</span>
                                                            <label class="radio-inline">{!! Form::radio('parking', 'out', null, ['class' => '']) !!}Out</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="col-md-5 p-12">Cargo: </td>
                                                        <td class="col-md-7 text-center">
                                                            <label class="radio-inline">{!! Form::radio('cargo', 'ok', null, ['class' => '']) !!}OK </label>
                                                            <span class="space-line">/</span>
                                                            <label class="radio-inline">{!! Form::radio('cargo', 'out', null, ['class' => '']) !!}Out</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="col-md-5 p-12">Tail:</td>
                                                        <td class="col-md-7 text-center">
                                                            <label class="radio-inline">{!! Form::radio('tail', 'ok', null, ['class' => '']) !!}OK </label>
                                                            <span class="space-line">/</span>
                                                            <label class="radio-inline">{!! Form::radio('tail', 'out', null, ['class' => '']) !!}Out</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="col-md-5 p-12">Flashers: </td>
                                                        <td class="col-md-7 text-center">
                                                            <label class="radio-inline">{!! Form::radio('flashers', 'ok', null, ['class' => '']) !!}OK </label>
                                                            <span class="space-line">/</span>
                                                            <label class="radio-inline">{!! Form::radio('flashers', 'out', null, ['class' => '']) !!}Out</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="col-md-5 p-12">Directional:</td>
                                                        <td class="col-md-7 text-center">
                                                            <label class="radio-inline">{!! Form::radio('directional', 'ok', null, ['class' => '']) !!}OK </label>
                                                            <span class="space-line">/</span>
                                                            <label class="radio-inline">{!! Form::radio('directional', 'out', null, ['class' => '']) !!}Out</label>
                                                        </td>
                                                    </tr>


                                                </tbody>
                                            </table>
                                        </div>

                                    </div>

                            </div>
                        </div>

                        <div class="gridbox-v6 gridbox-v6-2 clearfix">
                            <h4>TIRES</h4>
                            <div class="box-vs1 clearfix">

                                    <div class="grid-box-1 table-01 clearfix">

                                        <div class="col-md-12 col-sm-12  clearfix">
                                            <div class="row">

                                                <p class="height-auto"> PSI:</p>
                                                @if(isset($jobWeeklyVehicleSafetyChecklistForm) && is_array($jobWeeklyVehicleSafetyChecklistForm['PSI']))
                                                    @for($i = 0; $i < 4;$i++)
                                                        {!! Form::text('PSI['.$i.']', null , ['class' => 'form-control input-100
                                                            input-24']) !!}
                                                    @endfor

                                                @else

                                                    {!! Form::text('PSI[]', null, ['class' => 'form-control input-100 input-24']) !!}
                                                    {!! Form::text('PSI[]', null, ['class' => 'form-control input-100 input-24']) !!}
                                                    {!! Form::text('PSI[]', null, ['class' => 'form-control input-100 input-24']) !!}
                                                    {!! Form::text('PSI[]', null, ['class' => 'form-control input-100 input-24']) !!}
                                                @endif

                                            </div>
                                        </div>

                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <tbody>

                                                    <tr>
                                                        <td class="col-md-5 p-12">Front left:</td>
                                                        <td class="col-md-7 text-center">
                                                            <label class="radio-inline">{!! Form::radio('front_left', 'good', null, ['class' => '']) !!}GOOD</label>
                                                            <label class="radio-inline">{!! Form::radio('front_left', 'fair', null, ['class' => '']) !!}FAIR</label>
                                                            <label class="radio-inline">{!! Form::radio('front_left', 'poor', null, ['class' => '']) !!}POOR </label>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td class="col-md-5 p-12">Front right:</td>
                                                        <td class="col-md-7 text-center">
                                                            <label class="radio-inline">{!! Form::radio('front_right', 'good', null, ['class' => '']) !!}GOOD</label>
                                                            <label class="radio-inline">{!! Form::radio('front_right', 'fair', null, ['class' => '']) !!}FAIR</label>
                                                            <label class="radio-inline">{!! Form::radio('front_right', 'poor', null, ['class' => '']) !!}POOR </label>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td class="col-md-5 p-12">Rear left:</td>
                                                        <td class="col-md-7 text-center">
                                                            <label class="radio-inline">{!! Form::radio('rear_left', 'good', null, ['class' => '']) !!}GOOD</label>
                                                            <label class="radio-inline">{!! Form::radio('rear_left', 'fair', null, ['class' => '']) !!}FAIR</label>
                                                            <label class="radio-inline">{!! Form::radio('rear_left', 'poor', null, ['class' => '']) !!}POOR </label>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td class="col-md-5 p-12">Rear right: </td>
                                                        <td class="col-md-7 text-center">
                                                            <label class="radio-inline">{!! Form::radio('rear_right', 'good', null, ['class' => '']) !!}GOOD</label>
                                                            <label class="radio-inline">{!! Form::radio('rear_right', 'fair', null, ['class' => '']) !!}FAIR</label>
                                                            <label class="radio-inline">{!! Form::radio('rear_right', 'poor', null, ['class' => '']) !!}POOR </label>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td class="col-md-5 p-12">Conventional spare:</td>
                                                        <td class="col-md-7 text-center">
                                                            <label class="radio-inline">{!! Form::radio('conventional_spare', 'good', null, ['class' => '']) !!}GOOD</label>
                                                            <label class="radio-inline">{!! Form::radio('conventional_spare', 'fair', null, ['class' => '']) !!}FAIR</label>
                                                            <label class="radio-inline">{!! Form::radio('conventional_spare', 'poor', null, ['class' => '']) !!}POOR </label>
                                                        </td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="col-md-12 col-sm-12  clearfix">
                                            <div class="row">
                                                <p class="height-auto">Note and explain uneven wear:</p>
                                                {!! Form::textarea('tires_note', null, ['class' => 'form-control h-auto textarea-1','rows'=>'3']) !!}
                                            </div>
                                        </div>

                                    </div>

                            </div>
                        </div>

                        <div class="gridbox-v6 gridbox-v6-3 clearfix">
                            <h4>BRAKES</h4>
                            <div class="box-vs1 clearfix">

                                    <div class="grid-box-1 table-01 clearfix">

                                        <div class="col-md-12 col-sm-12  clearfix">
                                            <div class="row">
                                                <p class="height-auto">Check for master cylinder leaks. If unusual conditions, explain:</p>
                                                {!! Form::textarea('brakes_note', null, ['class' => 'form-control h-auto textarea-1','rows'=>'3']) !!}
                                            </div>
                                        </div>

                                    </div>

                            </div>
                        </div>

                    </div>

                    <div class="col-md-6 col-sm-12 p0 clearfix">

                        <div class="gridbox-v6 gridbox-v6-4 clearfix">
                            <div class="box-vs1 clearfix">

                                    <div class="grid-box-1 table-01 clearfix">

                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <tbody>

                                                    <tr>
                                                        <td class="col-md-5 p-12">Check brake pedal:</td>
                                                        <td class="col-md-7 text-center">
                                                            <label class="radio-inline">{!! Form::radio('check_brake_pedal', 'firm', null, ['class' => '']) !!}FIRM </label>
                                                            <span class="space-line">/</span>
                                                            <label class="radio-inline">{!! Form::radio('check_brake_pedal', 'soft', null, ['class' => '']) !!}SOFT</label>
                                                        </td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="col-md-12 col-sm-12  clearfix">
                                            <div class="row">
                                                <p class="height-auto">Comments:</p>
                                                {!! Form::textarea('comments', null, ['class' => 'form-control h-auto textarea-1','rows'=>'3']) !!}
                                            </div>
                                        </div>

                                        <div class="table-responsive mt-20">
                                            <table class="table table-bordered">
                                                <tbody>

                                                    <tr>
                                                        <td class="col-md-5 p-12">Check brake fluid: </td>
                                                        <td class="col-md-7 text-center">
                                                            <label class="radio-inline">{!! Form::radio('check_brake_fluid', 'full', null, ['class' => '']) !!}FULL </label>
                                                            <span class="space-line">/</span>
                                                            <label class="radio-inline">{!! Form::radio('check_brake_fluid', 'low', null, ['class' => '']) !!}LOW</label>
                                                        </td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>

                                    </div>

                            </div>
                        </div>

                        <div class="gridbox-v6 gridbox-v6-5 clearfix">
                            <h4>EXTERIOR</h4>
                            <div class="box-vs1 clearfix">

                                    <div class="grid-box-1 table-01 clearfix">

                                        <div class="table-responsive">
                                            <table class="table table-bordered">
                                                <tbody>

                                                    <tr>
                                                        <td class="col-md-5 p-12">Windshield wipers:</td>
                                                        <td class="col-md-7 text-center">
                                                            <label class="radio-inline">{!! Form::radio('windshield_wipers', 'good', null, ['class' => '']) !!}GOOD</label>
                                                            <label class="radio-inline">{!! Form::radio('windshield_wipers', 'fair', null, ['class' => '']) !!}FAIR</label>
                                                            <label class="radio-inline">{!! Form::radio('windshield_wipers', 'poor', null, ['class' => '']) !!}POOR </label>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td class="col-md-5 p-12">Paint, overall condition:</td>
                                                        <td class="col-md-7 text-center">
                                                            <label class="radio-inline">{!! Form::radio('paint_overall_condition', 'good', null, ['class' => '']) !!}GOOD</label>
                                                            <label class="radio-inline">{!! Form::radio('paint_overall_condition', 'fair', null, ['class' => '']) !!}FAIR</label>
                                                            <label class="radio-inline">{!! Form::radio('paint_overall_condition', 'poor', null, ['class' => '']) !!}POOR </label>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td class="col-md-5 p-12">Glass, overall condition: </td>
                                                        <td class="col-md-7 text-center">
                                                            <label class="radio-inline">{!! Form::radio('glass_overall_condition', 'no damage', null, ['class' => '']) !!}NO DAMAGE</label>
                                                            <label class="radio-inline">{!! Form::radio('glass_overall_condition', 'damage', null, ['class' => '']) !!}DAMAGE</label>
                                                        </td>
                                                    </tr>

                                                    <tr>
                                                        <td class="col-md-5 p-12">Ladder Rack:</td>
                                                        <td class="col-md-7 text-center">
                                                            <label class="radio-inline">{!! Form::radio('ladder_Rack', 'good', null, ['class' => '']) !!}GOOD</label>
                                                            <label class="radio-inline">{!! Form::radio('ladder_Rack', 'fair', null, ['class' => '']) !!}FAIR</label>
                                                            <label class="radio-inline">{!! Form::radio('ladder_Rack', 'poor', null, ['class' => '']) !!}POOR </label>
                                                        </td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                        </div>

                        <div class="gridbox-v6 gridbox-v6-6 clearfix">
                            <h4>ENGINE</h4>
                            <div class="box-vs1 clearfix">

                                <div class="grid-box-1 table-01 clearfix">

                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <tbody>

                                                <tr>
                                                    <td class="col-md-5 p-12">Engine: </td>
                                                    <td class="col-md-7 text-center">
                                                        <label class="radio-inline">{!! Form::radio('engine', 'clean', null, ['class' => '']) !!}CLEAN </label>
                                                        <span class="space-line">/</span>
                                                        <label class="radio-inline">{!! Form::radio('engine', 'dirty', null, ['class' => '']) !!}DIRTY</label>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col-md-12 col-sm-12  clearfix">
                                        <div class="row">
                                            <p class="height-auto">Note apparent leakage:</p>
                                            {!! Form::textarea('note_apparent_leakage', null, ['class' => 'form-control h-auto textarea-1','rows'=>'3']) !!}
                                        </div>
                                    </div>

                                    <div class="table-responsive mt-20">
                                        <table class="table table-bordered">
                                            <tbody>

                                                <tr>
                                                    <td class="col-md-5 p-12">Engine oil: </td>
                                                    <td class="col-md-7 text-center">
                                                        <label class="radio-inline">{!! Form::radio('engine_oil', 'full', null, ['class' => '']) !!}FULL </label>
                                                        <span class="space-line">/</span>
                                                        <label class="radio-inline">{!! Form::radio('engine_oil', 'low', null, ['class' => '']) !!}LOW</label>
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="col-md-12 col-sm-12  clearfix">
                                        <div class="row">
                                            <p class="height-auto">Oil Condition:</p>
                                            {!! Form::textarea('oil_condition', null, ['class' => 'form-control h-auto textarea-1','rows'=>'2']) !!}
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12 mt-20 clearfix">
                                        <div class="row">
                                            <p class="height-auto">Mileage of last oil change:</p>
                                            {!! Form::text('mileage_of_last_oil_change', null, ['class' => 'form-control input-100']) !!}
                                        </div>
                                    </div>

                                    <div class="table-responsive mt-20">
                                        <table class="table table-bordered">
                                            <tbody>

                                                <tr>
                                                <td class="col-md-5 p-12">Windshield washer fluid:</td>
                                                <td class="col-md-7 text-center">
                                                    <label class="radio-inline">{!! Form::radio('windshield_washer_fluid', 'full', null, ['class' => '']) !!}FULL </label>
                                                    <span class="space-line">/</span>
                                                    <label class="radio-inline">{!! Form::radio('windshield_washer_fluid', 'low', null, ['class' => '']) !!}LOW</label>
                                                </td>
                                                </tr>
                                                <tr>
                                                <td class="col-md-5 p-12">Transmission fluid condition: </td>
                                                <td class="col-md-7 text-center">
                                                    <label class="radio-inline">{!! Form::radio('transmission_fluid_condition', 'full', null, ['class' => '']) !!}FULL </label>
                                                    <span class="space-line">/</span>
                                                    <label class="radio-inline">{!! Form::radio('transmission_fluid_condition', 'low', null, ['class' => '']) !!}LOW</label>
                                                </td>
                                                </tr>
                                                <tr>
                                                <td class="col-md-5 p-12">Transmission Color:</td>
                                                <td class="col-md-7 text-center">
                                                    <label class="radio-inline">{!! Form::radio('transmission_color', 'red', null, ['class' => '']) !!}RED </label>
                                                    <span class="space-line">/</span>
                                                    <label class="radio-inline">{!! Form::radio('transmission_color', 'black', null, ['class' => '']) !!}BLACK</label>
                                                </td>
                                                </tr>
                                                <tr>
                                                <td class="col-md-5 p-12">Power steering fluid: </td>
                                                <td class="col-md-7 text-center">
                                                    <label class="radio-inline">{!! Form::radio('power_steering_fluid', 'full', null, ['class' => '']) !!}FULL </label>
                                                    <span class="space-line">/</span>
                                                    <label class="radio-inline">{!! Form::radio('power_steering_fluid', 'low', null, ['class' => '']) !!}LOW</label>
                                                </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>


                                </div>

                            </div>
                        </div>

                        </div>
                    </div>



                    </div>

                </div>
            </div>
        </div><!-- End of box-div -->
        <div class="grid-box-1 clearfix">
            <div class=" pull-left">
                {!! Form::submit((isset($jobWeeklyVehicleSafetyChecklistForm) & $jobWeeklyVehicleSafetyChecklistForm   != null)? 'Update' : 'Add', ['class' => 'btn btn-primary'])
                !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection
@push('script-head')
    <script type="text/javascript">
        $(function () {
            $('.vehicle_date').datetimepicker({
                format: 'YYYY-MM-DD'
            });
        })

    </script>

@endpush