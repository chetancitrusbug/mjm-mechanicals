@extends('layouts.backend')
@section('title',trans('job.view_form'))
@section('content')
@push('css')
    <style>
        input[type="number"]{
            width: 100%;
            border: 0px solid #000 !important;
        }
    </style>
@endpush
{!! Form::model($jobSystemRepairForm, [ 'method' => 'PATCH', 'url' => ['/admin/jobSystemRepairForm', $jobSystemRepairForm->id],
'class' => 'form-horizontal','id'=>'jobSystemRepairForm', 'enctype'=>'multipart/form-data' ]) !!}
<div class="panel panel-default">
    <div class="panel-heading">Indoor Comfort System Repair or Replace Decision Worksheet</div>
    <div class="panel-body">
        <!--<a href="http://13.127.235.254/dev/laravel/mjm-mechanicals/public/admin/job" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>Back</button></a>-->
        <a href="{{ url('/admin/job/' . $job->id . '/edit') }}" title="Back to Forms list" ><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true" ></i>Back</button></a>
        {!! Form::hidden('job_id', $job->id, ['class' => 'form-control input-100']) !!}

        <div class="grid-box-1 clearfix">
            <div class="col-md-4 col-sm-4 pull-right clearfix">
                <div class="img-logo-div">
                    <img src="{{asset('assets/images/carrier-logo-full.png')}}" class="img-responsive img-logo">
                </div>
            </div>
        </div>

        <div class="grid-box-1 carrier-root clearfix">
            <div class="col-md-6 col-sm-6 decesion-root clearfix">
                <div class="col-md-12 col-sm-12 clearfix">
                    <div class="decesion-box-1 clearfix">
                        <h4>Decision Section</h4>
                        <p>Complete this section to make a proper recommendation</p>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 clearfix">
                    <h3 class="h3-title-category">Category</h3>
                    <div class="table-responsive table-custom-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class="col-md-4 padd-15"></th>
                                    <th class="col-md-1 padd-15">Points</th>
                                    <th class="col-md-2"><p>Cooling and Heat Pumps <small>(Splits or Packaged)</small></p></th>
                                    <th class="col-md-2"><p>Furnaces and Boilers</p></th>
                                    <th class="col-md-2"><p>Gas/Electric Packaged Units</p></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="5" class="p-5 font-600">Age of Equipment</td>
                                </tr>
                                <tr>
                                    <td class="p-5">0 - 5 years</td>
                                    <td class="p-5">1</td>
                                    <td rowspan="4">{!! Form::number('age_equipment_cooling_and_heat',null,['class'=>'textarea-01 mh-135','min'=>'0','max'=>'20']) !!}</td>
                                    <td rowspan="4">{!! Form::number('age_equipment_furnace_and_boilers',null,['class'=>'textarea-01 mh-135','min'=>'0','max'=>'20']) !!}</td>
                                    <td rowspan="4">{!! Form::number('age_equipment_packaged_units',null,['class'=>'textarea-01 mh-135','min'=>'0','max'=>'20']) !!}</td>
                                </tr>
                                <tr>
                                    <td class="p-5">6 - 10 years</td>
                                    <td class="p-5">5</td>
                                </tr>
                                <tr>
                                    <td class="p-5">11 - 20 years </td>
                                    <td class="p-5">10</td>
                                </tr>
                                <tr>
                                    <td class="p-5">Over 20 years</td>
                                    <td class="p-5">20</td>
                                </tr>

                                <tr>
                                    <td colspan="5" class="p-5 font-600">Estimated Cost of Repairs</td>
                                </tr>
                                <tr>
                                    <td class="p-5">Less than $100</td>
                                    <td class="p-5">1</td>
                                    <td rowspan="4">{!! Form::number('estimate_cost_cooling_and_heat',null,['class'=>'textarea-01 mh-135','min'=>'0','max'=>'20']) !!}</td>
                                    <td rowspan="4">{!! Form::number('estimate_cost_furnace_and_boilers',null,['class'=>'textarea-01 mh-135','min'=>'0','max'=>'20']) !!}</td>
                                    <td rowspan="4">{!! Form::number('estimate_cost_packaged_units',null,['class'=>'textarea-01 mh-135','min'=>'0','max'=>'20']) !!}</td>
                                </tr>
                                <tr>
                                    <td class="p-5">$100 - $500</td>
                                    <td class="p-5">5</td>
                                </tr>
                                <tr>
                                    <td class="p-5">$501 - $1,000</td>
                                    <td class="p-5">10</td>
                                </tr>
                                <tr>
                                    <td class="p-5">Over $1,000</td>
                                    <td class="p-5">20</td>
                                </tr>

                                <tr>
                                    <td colspan="5" class="p-5 font-600">Repairs Under Warranty</td>
                                </tr>
                                <tr>
                                    <td class="p-5">Yes</td>
                                    <td class="p-5">0</td>
                                    <td rowspan="2">{!! Form::number('repairs_under_warranty_cooling_and_heat',null,['class'=>'textarea-01 mh-60','min'=>'0','max'=>'5']) !!}</td>
                                    <td rowspan="2">{!! Form::number('repairs_under_warranty_furnace_and_boilers',null,['class'=>'textarea-01 mh-60','min'=>'0','max'=>'5']) !!}</td>
                                    <td rowspan="2">{!! Form::number('repairs_under_warranty_packaged_units',null,['class'=>'textarea-01 mh-60','min'=>'0','max'=>'5']) !!}</td>
                                </tr>
                                <tr>
                                    <td class="p-5">No</td>
                                    <td class="p-5">5</td>
                                </tr>

                                <tr>
                                    <td colspan="5" class="p-5 font-600">SEER of Equipment<sup>1,7</sup></td>
                                </tr>
                                <tr>
                                    <td class="p-5">Over 10</td>
                                    <td class="p-5">1</td>
                                    <td rowspan="4">{!! Form::number('seer_of_equipment_cooling_and_heat',null,['class'=>'textarea-01 mh-135','min'=>'0','max'=>'20']) !!}</td>
                                    <td rowspan="4"><textarea class="textarea-01 mh-135 disabled" disabled=""></textarea></td>
                                    <td rowspan="4">{!! Form::number('seer_of_equipment_packaged_units',null,['class'=>'textarea-01 mh-135','min'=>'0','max'=>'20']) !!}</td>
                                </tr>
                                <tr>
                                    <td class="p-5">8-10</td>
                                    <td class="p-5">5</td>
                                </tr>
                                <tr>
                                    <td class="p-5">5-7</td>
                                    <td class="p-5">10</td>
                                </tr>
                                <tr>
                                    <td class="p-5">Less than 5</td>
                                    <td class="p-5">20</td>
                                </tr>

                                <tr>
                                    <td colspan="5" class="p-5 font-600">Type of Refrigerant Used<sup>2</sup></td>
                                </tr>
                                <tr>
                                    <td class="p-5">HFC</td>
                                    <td class="p-5">0</td>
                                    <td rowspan="2">{!! Form::number('type_of_refrigerant_cooling_and_heat',null,['class'=>'textarea-01 mh-60','min'=>'0','max'=>'5']) !!}</textarea></td>
                                    <td rowspan="2"><textarea class="textarea-01 mh-60 disabled" disabled=""></textarea></td>
                                    <td rowspan="2">{!! Form::number('type_of_refrigerant_packaged_units',null,['class'=>'textarea-01 mh-60','min'=>'0','max'=>'5']) !!}</textarea></td>
                                </tr>
                                <tr>
                                    <td class="p-5">HFC CFC or HCFC</td>
                                    <td class="p-5">5</td>
                                </tr>

                                <tr>
                                    <td colspan="5" class="p-5 font-600">AFUE<sup>3,7</sup></td>
                                </tr>
                                <tr>
                                    <td class="p-5">Over 87</td>
                                    <td class="p-5">1</td>
                                    <td rowspan="4"><textarea class="textarea-01 mh-135 disabled" disabled=""></textarea></td>
                                    <td rowspan="4">{!! Form::number('afue_furnace_and_boilers',null,['class'=>'textarea-01 mh-135','min'=>'0','max'=>'20']) !!}</td>
                                    <td rowspan="4">{!! Form::number('afue_packaged_units',null,['class'=>'textarea-01 mh-135','min'=>'0','max'=>'20']) !!}</td>
                                </tr>
                                <tr>
                                    <td class="p-5">80-87</td>
                                    <td class="p-5">5</td>
                                </tr>
                                <tr>
                                    <td class="p-5">70-79</td>
                                    <td class="p-5">10</td>
                                </tr>
                                <tr>
                                    <td class="p-5">Less than 70</td>
                                    <td class="p-5">20</td>
                                </tr>

                                <tr>
                                    <td colspan="5" class="p-5 font-600">Outdoor Equipment Condition<sup>4</sup></td>
                                </tr>
                                <tr>
                                    <td class="p-5">Good</td>
                                    <td class="p-5">1</td>
                                    <td rowspan="4">{!! Form::number('outdoor_equipment_cooling_and_heat',null,['class'=>'textarea-01 mh-135','min'=>'0','max'=>'15']) !!}</td>
                                    <td rowspan="4"><textarea class="textarea-01 mh-135 disabled" disabled=""></textarea></td>
                                    <td rowspan="4">{!! Form::number('outdoor_equipment_packaged_units',null,['class'=>'textarea-01 mh-135','min'=>'0','max'=>'15']) !!}</td>
                                </tr>
                                <tr>
                                    <td class="p-5">Average</td>
                                    <td class="p-5">5</td>
                                </tr>
                                <tr>
                                    <td class="p-5">Fair</td>
                                    <td class="p-5">10</td>
                                </tr>
                                <tr>
                                    <td class="p-5">Poor</td>
                                    <td class="p-5">15</td>
                                </tr>

                                <tr>
                                    <td colspan="5" class="p-5 font-600">Indoor Equipment Condition<sup>5,6</sup></td>
                                </tr>
                                <tr>
                                    <td class="p-5">Good</td>
                                    <td class="p-5">1</td>
                                    <td rowspan="4">{!! Form::number('indoor_equipment_cooling_and_heat',null,['class'=>'textarea-01 mh-135','min'=>'0','max'=>'15']) !!}</td>
                                    <td rowspan="4">{!! Form::number('indoor_equipment_furnace_and_boilers',null,['class'=>'textarea-01 mh-135','min'=>'0','max'=>'15']) !!}</td>
                                    <td rowspan="4"><textarea class="textarea-01 mh-135 disabled" disabled=""></textarea></td>
                                </tr>
                                <tr>
                                    <td class="p-5">Average</td>
                                    <td class="p-5">5</td>
                                </tr>
                                <tr>
                                    <td class="p-5">Fair</td>
                                    <td class="p-5">10</td>
                                </tr>
                                <tr>
                                    <td class="p-5">Poor</td>
                                    <td class="p-5">15</td>
                                </tr>

                                <tr>
                                    <td colspan="5" class="p-5 font-600">Owner Expects to Stay in Home</td>
                                </tr>
                                <tr>
                                    <td class="p-5">2 years or less</td>
                                    <td class="p-5">1</td>
                                    <td rowspan="4">{!! Form::number('owner_expects_cooling_and_heat',null,['class'=>'textarea-01 mh-135','min'=>'0','max'=>'15']) !!}</td>
                                    <td rowspan="4">{!! Form::number('owner_expects_furnace_and_boilers',null,['class'=>'textarea-01 mh-135','min'=>'0','max'=>'15']) !!}</td>
                                    <td rowspan="4">{!! Form::number('owner_expects_packaged_units',null,['class'=>'textarea-01 mh-135','min'=>'0','max'=>'15']) !!}</td>
                                </tr>
                                <tr>
                                    <td class="p-5">2-5 years</td>
                                    <td class="p-5">5</td>
                                </tr>
                                <tr>
                                    <td class="p-5">6-10 years</td>
                                    <td class="p-5">10</td>
                                </tr>
                                <tr>
                                    <td class="p-5">Over 10 years</td>
                                    <td class="p-5">15</td>
                                </tr>

                                <tr>
                                    <td colspan="5" class="p-5 font-600">Total Score</td>
                                </tr>
                                <tr>
                                    <td class="p-5" colspan="2">Repair:</td>
                                    <td class="p-5"> 0-40 </td>
                                    <td class="p-5"> 0-35 </td>
                                    <td class="p-5">  0-45 </td>
                                </tr>
                                <tr>
                                    <td class="p-5" colspan="2">Questionable:</td>
                                    <td class="p-5"> 41-60 </td>
                                    <td class="p-5"> 36-50 </td>
                                    <td class="p-5">  46-70 </td>
                                </tr>
                                <tr>
                                    <td class="p-5" colspan="2">Replace:</td>
                                    <td class="p-5"> 61+  </td>
                                    <td class="p-5"> 51+ </td>
                                    <td class="p-5"> 71+ </td>
                                </tr>

                            </tbody>
                        </table>
                    </div><!-- end of table-responsive -->
                </div>
            </div><!-- end of left-6 -->

            <div class="col-md-6 col-sm-6 redflag-root clearfix">
                <div class="col-md-12 col-sm-12 clearfix">
                    <div class="redflag-1 clearfix">
                        <h4 class="font-600">Red Flags</h4>
                        <p>Refer to the sales department if the customer's system requires changes due to any of the following conditions / observations:</p>
                    </div>
                </div>
                <div class="grid-box-1 table-01 redflag-table clearfix">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th class="col-md-8 text-center"></th>
                                        <th class="col-md-2 text-center">Y</th>
                                        <th class="col-md-2 text-center">N</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <h5 class="h5-heading">Airflow</h5>
                                            <ul class="ul_custom-001">
                                                <li>Are there airflow problems?</li>
                                                <li>Too hot in the summer or too cold in the winter?</li>
                                                <li>Leakage in the duct system? </li>
                                            </ul>
                                        </td>
                                        <td class="text-center valign-top">
                                            {!! Form::radio('airflow', 'y', null, ['class' => 'form-radio','id'=>'radio-one']) !!}
                                        </td>
                                        <td class="text-center valign-top">
                                            {!! Form::radio('airflow', 'n', null, ['class' => 'form-radio','id'=>'radio-one']) !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h5 class="h5-heading">Noise</h5>
                                            <ul class="ul_custom-001">
                                                <li>Are there noise issues (e.g., vibrations, whistling)?</li>
                                            </ul>
                                        </td>
                                        <td class="text-center valign-top">
                                            {!! Form::radio('noise', 'y', null, ['class' => 'form-radio','id'=>'radio-one']) !!}
                                        </td>
                                        <td class="text-center valign-top">
                                            {!! Form::radio('noise', 'y', null, ['class' => 'form-radio','id'=>'radio-one']) !!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <h5 class="h5-heading">Refrigerants</h5>
                                            <ul class="ul_custom-001">
                                                <li>Will I be changing refrigerants?<p>(May need to change metering device, line set, or coil.)</p></li>
                                            </ul>
                                        </td>
                                        <td class="text-center valign-top">
                                            {!! Form::radio('refrigerants', 'y', null, ['class' => 'form-radio','id'=>'radio-one']) !!}
                                        </td>
                                        <td class="text-center valign-top">
                                            {!! Form::radio('refrigerants', 'y', null, ['class' => 'form-radio','id'=>'radio-one']) !!}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <h5 class="h5-heading">Cycling</h5>
                                            <ul class="ul_custom-001">
                                                <li>Is the unit cycling a lot? </li>
                                            </ul>
                                        </td>
                                        <td class="text-center valign-top">
                                            {!! Form::radio('cycling', 'y', null, ['class' => 'form-radio','id'=>'radio-one']) !!}
                                        </td>
                                        <td class="text-center valign-top">
                                            {!! Form::radio('cycling', 'y', null, ['class' => 'form-radio','id'=>'radio-one']) !!}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <h5 class="h5-heading">Comfort</h5>
                                            <ul class="ul_custom-001">
                                                <li>Have there been problems with comfort (too hot or too cold) in the past?</li>
                                            </ul>
                                        </td>
                                        <td class="text-center valign-top">
                                            {!! Form::radio('comfort', 'y', null, ['class' => 'form-radio','id'=>'radio-one']) !!}
                                        </td>
                                        <td class="text-center valign-top">
                                            {!! Form::radio('comfort', 'y', null, ['class' => 'form-radio','id'=>'radio-one']) !!}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <h5 class="h5-heading">Remodeled</h5>
                                            <ul class="ul_custom-001">
                                                <li>Since the house was built, has it been remodeled or have rooms been added on? (More rooms to heat/cool? More insulation?) </li>
                                            </ul>
                                        </td>
                                        <td class="text-center valign-top">
                                            {!! Form::radio('remodeled', 'y', null, ['class' => 'form-radio','id'=>'radio-one']) !!}
                                        </td>
                                        <td class="text-center valign-top">
                                            {!! Form::radio('remodeled', 'y', null, ['class' => 'form-radio','id'=>'radio-one']) !!}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <h5 class="h5-heading">Building Codes</h5>
                                            <ul class="ul_custom-001">
                                                <li>Have there been any changes in building or mechanical codes (e.g., aluminum wiring prohibited; ordinances for outside noise from condensing units)?</li>
                                            </ul>
                                        </td>
                                        <td class="text-center valign-top">
                                            {!! Form::radio('building_codes', 'y', null, ['class' => 'form-radio','id'=>'radio-one']) !!}
                                        </td>
                                        <td class="text-center valign-top">
                                            {!! Form::radio('building_codes', 'y', null, ['class' => 'form-radio','id'=>'radio-one']) !!}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <h5 class="h5-heading">Venting</h5>
                                            <ul class="ul_custom-001">
                                                <li>Are there any concerns about venting (e.g., a midrange furnace vented into a masonry chimney)? </li>
                                            </ul>
                                        </td>
                                        <td class="text-center valign-top">
                                            {!! Form::radio('venting', 'y', null, ['class' => 'form-radio','id'=>'radio-one']) !!}
                                        </td>
                                        <td class="text-center valign-top">
                                            {!! Form::radio('venting', 'y', null, ['class' => 'form-radio','id'=>'radio-one']) !!}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <h5 class="h5-heading">Fuel Changes</h5>
                                            <ul class="ul_custom-001">
                                                <li>Would replacement be an opportunity to change fuel type (e.g., oil to gas; electric to gas; etc.)?  </li>
                                            </ul>
                                        </td>
                                        <td class="text-center valign-top">
                                            {!! Form::radio('fuel_changes', 'y', null, ['class' => 'form-radio','id'=>'radio-one']) !!}
                                        </td>
                                        <td class="text-center valign-top">
                                            {!! Form::radio('fuel_changes', 'y', null, ['class' => 'form-radio','id'=>'radio-one']) !!}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <h5 class="h5-heading">Safety Issues</h5>
                                            <ul class="ul_custom-001">
                                                <li>Carbon monoxide present?</li>
                                                <li>Fuel leaks?</li>
                                                <li>Unsafe wiring?</li>
                                            </ul>
                                        </td>
                                        <td class="text-center valign-top">
                                            {!! Form::radio('safety_issues', 'y', null, ['class' => 'form-radio','id'=>'radio-one']) !!}
                                        </td>
                                        <td class="text-center valign-top">
                                            {!! Form::radio('safety_issues', 'y', null, ['class' => 'form-radio','id'=>'radio-one']) !!}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <h5 class="h5-heading">Repair History</h5>
                                            <ul class="ul_custom-001">
                                                <li>Has the compressor been replaced?</li>
                                                <li>Has the heat exchanger been replaced?</li>
                                            </ul>
                                        </td>
                                        <td class="text-center valign-top">
                                            {!! Form::radio('repair_history', 'y', null, ['class' => 'form-radio','id'=>'radio-one']) !!}
                                        </td>
                                        <td class="text-center valign-top">
                                            {!! Form::radio('repair_history', 'y', null, ['class' => 'form-radio','id'=>'radio-one']) !!}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div><!-- col-12 -->

                    <div class="col-md-12">
                        <div class="row">
                            <div class="bg-footer-note clearfix">
                                <div class="col-md-6">
                                    <p><span class="font-600 h4-title-note">Notes:</span> </p>
                                    <ol class="ol-list">
                                        <li>Air Conditioners &amp; Heat Pumps  </li>
                                        <li>HFC (R134A, Puron®), CFC (R12, R-500), HCFC (R-22) </li>
                                        <li>Furnaces, Boilers &amp; Gas/Electric PACs </li>
                                        <li>Split Systems &amp; Packaged Units only </li>
                                        <li>Fan Coils &amp; Furnace Coils </li>
                                        <li>N/A for Packaged HP &amp; AC </li>
                                        <li>Consult Carrier's Blue Book for SEER/AFUE Ratings</li>
                                    </ol>
                                </div>
                                <div class="col-md-6">
                                    <p>
                                        <span class="font-600">Reminder:</span> If the outdoor unit is replaced, the indoor coil probably needs to be replaced.
                                    </p>
                                    <div class="col-md-12 col-sm-12 p-5-0 clearfix">
                                        <p class="small-p">Date:</p>
                                        {!! Form::text('date', null, ['class' => 'form-control input-100 form-input-1 repair_date', 'placeholder'=>'Date']) !!}
                                    </div>
                                    <div class="col-md-12 col-sm-12 p-5-0 clearfix">
                                        <p class="small-p">Customer:</p>
                                        {!! Form::text('customer', null, ['class' => 'form-control input-100 form-input-1', 'placeholder'=>'Customer']) !!}
                                    </div>
                                    <div class="col-md-12 col-sm-12 p-5-0 clearfix">
                                        <p class="small-p">Technician:</p>
                                        {!! Form::text('technician', null, ['class' => 'form-control input-100 form-input-1', 'placeholder'=>'Technician']) !!}
                                    </div>
                                </div>
                            </div>    <!--  end of bg-footer-note -->
                        </div>
                    </div>

                </div> <!-- end of box -->

            </div><!-- end of right-6  -->

        </div><!-- End of box-div -->

        <div class="grid-box-1 clearfix">
            <div class=" pull-left">
                {!! Form::submit((isset($jobSystemRepairForm) & $jobSystemRepairForm != null)? 'Update' : 'Add', ['class' => 'btn btn-primary'])
                !!}
            </div>
        </div>

    </div>
</div>
{!! Form::close() !!}
@endsection
@push('script-head')
    <script type="text/javascript">
        $(function () {
            $('.repair_date').datetimepicker({
                format: 'DD-MM-YYYY'
            });
        })

    </script>

@endpush