<!DOCTYPE html>
<html>
<head>
	<title>Contact Us - MJM</title>
</head>
<body>
    <p>Hello Admin,</p>
    <p>Special inquiry is generated. Please find below Attachments.</p>
	<table class="table table-hover">
		<tbody>
            @if(isset($data->file_1) && $data->file_1 != null)
		  	<tr>
			    <td>File 1 :</td>
			    <td>{{$data->file_1}}</td>
            </tr>
            @endif
            @if(isset($data->file_2) && $data->file_2 != null)
		  	<tr>
			    <td>File 2 :</td>
			    <td>{{$data->file_2}}</td>
              </tr>
              @endif
            @if(isset($data->file_3) && $data->file_3 != null)
		  	<tr>
			    <td>File 3 :</td>
			    <td>{{$data->file_3}}</td>
              </tr>
              @endif
            @if(isset($data->file_4) && $data->file_4 != null)
		  	<tr>
			    <td>File 4 :</td>
			    <td>{{$data->file_4}}</td>
              </tr>
              @endif
		  	<tr>
			    <td>Note :</td>
			    <td>{{$data->note}}</td>
              </tr>

            <tr>
                <td>Name :</td>
                <td>{{$name}}</td>
            </tr>
            <tr>
                <td>Email :</td>
                <td>{{$email}}</td>
            </tr>
		</tbody>
  	</table>
</body>
</html>