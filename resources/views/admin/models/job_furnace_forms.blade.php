<div class="panel panel-default">
    <div class="panel-heading">HVAC Inspection Report - Furnace</div>
    <div class="panel-body">
        <a href="#" title="Back to Forms list"><button type="button" class=" backToForm btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true" ></i>Back</button></a>
        {!! Form::hidden('job_id', $job->id, ['class' => 'form-control input-100']) !!}
        <div class="grid-box-1 clearfix">
            <div class="col-md-4 col-sm-4 pull-right clearfix">
                <p class="height-auto">Date:</p>
                {!! Form::text('date', null, ['class' => 'form-control input-100 HVAC_date']) !!}
                {!! $errors->first('date', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="grid-box-1 clearfix">
            <div class="col-md-4 col-sm-4 clearfix">
                <div class="row">
                    <div class="col-md-12 col-sm-12 clearfix">
                        <p class="height-auto">Customer Name</p>
                        {!! Form::text('customer_name', null, ['class' => 'form-control input-100']) !!}
                        {!! $errors->first('customer_name', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <p class="height-auto">Phone</p>
                        {!! Form::text('phone', null, ['class' => 'form-control input-100']) !!}
                        {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <p class="height-auto">E-Mail</p>
                        {!! Form::text('email', null, ['class' => 'form-control input-100']) !!}
                        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-8 clearfix">
                <div class="row">
                    <div class="col-md-12 col-sm-12 clearfix">
                        <p class="height-auto">Address</p>
                        {!! Form::textarea('address', null, ['class' => 'form-control h-auto textarea1','rows'=>'5']) !!}
                        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <p class="height-auto">System Age:</p>
                        {!! Form::text('system_age', null, ['class' => 'form-control input-100']) !!}
                        {!! $errors->first('system_age', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div><!-- End of box-div -->

        <div class="grid-box-1 table-01 clearfix">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td class="col-md-9 p-12">Are you staying warm enough? </td>
                                <td class="col-md-3 text-center">
                                    <label class="radio-inline">{!! Form::radio('staying_warm_enough', 'y', null, ['class' => '']) !!}Y</label>
                                    <span class="space-line">/</span>
                                    <label class="radio-inline">{!! Form::radio('staying_warm_enough', 'n', null, ['class' => '']) !!}N</label></td>
                            </tr>
                            <tr>
                                <td class="col-md-9 p-12">Are temperatures consistent throughout the house? </td>
                                <td class="col-md-3 text-center">
                                    <label class="radio-inline">{!! Form::radio('temrature_consistent', 'y', null, ['class' => '']) !!}Y</label>
                                    <span class="space-line">/</span>
                                    <label class="radio-inline">{!! Form::radio('temrature_consistent', 'n', null, ['class' => '']) !!}N</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-9 p-12">Is it dry inside your home during the winter? </td>
                                <td class="col-md-3 text-center">
                                    <label class="radio-inline">{!! Form::radio('dry_inside_home', 'y', null, ['class' => '']) !!}Y</label>
                                    <span class="space-line">/</span>
                                    <label class="radio-inline">{!! Form::radio('dry_inside_home', 'n', null, ['class' => '']) !!}N</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-9 p-12">Have there been any weird smells or sounds when the furnace turns on? </td>
                                <td class="col-md-3 text-center">
                                    <label class="radio-inline">{!! Form::radio('have_any_weird_smells', 'y', null, ['class' => '']) !!}Y</label>
                                    <span class="space-line">/</span>
                                    <label class="radio-inline">{!! Form::radio('have_any_weird_smells', 'n', null, ['class' => '']) !!}N</label>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end of box -->

        <div class="grid-box-1 table-01 clearfix">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="col-md-3 text-center">Furnace/Boiler</th>
                                <th class="col-md-3 text-center">Brand</th>
                                <th class="col-md-3 text-center">Model #</th>
                                <th class="col-md-3 text-center">Serial #</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <tr>
                                <td>{!! Form::text('furnace_boiler_1', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('brand_1', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('model_1', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('serial_1', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                            <tr>
                                <td>{!! Form::text('furnace_boiler_2', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('brand_2', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('model_2', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('serial_2', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                            <tr>
                                <td>{!! Form::text('furnace_boiler_3', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('brand_3', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('model_3', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('serial_3', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end of box -->

        <div class="grid-box-1 table-01 clearfix">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="col-md-3 text-center">Filter Size</th>
                                <th class="col-md-3 text-center">Quantity</th>
                                <th class="col-md-6 text-center">Accessories <small>(circle all that apply)</small></th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <tr>
                                <td>{!! Form::text('filter_size_1', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('quantity_1', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>
                                    <label class="radio-inline">{!! Form::radio('accessories_1', 'humidifier', false, ['class' => '']) !!}Humidifier</label>
                                    <label class="radio-inline">{!! Form::radio('accessories_1', 'condensate_pump', false, ['class' => '']) !!}Condensate Pump</label>
                                    <label class="radio-inline">{!! Form::radio('accessories_1', 'uv_light', false, ['class' => '']) !!}UV Light</label>
                                </td>
                            </tr>
                            <tr>
                                <td>{!! Form::text('filter_size_2', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('quantity_2', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>
                                    <label class="radio-inline">{!! Form::radio('accessories_2', 'electronic_air_cleaner', false, ['class' => '']) !!}Electronic Air Cleaner</label>
                                    <label class="radio-inline">{!! Form::radio('accessories_2', 'carbon_monoxide_detector', false, ['class' => '']) !!}Carbon Monoxide Detector</label>
                                </td>
                            </tr>
                            <tr>
                                <td>{!! Form::text('filter_size_3', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('quantity_3', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>
                                    <label class="radio-inline">{!! Form::radio('accessories_3', 'gps', false, ['class' => '']) !!}GPS</label>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end of box -->

        <div class="grid-box-1 table-01 clearfix">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="col-md-6 text-center" colspan="6">HEATING INFO <small>(circle all that apply)</small></th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <tr>
                                <td class="col-md-1 col-1">Gas Type</td>
                                <td class="col-md-3 col-3">
                                    <label class="radio-inline">{!! Form::radio('gas_type', 'natural', false, ['class' => '']) !!}Natural</label>
                                    <label class="radio-inline">{!! Form::radio('gas_type', 'propane', false, ['class' => '']) !!}Propane</label>
                                    <label class="radio-inline">{!! Form::radio('gas_type', 'oil', false, ['class' => '']) !!}Oil</label>
                                    <label class="radio-inline">{!! Form::radio('gas_type', 'none', false, ['class' => '']) !!}None</label>
                                </td>
                                <td class="col-md-1 col-1">Blower</td>
                                <td class="col-md-3 col-3">
                                    <label class="radio-inline">{!! Form::radio('blower', 'psc', false, ['class' => '']) !!}PSC</label>
                                    <label class="radio-inline">{!! Form::radio('blower', 'x13', false, ['class' => '']) !!}X13</label>
                                    <label class="radio-inline">{!! Form::radio('blower', 'ecm', false, ['class' => '']) !!}ECM</label>
                                </td>
                                <td class="col-md-1 col-1">Ignition</td>
                                <td class="col-md-3 col-3">
                                    <label class="radio-inline">{!! Form::radio('ignition', 'spark', false, ['class' => '']) !!}Spark </label>
                                    <label class="radio-inline">{!! Form::radio('ignition', 'standing_pilot', false, ['class' => '']) !!}Standing Pilot</label>
                                    <label class="radio-inline">{!! Form::radio('ignition', 'hsi', false, ['class' => '']) !!}H.S.I</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-1 col-1">Stages</td>
                                <td class="col-md-3 col-3">
                                    <label class="radio-inline">{!! Form::radio('stages', '1', false, ['class' => '']) !!}1</label>
                                    <label class="radio-inline">{!! Form::radio('stages', '2', false, ['class' => '']) !!}2</label>
                                    <label class="radio-inline">{!! Form::radio('stages', '3', false, ['class' => '']) !!}3</label>
                                    <label class="radio-inline">{!! Form::radio('stages', 'mod', false, ['class' => '']) !!}MOD</label>
                                </td>
                                <td class="col-md-1 col-1">Draft Mtr</td>
                                <td class="col-md-3 col-3">
                                    <label class="radio-inline">{!! Form::radio('draft_meter', 'none', false, ['class' => '']) !!}None</label>
                                    <label class="radio-inline">{!! Form::radio('draft_meter', 'std', false, ['class' => '']) !!}STD</label>
                                    <label class="radio-inline">{!! Form::radio('draft_meter', 'ecm', false, ['class' => '']) !!}ECM</label>
                                </td>
                                <td class="col-md-1 col-1">Efficiency</td>
                                <td class="col-md-3 col-3">
                                    <label class="radio-inline">{!! Form::radio('efficiency', 'electric', false, ['class' => '']) !!}ELECTRIC</label>
                                    <label class="radio-inline">{!! Form::radio('efficiency', '80+', false, ['class' => '']) !!}80+</label>
                                    <label class="radio-inline">{!! Form::radio('efficiency', '92+', false, ['class' => '']) !!}92+</label>
                                    <label class="radio-inline">{!! Form::radio('efficiency', '95+', false, ['class' => '']) !!}95+</label>
                                    <label class="radio-inline">{!! Form::radio('efficiency', '98+', false, ['class' => '']) !!}98+</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end of box -->

        <div class="grid-box-1 table-01 clearfix">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="col-md-6 text-center" colspan="6">FURNACE READINGS <small>(circle all that apply)</small></th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <tr>
                                <td class="col-md-3 font-bold">BLOWER AMPS:</td>
                                <td class="col-md-3">
                                    <span class="w-40">{!! Form::text('blower_amps_r', null, ['class' => 'text-check']) !!}R </span>
                                    <span class="w-40">{!! Form::text('blower_amps_a', null, ['class' => 'text-check']) !!}A </span>
                                </td>
                                <td class="col-md-3 font-bold">FLAME SIGNAL uA</td>
                                <td class="col-md-3">{!! Form::text('flame_signal_ua', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                            <tr>
                                <td class="col-md-3 font-bold">BLOWER CAPACITOR:</td>
                                <td class="col-md-3">
                                    <span class="w-40">{!! Form::text('blower_capacitor_r', null, ['class' => 'text-check']) !!}R </span>
                                    <span class="w-40">{!! Form::text('blower_capacitor_a', null, ['class' => 'text-check']) !!}A </span>
                                </td>
                                <td class="col-md-3 font-bold">GAS PRESSURE:</td>
                                <td class="col-md-3">
                                    <span class="w-40">{!! Form::text('gas_pressure_r', null, ['class' => 'text-check']) !!}R </span>
                                    <span class="w-40">{!! Form::text('gas_pressure_a', null, ['class' => 'text-check']) !!}A </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-3 font-bold">DRAFT MOTOR AMPS:</td>
                                <td class="col-md-3">
                                    <span class="w-40">{!! Form::text('draft_motor_amps_r', null, ['class' => 'text-check']) !!}R </span>
                                    <span class="w-40">{!! Form::text('draft_motor_amps_a', null, ['class' => 'text-check']) !!}A </span>
                                </td>
                                <td class="col-md-3 font-bold">THERMOSTAT SETPOINT</td>
                                <td class="col-md-3">{!! Form::text('thermostat_setpoint', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                            <tr>
                                <td class="col-md-3 font-bold">DRAFT MOTOR CAPACITOR:</td>
                                <td class="col-md-3">
                                    <span class="w-40">{!! Form::text('draft_motor_capacitor_r', null, ['class' => 'text-check']) !!}R </span>
                                    <span class="w-40">{!! Form::text('draft_motor_capacitor_a', null, ['class' => 'text-check']) !!}A </span>

                                </td>
                                <td class="col-md-3 font-bold">TEMP/ HUMIDITY:</td>
                                <td class="col-md-3">
                                    <span class="w-30">
                                        <label class="radio-inline">{!! Form::radio('temp_humidity', 'indoor', false, ['class' => '']) !!}Indoor</label>
                                    </span>
                                    <span class="w-30">
                                        <label class="radio-inline">{!! Form::radio('temp_humidity', 'oa', false, ['class' => '']) !!}OA</label>
                                    </span>
                                    <span class="w-30">
                                        <label class="radio-inline">{!! Form::radio('temp_humidity', 'rh', false, ['class' => '']) !!}RH</label>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-3 font-bold">IGNITOR OHMS:</td>
                                <td class="col-md-3">
                                    <span class="w-40">{!! Form::text('ignitor_ohms_r', null, ['class' => 'text-check']) !!}R </span>
                                    <span class="w-40">{!! Form::text('ignitor_ohms_a', null, ['class' => 'text-check']) !!}A </span>
                                </td>
                                <td class="col-md-3 font-bold">TSTAT TYPE</td>
                                <td class="col-md-3" rowspan="2">
                                    <label class="radio-inline">{!! Form::radio('tstat_type', 'mechanical', false, ['class' => '']) !!}Mechanical</label>
                                    <label class="radio-inline">{!! Form::radio('tstat_type', 'digital', false, ['class' => '']) !!}Digital</label>
                                    <label class="radio-inline">{!! Form::radio('tstat_type', 'programmable', false, ['class' => '']) !!}Programmable</label>
                                    <label class="radio-inline">{!! Form::radio('tstat_type', 'communicating', false, ['class' => '']) !!}Communicating</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-3 font-bold">TEMPERATURE RISE:</td>
                                <td class="col-md-6" colspan="2">
                                    <span class="w-30">
                                        {!! Form::text('temprature_rise_rated', null, ['class' => 'text-check']) !!}RATED</label>
                                    </span>
                                    <span class="w-30">
                                        {!! Form::text('temprature_rise_actual', null, ['class' => 'text-check']) !!}ACTUAL</label>
                                    </span>
                                    <span class="w-30">
                                        {!! Form::text('temprature_rise_degrees', null, ['class' => 'text-check']) !!}DEGREES</label>
                                    </span>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end of box -->

        <div class="grid-box-1 table-01 clearfix">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="col-md-6 text-center" colspan="6">CIRCLE ALL THAT APPLY</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <tr>
                                <td class="col-md-2 font-bold">MOTOR BEARINGS</td>
                                <td class="col-md-4">
                                    <label class="radio-inline">{!! Form::text('motor_bearings', null, ['class' => '']) !!}GOOD/TIGHT/NEEDS REPLACED</label>
                                </td>
                                <td class="col-md-2 font-bold">BURNERS</td>
                                <td class="col-md-4">
                                    <label class="radio-inline">{!! Form::text('burners', null, ['class' => '']) !!}CLEAN/RUSTY/REPLACE</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-2 font-bold">BLOWER WHEEL</td>
                                <td class="col-md-4">
                                    <label class="radio-inline">{!! Form::text('blower_wheel', null, ['class' => '']) !!}CLEAN/NEEDS CLEANED/OUT OF BALANCE</label>
                                </td>
                                <td class="col-md-2 font-bold">IGNITOR</td>
                                <td class="col-md-4">
                                    <label class="radio-inline">{!! Form::text('ignitor', null, ['class' => '']) !!}OK/REPLACE</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-2 font-bold">HEAT EXCHANGER</td>
                                <td class="col-md-4">
                                    <label class="radio-inline">{!! Form::text('heat_exchanger', null, ['class' => '']) !!}OK/SIGNS OF STRESS/CRACKED</label>
                                </td>
                                <td class="col-md-2 font-bold">LIMITS</td>
                                <td class="col-md-4">
                                    <label class="radio-inline">{!! Form::text('limits', null, ['class' => '']) !!}PASS/FAIL</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end of box -->

        <div class="grid-box-1 table-01 clearfix">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table">
                        <tbody class="text-left">
                            <tr>
                                <td class="col-md-12 bt-0">
                                    {!! Form::textarea('notes', null, ['class' => 'form-control h-auto textarea1','rows'=>'3',"placeholder"=>"NOTES:"]) !!}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end of box -->

        <div class="grid-box-1 clearfix">

            <div class="col-md-4 col-sm-4 clearfix">
                <p class="height-auto">Tech:</p>
                {!! Form::text('tech', null, ['class' => 'form-control input-100']) !!}
            </div>

            <div class="col-md-4 col-sm-4 clearfix">
                <p class="height-auto">Occupant:</p>
                {!! Form::text('occupant', null, ['class' => 'form-control input-100']) !!}
            </div>

            <div class="col-md-4 col-sm-4 clearfix">
                <p class="height-auto">Date</p>
                {!! Form::text('bottom_date', null, ['class' => 'form-control input-100']) !!}
            </div>

        </div>

        <div class="grid-box-1 clearfix">
            <div class=" pull-left">
                {!! Form::submit((isset($jobFurnaceForm) & $jobFurnaceForm != null)? 'Update' : 'Add', ['class' => 'btn btn-primary'])
                !!}
            </div>
        </div>

    </div>
</div>

@push('script-head')
<script type="text/javascript">
    $(function () {
        $('.bottom_date,.HVAC_date').datetimepicker({
            format: 'DD-MM-YYYY'
        });
    })

</script>


@endpush