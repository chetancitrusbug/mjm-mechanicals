<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\Client;
use App\Job;
use App\State;
use Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
class UsersController extends Controller
{

    public function login(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Login Success';
        $status = 'true';
        $error = '';
        $email = $request->email;
        $password = $request->password;
        $device_token = $request->device_token;
        $device_type = $request->device_type;
        $user = User::where('email',$email)->where('deleted_at',null)->select('*','id as user_id')->first();
		//$user = User::where('email',$email)->where('deleted_at',null)->first();
        if($user){
            if($user->status != 'active'){
                $messages = 'User is not Active';
                $code = 400;
                $status = 'false';
            }elseif(Hash::check($password, $user->password)){
                $user->api_token = md5(uniqid());
                $user->device_token = $device_token;
                $user->device_type = $device_type;
                $user->update();
                $data = $user;

            }else{
                $messages = 'Password is wrong';
                $code = 400;
                $status = 'false';
            }
        }else{
            $messages = 'Email/User not found';
            $code = 400;
            $status = 'false';
        }
        return response()->json(['data'=>$data,'code'=>$code,'message'=>$messages,'status'=>$status]);
        exit;
    }

    public function forgot(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Forgot passwrod mail Success';
        $status = 'true';
        $error = '';
        $email = $request->email;
        $response = Password::sendResetLink($request->only('email'), function (Message $message) {
            $message->subject($this->getEmailSubject());

        });
        if ($response == "passwords.sent") {
            $messages = 'Password reset link has been sent to email';
        } else if ($response == "passwords.user") {
            $status = false;
            $messages = 'User not found';
            $code = 400;
        } else {
            $status = false;
            $messages = 'Something went wrong ! Please try again later';
            $code = 400;
        }
        return response()->json(['data' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function register(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Registration Success';
        $status = 'true';
        $error = '';
        $rules = array(
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = \Config::get('constants.responce_code.validation_failed');
            $message = 'Not valid data';
			return $this->toJson($status, $data, $message, $validation, $code);
        } else {
            $data = $request->except('password');
            $data['password'] = bcrypt($request->password);
            $data['api_token'] = sha1(time() . uniqid() . $data['email']);
            $data['status'] = 'active';
            $user = User::create($data);

            if ($user) {
                $user->assignRole('EMP');
                $data = $user;
				$data['user_id']=$user->id;
                //$messages = 'Registration Successful, A notificaton is been sent to our team, we will review it and activate it within 24 hour.';
				$messages = 'Registration Successful';
            } else {
                $status = false;
                $code = \Config::get('constants.responce_code.bad_request');
                $messages = 'Something went wrong ! Please try again later';
            }
        }
        return response()->json(['data' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function customerCreate(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Customer Added Successfully';
        $status = 'true';
        $error = '';
        $rules = array(
            'job_number' => 'required',
            'email' => 'required|email',
            'fname' => 'sometimes',
            'lname' => 'sometimes',
            'job_site_address' => 'sometimes',
            'job_site_city' => 'sometimes',
            'country' => 'sometimes',
            'job_site_state' => 'sometimes',
            'job_site_zipcode' => 'sometimes',
            'billing_address' => 'sometimes',
            'billing_city' => 'sometimes',
            'billing_state' => 'sometimes',
            'billing_zipcode' => 'sometimes',
            'phone' => 'sometimes',
            'notes' => 'sometimes',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 200;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {

            $jobdata = Job::where('job_number', $request->job_number)->first();
            if ($jobdata != null) {
                if($jobdata->client_id ==  null)
                {

                    $data = $request->all();
                    $data['name'] = $data['fname'].' '.$data['lname'];
                    $data['street1'] = (($data['job_site_address'] != null) ? $data['job_site_address'] : '');
                    $data['city'] = (($data['job_site_city'] != null) ? $data['job_site_city'] : '');
                    $data['province'] = (($data['job_site_state'] != null) ? $data['job_site_state'] : '');
                    $data['zipcode'] = (($data['job_site_zipcode'] != null) ? $data['job_site_zipcode'] : '');
                    $data['street2'] = (($data['billing_address'] != null) ? $data['billing_address'] : '');
                    $data['bill_city'] = (($data['billing_city'] != null) ? $data['billing_city'] : '');
                    $data['bill_state'] = (($data['billing_state'] != null) ? $data['billing_state'] : '');
                    $data['bill_zipcode'] = (($data['billing_zipcode'] != null) ? $data['billing_zipcode'] : '');
                    $data['country'] = 1;
                    $client = Client::create($data);
                    $client->created_by=$client->id;
                    $client->save();

                    $jobdata->client_id = $client->id;
                    $jobdata->save();

                    if ($client) {
                        $data = Client::find($client->id);
                        $data['job_site_address'] = (($data['street1'] != null) ? $data['street1'] : '');
                        $data['job_site_city'] = (($data['city'] != null) ? $data['city'] : '');
                        $data['job_site_state'] = (($data['province'] != null) ? $data['province'] : '');
                        $data['job_site_zipcode'] = (($data['zipcode'] != null) ? $data['zipcode'] : '');
                        $data['billing_address'] = (($data['street2'] != null) ? $data['street2'] : '');
                        $data['billing_city'] = (($data['bill_city'] != null) ? $data['bill_city'] : '');
                        $data['billing_state'] = (($data['bill_state'] != null) ? $data['bill_state'] : '');
                        $data['billing_zipcode'] = (($data['bill_zipcode'] != null) ? $data['bill_zipcode'] : '');
                        $messages = 'Customer Added Successfully';
                        $status = true;
                        $code = 200;
                    } else {
                        $status = false;
                        $code = \Config::get('constants.responce_code.bad_request');
                        $messages = 'Something went wrong ! Please try again later';
                    }
                }
                else {
                    $status = false;
                    $messages = 'Job already asssign another client Please enter another job number';
                    $code = 200;
                }
            } else {
                $status = false;
                $messages = 'No Job Found Please add new Job';
                $code = 200;
            }
        }
        return response()->json(['data' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function clientList(Request $request){
        $data['clients'] = [];
        $message = "";
        $status = true;
        $code = 200;

        $formData = Client::select('id as client_id','name')->where('status','active')->get()->toArray();

        if(count($formData)>0){
            $data['clients']= $formData;
        }
        else{
            $status = false;
            $message = 'No Clients Found';
            $code = 200;
        }
        return response()->json(['status'=>$status, 'data'=>(object)$data,'message'=>$message,'code'=> $code]);
    }


    public function listState(Request $request){
        $data['states'] = [];
        $message = "";
        $status = true;
        $code = 200;

        $states =State::select('id','name')->where('country_id','1')->get()->toArray();

        if(count($states)>0){
            $data['states']=$states;
        }
        else{
            $status = false;
            $message = 'No States Found';
            $code = 200;
        }
        return response()->json(['status'=>$status, 'data'=>(object)$data,'message'=>$message,'code'=> $code]);
    }

    public function changePassword(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $validation = [];
        $code = 200;

        $id = $request->user_id;
        $rules = array(
			'old_password'=>'required',
            'password' => 'required|min:6|max:255',
            'password_confirmation' => 'required|same:password',
        );


        $messsages = array(
            'old_password.required' => __('Please enter current password'),
            'password_confirmation.same' => __('The confirm password and new password must match.'),
        );

        $validator = \Validator::make($request->all(), $rules, $messsages);

        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = \Config::get('constants.responce_code.validation_failed');
        } else {

            $user = User::where("id", $id)->first();
		    if ($user && Hash::check($request->input('old_password'), $user->password)) {
				if ($user) {
					$user->password = Hash::make($request->input('password'));
					$user->save();
					$message = 'Password changed successfully.';
				} else {
					$validator->errors()->add('current_password', 'Please enter correct current password');
					$validation = $validator;
					$code = \Config::get('constants.responce_code.validation_failed');
					$status = false;
				}
			}
			else{
				$message = 'Current password is Wrong';
				$status = false;
				$code=400;
			}

        }

		return response()->json(['status'=> $status,'data' => $data, 'message' => $message, 'validation' => $validation,'code'=>$code]);
        exit;
    }

    public function update(Request $request)
    {

        $data = array();
        $code = 200;
        $messages = 'Profile Update';
        $status = true;
        $id = $request->user_id;
		$name = $request->name;
        $user = User::find($id);
        if($name==''){
            $status = false;
            $messages = 'Enter Name';
            $code = '404';
        }
		if($status){
			if($user){
				$user->name = $request->name;
				$user->save();
				$data = $user;
				$data['user_id'] = $user->id;
			}else{
				$status = false;
				$messages = 'Profile Can not update';
				$code = '404';
			}
		}
        return response()->json(['data' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function logout(Request $request)
    {
        $data = [];
        $message = "";
        $validation = [];
        $code = 200;
        $status = true;

        if ($request->has('api_token')) {
            $api_token = $request->get('api_token');
            User::where("api_token", $api_token)->update(["api_token" => ""]);
        }
        $message = "Logout success!";
        return response()->json(['code' => $code, 'message' => $message, 'status' => $status]);
        exit;
    }

    // public function contactUs(Request $request)
    // {
    //     $rules = array(
    //         'email' => 'required|email',
    //         'name' => 'required',
    //         'phone' => 'nullable|regex:/^(?=.*[0-9])[ +()0-9]+$/',
    //         'message' => 'required',
    //     );

    //     $validator = \Validator::make($request->all(), $rules, []);

    //     if ($validator->fails())
    //     {
    //         $validation = $validator;
    //         $status = false;
    //         $code = 400;
    //         $msgArr = $validator->messages()->toArray();
    //         $messages = reset($msgArr)[0];

    //         return response()->json([
    //             'message' =>$messages,
    //             'success' => false,
    //             'status' => $code],400);
    //     }


    //     $contactUs=new \App\ContactUs();
    //     $contactUs->name=$request->name;
    //     $contactUs->email=$request->email;
    //     $contactUs->phone=$request->phone;
    //     $contactUs->message=$request->message;
    //     $contactUs->save();

    //     Mail::send('emails.contact_us', ['data' => $contactUs], function ($m) use ($contactUs) {
    //         $m->to(env('ADMIN_EMAIL'), env('ADMIN_USER'))->subject('Contact Inquiry');
    //     });

    //     $result=make_null($contactUs);

    //     return response()->json([
    //         'result' => $result,
    //         'message' => 'Message send successfully',
    //         'success' => true,
    //         'status' => 200,
    //     ],200);

    // }

    // public function specialInquiry(Request $request)
    // {
    //     $rules = array(
    //         'email' => 'required|email',
    //         'name' => 'required',
    //         'phone' => 'nullable|regex:/^(?=.*[0-9])[ +()0-9]+$/',

    //     );

    //     $validator = \Validator::make($request->all(), $rules, []);

    //     if ($validator->fails())
    //     {
    //         $validation = $validator;
    //         $status = false;
    //         $code = 400;
    //         $msgArr = $validator->messages()->toArray();
    //         $messages = reset($msgArr)[0];

    //         return response()->json([
    //             'message' =>$messages,
    //             'success' => false,
    //             'status' => $code],400);
    //     }

    //     $input = $request->all();
    //     if (!isset($input['file']))
    //     {
    //         return response()->json([
    //             'message' =>'Atleast one file required',
    //             'success' => false,
    //             'status' => 400],400);
    //     }

    //     foreach ($input['file'] as $key=>$value) {
    //         $filename = uniqid(time()) . '.' . $value->getClientOriginalExtension();
    //         if (!is_dir('uploads/inquiry/')) {
    //             mkdir('uploads/inquiry/', 0777, true);
    //         }
    //         $value->move('uploads/inquiry/', $filename);

    //         $input['file_'.($key+1)] = 'uploads/inquiry/'.$filename;
    //     }

    //     $specialInquiry=new \App\SpecialInquiry();
    //     $specialInquiry = $specialInquiry->create($input);

    //     Mail::send('emails.special_inquiry', ['data' => $specialInquiry,'name'=>$request->name,'email'=>$request->email], function ($m) use ($specialInquiry) {
    //         $m->to(env('ADMIN_EMAIL'), env('ADMIN_USER'))->subject('Special Inquiry');
    //     });

    //     $result=make_null($specialInquiry);

    //     return response()->json([
    //         'result' => $result,
    //         'message' => 'Message send successfully',
    //         'success' => true,
    //         'status' => 200,
    //     ],200);

    // }

    public function contactUs(Request $request)
    {
        $rules = array(
            'email' => 'required|email',
            'name' => 'required',
            'phone' => 'nullable|regex:/^(?=.*[0-9])[ +()0-9]+$/',
            'message' => 'required',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'code' => 400,
                'status' => false],400);
        }


        $contactUs=new \App\ContactUs();
        $contactUs->name=$request->name;
        $contactUs->email=$request->email;
        $contactUs->phone=$request->phone;
        $contactUs->message=$request->message;
        $contactUs->save();

        Mail::send('emails.contact_us', ['data' => $contactUs], function ($m) use ($contactUs) {
            $m->to('mjmmechanical@frontier.com', 'MJM Team')->subject('Contact Inquiry');
        });

        $result=$contactUs;

        return response()->json([
            'result' => $contactUs,
            'message' => 'Message send successfully',
            'success' => true,
            'code' => 200,
            'status' => true,
        ],200);

    }

    public function specialInquiry(Request $request)
    {
        $rules = array(
            'email' => 'required|email',
            'name' => 'required',
            'phone' => 'nullable|regex:/^(?=.*[0-9])[ +()0-9]+$/'

        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails())
        {
            $validation = $validator;
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];

            return response()->json([
                'message' =>$messages,
                'success' => false,
                'code' => 400,
                'status' => false],400);
        }
        $input = $request->all();
        if (!isset($input['file']))
        {
            return response()->json([
                'message' =>'Atleast one file required',
                'success' => false,
                'code' => 400,
                'status' => false],400);
        }

        foreach ($input['file'] as $key=>$value) {
            $filename = uniqid(time()) . '.' . $value->getClientOriginalExtension();
            if (!is_dir('uploads/inquiry/')) {
                mkdir('uploads/inquiry/', 0777, true);
            }
            $value->move('uploads/inquiry/', $filename);

            $input['file_'.($key+1)] = 'uploads/inquiry/'.$filename;
        }

        $specialInquiry=new \App\SpecialInquiry();
        $specialInquiry = $specialInquiry->create($input);

        Mail::send('emails.special_inquiry', ['data' => $specialInquiry,'name'=>$request->name,'email'=>$request->email], function ($m) use ($specialInquiry) {
            $m->to('mjmmechanical@frontier.com', 'MJM Team')->subject('Special Inquiry');
        });

        $result=$specialInquiry;

        return response()->json([
            'result' => $result,
            'message' => 'Message send successfully',
            'success' => true,
            'code' => 200,
            'status' => true,
        ],200);

    }

}
