<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobImageFolder extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'folderimages';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['folder_id', 'job_id','image_id'];
}
