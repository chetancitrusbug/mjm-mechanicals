<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateJobInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('job_invoice_form', function (Blueprint $table) {
            $table->string('ent_lvg')->nullable();
            $table->string('disposal_qty')->nullable();
            $table->tinyInteger('filter_size')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('job_project_worksheet_form_1', function (Blueprint $table) {
            $table->dropColumn('ent_lvg');
            $table->dropColumn('disposal_qty');
            $table->dropColumn('filter_size');

        });
    }
}
