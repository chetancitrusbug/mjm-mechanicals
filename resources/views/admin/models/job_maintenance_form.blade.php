<div class="panel panel-default">
    <div class="panel-heading">Residential Maintenance Club</div>
    <div class="panel-body">
        <a href="#" title="Back to Forms list"><button type="button" class=" backToForm btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true" ></i>Back</button></a>
        {!! Form::hidden('job_id', $job->id, ['class' => 'form-control input-100']) !!}
        <!--<a href="http://13.127.235.254/dev/laravel/mjm-mechanicals/public/admin/job" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>Back</button></a>-->

        <div class="grid-box-1 clearfix">
            <div class="col-md-12 col-sm-12 pull-left clearfix">
                <div class="img-center pull-left clearfix">
                    <img src="{{asset('assets/images/logo-1.png')}}" class="img-responsive w-300">
                </div>
                <div class="img-center pull-right clearfix">
                    <img src="{{asset('assets/images/img-2.png')}}" class="img-responsive w-200">
                </div>
            </div>
        </div>
        <hr>
        <div class="grid-box-1 clearfix">
            <div class="col-md-12 col-sm-12 clearfix">
                <div class="row">
                    <div class="col-md-12 col-sm-12 clearfix">
                        <p class="height-auto">Proposal for</p>
                        {!! Form::textarea('proposal', null, ['class' => 'form-control h-auto textarea1','rows'=>'5',"placeholder"=>"Enter Proposal"]) !!}
                    </div>

                    <div class="col-md-12 col-sm-12 clearfix">
                        <h2 class="text-left h2-center">“OUR PROMISE TO YOU”</h2>
                        <ul class="ul-custom">
                            <li>Perform bi-annual maintenance on all HVAC systems</li>
                            <li>Provide 1” pleated filters for all HVAC systems</li>
                            <li>
                                Enrollment in our loyalty discount program
                                <ul>
                                    <li>10% discount on all parts</li>
                                    <li>10% discount on basic Heating &amp; Cooling System replacement*</li>
                                    <li>10% discount on Plumbing Repairs</li>
                                    <li>10% discount for Water Heater replacements</li>
                                    <li>Same Day Priority Service</li>
                                    <li> Monthly E-mail Updates on Specials</li>
                                    <li>Multi system maintenance discount</li>
                                    <li>No Breakdown Guarantee! (See store for details)</li>
                                </ul>
                            </li>
                            <li>All work will be quoted to property owner before any repairs are made</li>
                        </ul>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <p class="p-custom-001">We promise to perform the items listed above for the price of $15.00/mo. per HVAC system for a
                            yearly total value of $180.00. Single equipment maintenance priced at 50% discount. Boilers &amp;
                            Geothermal systems will be quoted. </p>
                    </div>

                    <div class="col-md-12 col-sm-12 clearfix">
                        <h2 class="text-left h2-center">“Quality is not just something we say, it’s in everything we do.”</h2>
                    </div>

                    <div class="box-border-1">
                        <div class="col-md-8 col-sm-8 pull-left">
                            <p>Bi-annual maintenance on all HVAC systems: </p>
                        </div>
                        <div class="col-md-4 col-sm-4 pull-left">
                            {!! Form::text('annual_maintenance', null, ['class' => 'form-control','placeholder'=>'$']) !!}
                        </div>
                        <div class="col-md-8 col-sm-8 pull-left">
                            <p><b>(-10% Discount when enrolled in auto pay with secured credit card)  </b></p>
                        </div>
                        <div class="col-md-4 col-sm-4 pull-left">
                            {!! Form::text('discount', null, ['class' => 'form-control','placeholder'=>'$']) !!}
                        </div>
                        <p class="p15">Please sign and date below for acceptance of our promise or email confirmation to <a href="mailto:mjmmechanical@frontier.com">mjmmechanical@frontier.com</a>. You can also call us at 260-483-0878.</p>
                    </div>

                    <div class="col-md-12 clearfix">
                        <div class="row">
                            <div class="col-md-8 col-sm-8 pull-left">
                                {!! Form::text('signature', null, ['class' => 'form-control','placeholder'=>'Signature']) !!}
                            </div>
                            <div class="col-md-4 col-sm-4 pull-left">
                                {!! Form::text('date', null, ['class' => 'form-control maintenance_date','placeholder'=>'Date']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 text-center clearfix">
                        <p><b>Thank you for the opportunity to EARN your trust!</b></p>
                        <p>Welcome to the MJM Mechanical VIP Loyalty Club!</p>
                    </div>
                    <hr>
                    <div class="col-md-12 text-center clearfix">
                        <p>*90%+ Efficient Gas Forced Air Furnace, 13 SEER Air Conditioning System with exact replacement.
                            <b>OR</b> 80% Efficient Gas Forced Air Furnace, 13 SEER Air Conditioning System with exact replacement.
                            Enrollment in auto pay requires 2 year commitment.</p>
                    </div>
                </div>
            </div>
        </div><!-- End of box-div -->
        <div class="grid-box-1 clearfix">
            <div class="container">
                <div class="col-md-12">

                </div>
            </div>
        </div> <!-- end of box -->
        <div class="grid-box-1 clearfix">
            <div class=" pull-left">
                {!! Form::submit((isset($jobMaintenanceForm) & $jobMaintenanceForm != null)? 'Update' : 'Add', ['class' => 'btn btn-primary'])
                !!}
            </div>
        </div>
    </div>
</div>

@push('script-head')
<script type="text/javascript">
    $(function () {
        $('.maintenance_date').datetimepicker({
            format: 'DD-MM-YYYY'
        });
    })

</script>

@endpush