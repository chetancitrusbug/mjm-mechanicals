@extends('layouts.backend')
@section('title',trans('product.products'))
@section('pageTitle',trans('product.products'))

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                    <div class="title">
                        <i class="icon-circle-blank"></i>
                        @lang('product.products') @lang('product.logs')
                    </div>
                </div>

                <div class="box-content ">
                    <div class="row">
                        <a href="{{ url('/admin/products') }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>@lang('product.back')
                            </button>
                        </a>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-borderless" id="product-table">
                            <thead>
                            <tr>
                                <th data-priority="3">@lang('product.user_name')</th>
                                <th data-priority="7">@lang('product.start_datetime')</th>
                                <th data-priority="7">@lang('product.end_datetime')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('script-head')
<script>
      var dataurl ="{{ url('/admin/products/logs/datatable') }}";
        datatable = $('#product-table').DataTable({
           // "order": [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
             ajax: {
                    url: dataurl+'/'+"{{$product->id}}",
                    type: "get", // method , by default get

                },
                columns: [
                    { data: 'user_name',name:'user_name',"searchable" : true},
                    { data: 'start_datetime',name:'start_datetime',"searchable" : true},
                    { data: 'end_datetime',name:'end_datetime',"searchable" : true}

                ]
        });


</script>
@endpush
