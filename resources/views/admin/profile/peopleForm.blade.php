<div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''}}">
    {!! Form::label('first_name', trans('people.label.first_name'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('first_name', $user->people->first_name, ['class' => 'form-control']) !!}
        {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('last_name') ? 'has-error' : ''}}">
    {!! Form::label('last_name', trans('people.label.last_name'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('last_name', $user->people->last_name, ['class' => 'form-control']) !!}
        {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    @if($user->people->photo)
        <div class="col-md-6 col-md-offset-4">
            <img src="{!! asset('uploads/'.$user->people->photo) !!}" alt="" width="150px">
        </div>
    @endif
</div>

<div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
    @if($user->people->photo)
        {!! Form::label('photo', trans('people.label.change_photo'), ['class' => 'col-md-4 control-label']) !!}
    @else
        {!! Form::label('photo', trans('people.label.photo'), ['class' => 'col-md-4 control-label']) !!}
    @endif
    <div class="col-md-6">
        {!! Form::file('photo',  ['class' => 'form-control']) !!}
        {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
    </div>
</div>
{{--<div class="form-group {{ $errors->has('quality') ? 'has-error' : ''}}">--}}
{{--{!! Form::label('quality', trans('people.label.quality'), ['class' => 'col-md-4 control-label']) !!}--}}
{{--<div class="col-md-6">--}}
{{--{!! Form::number('quality', null, ['class' => 'form-control']) !!}--}}
{{--{!! $errors->first('quality', '<p class="help-block">:message</p>') !!}--}}
{{--</div>--}}
{{--</div>--}}
<div class="form-group {{ $errors->has('phone_number_1') ? 'has-error' : ''}}">
    {!! Form::label('phone_number_1',  trans('people.label.phone_number_1'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('phone_number_1', $user->people->phone_number_1, ['class' => 'form-control']) !!}
        {!! $errors->first('phone_number_1', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('phone_number_2') ? 'has-error' : ''}}">
    {!! Form::label('phone_number_2',  trans('people.label.phone_number_2'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('phone_number_2', $user->people->phone_number_2, ['class' => 'form-control']) !!}
        {!! $errors->first('phone_number_2', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('phone_type_1') ? 'has-error' : ''}}">
    {!! Form::label('phone_type_1', trans('people.label.phone_type_1'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('phone_type_1',$phoneTypes, $user->people->phone_type_1, ['class' => 'form-control']) !!}
        {!! $errors->first('phone_type_1', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('phone_type_2') ? 'has-error' : ''}}">
    {!! Form::label('phone_type_2', trans('people.label.phone_type_2'), ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('phone_type_2',$phoneTypes, $user->people->phone_type_2, ['class' => 'form-control']) !!}
        {!! $errors->first('phone_type_2', '<p class="help-block">:message</p>') !!}
    </div>
</div>
{{--<div class="form-group {{ $errors->has('emergency_password') ? 'has-error' : ''}}">--}}
{{--{!! Form::label('emergency_password', trans('people.label.emergancy_password'), ['class' => 'col-md-4 control-label']) !!}--}}
{{--<div class="col-md-6">--}}
{{--{!! Form::password('emergency_password', ['class' => 'form-control']) !!}--}}
{{--{!! $errors->first('emergency_password', '<p class="help-block">:message</p>') !!}--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="form-group {{ $errors->has('availability') ? 'has-error' : ''}}">--}}
{{--{!! Form::label('availability',  trans('people.label.availability'), ['class' => 'col-md-4 control-label']) !!}--}}
{{--<div class="col-md-6">--}}
{{--{!! Form::number('availability', null, ['class' => 'form-control']) !!}--}}
{{--{!! $errors->first('availability', '<p class="help-block">:message</p>') !!}--}}
{{--</div>--}}
{{--</div>--}}
{{--<div class="form-group {{ $errors->has('hold_key') ? 'has-error' : ''}}">--}}
{{--{!! Form::label('hold_key', trans('people.label.hold_key'), ['class' => 'col-md-4 control-label']) !!}--}}
{{--<div class="col-md-6">--}}
{{--{!! Form::number('hold_key', null, ['class' => 'form-control']) !!}--}}
{{--{!! $errors->first('hold_key', '<p class="help-block">:message</p>') !!}--}}
{{--</div>--}}
{{--</div>--}}