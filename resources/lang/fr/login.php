<?php

return [


    'label'=>[
        'e_mail' => 'E-mail',
        'password' => 'Mot de passe oublié',
        'remember' => 'Se souvenir de',
        'submit' => 'Soumettre',
        'sign_up' => 'Inscrivez-vous',
        'register' => "S'inscrire",
        'log_in' => 'Log In'
        'remember_me' => 'Se souvenir de moi',
        'sign_in' => 'Sign In',
        'forgot_your_password' => 'Vous avez oublié votre mot de passe',

    ]

];