<!-- Add Cash Add Modal -->
<div class="modal fade" id="folderimagesAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="images">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title images-title" id="account_form_model_lable">Add images</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="bidder_list">
                    <div class=" bidder no-padding-left no-padding-right gutter-bottom">

                        <div class=" clearfix details-container details-port-container">
                            <form method="post" id="folder_images_add_form" name="form">
                                  <input  name="job_id"  id="job_id" type="hidden" value="{{$job->id}}" >
                                  <input  name="imagefolderid"  id="imagefolderid" type="hidden" value="" >
                                  <input name="image_folder_id" id="image_folder_id" type="hidden" value="">
                                {{csrf_field()}}
                                <div class="form-group prepend-top">
                                    <div class="row">

                                        <div class="col-md-12">
                                            <label class="pull-left required" for="Projects_title">Images</label>
                                             <select id="jobImage_ids" class="form-control" name="jobImage_ids[]" multiple>
                                            </select>
                                            <div class="image_error"></div>

                                        </div>

                                    </div>
                                </div>

                                <div class="form-group prepend-top">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <button class="btn btn-read btn-inverted account_form_submit_button" type="submit" name="submit" value="Submit">
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Add Cash Available Modal Close-->
@push('js')
<script>
 function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


</script>
@endpush