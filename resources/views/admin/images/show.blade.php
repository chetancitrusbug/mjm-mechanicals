@extends('layouts.backend')

@section('title',trans('image.view_image'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('image.image')</div>
                <div class="panel-body">

                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('image.back')
                        </button>
                    </a>
                    @if(Auth::user()->can('access.user.edit'))
                    <a href="{{ url('/admin/images/' . $image->id . '/edit') }}" title="Edit image">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            @lang('image.edit_image')
                        </button>
                    </a>
                    @endif

                    <br/>
                    <br/>


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>

                            <tr>
                                <td>@lang('image.id')</td>
                                <td>{{ $image->id }}</td>
                            </tr>


                            <tr>
                                <td>@lang('image.title')</td>
                                <td>{{ $image->title }}</td>
                            </tr>

                            <tr>
                                <td>@lang('image.description')</td>
                                <td>{{ $image->description }}</td>
                            </tr>

                            <tr>
                                <td>image</td>
                                <td><a href={{ $image->image_full_url }} target="_blank"><img src={{ $image->image_full_url }} class="product_thumb"></a></td>
                            </tr>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection