<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobProjectWorksheetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_project_worksheet_form_1', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_id');
            $table->integer('user_id');
            $table->string('customer_name')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->tinyInteger('fan_switch_on_furnace')->nullable();
            $table->tinyInteger('fan_switch_on_tstat')->nullable();
            $table->tinyInteger('fan_center')->nullable();
            $table->tinyInteger('condensate_pump')->nullable();
            $table->tinyInteger('concrete_pad')->nullable();
            $table->tinyInteger('linesetFt')->nullable();
            $table->string('linesetFt_value')->nullable();
            $table->tinyInteger('insulate_ducts')->nullable();
            $table->tinyInteger('chimney_fliner')->nullable();
            $table->tinyInteger('transition_fittings')->nullable();
            $table->string('brand')->nullable();
            $table->string('fuel')->nullable();
            $table->string('model')->nullable();
            $table->string('BTU')->nullable();
            $table->string('location')->nullable();
            $table->string('stat_type')->nullable();
            $table->string('of_stat_wires')->nullable();
            $table->string('blower_type_direct')->nullable();
            $table->string('blower_Type_belt')->nullable();
            $table->string('motor_size')->nullable();
            $table->string('motor_speed')->nullable();
            $table->string('motor_volt')->nullable();
            $table->string('motor_brand')->nullable();
            $table->string('existing_vent_size')->nullable();
            $table->string('existing_vent_brand')->nullable();
            $table->string('location_of_panel')->nullable();
            $table->string('brand_of_panel')->nullable();
            $table->string('spare_circuits')->nullable();
            $table->string('electrician')->nullable();
            $table->tinyInteger('new_tstat_wire')->nullable();
            $table->string('furnace')->nullable();
            $table->string('AC_unit')->nullable();
            $table->string('light_in_crawlspace')->nullable();
            $table->string('status')->default('active')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_project_worksheet_form_1');
    }
}
