@extends('layouts.backend')

@section('title',trans('image.images'))
@section('pageTitle',trans('image.images'))

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="box bordered-box blue-border">
                <div class="box-header blue-background">
                                                  <div class="title">
                                                      <i class="icon-circle-blank"></i>
                                                     @lang('image.images')
                                                  </div>

                               </div>
                <div class="box-content ">


                    <div class="row">
                        <div class="col-md-6">
                                <a href="{{ url('/admin/images/create') }}" class="btn btn-success btn-sm"
                                   title="Add New image">
                                    <i class="fa fa-plus" aria-hidden="true"></i> @lang('image.add_new_image')
                                </a>

                        </div>

                        <div class="col-md-6">
                            {!! Form::open(['method' => 'GET', 'url' => '/admin/images', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}

                            {!! Form::close() !!}
                            </div>
                        </div>



                    <div class="table-responsive">
                        <table class="table table-borderless" id="images-table">
                            <thead>
                            <tr>
                                {{-- <th data-priority="1">@lang('image.id')</th> --}}
                                <th data-priority="3">@lang('image.title')</th>
                                <th data-priority="5">@lang('image.image_url')</th>
                                <th data-priority="7">@lang('image.status')</th>
                                <th data-priority="8">@lang('image.actions')</th>
                            </tr>
                            </thead>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('script-head')
<script>
var url ="{{ url('/admin/images/') }}";
var img_path ="{{ url('') }}";

        datatable = $('#images-table').DataTable({
            //order: [[ 0, "desc" ]],
            processing: true,
            serverSide: true,
             ajax: {
                    url: '{!! route('JobImagesControllerImagesData') !!}',
                    type: "get", // method , by default get

                },
                columns: [
                    // { data: 'id', name: 'Id',"searchable": false },
                    { data: 'title',name:'title',"searchable" : true},
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {

							var img= o.image_full_url;
							if(img){
                                  return '<a href="'+o.image_full_url+'" target="_blank"><img src="'+o.image_full_url+'" class="product_thumb"></a>';
                            }else{
								return 'No image';
							}
                        }
			        } ,
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                            if(o.status=='blocked')
                            status = "<a href='"+url+"/"+o.id+"?status=blocked' data-id="+o.id+" title='blocked'><button class='btn btn-default btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>@lang('image.blocked')</button></a>";
                            else if(o.status == 'inactive')

                            status = '<a href="'+url+'/'+o.id+'?status=inactive" title="inactive"><button class="btn btn-success btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>@lang("image.inactive")</button></a>';
                            else
                            status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> @lang('image.active')</button></a>";

                            return status;

                        }

                    },
					{
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";

                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>edit</button></a>&nbsp;";

                                d = "<a href='javascript:void(0);' class='btn btn-primary btn-xs'  ><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button></a>&nbsp;";

                            var v =  "<a href='"+url+"/"+o.id+"' data-id="+o.id+"><button class='btn btn-info btn-xs'><i class='fa fa-eye' aria-hidden='true'></i> @lang('image.view')</button></a>&nbsp;";


                            return v+e+d;
                        }
                    }

                ]
        });


    $(document).on('click', '.del-item', function (e) {
        var id = $(this).attr('data-id');

		var url ="{{ url('/admin/images/') }}";
        url = url + "/" + id;
		//alert(url);
        var r = confirm("Are you sure you want to delete image ?");
        if (r == true) {
            $.ajax({
                type: "delete",
                url: url ,
                headers: {
                    "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                },
                success: function (data) {
                    datatable.draw();
                    toastr.success('Action Success!', data.message)
                },
                error: function (xhr, status, error) {
                    var erro = ajaxError(xhr, status, error);
                    toastr.error('Action Not Procede!',erro)
                }
            });
        }
    });

</script>
@endpush
