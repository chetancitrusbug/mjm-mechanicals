<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\JobImage;
use Yajra\Datatables\Datatables;
use DB;
use Session;
use App\JobImageFolder;

class JobImagesController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {

        return view('admin.images.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {

        return view('admin.images.create');
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {

        $rules = [
            'image_url' => 'required|mimes:jpg,jpeg,png|max:100000',
        ];

        $customMessages = [
            'image_url.required' => 'Please Upload image',
        ];

        $this->validate($request, $rules, $customMessages);
        $data = $request->all();

        if ($request->file('image_url')) {

            $image_url = $request->file('image_url');
            $filename = uniqid(time()) . '.' . $image_url->getClientOriginalExtension();

            $image_url->move(public_path('jobimages'), $filename);

            $data['image_url'] = 'jobimages/' . $filename;


        }
        $data['title'] = (($data['title'] != null) ? $data['title'] : '');
        $data['description'] = (($data['description'] != null) ? $data['description'] : '');
        $image = JobImage::create($data);

        Session::flash('flash_message', 'Image added successfully!');

        return redirect('admin/images');
    }

    public function datatable(request $request)
    {
        $image = JobImage::orderBy('id','desc')->select('*');

         if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(title LIKE  '%$value%')";

               $image= JobImage::whereRaw($where_filter);
            }
        }
        $image= $image->get();

        //dd($image[0]->image_full_url);
        return Datatables::of($image)
            ->make(true);
        exit;
    }

     /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {

        $image = JobImage::find($id);

        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $image->status= 'inactive';
                $image->update();

                return redirect()->back();
            }else{
                $image->status= 'active';
                $image->update();
                return redirect()->back();
            }

        }
		if($image){
            return view('admin.images.show', compact('image'));
        }
        else{
             return redirect('/admin/images');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit(Request $request,$id)
    {
        $request->id=$id;
		$image = JobImage::where('id',$id)->first();

        return view('admin.images.edit', compact('image'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request,[
            'image_url' => 'sometimes|mimes:jpg,jpeg,png|max:100000',
        ]);
        $requestData = $request->all();

        $image = JobImage::findOrFail($id);

        if ($request->file('image_url')) {

            $image_url = $request->file('image_url');
            $filename = uniqid(time()) . '.' . $image_url->getClientOriginalExtension();

            $image_url->move(public_path('jobimages'), $filename);

            $requestData['image_url'] = 'jobimages/' . $filename;


        }
        $requestData['title'] = (($requestData['title'] != null) ? $requestData['title'] : '');
        $requestData['description'] = (($requestData['description'] != null) ? $requestData['description'] : '');

	    $image->update($requestData);

        flash('Image Updated Successfully!');

        return redirect('admin/images');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {

        $image = JobImage::find($id);
        $folderimage = JobImageFolder::where('image_id',$id)->get();
        foreach($folderimage as $delimage){
            $delimage->delete();
        }
        $image->delete();

        $message='Deleted';
        return response()->json(['message'=>$message],200);
    }

}
