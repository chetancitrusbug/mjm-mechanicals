<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobSystemRepairForm extends Model
{
    //
    use SoftDeletes;

    public $table='job_system_repair_replace_form';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','job_id','user_id','age_equipment_cooling_and_heat','age_equipment_furnace_and_boilers','age_equipment_packaged_units','estimate_cost_cooling_and_heat','estimate_cost_furnace_and_boilers','estimate_cost_packaged_units','repairs_under_warranty_cooling_and_heat','repairs_under_warranty_furnace_and_boilers','repairs_under_warranty_packaged_units','seer_of_equipment_cooling_and_heat','seer_of_equipment_packaged_units','type_of_refrigerant_cooling_and_heat','type_of_refrigerant_packaged_units','afue_furnace_and_boilers','afue_packaged_units','outdoor_equipment_cooling_and_heat','outdoor_equipment_packaged_units','indoor_equipment_cooling_and_heat','indoor_equipment_furnace_and_boilers','owner_expects_cooling_and_heat','owner_expects_furnace_and_boilers','owner_expects_packaged_units','airflow','noise','refrigerants','cycling','comfort','remodeled','building_codes','venting','fuel_changes','safety_issues','repair_history','date','customer','technician','status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function getDateAttribute($value)
    {
        if($value != ""){
            return \Carbon\Carbon::parse($value)->format('d-m-Y');
        }
        return $value;
    }

    public function setDateAttribute($value)
    {
        if($value != ""){
            $this->attributes['date'] = \Carbon\Carbon::parse($value)->format('Y-m-d');
        }
    }
}
