<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Job;
use App\Folder;
use App\Video;
use App\Foldervideo;
use App\Jobtask;
use App\Folderdocument;
use App\Client;
use App\Document;
use App\Jobassignemployee;
use Session;
use App\User;
use App\Jobcard;
use App\Builder;
use App\AssignjobBuilder;
use App\Foldernotes;
use App\JobGeothermalForm;
use App\JobFurnaceForm;
use App\JobInspectionForm;
use App\JobChecklistForm;
use App\JobMaintenanceForm;
use App\JobSystemRepairForm;
use App\JobPlumbingForm;
use App\JobInvoice;
use App\JobInvoiceOtherField;
use App\JobWeeklyVehicleSafetyChecklistForm;
use App\JobProjrctWorksheet1;
use App\JobProjrctWorksheet2;
use App\JobProjrctWorksheet3;
use App\JobProjrctWorksheet4;
use App\Jobcode;
use App\Forms;
use App\JobForms;
use DB;
use App\JobImage;
use App\JobImageFolder;
use App\Taskfolder;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index(Request $request)
    {

        return view('admin.job.index');
    }

    public function archiveJob(Request $request)
    {
        return view('admin.archiveJob.index');
    }

    public function archiveJobDatatable(request $request)
    {
		$job = Job::select('*')->orderBy('id','desc')->where('job.job_status', 'markasdone');
        if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(job.title LIKE '%$value%' OR job.job_number LIKE  '%$value%' OR job.duedate LIKE  '%$value%' )";
                $job = $job->whereRaw($where_filter);
            }
        }
        if ($request->has('filter_status') && $request->get('filter_status') != '' && $request->get('filter_status') != 'all') {
            $job = $job->where('job.status', $request->get('filter_status'));
        }
		// if ($request->has('filter_job_status') && $request->get('filter_job_status') != '' && $request->get('filter_job_status') != 'all') {
        //     $job = $job->where('job.job_status', $request->get('filter_job_status'));
        // }
        return Datatables::of($job)
            ->make(true);
        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
        $client = Client::pluck('name','id')->prepend('Select Client','');
        $builder = Builder::pluck('name','id')->prepend('Select Builder','');
        $user = User::select('users.*',  'roles.name as role_name', 'roles.id as role_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->whereIn('roles.name', ['EMP'])
            ->where('users.status','=','active')->get();

        $jobs = Job::select('job_number')->where('status', 'active')->pluck('job_number')->toArray();

        $jobcode = Jobcode::select('job_code.*')->whereNotIn('job_code.id',$jobs)->where('job_code.status', 'active')->pluck('job_code','id')->prepend('Select Job Code', '');


		return view('admin.job.create',compact('client','user','builder','jobcode'));
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $rules = [
            'job_number'=>'required',
            // 'title' => 'required',
            // 'description' => 'required',
            // 'client_id'=>'required',
            // 'job_employee_id'  => 'required|array|min:1',
            'duedate' => 'required'
            // 'notes' => 'required',
            //'job_builder_id'  => 'required',
        ];

        $customMessages = [
            'job_number.required' => 'The job code field is required.'
        ];

        $this->validate($request, $rules, $customMessages);

        $jobdata = Job::where('job_number', $request->job_number)->first();
        if ($jobdata == null) {

            $data = $request->all();

            //dd($request->all());
            $data['job_number'] = isset($request->job_number) ? $request->job_number : '';
            $data['title'] = isset($request->title) ? $request->title : '';
            $data['description'] = isset($request->description) ? $request->description : '';
            $data['client_id'] = isset($request->client_id) ? $request->client_id : '';
            $data['duedate'] = isset($request->duedate) ? $request->duedate : '';
            $data['notes'] = isset($request->notes) ? $request->notes : '';
            // if(!empty($request->input('job_employee_id'))){
                //if(!empty($request->input('job_builder_id'))){
                    $job = Job::create($data);
                    $job->created_by=$job->id;
                    $job->status='active';
                    $job->job_status='pending';
                    $job->save();
                    //   $assignjobbuilder = new AssignjobBuilder();
                    //         $assignjobbuilder->job_id = $job->id;
                    //         $assignjobbuilder->job_builder_id = $request->input('job_builder_id');
                    //         $assignjobbuilder->save();
                    if(!empty($request->input('job_employee_id'))){
                        foreach($request->input('job_employee_id') as $job_employee_id){
                                $jobassignemp = new Jobassignemployee();
                                $jobassignemp->job_id = $job->id;
                                $jobassignemp->job_employee_id = $job_employee_id;
                                $jobassignemp->save();
                        }
                    }

                    if($request->heating_documents)
                    {
                        $documents = $request->heating_documents;
                        foreach ($documents as $key => $value) {
                            $document = $value;
                            $this->documentAdd($document,'2','Heating',$job->id);
                        }
                    }
                    if ($request->heating_videos) {
                        $videos = $request->heating_videos;
                        foreach ($videos as $key => $value) {
                            $video = $value;
                            $this->videoAdd($video,'2','Heating',$job->id);
                        }
                    }

                    if($request->cooling_documents)
                    {
                        $documents = $request->cooling_documents;
                        foreach ($documents as $key => $value) {
                            $document = $value;
                            $this->documentAdd($document,'3','Cooling',$job->id);
                        }
                    }
                    if ($request->cooling_videos) {
                        $videos = $request->cooling_videos;
                        foreach ($videos as $key => $value) {
                            $video = $value;
                            $this->videoAdd($video,'3','Cooling',$job->id);
                        }
                    }


                    if($request->plumbing_documents)
                    {
                        $documents = $request->plumbing_documents;
                        foreach ($documents as $key => $value) {
                            $document = $value;
                            $this->documentAdd($document,'4','Plumbing',$job->id);
                        }
                    }
                    if ($request->plumbing_videos) {
                        $videos = $request->plumbing_videos;
                        foreach ($videos as $key => $value) {
                            $video = $value;
                            $this->videoAdd($video,'4','Plumbing',$job->id);
                        }
                    }


                //}

            // }

            Session::flash('flash_message', 'Job added!');
            return redirect('admin/job/'.$job->id.'/edit');
        }
        else {
            Session::flash('flash_error', 'Job number already exist!');
        }
        return redirect('admin/job/');

    }

    public function videoAdd($video,$number,$folder_name,$job_id)
    {
        $filename = uniqid(time()) . '.' . $video->getClientOriginalExtension();

        $video->move(public_path('Video'), $filename);
        $data['title'] = $video->getClientOriginalName();
        $data['video_url'] = $filename;
        $data['user_type'] = 'admin';
        $video = Video::create($data);

        $user_id = \Auth::user()->id;

        $folder=Folder::where('job_id',$job_id)->where('folder_name',$folder_name)->first();

        if($folder == null){
            $folder = new Folder();
            $folder->job_id = $job_id;
            $folder->folder_name = $folder_name;
            $folder->sortno = $number;
            $folder->employee_id = $user_id;
            $folder->user_type = 'admin';
            $folder->folder_img = '';
            $folder->save();
            $folder->created_by = $folder->id;
            $folder->save();
        }


        $foldervideo = new Foldervideo();
        $foldervideo->folder_id = $folder->id;
        $foldervideo->job_id = $job_id;
        $foldervideo->user_type = 'admin';
        $foldervideo->employee_id = $user_id;
        $foldervideo->video_id = $video->id;
        $foldervideo->save();
        return $foldervideo;
    }
    public function documentAdd($document,$number,$folder_name,$job_id)
    {
        $filename = uniqid(time()) . '.' . $document->getClientOriginalExtension();

        $document->move(public_path('Document'), $filename);
        $data['title'] = $document->getClientOriginalName();
        $data['document_url'] = $filename;
        $data['user_type'] = 'admin';
        $document = Document::create($data);
        $user_id = \Auth::user()->id;

        $folder=Folder::where('job_id',$job_id)->where('folder_name',$folder_name)->first();

        if($folder == null){
            $folder = new Folder();
            $folder->job_id = $job_id;
            $folder->folder_name = $folder_name;
            $folder->sortno=$number;
            $folder->employee_id=$user_id;
            $folder->user_type='admin';
            $folder->folder_img='';
            $folder->save();
            $folder->created_by=$folder->id;
            $folder->save();
        }

        $folderdocument = new Folderdocument();
        $folderdocument->folder_id = $folder->id;
        $folderdocument->job_id = $job_id;
        $folderdocument->user_type = 'admin';
        $folderdocument->employee_id = $user_id;
        $folderdocument->document_id = $document->id;
        $folderdocument->save();

        return $folderdocument;
    }
    public function datatable(request $request)
    {
		$job = Job::select('*')->orderBy('id','desc');
        if($request->has('search') && $request->get('search') != '' ){
            $search = $request->get('search');
            if($search['value'] != ''){
                $value = $search['value'];
                $where_filter = "(job.title LIKE '%$value%' OR job.description LIKE  '%$value%'  )";
                $job = $job->whereRaw($where_filter);
            }
        }
        if ($request->has('filter_status') && $request->get('filter_status') != '' && $request->get('filter_status') != 'all') {
            $job = $job->where('job.status', $request->get('filter_status'));
        }
		if ($request->has('filter_job_status') && $request->get('filter_job_status') != '' && $request->get('filter_job_status') != 'all') {
            $job = $job->where('job.job_status', $request->get('filter_job_status'));
        }
        return Datatables::of($job)
            ->make(true);
        exit;
    }

     /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {
        $job = Job::findOrFail($id);
        //change job status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $job->status= 'inactive';
                $job->update();

                return redirect()->back();
            }else{
                $job->status= 'active';
                $job->update();
                return redirect()->back();
            }

        }

        return view('admin.job.show', compact('job','client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function edit(Request $request,$id)
    {
        $request->id=$id;
		$job = Job::where('id',$id)->first();
        $client = Client::pluck('name','id');
        $folder = Folder::with('documentList','videolist')->where('job_id',$id)->orderby('sortno','asc')->get();
        $taskfolder = Taskfolder::with('taskList')->where('job_id',$id)->orderby('sortno','asc')->get();
        $notes=Foldernotes::with('noteslist')->where('job_id',$id)->orderby('id','desc')->get();
        $jobImageFolder = JobImageFolder::select('jobimages.*','folderimages.*','folderimages.id as folderimages_id')->join('jobimages','folderimages.image_id','jobimages.id')->where('folderimages.job_id',$id)->where('jobimages.status', 'active')->orderby('folderimages.id','desc')->get();
        // $jobGeothermalForm = JobGeothermalForm::where('job_id', $id)->orderby('status', 'active')->first();
        // $jobFurnaceForm = JobFurnaceForm::where('job_id', $id)->orderby('status', 'active')->first();
        // $jobInspectionForm = JobInspectionForm::where('job_id', $id)->orderby('status', 'active')->first();
        // $jobChecklistForm = JobChecklistForm::where('job_id', $id)->orderby('status', 'active')->first();
        // $jobMaintenanceForm = JobMaintenanceForm::where('job_id', $id)->orderby('status', 'active')->first();
        // $jobSystemRepairForm = JobSystemRepairForm::where('job_id', $id)->orderby('status', 'active')->first();
        // $jobPlumbingForm = JobPlumbingForm::where('job_id', $id)->orderby('status', 'active')->first();
        // $jobWeeklyVehicleSafetyChecklistForm = JobWeeklyVehicleSafetyChecklistForm::where('job_id', $id)->orderby('status', 'active')->first();
        // if($jobWeeklyVehicleSafetyChecklistForm != null && $jobWeeklyVehicleSafetyChecklistForm['PSI'] != null)
        // {
        //     $jobWeeklyVehicleSafetyChecklistForm['PSI'] = explode(",",$jobWeeklyVehicleSafetyChecklistForm['PSI']);
        // }

        // $jobInvoiceForm = JobInvoice::select('job_invoice_form.*', 'job_invoice_other_fields_form.*', 'job.title as job_name', 'job_invoice_form.id as job_main_invoice_id')->join('job', 'job_invoice_form.job_id', 'job.id')->leftJoin('job_invoice_other_fields_form', 'job_invoice_other_fields_form.job_invoice_id', 'job_invoice_form.id')->where('job_invoice_form.job_id', $id)->first();


        // $jobProjectWorksheetForm = JobProjrctWorksheet1::select('job_project_worksheet_form_1.*', 'job_project_worksheet_form_2.*', 'job_project_worksheet_form_3.*', 'job_project_worksheet_form_4.*', 'job.title as job_name', 'job_project_worksheet_form_1.id as job_project_worksheet_id')->join('job', 'job_project_worksheet_form_1.job_id', 'job.id')->leftJoin('job_project_worksheet_form_2', 'job_project_worksheet_form_2.job_project_worksheet_form_1_id', 'job_project_worksheet_form_1.id')->leftJoin('job_project_worksheet_form_3', 'job_project_worksheet_form_3.job_project_worksheet_form_1_id', 'job_project_worksheet_form_1.id')->leftJoin('job_project_worksheet_form_4', 'job_project_worksheet_form_4.job_project_worksheet_form_1_id', 'job_project_worksheet_form_1.id')->where('job_project_worksheet_form_1.job_id', $id)->first();

        $jobGeothermalForm = null;
        $jobFurnaceForm = null;
        $jobInspectionForm = null;
        $jobChecklistForm = null;
        $jobMaintenanceForm = null;
        $jobSystemRepairForm = null;
        $jobPlumbingForm = null;
        $jobWeeklyVehicleSafetyChecklistForm = null;
        $jobInvoiceForm = null;
        $jobProjectWorksheetForm = null;

        $jobtask = Jobtask::where('job_id',$id)->orderby('id','desc')->get();
        $user = User::select('users.*',  'roles.name as role_name', 'roles.id as role_id')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'roles.id', '=', 'role_user.role_id')
            ->whereIn('roles.name', ['EMP'])
            ->where('users.status','=','active')->get();
        $jobassignemp = Jobassignemployee::where('job_id',$id)->get();
        $jobcarddetail = Jobcard::with('jobcarduserName')->where('job_id',$id)->select('*')->orderBy('id','desc')->get();
        $builder = Builder::pluck('name','id');
        $assignjobbuilder = AssignjobBuilder::where('job_id',$id)->first();
        $job_builder_id=(isset($assignjobbuilder->job_builder_id) ? $assignjobbuilder->job_builder_id : '');



        $jobcode = Jobcode::select('job_code.*')->where('job_code.id',$job->job_number)->where('job_code.status', 'active')->pluck('job_code','id')->prepend('Select Job Code', '');

        $jobImage = JobImage::select('jobimages.*', 'jobimages.id as job_image_id')->join('folderimages', 'folderimages.image_id', 'jobimages.id')->where('folderimages.job_id', $request->id)->where('jobimages.status', 'active')->get();

        $jobForms = JobForms::select('forms.*', 'jobsforms.*', 'forms.title as form_title', 'jobsforms.created_at as form_created_at')->join('forms', 'forms.id', 'jobsforms.form_id')->where('jobsforms.job_id', $request->id)->orderby('jobsforms.id', 'DESC')->get();

        $forms = Forms::all();
        //dd($forms);

        return view('admin.job.edit', compact('job','client','folder','taskfolder','notes','jobtask','user','jobassignemp','jobcarddetail','job_builder_id','builder','jobGeothermalForm','jobFurnaceForm','jobInspectionForm','jobChecklistForm','jobMaintenanceForm','jobSystemRepairForm','jobPlumbingForm','jobWeeklyVehicleSafetyChecklistForm','jobInvoiceForm','jobProjectWorksheetForm','jobcode','jobImageFolder','jobImage','jobForms','forms'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $rules = [
            // 'job_number'=>'required',
            // 'title' => 'required',
            // 'description' => 'required',
            // 'client_id'=>'required',
            // 'job_employee_id'  => 'required|array|min:1',
            'duedate' => 'required',
            // 'notes' => 'required',
            //'job_builder_id'  => 'required',
        ];

        $customMessages = [
            'job_number.required' => 'The job code field is required.'
        ];

        $this->validate($request, $rules, $customMessages);
        $requestData = $request->all();

        $requestData['job_number'] = isset($request->job_number) ? $request->job_number : '';
        $requestData['title'] = isset($request->title) ? $request->title : '';
        $requestData['description'] = isset($request->description) ? $request->description : '';
        $requestData['client_id'] = isset($request->client_id) ? $request->client_id : '';
        $requestData['duedate'] = isset($request->duedate) ? $request->duedate : '';
        $requestData['notes'] = isset($request->notes) ? $request->notes : '';
        if ((isset($request->completedjob) && $request->completedjob == 'on')) {
            $requestData['job_status']= 'markasdone';
        }

        $job = Job::findOrFail($id);
	    $job->update($requestData);
        $jobassignemployee = Jobassignemployee::where('job_id',$id);
        $jobassignemployee->delete();

        if(!empty($request->input('job_employee_id'))){
            foreach($request->input('job_employee_id') as $job_employee_id){

                $jobassignemp = new Jobassignemployee();
                $jobassignemp->job_id = $job->id;
                $jobassignemp->job_employee_id = $job_employee_id;
                $jobassignemp->save();
            }
        }
        // $assignjobbuilder = AssignjobBuilder::where('job_id',$id);
        // $assignjobbuilder->delete();
        //                 $assignjobbuilder = new AssignjobBuilder();
        //                 $assignjobbuilder->job_id = $job->id;
        //                 $assignjobbuilder->job_builder_id = $request->input('job_builder_id');
        //                 $assignjobbuilder->save();
        Session::flash('flash_message','Job Updated Successfully!');
        // if($job->job_status == 'markasdone')
        // {
        //     return redirect('admin/archiveJob');
        // }
        return redirect('admin/job');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    public function destroy($id)
    {

        $job = Job::find($id);
        $job->delete();

          $message='Deleted';
        return response()->json(['message'=>$message],200);
    }

    public function jobcardlist(Request $request,$id)
    {
        $jobcarddetail = Jobcard::with('jobcarddetailList','jobcarduserName')->where('id',$id)->get();
        return view('admin.job.jobcard',compact('jobcarddetail'));
    }




    public function destroyimage($id)
    {
        $JobImageFolder = JobImageFolder::where('id',$id)->first();
        $JobImageFolder->delete();

         $message='Deleted';
        return response()->json(['message'=>$message],200);
    }


    public function folderimageadd(Request $request)
    {
       $jobImage = JobImage::select('jobimages.*')->where('status', 'active')->get();

       if(!empty($request->input('jobImage_ids'))){
            foreach($request->input('jobImage_ids') as $jobImage_id){
                $user_id=\Auth::user()->id;
		        $image = new JobImageFolder();
                $image->folder_id = $request->input('imagefolderid');
                $image->job_id = $request->input('job_id');
                $image->image_id = $jobImage_id;
                $image->save();
            }
        }
       return json_encode(array('msg'=>'Success','jobImage'=>$request->input('jobImage_ids')));

       exit;
    }

    public function folderimagelist(Request $request)
    {
        $jobImage = JobImage::select('jobimages.*')->where('jobimages.status', 'active')->get();
        return json_encode(array('msg'=>'Success','jobImage'=>$jobImage));

       exit;
    }


}
