<?php

return [

	'add_builder' => 'Add Builder',
    'add_new_builder' => 'Add New Builder',
    'back' => 'Back',
    'edit_builder' => 'Edit Builder',
    'update' => 'Update',
    'name' => 'Name',
	'builders' => 'Builders',
    'id' => 'Id',
	'status' => 'Status',
	'actions' => 'Actions',
	'active' => 'Active',
    'inactive' => 'Inactive',
    'blocked' => 'Blocked',
	'view' => 'View',
	'create' => 'Create',
	'edit' => 'Edit',
	'delete' => 'Delete',
	'view_builder'=>'View Builder',
	'builder'=>'Builder',
    'email'=>'Email',
    'address'=>'Address',
    'phone'=>'Contact No',
];