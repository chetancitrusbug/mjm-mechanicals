<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobInvoiceTable extends Migration
{
    /**
 * Run the migrations.
 *
 * @return void
 */
public function up()
{

    Schema::create('job_invoice_form', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_id');
            $table->integer('user_id');
            $table->tinyInteger('compressor')->nullable();
            $table->tinyInteger('suction')->nullable();
            $table->string('suction_value')->nullable();
            $table->tinyInteger('head')->nullable();
            $table->string('head_value')->nullable();
            $table->tinyInteger('volts')->nullable();
            $table->string('volts_value')->nullable();
            $table->tinyInteger('electrical_connections_compressor')->nullable();
            $table->tinyInteger('contractor_tight_clean')->nullable();
            $table->tinyInteger('oil_level_condition')->nullable();
            $table->tinyInteger('burn_out')->nullable();
            $table->tinyInteger('condenser_coil')->nullable();
            $table->tinyInteger('clean_coil_check_fin_cond')->nullable();
            $table->tinyInteger('ent')->nullable();
            $table->tinyInteger('lvg')->nullable();
            $table->tinyInteger('refrigerant')->nullable();
            $table->tinyInteger('leak')->nullable();
            $table->tinyInteger('charge')->nullable();
            $table->tinyInteger('fan_and_motor')->nullable();
            $table->tinyInteger('volts_amps')->nullable();
            $table->string('fan_and_motor_volts_value')->nullable();
            $table->string('fan_and_motor_amps_value')->nullable();
            $table->tinyInteger('electrical_connections_fan_and_motor')->nullable();
            $table->tinyInteger('contacts_tight_clean')->nullable();
            $table->tinyInteger('fan_pulleys')->nullable();
            $table->tinyInteger('check_lube_bearings_motor')->nullable();
            $table->tinyInteger('cfm')->nullable();
            $table->string('cfm_value')->nullable();
            $table->tinyInteger('evaporator_coil')->nullable();
            $table->tinyInteger('clean_coil_check_fin')->nullable();
            $table->tinyInteger('ent_db_lvg_db')->nullable();
            $table->string('ent_db')->nullable();
            $table->string('lvg_db')->nullable();
            $table->tinyInteger('ent_wb_lvg_wb')->nullable();
            $table->string('ent_wb')->nullable();
            $table->string('lvg_wb')->nullable();
            $table->tinyInteger('condensate_areas')->nullable();
            $table->tinyInteger('inspect_clean_drain_pan')->nullable();
            $table->tinyInteger('inspect_clean_drain')->nullable();
            $table->tinyInteger('air_filters')->nullable();
            $table->tinyInteger('cleaned')->nullable();
            $table->tinyInteger('replaced')->nullable();
            $table->string('filter_size_value')->nullable();
            $table->tinyInteger('heating_assy')->nullable();
            $table->tinyInteger('burner_heat_exchanger')->nullable();
            $table->tinyInteger('fuel_supply_pressure')->nullable();
            $table->tinyInteger('pilot_assembly')->nullable();
            $table->tinyInteger('flame_adjustment')->nullable();
            $table->tinyInteger('primary_relay_flue')->nullable();
            $table->tinyInteger('fan_limit_switch_oper')->nullable();
            $table->tinyInteger('blower_assembly')->nullable();
            $table->tinyInteger('rv_valve')->nullable();
            $table->tinyInteger('strip_heat')->nullable();
            $table->tinyInteger('defrost_cycle')->nullable();
            $table->tinyInteger('electrical_compts')->nullable();
            $table->tinyInteger('relays')->nullable();
            $table->tinyInteger('contactors')->nullable();
            $table->tinyInteger('overload')->nullable();
            $table->tinyInteger('press_switch')->nullable();
            $table->tinyInteger('thermostat')->nullable();
            $table->tinyInteger('ok_thermostat')->nullable();
            $table->tinyInteger('replace')->nullable();
            $table->tinyInteger('relocate')->nullable();
            $table->string('time_arrived')->nullable();
            $table->string('time_departed')->nullable();
            $table->string('travel_time')->nullable();
            $table->string('ending')->nullable();
            $table->string('start')->nullable();
            $table->string('total_miles')->nullable();
            $table->string('total_miles_x1')->nullable();
            $table->string('total_miles_x2')->nullable();
            $table->string('total_miles_mi')->nullable();
            $table->string('trip_miles_hr')->nullable();
            $table->string('totalcharges')->nullable();
            $table->string('quantity_1')->nullable();
            $table->string('quantity_2')->nullable();
            $table->string('quantity_3')->nullable();
            $table->string('quantity_4')->nullable();
            $table->string('quantity_5')->nullable();
            $table->string('quantity_6')->nullable();
            $table->string('quantity_7')->nullable();
            $table->string('quantity_8')->nullable();
            $table->string('quantity_9')->nullable();
            $table->string('quantity_10')->nullable();
            $table->string('quantity_11')->nullable();
            $table->string('quantity_12')->nullable();
            $table->string('quantity_13')->nullable();
            $table->string('quantity_14')->nullable();
            $table->string('quantity_15')->nullable();
            $table->string('quantity_16')->nullable();
            $table->string('status')->default('active')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_invoice_form');
    }
}
