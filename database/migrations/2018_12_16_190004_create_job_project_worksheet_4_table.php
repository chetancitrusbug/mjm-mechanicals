<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobProjectWorksheet4Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_project_worksheet_form_4', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_project_worksheet_form_1_id');
            $table->string('3_b_vent_del', 50)->nullable();
            $table->string('3_90_elbows_del', 50)->nullable();
            $table->string('3_45_elbows_del', 50)->nullable();
            $table->string('3_5_length_del', 50)->nullable();
            $table->string('3_3_length_del', 50)->nullable();
            $table->string('3_2_length_del', 50)->nullable();
            $table->string('3_1_length_del', 50)->nullable();
            $table->string('3_1_adjustable_del', 50)->nullable();
            $table->string('3_tee_del', 50)->nullable();
            $table->string('3_wyes_del', 50)->nullable();
            $table->string('3_increasers_del', 50)->nullable();
            $table->string('3_4_12_flash_del', 50)->nullable();
            $table->string('3_5_12_flash_del', 50)->nullable();
            $table->string('3_6_12_flash_del', 50)->nullable();
            $table->string('3_7_12_flash_del', 50)->nullable();
            $table->string('3_reducer_del', 50)->nullable();
            $table->string('3_cap_del', 50)->nullable();
            $table->string('3_collar_del', 50)->nullable();
            $table->string('4_b_vent_del', 50)->nullable();
            $table->string('4_90_elbows_del', 50)->nullable();
            $table->string('4_45_elbows_del', 50)->nullable();
            $table->string('4_5_length_del', 50)->nullable();
            $table->string('4_3_length_del', 50)->nullable();
            $table->string('4_2_length_del', 50)->nullable();
            $table->string('4_1_length_del', 50)->nullable();
            $table->string('4_1_adjustable_del', 50)->nullable();
            $table->string('4_tee_del', 50)->nullable();
            $table->string('4_wyes_del', 50)->nullable();
            $table->string('4_increasers_del', 50)->nullable();
            $table->string('4_4_12_flash_del', 50)->nullable();
            $table->string('4_5_12_flash_del', 50)->nullable();
            $table->string('4_6_12_flash_del', 50)->nullable();
            $table->string('4_7_12_flash_del', 50)->nullable();
            $table->string('4_reducer_del', 50)->nullable();
            $table->string('4_cap_del', 50)->nullable();
            $table->string('4_collar_del', 50)->nullable();
            $table->string('5_b_vent_del', 50)->nullable();
            $table->string('5_90_elbows_del', 50)->nullable();
            $table->string('5_45_elbows_del', 50)->nullable();
            $table->string('5_5_length_del', 50)->nullable();
            $table->string('5_3_length_del', 50)->nullable();
            $table->string('5_2_length_del', 50)->nullable();
            $table->string('5_1_length_del', 50)->nullable();
            $table->string('5_1_adjustable_del', 50)->nullable();
            $table->string('5_tee_del', 50)->nullable();
            $table->string('5_wyes_del', 50)->nullable();
            $table->string('5_increasers_del', 50)->nullable();
            $table->string('5_4_12_flash_del', 50)->nullable();
            $table->string('5_5_12_flash_del', 50)->nullable();
            $table->string('5_6_12_flash_del', 50)->nullable();
            $table->string('5_7_12_flash_del', 50)->nullable();
            $table->string('5_reducer_del', 50)->nullable();
            $table->string('5_cap_del', 50)->nullable();
            $table->string('5_collar_del', 50)->nullable();
            $table->string('6_b_vent_del', 50)->nullable();
            $table->string('6_90_elbows_del', 50)->nullable();
            $table->string('6_45_elbows_del', 50)->nullable();
            $table->string('6_5_length_del', 50)->nullable();
            $table->string('6_3_length_del', 50)->nullable();
            $table->string('6_2_length_del', 50)->nullable();
            $table->string('6_1_length_del', 50)->nullable();
            $table->string('6_1_adjustable_del', 50)->nullable();
            $table->string('6_tee_del', 50)->nullable();
            $table->string('6_wyes_del', 50)->nullable();
            $table->string('6_increasers_del', 50)->nullable();
            $table->string('6_4_12_flash_del', 50)->nullable();
            $table->string('6_5_12_flash_del', 50)->nullable();
            $table->string('6_6_12_flash_del', 50)->nullable();
            $table->string('6_7_12_flash_del', 50)->nullable();
            $table->string('6_reducer_del', 50)->nullable();
            $table->string('6_cap_del', 50)->nullable();
            $table->string('6_collar_del', 50)->nullable();
            $table->string('3_b_vent_usd', 50)->nullable();
            $table->string('3_90_elbows_usd', 50)->nullable();
            $table->string('3_45_elbows_usd', 50)->nullable();
            $table->string('3_5_length_usd', 50)->nullable();
            $table->string('3_3_length_usd', 50)->nullable();
            $table->string('3_2_length_usd', 50)->nullable();
            $table->string('3_1_length_usd', 50)->nullable();
            $table->string('3_1_adjustable_usd', 50)->nullable();
            $table->string('3_tee_usd', 50)->nullable();
            $table->string('3_wyes_usd', 50)->nullable();
            $table->string('3_increasers_usd', 50)->nullable();
            $table->string('3_4_12_flash_usd', 50)->nullable();
            $table->string('3_5_12_flash_usd', 50)->nullable();
            $table->string('3_6_12_flash_usd', 50)->nullable();
            $table->string('3_7_12_flash_usd', 50)->nullable();
            $table->string('3_reducer_usd', 50)->nullable();
            $table->string('3_cap_usd', 50)->nullable();
            $table->string('3_collar_usd', 50)->nullable();
            $table->string('4_b_vent_usd', 50)->nullable();
            $table->string('4_90_elbows_usd', 50)->nullable();
            $table->string('4_45_elbows_usd', 50)->nullable();
            $table->string('4_5_length_usd', 50)->nullable();
            $table->string('4_3_length_usd', 50)->nullable();
            $table->string('4_2_length_usd', 50)->nullable();
            $table->string('4_1_length_usd', 50)->nullable();
            $table->string('4_1_adjustable_usd', 50)->nullable();
            $table->string('4_tee_usd', 50)->nullable();
            $table->string('4_wyes_usd', 50)->nullable();
            $table->string('4_increasers_usd', 50)->nullable();
            $table->string('4_4_12_flash_usd', 50)->nullable();
            $table->string('4_5_12_flash_usd', 50)->nullable();
            $table->string('4_6_12_flash_usd', 50)->nullable();
            $table->string('4_7_12_flash_usd', 50)->nullable();
            $table->string('4_reducer_usd', 50)->nullable();
            $table->string('4_cap_usd', 50)->nullable();
            $table->string('4_collar_usd', 50)->nullable();
            $table->string('5_b_vent_usd', 50)->nullable();
            $table->string('5_90_elbows_usd', 50)->nullable();
            $table->string('5_45_elbows_usd', 50)->nullable();
            $table->string('5_5_length_usd', 50)->nullable();
            $table->string('5_3_length_usd', 50)->nullable();
            $table->string('5_2_length_usd', 50)->nullable();
            $table->string('5_1_length_usd', 50)->nullable();
            $table->string('5_1_adjustable_usd', 50)->nullable();
            $table->string('5_tee_usd', 50)->nullable();
            $table->string('5_wyes_usd', 50)->nullable();
            $table->string('5_increasers_usd', 50)->nullable();
            $table->string('5_4_12_flash_usd', 50)->nullable();
            $table->string('5_5_12_flash_usd', 50)->nullable();
            $table->string('5_6_12_flash_usd', 50)->nullable();
            $table->string('5_7_12_flash_usd', 50)->nullable();
            $table->string('5_reducer_usd', 50)->nullable();
            $table->string('5_cap_usd', 50)->nullable();
            $table->string('5_collar_usd', 50)->nullable();
            $table->string('6_b_vent_usd', 50)->nullable();
            $table->string('6_90_elbows_usd', 50)->nullable();
            $table->string('6_45_elbows_usd', 50)->nullable();
            $table->string('6_5_length_usd', 50)->nullable();
            $table->string('6_3_length_usd', 50)->nullable();
            $table->string('6_2_length_usd', 50)->nullable();
            $table->string('6_1_length_usd', 50)->nullable();
            $table->string('6_1_adjustable_usd', 50)->nullable();
            $table->string('6_tee_usd', 50)->nullable();
            $table->string('6_wyes_usd', 50)->nullable();
            $table->string('6_increasers_usd', 50)->nullable();
            $table->string('6_4_12_flash_usd', 50)->nullable();
            $table->string('6_5_12_flash_usd', 50)->nullable();
            $table->string('6_6_12_flash_usd', 50)->nullable();
            $table->string('6_7_12_flash_usd', 50)->nullable();
            $table->string('6_reducer_usd', 50)->nullable();
            $table->string('6_cap_usd', 50)->nullable();
            $table->string('6_collar_usd', 50)->nullable();

            $table->tinyInteger('furnace_type_up_flow')->nullable();
            $table->tinyInteger('furnace_type_counter_flow')->nullable();
            $table->tinyInteger('furnace_type_condensing')->nullable();
            $table->tinyInteger('furnace_type_horizontal')->nullable();
            $table->string('furnace_type_notes')->nullable();
            $table->string('equipment_furnace')->nullable();
            $table->string('equipment_make')->nullable();
            $table->string('equipment_model')->nullable();
            $table->string('equipment_serial_1')->nullable();
            $table->string('equipment_eac_filter')->nullable();
            $table->string('equipment_eac_filte_size')->nullable();
            $table->string('equipment_thermostat')->nullable();
            $table->string('equipment_thermostat_make')->nullable();
            $table->string('equipment_thermostat_model')->nullable();
            $table->string('equipment_cond_unit')->nullable();
            $table->string('equipment_cond_unit_make')->nullable();
            $table->string('equipment_cond_unit_model')->nullable();
            $table->string('equipment_serial_2')->nullable();
            $table->string('equipment_evap_coil')->nullable();
            $table->string('equipment_evap_coi_make')->nullable();
            $table->string('equipment_evap_coi_model')->nullable();
            $table->string('hot_water_tank_size')->nullable();
            $table->tinyInteger('hot_water_tank_type_standard_atmosphere')->nullable();
            $table->tinyInteger('hot_water_tank_type_direct_vent')->nullable();
            $table->tinyInteger('hot_water_tank_type_induced_vent')->nullable();
            $table->tinyInteger('hot_water_tank_stand_required')->nullable();
            $table->tinyInteger('hot_water_tank_expansion_tank')->nullable();
            $table->tinyInteger('hot_water_tank_prv')->nullable();
            $table->tinyInteger('hot_water_tank_t_p_drain')->nullable();
            $table->tinyInteger('hot_water_tank_run_to_outside')->nullable();
            $table->tinyInteger('hot_water_tank_floor')->nullable();
            $table->tinyInteger('hot_water_tank_exist')->nullable();
            $table->tinyInteger('hot_water_tank_pan')->nullable();
            $table->string('water_htr_make')->nullable();
            $table->string('serial_3_make')->nullable();
            $table->string('fireplace_make')->nullable();
            $table->string('rooftop_unit_make')->nullable();
            $table->string('serial_4_make')->nullable();
            $table->string('wall_heater_make')->nullable();
            $table->string('other_make')->nullable();
            $table->string('water_htr_model')->nullable();
            $table->string('serial_3_model')->nullable();
            $table->string('fireplace_model')->nullable();
            $table->string('rooftop_unit_model')->nullable();
            $table->string('serial_4_model')->nullable();
            $table->string('wall_heater_model')->nullable();
            $table->string('other_model')->nullable();
            $table->string('status')->default('active')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_project_worksheet_form_4');
    }
}
