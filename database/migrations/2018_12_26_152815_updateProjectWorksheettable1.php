<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateProjectWorksheettable1 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
         Schema::table('job_project_worksheet_form_1', function (Blueprint $table) {
            $table->dropColumn('furnace');
            $table->string('distance_to_furnace')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('job_project_worksheet_form_1', function (Blueprint $table) {
            $table->dropColumn('distance_to_furnace');
            $table->string('furnace')->nullable();
        });
    }

}
