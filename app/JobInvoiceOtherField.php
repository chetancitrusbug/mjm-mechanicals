<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobInvoiceOtherField extends Model
{
    use SoftDeletes;

    public $table='job_invoice_other_fields_form';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'job_invoice_id', 'item_or_part_description_1', 'item_or_part_description_2', 'item_or_part_description_3', 'item_or_part_description_4', 'item_or_part_description_5', 'item_or_part_description_6', 'item_or_part_description_7', 'item_or_part_description_8', 'item_or_part_description_9', 'item_or_part_description_10', 'item_or_part_description_11', 'item_or_part_description_12', 'item_or_part_description_13', 'item_or_part_description_14', 'item_or_part_description_15', 'item_or_part_description_16', 'price_1', 'price_2', 'price_3', 'price_4', 'price_5', 'price_6', 'price_7', 'price_8', 'price_9', 'price_10', 'price_11', 'price_12', 'price_13', 'price_14', 'price_15', 'price_16', 'amount_1', 'amount_2', 'amount_3', 'amount_4', 'amount_5', 'amount_6', 'amount_7', 'amount_8', 'amount_9', 'amount_10', 'amount_11', 'amount_12', 'amount_13', 'amount_14', 'amount_15', 'amount_16', 'total_parts', 'write_or_code_1', 'write_or_code_2', 'write_or_code_3', 'charges_from_amount_1', 'charges_from_amount_2', 'charges_from_amount_3', 'total_other_charges', 'refrig', 'refrig_qty', 'recovered', 'recycled', 'reclaimed', 'returned_to_this_system', 'disposal_1', 'non_useable', 'disposal_2', 'recovered_qty', 'recycled_qty', 'reclaimed_qty', 'returned_to_this_system_qty', 'non_useable_qty', 'changed_out', 'dis_mantled', 'refrigerant_disposal', 'our_personnel_recommend', 'accepted', 'declined', 'name', 'email', 'street', 'city', 'state', 'zip', 'make', 'model', 'serial_number', 'job_location', 'desc_of_work_1', 'desc_of_work_2', 'desc_of_work_3', 'desc_of_work_4', 'desc_of_work_5', 'desc_of_work_6', 'desc_of_work_7', 'desc_of_work_8', 'desc_of_work_9', 'tech_1_regular_hrs', 'tech_1_regular_hr', 'tech_2_regular_hrs', 'tech_2_regular_hr', 'tech_1_overtime_hrs', 'tech_1_overtime_hr', 'tech_2_overtime_hrs', 'tech_2_overtime_hr', 'technician_signature', 'cert', 'authorized_signature', 'above_ordered_work_has_been_completed', 'main_date', 'date_ordered', 'date_scheduled', 'phone', 'wk_or_cell', 'warranty', 'contract', 'service_contract', 'normal', 'res', 'comm', 'service_1', 'service_2', 'service_3', 'service_4', 'service_5', 'service_6','service_7','service_8','service_9','service_10','service_11','service_total_other_charges','sub_total','trip_charge','tax','total_amount_due','date','status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];



}
