<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobChecklistForm extends Model
{
    //
    use SoftDeletes;

    public $table='job_checklist_form';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','job_id','user_id','all_register_open','install_toe_kicks','install_thermostat','furnace_cleaned','place_new_filter','place_new_filter_size','place_new_filter_type','set_ac_and_level','pad_size','basement_rack','weigh_in_charge','pre_charge','total_added','line_set_length','inside_of_ac_panel','install_start_assist','spp6','factory_kit','verify_fireplace_valve','startup_furnace','gas_pressure_lo','gas_pressure_hi','fan_speed','temp_rise','ac_hp_fan_speed','stickers_on_equipment','drain_installed','floor_drain','sump_pit','condo_pump','flue_pipes','flue_pipes_size','flue_pipes_side_wall','flue_pipes_roof','flue_pipes_2pipe','flue_pipes_concentric','stat_wire_size','thermostat','ac_hp','humidifier','by_pass','powered','water_line_installed','tested','control_type_stat','manufacture_control','brand','fresh_air_damper','ac_disconnect_installed','status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

}
