<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobMaintenanceForm extends Model
{
    //
    use SoftDeletes;

    public $table='job_maintenance_form';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','job_id','user_id','proposal','annual_maintenance','discount','signature','date','status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function getDateAttribute($value)
    {
        if($value != ""){
            return \Carbon\Carbon::parse($value)->format('d-m-Y');
        }
        return $value;
    }

    public function setDateAttribute($value)
    {
        if($value != ""){
            $this->attributes['date'] = \Carbon\Carbon::parse($value)->format('Y-m-d');
        }
    }
}
