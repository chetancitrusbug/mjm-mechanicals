<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobGeothermalForm extends Model
{
    //
    use SoftDeletes;

    public $table='job_geothermal_form';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'job_id','created_by', 'model', 'serial', 'install_date', 'open_closed_loop', 'desuperheater', 'source_coax_a', 'source_coax_b', 'source_coax_c', 'source_coax_d', 'load_coax_a', 'load_coax_b', 'load_coax_c', 'load_coax_d', 'temperature_rise_drop_across_coaxial_heat_exchanger_cooling_e', 'temperature_rise_drop_across_coaxial_heat_exchanger_cooling_f', 'temperature_rise_drop_across_coaxial_heat_exchanger_cooling_g', 'temperature_rise_drop_across_coaxial_heat_exchanger_heating_e', 'temperature_rise_drop_across_coaxial_heat_exchanger_heating_f', 'temperature_rise_drop_across_coaxial_heat_exchanger_heating_g', 'temperature_rise_drop_across_air_coil_cooling_h', 'temperature_rise_drop_across_air_coil_cooling_i', 'temperature_rise_drop_across_air_coil_cooling_j', 'temperature_rise_drop_across_air_coil_heating_h', 'temperature_rise_drop_across_air_coil_heating_i', 'temperature_rise_drop_across_air_coil_heating_j', 'load_coax_cooling_h', 'load_coax_cooling_i', 'load_coax_cooling_j', 'load_coax_heating_h', 'load_coax_heating_i', 'load_coax_heating_j', 'HR_HE_brine_factor_k', 'HR_cooling_i', 'HE_heating_i', 'watts_cooling_m', 'watts_cooling_n', 'watts_cooling_o', 'watts_heating_m', 'watts_heating_n', 'watts_heating_o', 'capacity_cooling_p', 'capacity_heating_p', 'efficiency_cooling_q', 'efficiency_heating_q', 'SH_SC_cooling_r', 'SH_SC_cooling_s', 'SH_SC_cooling_t', 'SH_SC_cooling_u', 'SH_SC_cooling_v', 'SH_SC_cooling_w', 'SH_SC_cooling_x', 'SH_SC_heating_r', 'SH_SC_heating_s', 'SH_SC_heating_t', 'SH_SC_heating_u', 'SH_SC_heating_v', 'SH_SC_heating_w', 'SH_SC_heating_x', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

}
