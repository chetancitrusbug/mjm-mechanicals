<?php

namespace App\Helpers;
use Request;
use App\ActionLog as ActionLogModel;

class ActionLog
{

    public static function addToLog( $action,$detail,$tablename,$field_id,$subdetail = false)
    {

        $log = [];
        $log['actioner_id'] = auth()->check() ? auth()->user()->id : 1;
        $log['actioner_name'] = auth()->check() ? auth()->user()->name : 1;
        $log['action'] = $action;
        $log['detail'] = $detail;
        $log['tablename'] = $tablename;
        $log['field_id'] = $field_id;
        $log['role']=  auth()->check() ? auth()->user()->roles[0]->name : 1;
        $log['url'] = Request::fullUrl();
        $log['ip'] = Request::ip();
        if($subdetail){
            $log['sub_detail'] = serialize($subdetail);
        }
        ActionLogModel::create($log);
    }

    public static function actionlogLists()
    {
        return ActionLogModel::latest()->get();
    }

}