@extends('layouts.backend')

@section('title',trans('image.edit_image'))
@section('pageTitle',trans('image.edit_image'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('image.edit_image')</div>
                <div class="panel-body">
                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('image.back')
                        </button>
                    </a>
                    <br/>
                    <br/>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model($image, [
                        'method' => 'PATCH',
                        'url' => ['/admin/images', $image->id],
                        'class' => 'form-horizontal',
                        'id'=>'formimage',
                        'enctype'=>'multipart/form-data'
                    ]) !!}


                    @include ('admin.images.form', ['submitButtonText' => trans('image.update')])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
