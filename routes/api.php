<?php

use Illuminate\Http\Request;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *, Origin, Content-Type, Authorization, X-Auth-Token, x-xsrf-token ');
header('Access-Control-Allow-Methods: GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS');

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1.0/', 'namespace' => 'Api'], function () {

    Route::post('user/login', 'UsersController@login');
    Route::post('user/forgot', 'UsersController@forgot');
    Route::post('user/registration', 'UsersController@register');
    Route::post('contact-us', 'UsersController@contactUs');
    Route::post('special-inquiry', 'UsersController@specialInquiry');
    Route::get('listState', 'UsersController@listState');
    // job code
    Route::get('jobCodesList', 'JobsController@jobCodesList');
});

Route::group(['prefix' => 'v1.0', 'middleware' => ["api_pass"], 'namespace' => 'Api'], function () {
    //Route::get('getjob','JobsController@index');
    Route::post('user/changepassword', 'UsersController@changePassword');
    Route::post('user/update', 'UsersController@update');
    Route::post('user/logout', 'UsersController@logout');
    Route::get('jobs/joblist', 'JobsController@joblist');
    Route::get('jobs/jobdetail', 'JobsController@jobdetail');
    Route::get('jobs/jobtasklist', 'JobsController@jobtasklist');
    Route::get('jobs/jobfolderlist', 'JobsController@jobfolderlist');
	Route::get('jobs/jobtaskfolderlist', 'JobsController@jobtaskfolderlist');
    Route::get('jobs/foldervideodetail', 'JobsController@foldervideodetail');
    Route::get('jobs/folderdocumentdetail', 'JobsController@folderdocumentdetail');
    Route::get('jobs/videodetail', 'JobsController@videodetail');
    Route::get('jobs/documentdetail', 'JobsController@documentdetail');
    Route::post('jobs/createjobcard', 'JobsController@createjobcard');
    Route::get('jobs/jobcardlist', 'JobsController@jobcardlist');
    Route::get('jobs/jobcarddetail', 'JobsController@jobcarddetail');
    Route::post('jobs/addimage', 'JobsController@addimage');
    Route::post('jobs/adddocument', 'JobsController@adddocument');
    Route::post('jobs/addvideo', 'JobsController@addvideo');
    Route::get('products/productlist', 'ProductsController@productlist');
    Route::get('products/productdetail', 'ProductsController@productdetail');
    Route::post('jobs/updatejobtask', 'JobsController@updatejobtask');
    Route::post('jobs/markasdone', 'JobsController@markasdone');
    Route::post('jobs/addnotes', 'JobsController@addnotes');
	 Route::post('jobs/editnotes', 'JobsController@editnotes');
    Route::post('jobs/deletenotes', 'JobsController@deletenotes');
    Route::get('jobs/noteslist', 'JobsController@notesfolderlist');
	Route::get('jobs/archievejoblist', 'JobsController@jobsDoneList');
    Route::post('products/productinquiry', 'ProductsController@productinquiry');



    // form list
    Route::get('jobs/jobFormsList', 'JobFormsController@jobFormsList');


    //Job Geothermal Form
    Route::get('jobs/jobGeothermalFormList', 'JobFormsController@jobGeothermalFormList');
    Route::post('jobs/addJobGeothermalForm', 'JobFormsController@addJobGeothermalForm');
    Route::post('jobs/deleteJobGeothermalForm', 'JobFormsController@deleteJobGeothermalForm');
    Route::post('jobs/editJobGeothermalForm', 'JobFormsController@editJobGeothermalForm');

    //Job Furnace Form
    Route::get('jobs/jobFurnaceFormList', 'JobFormsController@jobFurnaceFormList');
    Route::post('jobs/addJobFurnaceForm', 'JobFormsController@addJobFurnaceForm');
    // Route::post('jobs/deleteJobFurnaceForm', 'JobFormsController@deleteJobFurnaceForm');
    Route::post('jobs/editJobFurnaceForm', 'JobFormsController@editJobFurnaceForm');

    //Job Inspection Report
    Route::get('jobs/jobInspectionFormList', 'JobFormsController@jobInspectionFormList');
    Route::post('jobs/addJobInspectionForm', 'JobFormsController@addJobInspectionForm');
    // Route::post('jobs/deleteJobInspectionForm', 'JobFormsController@deleteJobInspectionForm');
    Route::post('jobs/editJobInspectionForm', 'JobFormsController@editJobInspectionForm');

    //Job Checklist Report
    Route::get('jobs/jobChecklistFormList', 'JobFormsController@jobChecklistFormList');
    Route::post('jobs/addJobChecklistForm', 'JobFormsController@addJobChecklistForm');
    // Route::post('jobs/deleteJobChecklistForm', 'JobFormsController@deleteJobChecklistForm');
    Route::post('jobs/editJobChecklistForm', 'JobFormsController@editJobChecklistForm');

    //Job Maintenance Report
    Route::get('jobs/jobMaintenanceFormList', 'JobFormsController@jobMaintenanceFormList');
    Route::post('jobs/addJobMaintenanceForm', 'JobFormsController@addJobMaintenanceForm');
    // Route::post('jobs/deleteJobMaintenanceForm', 'JobFormsController@deleteJobMaintenanceForm');
    Route::post('jobs/editJobMaintenanceForm', 'JobFormsController@editJobMaintenanceForm');

    //Job System Repair Replace From
    Route::get('jobs/jobSystemRepairFormList', 'JobFormsController@jobSystemRepairFormList');
    Route::post('jobs/addJobSystemRepairForm', 'JobFormsController@addJobSystemRepairForm');
    // Route::post('jobs/deleteJobSystemRepairForm', 'JobFormsController@deleteJobSystemRepairForm');
    Route::post('jobs/editJobSystemRepairForm', 'JobFormsController@editJobSystemRepairForm');

    //Job Plumbing Report
    Route::get('jobs/jobPlumbingFormList', 'JobFormsController@jobPlumbingFormList');
    Route::post('jobs/addJobPlumbingForm', 'JobFormsController@addJobPlumbingForm');
    Route::post('jobs/editJobPlumbingForm', 'JobFormsController@editJobPlumbingForm');


    //Job Invoice Report
    Route::get('jobs/jobInvoiceFormList', 'JobFormsController@jobInvoiceFormList');
    Route::post('jobs/addJobInvoiceForm', 'JobFormsController@addJobInvoiceForm');
    Route::post('jobs/editJobInvoiceForm', 'JobFormsController@editJobInvoiceForm');

    //Job Weekly Vehicle Safety Checklist Form Report
    Route::get('jobs/jobWeeklyVehicleSafetyChecklistFormList', 'JobFormsController@jobWeeklyVehicleSafetyChecklistFormList');
    Route::post('jobs/addJobWeeklyVehicleSafetyChecklistForm', 'JobFormsController@addJobWeeklyVehicleSafetyChecklistForm');
    Route::post('jobs/editJobWeeklyVehicleSafetyChecklistForm', 'JobFormsController@editJobWeeklyVehicleSafetyChecklistForm');

    //Job proect worksheet
    Route::get('jobs/jobProjrctWorksheetFormList', 'JobFormsController@jobProjrctWorksheetFormList');
    Route::post('jobs/addJobProjrctWorksheetForm', 'JobFormsController@addJobProjrctWorksheetForm');
    Route::post('jobs/editJobProjrctWorksheetForm', 'JobFormsController@editJobProjrctWorksheetForm');

    // Material
    Route::get('listMaterial', 'MaterialController@listMaterial');
    Route::get('materialDetail', 'MaterialController@materialDetail');
    Route::get('getMaterialUsed', 'MaterialController@getMaterialUsed');


    // Job
    Route::any('checkInOutJob', 'JobsController@checkInOutJob');
    Route::post('jobCreate', 'JobsController@jobCreate');
    Route::post('addJobImage', 'JobsController@addJobImage');
    Route::get('jobImagesList', 'JobsController@jobImagesList');
    Route::get('jobImageDetail', 'JobsController@jobImageDetail');
    Route::get('jobImageDelete', 'JobsController@jobImageDelete');

    //Job Form
    Route::any('checkInOutJob', 'JobsController@checkInOutJob');

    //Tools
    Route::get('checkInOutTools', 'ProductsController@checkInOutTools');
    Route::get('toolsAvailableList', 'ProductsController@toolsAvailableList');
    Route::get('toolsUsedList', 'ProductsController@toolsUsedList');


    // Customer create
    Route::post('/customerCreate', 'UsersController@customerCreate');
    Route::get('/clientList', 'UsersController@clientList');





});
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
// });

// Route::group(['middleware' => ['auth', '']], function () {

// });

// Route::group(['prefix' => 'v1.0/', 'middleware' => 'api_pass', 'namespace' => 'Api'], function () {
// //     //
// //     Route::get('/get-subjects', 'TicketController@getSubjects');
// //     Route::get('/get-subject/{subject_id}', 'TicketController@getSubject');
// //     //
// //     Route::get('/get-tickets', 'TicketController@getTickets');
// //     Route::get('/get-ticket/{ticket_id}', 'TicketController@getTicket');
// // //    //
// //     Route::post('/create-ticket', 'TicketController@postTicket');
// //     Route::put('/edit-ticket/{ticket_id}', 'TicketController@putTicket');
// //     //
// //     Route::delete('/delete-ticket/{ticket_id}', 'TicketController@deleteTicket');


// });

function make_null($value){
    $value = $value->toArray();
    array_walk_recursive($value, function (&$item, $key) {
        $item =  $item === null ? "" : $item;
    });
    return $value;

}