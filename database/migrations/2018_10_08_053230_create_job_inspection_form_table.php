<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobInspectionFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_inspection_form', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_id');
            $table->integer('user_id');
            $table->date('date')->nullable();
            $table->string('customer_name')->nullable();
            $table->text('address')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('system_age')->nullable();
            $table->enum('stay_cool_enough',['y','n'])->nullable();
            $table->enum('has_been_cycling',['y','n'])->nullable();
            $table->enum('temperature_consistent',['y','n'])->nullable();
            $table->enum('have_any_weird_smells',['y','n'])->nullable();
            $table->enum('increase_electric_bill',['y','n'])->nullable();
            $table->string('ac_hp_1')->nullable();
            $table->string('brand_1')->nullable();
            $table->string('model_1')->nullable();
            $table->string('serial_1')->nullable();
            $table->string('ac_hp_2')->nullable();
            $table->string('brand_2')->nullable();
            $table->string('model_2')->nullable();
            $table->string('serial_2')->nullable();
            $table->string('ac_hp_3')->nullable();
            $table->string('brand_3')->nullable();
            $table->string('model_3')->nullable();
            $table->string('serial_3')->nullable();
            $table->string('filter_size_1')->nullable();
            $table->string('quantity_1')->nullable();
            $table->string('accessories_1')->nullable();
            $table->string('filter_size_2')->nullable();
            $table->string('quantity_2')->nullable();
            $table->string('accessories_2')->nullable();
            $table->string('filter_size_3')->nullable();
            $table->string('quantity_3')->nullable();
            $table->string('accessories_3')->nullable();
            $table->string('seer')->nullable();
            $table->string('stages')->nullable();
            $table->string('fan_type')->nullable();
            $table->string('cap')->nullable();
            $table->string('disconnect')->nullable();
            $table->string('refrigerant')->nullable();
            $table->string('blower_amps_r')->nullable();
            $table->string('blower_amps_a')->nullable();
            $table->string('blower_capacitor_r')->nullable();
            $table->string('blower_capacitor_a')->nullable();
            $table->string('copressor_amps_r')->nullable();
            $table->string('copressor_amps_a')->nullable();
            $table->string('fan_motor_r')->nullable();
            $table->string('fan_motor_a')->nullable();
            $table->string('outdoor_capacitor_r')->nullable();
            $table->string('outdoor_capacitor_a')->nullable();
            $table->string('sub_cooling_r')->nullable();
            $table->string('sub_cooling_a')->nullable();
            $table->string('psi_low_side')->nullable();
            $table->string('psi_high_side')->nullable();
            $table->string('low_line_temp')->nullable();
            $table->string('high_line_temp')->nullable();
            $table->string('indoor_outdoor_db')->nullable();
            $table->string('indoor_outdoor_wb')->nullable();
            $table->string('condenser_coil')->nullable();
            $table->string('evaporator_coil')->nullable();
            $table->string('drain_line')->nullable();
            $table->string('refrigerant_leak')->nullable();
            $table->string('ref_add')->nullable();
            $table->string('ref_add_qty')->nullable();
            $table->string('ref_add_lbs')->nullable();
            $table->string('ref_removed')->nullable();
            $table->string('ref_removed_qty')->nullable();
            $table->string('ref_removed_lbs')->nullable();
            $table->text('notes')->nullable();
            $table->string('tech')->nullable();
            $table->string('occupant')->nullable();
            $table->string('bottom_date')->nullable();
            $table->string('status')->default('active')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_inspection_form');
    }
}
