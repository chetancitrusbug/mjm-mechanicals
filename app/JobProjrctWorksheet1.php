<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobProjrctWorksheet1 extends Model
{
    use SoftDeletes;

    public $table='job_project_worksheet_form_1';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'job_id', 'user_id', 'customer_name', 'address', 'phone', 'fan_switch_on_furnace', 'fan_switch_on_tstat', 'fan_center', 'condensate_pump', 'concrete_pad', 'linesetFt', 'linesetFt_value', 'insulate_ducts', 'chimney_fliner', 'transition_fittings', 'brand', 'fuel', 'model', 'BTU', 'location', 'stat_type', 'of_stat_wires', 'blower_type_direct', 'blower_Type_belt', 'motor_size', 'motor_speed', 'motor_volt', 'motor_brand', 'existing_vent_size', 'existing_vent_brand', 'location_of_panel', 'brand_of_panel', 'spare_circuits', 'electrician', 'new_tstat_wire', 'AC_unit', 'light_in_crawlspace', 'status','distance_to_furnace'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}

