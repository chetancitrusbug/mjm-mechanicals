@extends('layouts.backend')
@section('title',trans('job.view_form'))
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">HVAC Inspection Report - A / C</div>
    {!! Form::model($jobInspectionForm, [ 'method' => 'PATCH', 'url' => ['/admin/jobInspectionForm', $jobInspectionForm->id],
    'class' => 'form-horizontal','id'=>'jobInspectionForm', 'enctype'=>'multipart/form-data' ]) !!}
    <div class="panel-body">
        <a href="{{ url('/admin/job/' . $job->id . '/edit') }}" title="Back to Forms list"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true" ></i>Back</button></a>
        {!! Form::hidden('job_id', $job->id, ['class' => 'form-control input-100']) !!}
        <div class="grid-box-1 clearfix">
            <div class="col-md-4 col-sm-4 pull-right clearfix">
                <p class="height-auto">Date:</p>
                {!! Form::text('date', null, ['class' => 'form-control input-100 HVAC_date']) !!}
                {!! $errors->first('date', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="grid-box-1 clearfix">
            <div class="col-md-4 col-sm-4 clearfix">
                <div class="row">
                    <div class="col-md-12 col-sm-12 clearfix">
                        <p class="height-auto">Customer Name</p>
                        {!! Form::text('customer_name', null, ['class' => 'form-control input-100']) !!}
                        {!! $errors->first('customer_name', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <p class="height-auto">Phone</p>
                        {!! Form::text('phone', null, ['class' => 'form-control input-100']) !!}
                        {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <p class="height-auto">E-Mail</p>
                        {!! Form::text('email', null, ['class' => 'form-control input-100']) !!}
                        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-8 clearfix">
                <div class="row">
                    <div class="col-md-12 col-sm-12 clearfix">
                        <p class="height-auto">Address</p>
                        {!! Form::textarea('address', null, ['class' => 'form-control h-auto textarea1','rows'=>'5']) !!}
                        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <p class="height-auto">System Age:</p>
                        {!! Form::text('system_age', null, ['class' => 'form-control input-100']) !!}
                        {!! $errors->first('system_age', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div><!-- End of box-div -->

        <div class="grid-box-1 table-01 clearfix">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td class="col-md-9 p-12">Does your home stay cool enough? </td>
                                <td class="col-md-3 text-center">
                                    <label class="radio-inline">{!! Form::radio('stay_cool_enough', 'y', null, ['class' => '']) !!}Y</label>
                                    <span class="space-line">/</span>
                                    <label class="radio-inline">{!! Form::radio('stay_cool_enough', 'n', null, ['class' => '']) !!}N</label></td>
                            </tr>
                            <tr>
                                <td class="col-md-9 p-12">Has it been cycling too often? </td>
                                <td class="col-md-3 text-center">
                                    <label class="radio-inline">{!! Form::radio('has_been_cycling', 'y', null, ['class' => '']) !!}Y</label>
                                    <span class="space-line">/</span>
                                    <label class="radio-inline">{!! Form::radio('has_been_cycling', 'n', null, ['class' => '']) !!}N</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-9 p-12">Are temperatures consistent throughout the house? </td>
                                <td class="col-md-3 text-center">
                                    <label class="radio-inline">{!! Form::radio('temperature_consistent', 'y', null, ['class' => '']) !!}Y</label>
                                    <span class="space-line">/</span>
                                    <label class="radio-inline">{!! Form::radio('temperature_consistent', 'n', null, ['class' => '']) !!}N</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-9 p-12">Have there been any weird smells or sounds when the equipment turns on? </td>
                                <td class="col-md-3 text-center">
                                    <label class="radio-inline">{!! Form::radio('have_any_weird_smells', 'y', null, ['class' => '']) !!}Y</label>
                                    <span class="space-line">/</span>
                                    <label class="radio-inline">{!! Form::radio('have_any_weird_smells', 'n', null, ['class' => '']) !!}N</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-9 p-12">Have they noticed a large increase in their electric bill?</td>
                                <td class="col-md-3 text-center">
                                    <label class="radio-inline">{!! Form::radio('increase_electric_bill', 'y', null, ['class' => '']) !!}Y</label>
                                    <span class="space-line">/</span>
                                    <label class="radio-inline">{!! Form::radio('increase_electric_bill', 'n', null, ['class' => '']) !!}N</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end of box -->

        <div class="grid-box-1 table-01 clearfix">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="col-md-3 text-center">A/C or H/P</th>
                                <th class="col-md-3 text-center">Brand</th>
                                <th class="col-md-3 text-center">Model #</th>
                                <th class="col-md-3 text-center">Serial #</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <tr>
                                <td>{!! Form::text('ac_hp_1', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('brand_1', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('model_1', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('serial_1', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                            <tr>
                                <td>{!! Form::text('ac_hp_2', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('brand_2', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('model_2', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('serial_2', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                            <tr>
                                <td>{!! Form::text('ac_hp_3', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('brand_3', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('model_3', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('serial_3', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end of box -->

        <div class="grid-box-1 table-01 clearfix">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="col-md-3 text-center">Filter Size</th>
                                <th class="col-md-3 text-center">Quantity</th>
                                <th class="col-md-6 text-center">Accessories <small>(circle all that apply)</small></th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <tr>
                                <td>{!! Form::text('filter_size_1', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('quantity_1', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>
                                    <label class="radio-inline">{!! Form::radio('accessories_1', 'humidifier', false, ['class' => '']) !!}Humidifier</label>
                                    <label class="radio-inline">{!! Form::radio('accessories_1', 'condensate_pump', false, ['class' => '']) !!}Condensate Pump</label>
                                    <label class="radio-inline">{!! Form::radio('accessories_1', 'uv_light', false, ['class' => '']) !!}UV Light</label>
                                </td>
                            </tr>
                            <tr>
                                <td>{!! Form::text('filter_size_2', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('quantity_2', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>
                                    <label class="radio-inline">{!! Form::radio('accessories_2', 'electronic_air_cleaner', false, ['class' => '']) !!}Electronic Air Cleaner</label>
                                    <label class="radio-inline">{!! Form::radio('accessories_2', 'carbon_monoxide_detector', false, ['class' => '']) !!}Carbon Monoxide Detector</label>
                                </td>
                            </tr>
                            <tr>
                                <td>{!! Form::text('filter_size_3', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>{!! Form::text('quantity_3', null, ['class' => 'form-control input-box-1']) !!}</td>
                                <td>
                                    <label class="radio-inline">{!! Form::radio('accessories_3', 'gps', false, ['class' => '']) !!}GPS</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end of box -->

        <div class="grid-box-1 table-01 clearfix">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="col-md-6 text-center" colspan="6">A/C INFO <small>(circle all that apply)</small></th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <tr>
                                <td class="col-md-1 col-1">SEER</td>
                                <td class="col-md-3 col-3">
                                    <label class="radio-inline">{!! Form::radio('seer', '6', false, ['class' => '']) !!}6</label>
                                    <label class="radio-inline">{!! Form::radio('seer', '7', false, ['class' => '']) !!}7</label>
                                    <label class="radio-inline">{!! Form::radio('seer', '8', false, ['class' => '']) !!}8</label>
                                    <label class="radio-inline">{!! Form::radio('seer', '10', false, ['class' => '']) !!}10</label>
                                    <label class="radio-inline">{!! Form::radio('seer', '12', false, ['class' => '']) !!}12</label>
                                    <label class="radio-inline">{!! Form::radio('seer', '13', false, ['class' => '']) !!}13</label>
                                    <label class="radio-inline">{!! Form::radio('seer', '15+', false, ['class' => '']) !!}15+</label>
                                </td>
                                <td class="col-md-1 col-1">FAN TYPE</td>
                                <td class="col-md-3 col-3">
                                    <label class="radio-inline">{!! Form::radio('fan_type', 'psc', false, ['class' => '']) !!}PSC</label>
                                    <label class="radio-inline">{!! Form::radio('fan_type', 'ecm', false, ['class' => '']) !!}ECM</label>
                                </td>
                                <td class="col-md-1 col-1">DISCONNECT</td>
                                <td class="col-md-3 col-3">
                                    <label class="radio-inline">{!! Form::radio('disconnect', 'y', false, ['class' => '']) !!}YES</label>
                                    <label class="radio-inline">{!! Form::radio('disconnect', 'n', false, ['class' => '']) !!}NO</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-1 col-1">Stages</td>
                                <td class="col-md-3 col-3">
                                    <label class="radio-inline">{!! Form::radio('stages', '1', false, ['class' => '']) !!}1</label>
                                    <label class="radio-inline">{!! Form::radio('stages', '2', false, ['class' => '']) !!}2</label>
                                    <label class="radio-inline">{!! Form::radio('stages', 'vs', false, ['class' => '']) !!}VS</label>
                                    <label class="radio-inline">{!! Form::radio('stages', 'mod', false, ['class' => '']) !!}MOD</label>
                                </td>
                                <td class="col-md-1 col-1">CAP</td>
                                <td class="col-md-3 col-3">
                                    <label class="radio-inline">{!! Form::radio('cap', 'run', false, ['class' => '']) !!}RUN</label>
                                    <label class="radio-inline">{!! Form::radio('cap', 'start', false, ['class' => '']) !!}START</label>
                                </td>
                                <td class="col-md-1 col-1">REFRIGERANT</td>
                                <td class="col-md-3 col-3">
                                    <label class="radio-inline">{!! Form::radio('refrigerant', 'r22', false, ['class' => '']) !!}R22</label>
                                    <label class="radio-inline">{!! Form::radio('refrigerant', 'r410a', false, ['class' => '']) !!}R410A</label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end of box -->

        <div class="grid-box-1 table-01 clearfix">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="col-md-6 text-center" colspan="6">A/C READINGS</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <tr>
                                <td class="col-md-3 font-bold">BLOWER AMPS:</td>
                                <td class="col-md-3">
                                    <span class="w-40">{!! Form::text('blower_amps_r', null, ['class' => 'text-check']) !!} R </span>
                                    <span class="w-40">{!! Form::text('blower_amps_a', null, ['class' => 'text-check']) !!}A </span>
                                </td>
                                <td class="col-md-3 font-bold">PSI LOW SIDE:</td>
                                <td class="col-md-3">{!! Form::text('psi_low_side', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                            <tr>
                                <td class="col-md-3 font-bold">BLOWER CAPACITOR:</td>
                                <td class="col-md-3">
                                    <span class="w-40">{!! Form::text('blower_capacitor_r', null, ['class' => 'text-check']) !!} R</span>
                                    <span class="w-40">{!! Form::text('blower_capacitor_a', null, ['class' => 'text-check']) !!} A</span>
                                </td>
                                <td class="col-md-3 font-bold">PSI HIGH SIDE:</td>
                                <td class="col-md-3">{!! Form::text('psi_high_side', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                            <tr>
                                <td class="col-md-3 font-bold">COMPRESSOR AMPS:</td>
                                <td class="col-md-3">
                                    <span class="w-40">{!! Form::text('copressor_amps_r', null, ['class' => 'text-check']) !!}R </span>
                                    <span class="w-40">{!! Form::text('copressor_amps_a', null, ['class' => 'text-check']) !!}A </span>
                                </td>
                                <td class="col-md-3 font-bold">LOW LINE TEMP:</td>
                                <td class="col-md-3">{!! Form::text('low_line_temp', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                            <tr>
                                <td class="col-md-3 font-bold">FAN MOTOR AMPS:</td>
                                <td class="col-md-3">
                                    <span class="w-40">{!! Form::text('fan_motor_r', null, ['class' => 'text-check']) !!}R </span>
                                    <span class="w-40">{!! Form::text('fan_motor_a', null, ['class' => 'text-check']) !!}A </span>
                                </td>
                                <td class="col-md-3 font-bold">HIGH LINE TEMP:</td>
                                <td class="col-md-3">{!! Form::text('high_line_temp', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                            <tr>
                                <td class="col-md-3 font-bold">OUTDOOR CAPACITOR:</td>
                                <td class="col-md-3">
                                    <span class="w-40">{!! Form::text('outdoor_capacitor_r', null, ['class' => 'text-check']) !!}R </span>
                                    <span class="w-40">{!! Form::text('outdoor_capacitor_a', null, ['class' => 'text-check']) !!}A </span>
                                </td>
                                <td class="col-md-3 font-bold">Indoor/ Outdoor Db:</td>
                                <td class="col-md-3">{!! Form::text('indoor_outdoor_db', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                            <tr>
                                <td class="col-md-3 font-bold">SUPERHEAT/SUBCOOLING:</td>
                                <td class="col-md-3">
                                    <span class="w-40">{!! Form::text('sub_cooling_r', null, ['class' => 'text-check']) !!}R </span>
                                    <span class="w-40">{!! Form::text('sub_cooling_a', null, ['class' => 'text-check']) !!}A </span>
                                </td>
                                <td class="col-md-3 font-bold">Indoor/ Outdoor Wb:</td>
                                <td class="col-md-3">{!! Form::text('indoor_outdoor_wb', null, ['class' => 'form-control input-box-1']) !!}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end of box -->

        <div class="grid-box-1 table-01 clearfix">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class="col-md-6 text-center" colspan="6">CIRCLE ALL THAT APPLY</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <tr>
                                <td class="col-md-2 font-bold">CONDENSER COIL:</td>
                                <td class="col-md-4">
                                    <label class="radio-inline">{!! Form::text('condenser_coil', null, ['class' => '']) !!}CLEAN/NEEDS CLEANED/CLEANED</label>
                                </td>
                                <td class="col-md-2 font-bold">REFRIGERANT LEAK:</td>
                                <td class="col-md-4">
                                    <label class="radio-inline">{!! Form::text('refrigerant_leak', null, ['class' => '']) !!}YES/NO</label>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-2 font-bold">EVAPORATOR COIL:</td>
                                <td class="col-md-4">
                                    <label class="radio-inline">{!! Form::text('evaporator_coil', null, ['class' => '']) !!}CLEAN/NEEDS CLEANED/CLEANED</label>
                                </td>
                                <td class="col-md-2 font-bold">REF. ADD?
                                    <label class="radio-inline">{!! Form::text('ref_add', null, ['class' => '']) !!}Y/N</label>
                                </td>
                                <td class="col-md-4">
                                    {!! Form::text('ref_add_qty', null, ['class' => 'form-control input-box-2','placeholder'=>'QTY']) !!}
                                    {!! Form::text('ref_add_lbs', null, ['class' => 'form-control input-box-2','placeholder'=>'Lbs.']) !!}
                                </td>
                            </tr>
                            <tr>
                                <td class="col-md-2 font-bold">DRAIN LINE:</td>
                                <td class="col-md-4">
                                    <label class="radio-inline">{!! Form::text('drain_line', null, ['class' => '']) !!}CLEAN/NEEDS CLEANED/CLEANED</label>
                                </td>
                                <td class="col-md-2 font-bold">REF. REMOVED?
                                    <label class="radio-inline">{!! Form::text('ref_removed', null, ['class' => '']) !!}Y/N</label>
                                </td>
                                <td class="col-md-4">
                                    {!! Form::text('ref_removed_qty', null, ['class' => 'form-control input-box-2','placeholder'=>'QTY']) !!}
                                    {!! Form::text('ref_removed_lbs', null, ['class' => 'form-control input-box-2','placeholder'=>'Lbs.']) !!}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end of box -->

        <div class="grid-box-1 table-01 clearfix">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table">
                        <tbody class="text-left">
                            <tr>
                                <td class="col-md-12 bt-0">
                                    {!! Form::textarea('notes', null, ['class' => 'form-control h-auto textarea1','rows'=>'3',"placeholder"=>"NOTES:"]) !!}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div> <!-- end of box -->

        <div class="grid-box-1 clearfix">

            <div class="col-md-4 col-sm-4 clearfix">
                <p class="height-auto">Tech:</p>
                {!! Form::text('tech', null, ['class' => 'form-control input-100']) !!}
            </div>

            <div class="col-md-4 col-sm-4 clearfix">
                <p class="height-auto">Occupant:</p>
                {!! Form::text('occupant', null, ['class' => 'form-control input-100']) !!}
            </div>

            <div class="col-md-4 col-sm-4 clearfix">
                <p class="height-auto">Date</p>
                {!! Form::text('bottom_date', null, ['class' => 'form-control input-100 bottom_date']) !!}
            </div>

        </div>

        <div class="grid-box-1 clearfix">
            <div class=" pull-left">
                {!! Form::submit((isset($jobInspectionForm) & $jobInspectionForm != null)? 'Update' : 'Add', ['class' => 'btn btn-primary'])
                !!}
            </div>
        </div>

    </div>
    {!! Form::close() !!}
</div>
@endsection
@push('script-head')
<script type="text/javascript">
    $(function () {
        $('.bottom_date,.HVAC_date').datetimepicker({
            format: 'DD-MM-YYYY'
        });
    })

</script>

@endpush