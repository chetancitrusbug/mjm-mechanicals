@extends('layouts.backend')

@section('title',trans('product.edit_product'))
@section('pageTitle',trans('product.edit_product'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('product.edit_product')</div>
                <div class="panel-body">
                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('document.back')
                        </button>
                    </a>
                    <br/>
                    <br/>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model($product, [
                        'method' => 'PATCH',
                        'url' => ['/admin/products', $product->id],
                        'class' => 'form-horizontal',
                        'id'=>'formProduct',
                        'enctype'=>'multipart/form-data'
                    ]) !!}


                    @include ('admin.products.form', ['submitButtonText' => trans('document.update')])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
