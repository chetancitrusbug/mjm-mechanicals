<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobWeeklyVehicleSafetyChecklistForm extends Model
{
    use SoftDeletes;

    public $table='job_weekly_vehicle_safety_checklist_form';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'job_id', 'user_id', 'date', 'vehicle_unit_number', 'license_number', 'mileage', 'driver', 'year', 'make', 'model', 'head', 'back_up', 'parking', 'cargo', 'tail', 'flashers', 'directional', 'PSI', 'front_left', 'front_right', 'rear_left', 'rear_right', 'conventional_spare', 'tires_note', 'brakes_note', 'check_brake_pedal', 'comments', 'check_brake_fluid', 'windshield_wipers', 'paint_overall_condition', 'glass_overall_condition', 'ladder_Rack', 'engine', 'note_apparent_leakage', 'engine_oil', 'oil_condition', 'mileage_of_last_oil_change', 'windshield_washer_fluid', 'transmission_fluid_condition', 'transmission_color', 'power_steering_fluid','status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    // public function getDateAttribute($value)
    // {
    //     if($value != ""){
    //         return \Carbon\Carbon::parse($value)->format('d-m-Y');
    //     }
    //     return $value;
    // }

    // public function setDateAttribute($value)
    // {
    //     if($value != ""){
    //         $this->attributes['date'] = \Carbon\Carbon::parse($value)->format('Y-m-d');
    //     }
    // }
}
