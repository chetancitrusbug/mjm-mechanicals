@extends('layouts.backend')
@section('title',trans('job.view_form'))
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">MJM New Homes Final Checklist</div>
    {!! Form::model($jobChecklistForm, [ 'method' => 'PATCH', 'url' => ['/admin/jobChecklistForm', $jobChecklistForm->id], 'class'
    => 'form-horizontal','id'=>'jobChecklistForm', 'enctype'=>'multipart/form-data' ]) !!}
    <div class="panel-body">
        <!--<a href="http://13.127.235.254/dev/laravel/mjm-mechanicals/public/admin/job" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>Back</button></a>-->
        <a href="{{ url('/admin/job/' . $job->id . '/edit') }}" title="Back to Forms list"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true" ></i>Back</button></a>
        {!! Form::hidden('job_id', $job->id, ['class' => 'form-control input-100']) !!}

        <div class="grid-box-1 box-HVAC-checklist clearfix">
            <div class="col-md-12 col-sm-12 clearfix">
                <div class="row">
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('all_register_open', 1, old('all_register_open',isset($jobChecklistForm->all_register_open)?$jobChecklistForm->all_register_open:''), ['class' => '']) !!}Install and account for all grills and registers-All registers open.</label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('install_toe_kicks', 1, old('install_toe_kicks',isset($jobChecklistForm->install_toe_kicks)?$jobChecklistForm->install_toe_kicks:''), ['class' => '']) !!}Install all toe kicks.</label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('install_thermostat', 1, old('install_thermostat',isset($jobChecklistForm->install_thermostat)?$jobChecklistForm->install_thermostat:''), ['class' => '']) !!}Install thermostat, make sure it is on and programmed. Time &amp; Date.</label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('furnace_cleaned', 1, old('furnace_cleaned',isset($jobChecklistForm->furnace_cleaned)?$jobChecklistForm->furnace_cleaned:''), ['class' => '']) !!}Furnace should be cleaned and ensure all corner and joints are sealed. </label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('place_new_filter', 1, old('place_new_filter',isset($jobChecklistForm->place_new_filter)?$jobChecklistForm->place_new_filter:''), ['class' => '']) !!}
                            <span class="pull-left">Place new filter. </span>
                            <p></p><p><span class="span-right pull-left clearfix"> Size  </span>{!! Form::text('place_new_filter_size', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p></p><p><span class="span-right pull-left clearfix"> Type  </span>{!! Form::text('place_new_filter_type', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                        </label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('set_ac_and_level', 1, old('set_ac_and_level',isset($jobChecklistForm->set_ac_and_level)?$jobChecklistForm->set_ac_and_level:''), ['class' => '']) !!}
                            <span class="pull-left">Set AC and level. </span>
                            <p></p><p><span class="span-right pull-left clearfix"> Pad size  </span>{!! Form::text('pad_size', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p></p><p><span class="span-right pull-left clearfix"> Basement Rack  </span>{!! Form::text('basement_rack', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                        </label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline clearfix">{!! Form::checkbox('weigh_in_charge', 1, old('weigh_in_charge',isset($jobChecklistForm->weigh_in_charge)?$jobChecklistForm->weigh_in_charge:''), ['class' => '']) !!}
                            <span class="pull-left clearfix">Weigh-in refrigerant charge.</span>
                            <p><span class="span-right pull-left clearfix"> Pre charge  </span>{!! Form::text('pre_charge', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p><span class="span-right pull-left clearfix"> Total added  </span>{!! Form::text('total_added', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p><span class="pull-left "> Line set length  </span>{!! Form::text('line_set_length', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p class="heigth-50"><span class="span-right pull-left clearfix"> Note inside of AC panel </span>{!! Form::text('inside_of_ac_panel', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                        </label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('install_start_assist', 1, old('install_start_assist',isset($jobChecklistForm->install_start_assist)?$jobChecklistForm->install_start_assist:''), ['class' => '']) !!}
                            <span class="pull-left">Install Start assist.</span>
                            <p><span class="span-right pull-left clearfix"> SPP6  </span>{!! Form::text('spp6', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p><span class="span-right pull-left clearfix"> Factory kit </span>{!! Form::text('factory_kit', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                        </label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('verify_fireplace_valve', 1, old('verify_fireplace_valve',isset($jobChecklistForm->verify_fireplace_valve)?$jobChecklistForm->verify_fireplace_valve:''), ['class' => '']) !!}Verify Fireplace valve is open. </label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline clearfix">{!! Form::checkbox('startup_furnace', 1, old('startup_furnace',isset($jobChecklistForm->startup_furnace)?$jobChecklistForm->startup_furnace:''), ['class' => '']) !!}
                            <p><span class="pull-left clearfix">Start-up furnace. Gas pressure set.</span>
                            </p><p><span class="span-right pull-left clearfix"> Lo </span>{!! Form::text('gas_pressure_lo', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p><span class="span-right pull-left clearfix">  HI </span>{!! Form::text('gas_pressure_hi', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p><span class="span-right pull-left span-right"> Fan Speed </span>{!! Form::text('fan_speed', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p><span class="span-right pull-left clearfix"> Temp Rise </span>{!! Form::text('temp_rise', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p class="heigth-50"><span class="span-right pull-left clearfix"> AC/HP Fan Speed </span>{!! Form::text('ac_hp_fan_speed', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p> <p></p>
                        </label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('stickers_on_equipment', 1, old('stickers_on_equipment',isset($jobChecklistForm->stickers_on_equipment)?$jobChecklistForm->stickers_on_equipment:''), ['class' => '']) !!}Stickers on all equipment.  </label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline block-label h-50">{!! Form::checkbox('drain_installed', 1, old('drain_installed',isset($jobChecklistForm->drain_installed)?$jobChecklistForm->drain_installed:''), ['class' => '']) !!}
                            <span class="pull-left">Drain Installed.</span>
                            <p><span class="span-right pull-left clearfix"> Floor Drain  </span>{!! Form::text('floor_drain', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p><span class="span-right pull-left clearfix"> Sump pit </span>{!! Form::text('sump_pit', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p><span class="span-right pull-left clearfix"> Condo pump </span>{!! Form::text('condo_pump', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                        </label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline block-label h-50">{!! Form::checkbox('flue_pipes', 1, old('flue_pipes',isset($jobChecklistForm->flue_pipes)?$jobChecklistForm->flue_pipes:''), ['class' => '']) !!}
                            <span class="pull-left">Flue Pipes.</span>
                            <p><span class="span-right pull-left clearfix"> Size </span>{!! Form::text('flue_pipes_size', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p><span class="span-right pull-left clearfix"> Sidewall </span>{!! Form::text('flue_pipes_side_wall', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p><span class="span-right pull-left clearfix"> Roof </span>{!! Form::text('flue_pipes_roof', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p><span class="pull-left"> 2 pipe </span>{!! Form::text('flue_pipes_2pipe', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p><span class="span-right pull-left clearfix"> Concentric </span>{!! Form::text('flue_pipes_concentric', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p><p></p>
                        </label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline block-label h-50">{!! Form::checkbox('stat_wire_size', 1, old('stat_wire_size',isset($jobChecklistForm->stat_wire_size)?$jobChecklistForm->stat_wire_size:''), ['class' => '']) !!}
                            <span class="pull-left">Stat Wire Size.</span>
                            <p><span class="span-right pull-left clearfix"> Thermostat  </span>{!! Form::text('thermostat', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p><span class="span-right pull-left clearfix"> AC/HP </span>{!! Form::text('ac_hp', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                        </label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline block-label h-50">{!! Form::checkbox('humidifier', 1, old('humidifier',isset($jobChecklistForm->humidifier)?$jobChecklistForm->humidifier:''), ['class' => '']) !!}
                            <span class="pull-left">Humidifier. </span>
                            <p><span class="span-right pull-left clearfix">  By-pass  </span>{!! Form::text('by_pass', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p><span class="span-right pull-left clearfix"> Powered </span>{!! Form::text('powered', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p><span class="span-right pull-left clearfix"> Water line installed </span>{!! Form::text('water_line_installed', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p><span class="pull-left"> Tested </span>{!! Form::text('tested', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p><span class="span-right pull-left clearfix"> Control Type- Stat </span>{!! Form::text('control_type_stat', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p class="heigth-50"><span class="span-right pull-left clearfix">  Manufacture control </span>{!! Form::text('manufacture_control', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                            <p class="heigth-50"><span class="pull-left"> Brand </span>{!! Form::text('brand', null, ['class' => 'form-control w-20per input-20 pull-left']) !!}</p>
                        </label>
                    </div>
                    <div class="col-md-12 col-sm-12 mt-20 clearfix">
                        <label class="checkbox-inline h-50">{!! Form::checkbox('fresh_air_damper', 1, old('fresh_air_damper',isset($jobChecklistForm->fresh_air_damper)?$jobChecklistForm->fresh_air_damper:''), ['class' => '']) !!}Fresh Air Damper Installed and Tested. </label>
                    </div>
                    <div class="col-md-12 col-sm-12 clearfix">
                        <label class="checkbox-inline">{!! Form::checkbox('ac_disconnect_installed', 1, old('ac_disconnect_installed',isset($jobChecklistForm->ac_disconnect_installed)?$jobChecklistForm->ac_disconnect_installed:''), ['class' => '']) !!}AC Disconnect Installed and Wired. / Furnace Final Wired. </label>
                    </div>
                </div>
            </div>
        </div><!-- End of box-div -->

        <div class="grid-box-1 clearfix">
            <div class=" pull-left">
                {!! Form::submit((isset($jobChecklistForm) & $jobChecklistForm != null)? 'Update' : 'Add', ['class' => 'btn btn-primary'])
                !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endsection