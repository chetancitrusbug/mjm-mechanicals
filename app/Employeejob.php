<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employeejob extends Model
{
    public $table='employeejob';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'jobtask_id', 'job_id','employee_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
