<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\EmailController;
use App\Job;
use App\User;
use App\Folder;
use App\JobGeothermalForm;
use App\JobFurnaceForm;
use App\JobInspectionForm;
use App\JobChecklistForm;
use App\JobMaintenanceForm;
use App\JobSystemRepairForm;
use App\JobLogs;
use App\JobPlumbingForm;
use App\JobInvoice;
use App\JobInvoiceOtherField;
use App\JobWeeklyVehicleSafetyChecklistForm;
use App\JobProjrctWorksheet1;
use App\JobProjrctWorksheet2;
use App\JobProjrctWorksheet3;
use App\JobProjrctWorksheet4;
use App\Jobcode;
use App\Forms;
use App\JobForms;
use DB;
use Mail;

class JobFormsController extends Controller
{
    // forms
    public function jobFormsList(Request $request){
        $data['list'] = [];
        $code = 200;
        $message = 'Listed Successfully';
        $status = 'true';

        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $flag = 0;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {

            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();


            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }
             if($status){
                $jobForms = JobForms::select('forms.*','jobsforms.*','forms.title as form_title','jobsforms.created_at as form_created_at')->join('forms','forms.id','jobsforms.form_id')->orderBy('jobsforms.id','DESC')->where('jobsforms.job_id',$request->job_id)->orderby('jobsforms.id','DESC')->get();

                if ($jobForms != null) {
                    $data['list'] = $jobForms;
                }
            }

        }
        return response()->json(['data' => $data, 'code' => $code, 'message' => $message, 'status' => $status]);

    }

    public function addJobGeothermalForm(Request $request){
        $data = [];
        $message = "Added Successfully";
        $status = true;
        $code = 200;

        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
        );
        if ($request->install_date != null) {
            $rules['install_date'] = 'required|date|date_format:Y-m-d';
        }
        if ($request->desuperheater != null) {
            $rules['desuperheater'] = 'required|in:Y,N';
        }

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();

            $jobGeothermalForm = JobGeothermalForm::where('job_id', $request->job_id)->where('status', 'active')->first();


            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }
            // else if($jobGeothermalForm != null){
            //     $status = false;
            //     $message = 'Already Added form for this job';
            //     $code = 400;
            // }

            if($status){
                $requestData = $request->except('user_id','api_token');
                $requestData['created_by'] = $user_id;
                $data = JobGeothermalForm::create($requestData);

                $forms= Forms::where('form_code', 'geo_thermal')->first();
                if(isset($forms->id))
                {
                    $formdata['job_id'] = $job_id;
                    $formdata['job_form_id'] = $data->id;
                    $formdata['form_id'] = $forms->id;
                    JobForms::create($formdata);
                }

            }
        }
        $data = (object) $data;
        return response()->json(['data' => $data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function jobGeothermalFormList(Request $request){
        $data = [];
        $message = "Success";
        $status = true;
        $flag = 0;
        $code = 200;
        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'job_form_id'=>'required|integer'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $flag = 0;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();

            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
                $flag = 0;

            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
                $flag = 0;

            }


            if($status){
                $data = JobGeothermalForm::select('job_geothermal_form.*','job.title as job_name')->join('job','job_geothermal_form.job_id','job.id')->where('job_geothermal_form.id',$request->job_form_id)->first();

                if($data == null){
                    $status = false;
                    $message = 'No Form Found';
                    $code = 400;
                    $data = [];
                    $flag = 0;
                }
                else {
                    $resArr = array();
                    $resArr['job_information'] = array('job_name' => $data->job_name, 'model' => $data->model,
                        'job_geothermal_form_id' => $data->id,
                        'serial' => $data->serial,
                        'install_date' => $data->install_date,
                        'open_closed_loop' => $data->open_closed_loop,
                        'desuperheater' => $data->desuperheater);
                    $resArr['flow_rate_in_gpm'] = array(
                        'water_in_pressure_source_coax' => $data->source_coax_a,
                        'water_out_pressure_source_coax' => $data->source_coax_b,
                        'pressure_drop_source_coax' => $data->source_coax_c,
                        'rate_in_table_source_coax' => $data->source_coax_d,

                        'water_in_pressure_load_coax' => $data->load_coax_a,
                        'water_out_pressure_load_coax' => $data->load_coax_b,
                        'pressure_drop_load_coax' => $data->load_coax_c,
                        'rate_in_table_load_coax' => $data->load_coax_d);
                    $resArr['temperature_rise_drop_across_coaxial_heat_exchanger'] = array(
                        'water_in_temperature_cooling' => $data->temperature_rise_drop_across_coaxial_heat_exchanger_cooling_e,
                        'water_out_temperature_cooling' => $data->temperature_rise_drop_across_coaxial_heat_exchanger_cooling_f,
                        'temperature_difference_cooling' => $data->temperature_rise_drop_across_coaxial_heat_exchanger_cooling_g,

                        'water_in_temperature_heating' => $data->temperature_rise_drop_across_coaxial_heat_exchanger_heating_e,
                        'water_out_temperature_heating' => $data->temperature_rise_drop_across_coaxial_heat_exchanger_heating_f,
                        'temperature_difference_heating' => $data->temperature_rise_drop_across_coaxial_heat_exchanger_heating_g);

                    $resArr['temperature_rise_drop_across_air_coil'] = array(
                        'suppy_air_temperature_cooling' => $data->temperature_rise_drop_across_air_coil_cooling_h,
                        'return_air_temperature_cooling' => $data->temperature_rise_drop_across_air_coil_cooling_i,
                        'temperature_difference_cooling' => $data->temperature_rise_drop_across_air_coil_cooling_j,

                        'suppy_air_temperature_heating' => $data->temperature_rise_drop_across_air_coil_heating_h,
                        'return_air_temperature_heating' => $data->temperature_rise_drop_across_air_coil_heating_i,
                        'temperature_difference_heating' => $data->temperature_rise_drop_across_air_coil_heating_j);

                    $resArr['temperature_rise_drop_across_air_coil_load_coax'] = array(
                        'lwt_cooling' => $data->load_coax_cooling_h,
                        'ewt_cooling_1' => $data->load_coax_cooling_i,
                        'ewt_cooling_2' => $data->load_coax_cooling_j,

                        'lwt_heating' => $data->load_coax_heating_h,
                        'ewt_heating_1' => $data->load_coax_heating_i,
                        'ewt_heating_2' => $data->load_coax_heating_j);

                    $resArr['HR_HE'] = array(
                        'brine_factor' => $data->HR_HE_brine_factor_k,
                        'HR_HE_cooling' => $data->HR_cooling_i,
                        'HR_HE_heating' => $data->HE_heating_i);

                    $resArr['watts'] = array(
                        'volts_cooling' => $data->watts_cooling_m,
                        'total_amps_cooling' => $data->watts_cooling_n,
                        'watts_cooling' => $data->watts_cooling_o,

                        'volts_heating' => $data->watts_heating_m,
                        'total_amps_heating' => $data->watts_heating_n,
                        'watts_heating' => $data->watts_heating_o);

                    $resArr['capacity'] = array(
                        'cooling_capacity' => $data->capacity_cooling_p,
                        'heating_capacity' => $data->capacity_heating_p);

                    $resArr['efficiency'] = array(
                        'cooling_EER' => $data->efficiency_cooling_q,
                        'heating_COP' => $data->efficiency_heating_q);

                    $resArr['SH_SC_cooling'] = array(
                        'suction_pressure' => $data->SH_SC_cooling_r,
                        'suction_saturation_temperature' => $data->SH_SC_cooling_s,
                        'suction_line_temperature' => $data->SH_SC_cooling_t,
                        'SH' => $data->SH_SC_cooling_u,
                        'head_pressure' => $data->SH_SC_cooling_v,
                        'high_pressure_saturation_temperature' => $data->SH_SC_cooling_w,
                        'liquid_line_temperature' => $data->SH_SC_cooling_x);

                    $resArr['SH_SC_heating'] = array(
                        'suction_pressure' => $data->SH_SC_heating_r,
                        'suction_saturation_temperature' => $data->SH_SC_heating_s,
                        'suction_line_temperature' => $data->SH_SC_heating_t,
                        'SH' => $data->SH_SC_heating_u,
                        'head_pressure' => $data->SH_SC_heating_v,
                        'high_pressure_saturation_temperature' => $data->SH_SC_heating_w,
                        'liquid_line_temperature' => $data->SH_SC_heating_x);

                        $flag = 1;
                        $data = $resArr;
                }
            }
        }
        $data = (object)$data;
        return response()->json(['flag' => $flag,'data' => $data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function editJobGeothermalForm(Request $request){
        $data = [];
        $message = "Updated Successfully";
        $status = true;
        $code = 200;

        $rules = array(
            'job_geothermal_form_id' => 'required|integer',
            'user_id' => 'required|integer',
            'job_id' => 'required|integer',
        );

        if ($request->install_date != null) {
            $rules['install_date'] = 'required|date|date_format:Y-m-d';
        }
        if ($request->desuperheater != null) {
            $rules['desuperheater'] = 'required|in:Y,N';
        }

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users = User::where('users.id', $user_id)->first();
            $job = Job::where('job.id', $job_id)->first();
            $jobGeothermalForm = JobGeothermalForm::where('id', $request->job_geothermal_form_id)->where('status', 'active')->first();

            if (!$users) {
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            } else if (!$job) {
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }

             if($jobGeothermalForm == null){
                $status = false;
                $message = 'No Form Found';
                $code = 400;
            }


            if ($status) {
                $requestData = $request->except('user_id', 'api_token','job_geothermal_form_id','job_id');
                $data = $jobGeothermalForm->update($requestData);
            }
        }
        $data = (object) $data;

        return response()->json(['data'=>$data,'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function addJobFurnaceForm(Request $request){
        $data = (object)[];
        $message = "Added Successfully";
        $status = true;
        $code = 200;

        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'date'=>'nullable|date|date_format:Y-m-d',
            'email'=>'nullable|email',
            'system_age'=>'nullable|integer',
            'staying_warm_enough'=>'nullable|in:y,n',
            'temrature_consistent'=>'nullable|in:y,n',
            'dry_inside_home'=>'nullable|in:y,n',
            'have_any_weird_smells'=>'nullable|in:y,n',
            'accessories_1'=>'nullable|in:humidifier,condensate_pump,uv_light',
            'accessories_2'=>'nullable|in:electronic_air_cleaner,carbon_monoxide_detector',
            'accessories_3'=>'nullable|in:gps',
            'gas_type'=>'nullable|in:natural,propane,oil,none',
            'blower'=>'nullable|in:psc,x13,ecm',
            'ignition'=>'nullable|in:spark,standing_pilot,hsi',
            'stages'=>'nullable|in:1,2,3,mod',
            'draft_meter'=>'nullable|in:none,std,ecm',
            'efficiency'=>'nullable|in:electric,80+,92+,95+,98+',
            'temp_humidity'=>'nullable|in:indoor,oa,rh',
            'tstat_type'=>'nullable|in:mechanical,digital,programmable,communicating',
            // 'motor_bearings'=>'nullable|in:good,tight,need_replaced',
            // 'blower_wheel'=>'nullable|in:clean,need_cleaned,out_of_balance',
            // 'heat_exchanger'=>'nullable|in:ok,sign_of_strees,cracked',
            // 'burners'=>'nullable|in:clean,rusty,replace',
            // 'ignitor'=>'nullable|in:ok,replace',
            // 'limits'=>'nullable|in:pass,fail',
            'bottom_date'=>'nullable|date|date_format:Y-m-d',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();
            $jobFurnaceForm = JobFurnaceForm::where('job_id', $request->job_id)->where('status', 'active')->first();

            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }
            // else if($jobFurnaceForm != null){
            //     $status = false;
            //     $message = 'Already Added form for this job';
            //     $code = 400;
            // }

            if($status){
                $requestData = $request->except('api_token');
                $requestData['user_id'] = $user_id;
                $data = JobFurnaceForm::create($requestData);
                $data = (object)(make_null(collect($data)));
                $forms = Forms::where('form_code', 'HVAC_inspection_furnace')->first();

                if(isset($forms->id))
                {
                    $formdata['job_id'] = $job_id;
                    $formdata['job_form_id'] = $data->id;
                    $formdata['form_id'] = $forms->id;
                    JobForms::create($formdata);
                }
            }
        }
        return response()->json(['data' => $data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'create']);
    }

    public function editJobFurnaceForm(Request $request){
        $data = (object)[];
        $message = "Updated Successfully";
        $status = true;
        $code = 200;

        $rules = array(
            'job_furnace_form_id' => 'required|integer',
            'user_id' => 'required|integer',
            'job_id' => 'required|integer',
            'date'=>'nullable|date|date_format:Y-m-d',
            'email'=>'nullable|email',
            'system_age'=>'nullable|integer',
            'staying_warm_enough'=>'nullable|in:y,n',
            'temrature_consistent'=>'nullable|in:y,n',
            'dry_inside_home'=>'nullable|in:y,n',
            'have_any_weird_smells'=>'nullable|in:y,n',
            'accessories_1'=>'nullable|in:humidifier,condensate_pump,uv_light',
            'accessories_2'=>'nullable|in:electronic_air_cleaner,carbon_monoxide_detector',
            'accessories_3'=>'nullable|in:gps',
            'gas_type'=>'nullable|in:natural,propane,oil,none',
            'blower'=>'nullable|in:psc,x13,ecm',
            'ignition'=>'nullable|in:spark,standing_pilot,hsi',
            'stages'=>'nullable|in:1,2,3,mod',
            'draft_meter'=>'nullable|in:none,std,ecm',
            'efficiency'=>'nullable|in:electric,80+,92+,95+,98+',
            'temp_humidity'=>'nullable|in:indoor,oa,rh',
            'tstat_type'=>'nullable|in:mechanical,digital,programmable,communicating',
            // 'motor_bearings'=>'nullable|in:good,tight,need_replaced',
            // 'blower_wheel'=>'nullable|in:clean,need_cleaned,out_of_balance',
            // 'heat_exchanger'=>'nullable|in:ok,sign_of_strees,cracked',
            // 'burners'=>'nullable|in:clean,rusty,replace',
            // 'ignitor'=>'nullable|in:ok,replace',
            // 'limits'=>'nullable|in:pass,fail',
            'bottom_date'=>'nullable|date|date_format:Y-m-d',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users = User::where('users.id', $user_id)->first();
            $job = Job::where('job.id', $job_id)->first();
            $jobFurnaceForm = JobFurnaceForm::where('id', $request->job_furnace_form_id)->where('status', 'active')->first();

            if (!$users) {
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            } else if (!$job) {
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }

             if($jobFurnaceForm == null){
                $status = false;
                $message = 'No Form Found';
                $code = 400;
            }


            if ($status) {
                $requestData = $request->except('user_id', 'api_token','job_furnace_form_id','job_id');
                $data = $jobFurnaceForm->update($requestData);
                $data = (object)(make_null(collect($requestData)));
            }
        }

        return response()->json(['data'=>$data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'update']);
    }

    public function jobFurnaceFormList(Request $request){
        $data = (object)[];
        $message = "Success";
        $status = true;
        $flag = 1;
        $code = 200;
        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'job_form_id'=>'required|integer'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $flag = 1;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();

            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
                $flag = 1;

            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
                $flag = 1;

            }


            if($status){
                $data = JobFurnaceForm::select('job_furnace_form.*','job.title as job_name')->join('job','job_furnace_form.job_id','job.id')->where('job_furnace_form.id',$request->job_form_id)->first();

                if($data == null){
                    $status = false;
                    $message = 'No Form Found';
                    $code = 400;
                    $data = (object)[];
                    $flag = 1;
                }
                else {
                    $flag = 0;
                    $data = (object)(make_null($data));
                }
            }
        }
        return response()->json(['flag' => $flag,'data' => $data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'list']);
    }

    public function addJobInspectionForm(Request $request){
        $data = (object)[];
        $message = "Added Successfully";
        $status = true;
        $code = 200;

        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'date'=>'nullable|date|date_format:Y-m-d',
            'email'=>'nullable|email',
            'system_age'=>'nullable|integer',
            'stay_cool_enough'=>'nullable|in:y,n',
            'has_been_cycling'=>'nullable|in:y,n',
            'temperature_consistent'=>'nullable|in:y,n',
            'have_any_weird_smells'=>'nullable|in:y,n',
            'increase_electric_bill'=>'nullable|in:y,n',
            'accessories_1'=>'nullable|in:humidifier,condensate_pump,uv_light',
            'accessories_2'=>'nullable|in:electronic_air_cleaner,carbon_monoxide_detector',
            'accessories_3'=>'nullable|in:gps',
            'seer'=>'nullable|in:6,7,8,10,12,13,15+',
            'stages'=>'nullable|in:1,2,vs,mod',
            'fan_type'=>'nullable|in:psc,ecm',
            'cap'=>'nullable|in:run,start',
            'disconnect'=>'nullable|in:y,n',
            'refrigerant'=>'nullable|in:r22,r410a',
            // 'condenser_coil'=>'nullable|in:clean,needs_cleaned,cleaned',
            // 'evaporator_coil'=>'nullable|in:clean,needs_cleaned,cleaned',
            // 'drain_line'=>'nullable|in:clean,needs_cleaned,cleaned',
            // 'refrigerant_leak'=>'nullable|in:y,n',
            // 'ref_add'=>'nullable|in:y,n',
            // 'ref_removed'=>'nullable|in:y,n',
            'bottom_date'=>'nullable|date|date_format:Y-m-d',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();

            $jobInspectionForm = JobInspectionForm::where('job_id', $request->job_id)->where('status', 'active')->first();

            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }
            // else if($jobInspectionForm != null){
            //     $status = false;
            //     $message = 'Already Added form for this job';
            //     $code = 400;
            // }

            if($status){
                $requestData = $request->except('api_token');
                $requestData['user_id'] = $user_id;
                $data = JobInspectionForm::create($requestData);
                $data = (object)(make_null(collect($data)));

                $forms = Forms::where('form_code', 'HVAC_inspection')->first();

                if(isset($forms->id))
                {
                    $formdata['job_id'] = $job_id;
                    $formdata['job_form_id'] = $data->id;
                    $formdata['form_id'] = $forms->id;
                    JobForms::create($formdata);
                }

            }
        }
        return response()->json(['data' => $data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'create']);
    }

    public function editJobInspectionForm(Request $request){
        $data = (object)[];
        $message = "Updated Successfully";
        $status = true;
        $code = 200;

        $rules = array(
            'job_inspection_form_id' => 'required|integer',
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'date'=>'nullable|date|date_format:Y-m-d',
            'email'=>'nullable|email',
            'system_age'=>'nullable|integer',
            'stay_cool_enough'=>'nullable|in:y,n',
            'has_been_cycling'=>'nullable|in:y,n',
            'temperature_consistent'=>'nullable|in:y,n',
            'have_any_weird_smells'=>'nullable|in:y,n',
            'increase_electric_bill'=>'nullable|in:y,n',
            'accessories_1'=>'nullable|in:humidifier,condensate_pump,uv_light',
            'accessories_2'=>'nullable|in:electronic_air_cleaner,carbon_monoxide_detector',
            'accessories_3'=>'nullable|in:gps',
            'seer'=>'nullable|in:6,7,8,10,12,13,15+',
            'stages'=>'nullable|in:1,2,vs,mod',
            'fan_type'=>'nullable|in:psc,ecm',
            'cap'=>'nullable|in:run,start',
            'disconnect'=>'nullable|in:y,n',
            'refrigerant'=>'nullable|in:r22,r410a',
            // 'condenser_coil'=>'nullable|in:clean,needs_cleaned,cleaned',
            // 'evaporator_coil'=>'nullable|in:clean,needs_cleaned,cleaned',
            // 'drain_line'=>'nullable|in:clean,needs_cleaned,cleaned',
            // 'refrigerant_leak'=>'nullable|in:y,n',
            // 'ref_add'=>'nullable|in:y,n',
            // 'ref_removed'=>'nullable|in:y,n',
            'bottom_date'=>'nullable|date|date_format:Y-m-d',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users = User::where('users.id', $user_id)->first();
            $job = Job::where('job.id', $job_id)->first();
            $jobInspectionForm = JobInspectionForm::where('id', $request->job_inspection_form_id)->where('status', 'active')->first();

            if (!$users) {
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            } else if (!$job) {
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }

             if($jobInspectionForm == null){
                $status = false;
                $message = 'No Form Found';
                $code = 400;
            }


            if ($status) {
                $requestData = $request->except('user_id', 'api_token','job_inspection_form_id','job_id');
                $data = $jobInspectionForm->update($requestData);
                $data = (object)(make_null(collect($requestData)));
            }
        }

        return response()->json(['data'=>$data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'update']);
    }

    public function jobInspectionFormList(Request $request){
        $data = (object)[];
        $message = "Success";
        $status = true;
        $flag = 1;
        $code = 200;
        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'job_form_id'=>'required|integer'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $flag = 1;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();

            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
                $flag = 1;

            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
                $flag = 1;

            }


            if($status){
                $data = JobInspectionForm::select('job_inspection_form.*','job.title as job_name')->join('job','job_inspection_form.job_id','job.id')->where('job_inspection_form.id',$request->job_form_id)->first();

                if($data == null){
                    $status = false;
                    $message = 'No Form Found';
                    $code = 400;
                    $data = (object)[];
                    $flag = 1;
                }
                else {
                    $flag = 1;
                    $data = (object)(make_null($data));
                }
            }
        }
        return response()->json(['flag' => $flag,'data' => $data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'list']);
    }

    public function addJobChecklistForm(Request $request){
        $data = (object)[];
        $message = "Added Successfully";
        $status = true;
        $code = 200;

        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();

            $jobChecklistForm = JobChecklistForm::where('job_id', $request->job_id)->where('status', 'active')->first();

            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }
            // else if($jobChecklistForm != null){
            //     $status = false;
            //     $message = 'Already Added form for this job';
            //     $code = 400;
            // }

            if($status){
                $requestData = $request->except('api_token');
                $requestData['user_id'] = $user_id;
                $data = JobChecklistForm::create($requestData);
                $data = (object)(make_null(collect($data)));

                $forms = Forms::where('form_code', 'home_final_checklist')->first();

                if(isset($forms->id))
                {
                    $formdata['job_id'] = $job_id;
                    $formdata['job_form_id'] = $data->id;
                    $formdata['form_id'] = $forms->id;
                    JobForms::create($formdata);
                }

            }
        }
        return response()->json(['data' => $data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'create']);
    }

    public function editJobChecklistForm(Request $request){
        $data = (object)[];
        $message = "Updated Successfully";
        $status = true;
        $code = 200;

        $rules = array(
            'job_checklist_form_id' => 'required|integer',
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users = User::where('users.id', $user_id)->first();
            $job = Job::where('job.id', $job_id)->first();
            $jobChecklistForm = JobChecklistForm::where('id', $request->job_checklist_form_id)->where('status', 'active')->first();

            if (!$users) {
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            } else if (!$job) {
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }

             if($jobChecklistForm == null){
                $status = false;
                $message = 'No Form Found';
                $code = 400;
            }


            if ($status) {
                $requestData = $request->except('user_id', 'api_token','job_checklist_form_id','job_id');
                $data = $jobChecklistForm->update($requestData);
                $data = (object)(make_null(collect($requestData)));
            }
        }

        return response()->json(['data'=>$data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'update']);
    }

    public function jobChecklistFormList(Request $request){
        $data = (object)[];
        $message = "Success";
        $status = true;
        $flag = 1;
        $code = 200;
        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'job_form_id'=>'required|integer'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $flag = 1;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();

            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
                $flag = 1;

            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
                $flag = 1;

            }


            if($status){
                $data = JobChecklistForm::select('job_checklist_form.*','job.title as job_name')->join('job','job_checklist_form.job_id','job.id')->where('job_checklist_form.id',$request->job_form_id)->first();

                if($data == null){
                    $status = false;
                    $message = 'No Form Found';
                    $code = 400;
                    $data = (object)[];
                    $flag = 1;
                }
                else {
                    $flag = 0;
                    $data = (object)(make_null($data));
                }
            }
        }
        return response()->json(['flag' => $flag,'data' => $data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'list']);
    }

    public function addJobMaintenanceForm(Request $request){
        $data = (object)[];
        $message = "Added Successfully";
        $status = true;
        $code = 200;

        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'date'=>'nullable|date|date_format:Y-m-d',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();

            $jobMaintenanceForm = JobMaintenanceForm::where('job_id', $request->job_id)->where('status', 'active')->first();

            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }
            // else if($jobMaintenanceForm != null){
            //     $status = false;
            //     $message = 'Already Added form for this job';
            //     $code = 400;
            // }

            if($status){
                $requestData = $request->except('api_token');
                $requestData['user_id'] = $user_id;
                $data = JobMaintenanceForm::create($requestData);
                $data = (object)(make_null(collect($data)));


                $forms = Forms::where('form_code', 'residential_maintenance')->first();

                if(isset($forms->id))
                {
                    $formdata['job_id'] = $job_id;
                    $formdata['job_form_id'] = $data->id;
                    $formdata['form_id'] = $forms->id;
                    JobForms::create($formdata);
                }
            }
        }

        return response()->json(['data' => $data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'create']);
    }

    public function editJobMaintenanceForm(Request $request){
        $data = (object)[];
        $message = "Updated Successfully";
        $status = true;
        $code = 200;

        $rules = array(
            'job_maintenance_form_id' => 'required|integer',
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'date'=>'nullable|date|date_format:Y-m-d',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users = User::where('users.id', $user_id)->first();
            $job = Job::where('job.id', $job_id)->first();
            $jobMaintenanceForm = JobMaintenanceForm::where('id', $request->job_maintenance_form_id)->where('status', 'active')->first();

            if (!$users) {
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            } else if (!$job) {
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }

             if($jobMaintenanceForm == null){
                $status = false;
                $message = 'No Form Found';
                $code = 400;
            }


            if ($status) {
                $requestData = $request->except('user_id', 'api_token','job_maintenance_form_id','job_id');
                $data = $jobMaintenanceForm->update($requestData);
                $data = (object)(make_null(collect($requestData)));
            }
        }

        return response()->json(['data'=>$data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'update']);
    }

    public function jobMaintenanceFormList(Request $request){
        $data = (object)[];
        $message = "Success";
        $status = true;
        $flag = 1;
        $code = 200;
        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'job_form_id'=>'required|integer'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $flag = 1;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();

            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
                $flag = 1;

            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
                $flag = 1;

            }


            if($status){
                $data = JobMaintenanceForm::select('job_maintenance_form.*','job.title as job_name')->join('job','job_maintenance_form.job_id','job.id')->where('job_maintenance_form.id',$request->job_form_id)->first();

                if($data == null){
                    $status = false;
                    $message = 'No Form Found';
                    $code = 400;
                    $data = (object)[];
                    $flag = 1;
                }
                else {
                    $flag = 0;
                    $data = (object)(make_null($data));
                }
            }
        }
        return response()->json(['flag' => $flag,'data' => $data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'list']);
    }

    public function addJobSystemRepairForm(Request $request){
        $data = (object)[];
        $message = "Added Successfully";
        $status = true;
        $code = 200;

        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'date'=>'nullable|date|date_format:Y-m-d',
            'age_equipment_cooling_and_heat'=>'nullable|integer',
            'age_equipment_furnace_and_boilers'=>'nullable|integer',
            'age_equipment_packaged_units'=>'nullable|integer',
            'estimate_cost_cooling_and_heat'=>'nullable|integer',
            'estimate_cost_furnace_and_boilers'=>'nullable|integer',
            'estimate_cost_packaged_units'=>'nullable|integer',
            'repairs_under_warranty_cooling_and_heat'=>'nullable|integer',
            'repairs_under_warranty_furnace_and_boilers'=>'nullable|integer',
            'repairs_under_warranty_packaged_units'=>'nullable|integer',
            'seer_of_equipment_cooling_and_heat'=>'nullable|integer',
            'seer_of_equipment_packaged_units'=>'nullable|integer',
            'type_of_refrigerant_cooling_and_heat'=>'nullable|integer',
            'type_of_refrigerant_packaged_units'=>'nullable|integer',
            'afue_furnace_and_boilers'=>'nullable|integer',
            'afue_packaged_units'=>'nullable|integer',
            'outdoor_equipment_cooling_and_heat'=>'nullable|integer',
            'outdoor_equipment_packaged_units'=>'nullable|integer',
            'indoor_equipment_cooling_and_heat'=>'nullable|integer',
            'indoor_equipment_furnace_and_boilers'=>'nullable|integer',
            'owner_expects_cooling_and_heat'=>'nullable|integer',
            'owner_expects_furnace_and_boilers'=>'nullable|integer',
            'owner_expects_packaged_units'=>'nullable|integer',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();

            $jobSystemReplaceForm = JobSystemRepairForm::where('job_id', $request->job_id)->where('status', 'active')->first();

            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }
            // else if($jobSystemReplaceForm != null){
            //     $status = false;
            //     $message = 'Already Added form for this job';
            //     $code = 400;
            // }

            if($status){
                $requestData = $request->except('api_token');
                $requestData['user_id'] = $user_id;
                $data = JobSystemRepairForm::create($requestData);
                $data = (object)(make_null(collect($data)));

                $forms = Forms::where('form_code', 'indoor_comfort_system_repair')->first();

                if(isset($forms->id))
                {
                    $formdata['job_id'] = $job_id;
                    $formdata['job_form_id'] = $data->id;
                    $formdata['form_id'] = $forms->id;
                    JobForms::create($formdata);
                }
            }
        }

        return response()->json(['data' => $data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'create']);
    }

    public function editJobSystemRepairForm(Request $request){
        $data = (object)[];
        $message = "Updated Successfully";
        $status = true;
        $code = 200;

        $rules = array(
            'job_system_repair_form_id' => 'required|integer',
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'date'=>'nullable|date|date_format:Y-m-d',
            'age_equipment_cooling_and_heat'=>'nullable|integer',
            'age_equipment_furnace_and_boilers'=>'nullable|integer',
            'age_equipment_packaged_units'=>'nullable|integer',
            'estimate_cost_cooling_and_heat'=>'nullable|integer',
            'estimate_cost_furnace_and_boilers'=>'nullable|integer',
            'estimate_cost_packaged_units'=>'nullable|integer',
            'repairs_under_warranty_cooling_and_heat'=>'nullable|integer',
            'repairs_under_warranty_furnace_and_boilers'=>'nullable|integer',
            'repairs_under_warranty_packaged_units'=>'nullable|integer',
            'seer_of_equipment_cooling_and_heat'=>'nullable|integer',
            'seer_of_equipment_packaged_units'=>'nullable|integer',
            'type_of_refrigerant_cooling_and_heat'=>'nullable|integer',
            'type_of_refrigerant_packaged_units'=>'nullable|integer',
            'afue_furnace_and_boilers'=>'nullable|integer',
            'afue_packaged_units'=>'nullable|integer',
            'outdoor_equipment_cooling_and_heat'=>'nullable|integer',
            'outdoor_equipment_packaged_units'=>'nullable|integer',
            'indoor_equipment_cooling_and_heat'=>'nullable|integer',
            'indoor_equipment_furnace_and_boilers'=>'nullable|integer',
            'owner_expects_cooling_and_heat'=>'nullable|integer',
            'owner_expects_furnace_and_boilers'=>'nullable|integer',
            'owner_expects_packaged_units'=>'nullable|integer',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users = User::where('users.id', $user_id)->first();
            $job = Job::where('job.id', $job_id)->first();
            $jobSystemReplaceForm = JobSystemRepairForm::where('id', $request->job_system_repair_form_id)->where('status', 'active')->first();

            if (!$users) {
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            } else if (!$job) {
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }

             if($jobSystemReplaceForm == null){
                $status = false;
                $message = 'No Form Found';
                $code = 400;
            }


            if ($status) {
                $requestData = $request->except('user_id', 'api_token','job_system_repair_form_id','job_id');
                $data = $jobSystemReplaceForm->update($requestData);
                $data = (object)(make_null(collect($requestData)));
            }
        }

        return response()->json(['data'=>$data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'update']);
    }

    public function jobSystemRepairFormList(Request $request){
        $data = (object)[];
        $message = "Success";
        $status = true;
        $flag = 1;
        $code = 200;
        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'job_form_id'=>'required|integer'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $flag = 1;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();

            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
                $flag = 1;

            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
                $flag = 1;

            }

            if($status){
                $data = JobSystemRepairForm::select('job_system_repair_replace_form.*','job.title as job_name')->join('job','job_system_repair_replace_form.job_id','job.id')->where('job_system_repair_replace_form.id',$request->job_form_id)->first();

                if($data == null){
                    $status = false;
                    $message = 'No Form Found';
                    $code = 400;
                    $data = (object)[];
                    $flag = 1;
                }
                else {
                    $flag = 1;
                    $data = (object)(make_null($data));
                }
            }
        }
        return response()->json(['flag' => $flag,'data' => $data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'list']);
    }
    // job plumbing form
    public function addJobPlumbingForm(Request $request){
        $data = (object)[];
        $message = "Added Successfully";
        $status = true;
        $code = 200;

        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'date'=>'nullable|date|date_format:Y-m-d',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();
            $jobPlumbingForm = JobPlumbingForm::where('job_id', $request->job_id)->where('status', 'active')->first();

            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }
            // else if($jobPlumbingForm != null){
            //     $status = false;
            //     $message = 'Already Added form for this job';
            //     $code = 400;
            // }

            if($status){
                $requestData = $request->except('api_token');
                $requestData['user_id'] = $user_id;
                $data = JobPlumbingForm::create($requestData);
                $data = (object)(make_null(collect($data)));

                $forms = Forms::where('form_code', 'plumbing')->first();

                if(isset($forms->id))
                {
                    $formdata['job_id'] = $job_id;
                    $formdata['job_form_id'] = $data->id;
                    $formdata['form_id'] = $forms->id;
                    JobForms::create($formdata);
                }
            }
        }
        return response()->json(['data' => $data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'create']);
    }

    public function editJobPlumbingForm(Request $request){
        $data = (object)[];
        $message = "Updated Successfully";
        $status = true;
        $code = 200;

        $rules = array(
            'job_plumbing_form_id' => 'required|integer',
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'date'=>'nullable|date|date_format:Y-m-d'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users = User::where('users.id', $user_id)->first();
            $job = Job::where('job.id', $job_id)->first();
            $jobPlumbingForm = JobPlumbingForm::where('id', $request->job_plumbing_form_id)->where('status', 'active')->first();

            if (!$users) {
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            } else if (!$job) {
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }

             if($jobPlumbingForm == null){
                $status = false;
                $message = 'No Form Found';
                $code = 400;
            }


            if ($status) {
                $requestData = $request->except('user_id', 'api_token','job_plumbing_form_id','job_id');
                $data = $jobPlumbingForm->update($requestData);
                $data = (object)(make_null(collect($requestData)));
            }
        }

        return response()->json(['data'=>$data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'update']);
    }

    public function jobPlumbingFormList(Request $request){
        $data = (object)[];
        $message = "Success";
        $status = true;
        $flag = 1;
        $code = 200;
        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'job_form_id'=>'required|integer'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $flag = 1;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();

            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
                $flag = 1;

            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
                $flag = 1;

            }


            if($status){
                $data = JobPlumbingForm::select('job_plumbing_form.*','job.title as job_name','job_plumbing_form.id as job_plumbing_form_id')->join('job','job_plumbing_form.job_id','job.id')->where('job_plumbing_form.id',$request->job_form_id)->first();

                if($data == null){
                    $status = false;
                    $message = 'No Form Found';
                    $code = 400;
                    $data = (object)[];
                    $flag = 1;
                }
                else {
                    $flag = 0;
                    $data = (object)(make_null($data));
                }
            }
        }
        return response()->json(['flag' => $flag,'data' => $data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'list']);
    }
    // job weekly vehicle safety checklist form
    public function addJobWeeklyVehicleSafetyChecklistForm(Request $request){
        $data = (object)[];
        $message = "Added Successfully";
        $status = true;
        $code = 200;

        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'date'=>'nullable|date|date_format:Y-m-d',
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();
            $formData = JobWeeklyVehicleSafetyChecklistForm::where('job_id', $request->job_id)->where('status', 'active')->first();

            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }
            // else if($formData != null){
            //     $status = false;
            //     $message = 'Already Added form for this job';
            //     $code = 400;
            // }

            if($status){
                $requestData = $request->except('api_token');
                $requestData['user_id'] = $user_id;
                $data = JobWeeklyVehicleSafetyChecklistForm::create($requestData);
                $data = (object)(make_null(collect($data)));

                $forms = Forms::where('form_code', 'weekly_vehicle_safety')->first();

                if(isset($forms->id))
                {
                    $formdata['job_id'] = $job_id;
                    $formdata['job_form_id'] = $data->id;
                    $formdata['form_id'] = $forms->id;
                    JobForms::create($formdata);
                }
            }
        }
        return response()->json(['data' => $data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'create']);
    }

    public function editJobWeeklyVehicleSafetyChecklistForm(Request $request){
        $data = (object)[];
        $message = "Updated Successfully";
        $status = true;
        $code = 200;

        $rules = array(
            'job_weekly_vehicle_safety_checklist_form_id' => 'required|integer',
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'date'=>'nullable|date|date_format:Y-m-d',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users = User::where('users.id', $user_id)->first();
            $job = Job::where('job.id', $job_id)->first();
            $formData = JobWeeklyVehicleSafetyChecklistForm::where('id', $request->job_weekly_vehicle_safety_checklist_form_id)->where('status', 'active')->first();

            if (!$users) {
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            } else if (!$job) {
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }

             if($formData == null){
                $status = false;
                $message = 'No Form Found';
                $code = 400;
            }


            if ($status) {
                $requestData = $request->except('user_id', 'api_token','job_plumbing_form_id','job_id');
                $data = $formData->update($requestData);
                $data = (object)(make_null(collect($requestData)));
            }
        }

        return response()->json(['data'=>$data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'update']);
    }

    public function jobWeeklyVehicleSafetyChecklistFormList(Request $request){
        $data = (object)[];
        $message = "Success";
        $status = true;
        $flag = 1;
        $code = 200;
        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'job_form_id'=>'required|integer'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $flag = 1;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();

            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
                $flag = 1;

            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
                $flag = 1;

            }


            if($status){
                $data = JobWeeklyVehicleSafetyChecklistForm::select('job_weekly_vehicle_safety_checklist_form.*','job.title as job_name','job_weekly_vehicle_safety_checklist_form.id as job_weekly_vehicle_safety_checklist_form_id')->join('job','job_weekly_vehicle_safety_checklist_form.job_id','job.id')->where('job_weekly_vehicle_safety_checklist_form.id',$request->job_form_id)->first();

                if($data == null){
                    $status = false;
                    $message = 'No Form Found';
                    $code = 400;
                    $data = (object)[];
                    $flag = 1;
                }
                else {
                    $flag = 0;
                    $data = (object)(make_null($data));
                }
            }
        }
        return response()->json(['flag' => $flag,'data' => $data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'list']);
    }

    // job invoicer form // JobInvoice // JobInvoiceOtherField
    public function addJobInvoiceForm(Request $request){
        $data = (object)[];
        $message = "Added Successfully";
        $status = true;
        $code = 200;

        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'date'=>'nullable|date|date_format:Y-m-d',
            'main_date'=>'nullable|date|date_format:Y-m-d',
            'date_ordered'=>'nullable|date|date_format:Y-m-d',
            'date_scheduled'=>'nullable|date|date_format:Y-m-d'
        );

        $validator = \Validator::make($request->all(), $rules, []);

        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();
            $jobForm = JobInvoice::where('job_id', $request->job_id)->where('status', 'active')->first();

            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }
            // else if($jobForm != null){
            //     $status = false;
            //     $message = 'Already Added form for this job';
            //     $code = 400;
            // }

            if($status){
                $requestData = $request->except('api_token');
                $requestData['user_id'] = $user_id;
                $data = JobInvoice::create($requestData);

                $forms = Forms::where('form_code', 'invoice')->first();

                if(isset($forms->id))
                {
                    $formdata['job_id'] = $job_id;
                    $formdata['job_form_id'] = $data->id;
                    $formdata['form_id'] = $forms->id;
                    JobForms::create($formdata);
                }

                $otherdata = $requestData;
                $otherdata['job_invoice_id'] = $data->id;
                $data = JobInvoiceOtherField::create($otherdata);
                $data = (object)(make_null(collect($data)));


            }
        }
        return response()->json(['data' => $data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'create']);
    }

    public function editJobInvoiceForm(Request $request){
        $data = (object)[];
        $message = "Updated Successfully";
        $status = true;
        $code = 200;

        $rules = array(
            'job_invoice_id' => 'required|integer',
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'date'=>'nullable|date|date_format:Y-m-d',
            'main_date'=>'nullable|date|date_format:Y-m-d',
            'date_ordered'=>'nullable|date|date_format:Y-m-d',
            'date_scheduled'=>'nullable|date|date_format:Y-m-d'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users = User::where('users.id', $user_id)->first();
            $job = Job::where('job.id', $job_id)->first();
            $jobForm = JobInvoice::where('id', $request->job_invoice_id)->where('status', 'active')->first();

            if (!$users) {
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            } else if (!$job) {
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }

             if($jobForm == null){
                $status = false;
                $message = 'No Form Found';
                $code = 400;
            }


            if ($status) {
                $jobOtherForm = JobInvoiceOtherField::where('job_invoice_id', $jobForm->id)->where('status', 'active')->first();
                if($jobOtherForm == null)
                {
                    $requestData = $request->except('user_id', 'api_token','job_invoice_id','job_id');
                    $requestData['job_invoice_id'] = $data->id;
                    $data = JobInvoiceOtherField::create($requestData);
                }
                else {
                    $requestData = $request->except('user_id', 'api_token','job_invoice_id','job_id');
                    $data = $jobOtherForm->update($requestData);
                }
                $requestData = $request->except('user_id', 'api_token','job_invoice_id','job_id');
                $data = $jobForm->update($requestData);
                $data = (object)(make_null(collect($requestData)));
            }
        }

        return response()->json(['data'=>$data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'update']);
    }

    public function jobInvoiceFormList(Request $request){
        $data = (object)[];
        $message = "Success";
        $status = true;
        $flag = 1;
        $code = 200;
        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'job_form_id'=>'required|integer'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $flag = 1;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();

            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
                $flag = 1;

            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
                $flag = 1;

            }

            if($status){
                //select('job_invoice_form.*','job.title as job_name','job_invoice_form.id as job_invoice_id', 'job_invoice_other_fields_form.job_invoice_id as other_from_id')
                $data = JobInvoice::select('job_invoice_form.*','job_invoice_other_fields_form.*','job.title as job_name','job_invoice_form.id as job_main_invoice_id')->join('job','job_invoice_form.job_id','job.id')->leftJoin('job_invoice_other_fields_form', 'job_invoice_other_fields_form.job_invoice_id','job_invoice_form.id')->where('job_invoice_form.id',$request->job_form_id)->first();
                if($data == null){
                    $status = false;
                    $message = 'No Form Found';
                    $code = 400;
                    $data = (object)[];
                    $flag = 1;
                }
                else {
                    $flag = 0;
                    $data->job_invoice_id = $data->job_main_invoice_id;
                    $data = (object)(make_null($data));
                }
            }
        }
        return response()->json(['flag' => $flag,'data' => $data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'list']);
    }

    // JobProjrctWorksheet1 JobProjrctWorksheet2 JobProjrctWorksheet3 JobProjrctWorksheet4

    public function addJobProjrctWorksheetForm(Request $request){
        $data = (object)[];
        $message = "Added Successfully";
        $status = true;
        $code = 200;

        $allData = $request->except('api_token');

        $requestData1 = (array) json_decode(isset($allData['json_data_1']) ? $allData['json_data_1'] : '');
        $requestData2 = (array) json_decode(isset($allData['json_data_2']) ? $allData['json_data_2'] : '');
        $requestData3 = (array) json_decode(isset($allData['json_data_3']) ? $allData['json_data_3'] : '');
        $requestData4 = (array) json_decode(isset($allData['json_data_4']) ? $allData['json_data_4'] : '');
        $requestData5 = (array) json_decode(isset($allData['json_data_5']) ? $allData['json_data_5'] : '');

        $requestData = (array) array_merge($requestData1, $requestData2, $requestData3 ,$requestData4, $requestData5);
        $requestData['job_id']= (isset($allData['job_id']) ? $allData['job_id'] : '');
        $requestData['user_id']= (isset($allData['user_id']) ? $allData['user_id'] : '');

        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'sold_date'=>'nullable|date|date_format:Y-m-d',
            'install_date'=>'nullable|date|date_format:Y-m-d',
            'quoted_by'=>'nullable|date|date_format:Y-m-d',
            'apt_date'=>'nullable|date|date_format:Y-m-d',
            'client_information_date'=>'nullable|date|date_format:Y-m-d',
            'client_information_meter_set_date'=>'nullable|date|date_format:Y-m-d',
            'permits_date'=>'nullable|date|date_format:Y-m-d',
            'permit_order_date_gas_piping'=>'nullable|date|date_format:Y-m-d',
            'permit_order_date_mechanical'=>'nullable|date|date_format:Y-m-d',
            'permit_order_date_plumbing'=>'nullable|date|date_format:Y-m-d',
            'permit_order_date_electrical'=>'nullable|date|date_format:Y-m-d',
            'permit_order_date_boiler'=>'nullable|date|date_format:Y-m-d',
            'time'=>'nullable|date_format:h:i:s'
        );

        $validator = \Validator::make($requestData, $rules, []);
        //dd($validator->fails());
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {

            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();
            $jobForm = JobProjrctWorksheet1::where('job_id', $request->job_id)->where('status', 'active')->first();

            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }
            // else if($jobForm != null){
            //     $status = false;
            //     $message = 'Already Added form for this job';
            //     $code = 400;
            // }

            if($status){
                $data = JobProjrctWorksheet1::create($requestData);

                $forms = Forms::where('form_code', 'project_worksheet')->first();

                if(isset($forms->id))
                {
                    $formdata['job_id'] = $job_id;
                    $formdata['job_form_id'] = $data->id;
                    $formdata['form_id'] = $forms->id;
                    JobForms::create($formdata);
                }

                $otherdata = $requestData;
                $otherdata['job_project_worksheet_form_1_id'] = $data->id;
                $otherformdata = JobProjrctWorksheet2::create($otherdata);
                $otherformdata = JobProjrctWorksheet3::create($otherdata);
                $otherformdata = JobProjrctWorksheet4::create($otherdata);
                $data = (object)(make_null(collect($requestData)));

            }
        }
        return response()->json(['data' => $data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'create']);
    }

    public function editJobProjrctWorksheetForm(Request $request){
        $data = (object)[];
        $message = "Updated Successfully";
        $status = true;
        $code = 200;
        $allData = $request->except('user_id', 'api_token','job_project_worksheet_id','job_id');
        $requestData1 = (array) json_decode(isset($allData['json_data_1']) ? $allData['json_data_1'] : '');
        $requestData2 = (array) json_decode(isset($allData['json_data_2']) ? $allData['json_data_2'] : '');
        $requestData3 = (array) json_decode(isset($allData['json_data_3']) ? $allData['json_data_3'] : '');
        $requestData4 = (array) json_decode(isset($allData['json_data_4']) ? $allData['json_data_4'] : '');
        $requestData5 = (array) json_decode(isset($allData['json_data_5']) ? $allData['json_data_5'] : '');

        $requestData = (array) array_merge($requestData1, $requestData2, $requestData3,$requestData4, $requestData5);
        $requestData['job_id']= (isset($request->job_id) ? $request->job_id : '');
        $requestData['user_id']= (isset($request->user_id) ? $request->user_id : '');
        $requestData['job_project_worksheet_id']= (isset($request->job_project_worksheet_id) ? $request->job_project_worksheet_id : '');


        $rules = array(
            'job_project_worksheet_id' => 'required|integer',
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'sold_date'=>'nullable|date|date_format:Y-m-d',
            'install_date'=>'nullable|date|date_format:Y-m-d',
            'quoted_by'=>'nullable|date|date_format:Y-m-d',
            'apt_date'=>'nullable|date|date_format:Y-m-d',
            'client_information_date'=>'nullable|date|date_format:Y-m-d',
            'client_information_meter_set_date'=>'nullable|date|date_format:Y-m-d',
            'permits_date'=>'nullable|date|date_format:Y-m-d',
            'permit_order_date_gas_piping'=>'nullable|date|date_format:Y-m-d',
            'permit_order_date_mechanical'=>'nullable|date|date_format:Y-m-d',
            'permit_order_date_plumbing'=>'nullable|date|date_format:Y-m-d',
            'permit_order_date_electrical'=>'nullable|date|date_format:Y-m-d',
            'permit_order_date_boiler'=>'nullable|date|date_format:Y-m-d',
            'time'=>'nullable|date_format:h:i:s'
        );

        $validator = \Validator::make($requestData, $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users = User::where('users.id', $user_id)->first();
            $job = Job::where('job.id', $job_id)->first();
            $jobForm = JobProjrctWorksheet1::where('id', $request->job_project_worksheet_id)->where('job_id', $request->job_id)->where('status', 'active')->first();

            if (!$users) {
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            } else if (!$job) {
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }

             if($jobForm == null){
                $status = false;
                $message = 'No Form Found';
                $code = 400;
            }


            if ($status) {

                $data = $jobForm->update($requestData);
				$requestData['job_project_worksheet_form_1_id'] = $jobForm->id;

                $jobProjectWorksheetForm2 = JobProjrctWorksheet2::where('job_project_worksheet_form_1_id', $jobForm->id)->where('status', 'active')->first();
                if($jobProjectWorksheetForm2 == null)
                {
                    $data = JobProjrctWorksheet2::create($requestData);
                }
                else {
                    $data = $jobProjectWorksheetForm2->update($requestData);
                }

                $jobProjectWorksheetForm3 = JobProjrctWorksheet3::where('job_project_worksheet_form_1_id', $jobForm->id)->where('status', 'active')->first();
                if($jobProjectWorksheetForm3 == null)
                {
                    $data = JobProjrctWorksheet3::create($requestData);
                }
                else {
                    $data = $jobProjectWorksheetForm3->update($requestData);
                }

                $jobProjectWorksheetForm4 = JobProjrctWorksheet4::where('job_project_worksheet_form_1_id', $jobForm->id)->where('status', 'active')->first();
                if($jobProjectWorksheetForm4 == null)
                {
                    $data = JobProjrctWorksheet4::create($requestData);
                }
                else {
                    $data = $jobProjectWorksheetForm4->update($requestData);
                }


                $data = (object)(make_null(collect($requestData)));
            }
        }

        return response()->json(['data'=>$data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'update']);
    }

    public function jobProjrctWorksheetFormList(Request $request){
        $data = (object)[];
        $message = "Success";
        $status = true;
        $flag = 1;
        $code = 200;
        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'job_form_id'=>'required|integer'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $flag = 1;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {
            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();

            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
                $flag = 1;

            }else if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
                $flag = 1;

            }

            if($status){
                $data = JobProjrctWorksheet1::select('job_project_worksheet_form_1.*','job_project_worksheet_form_2.*','job_project_worksheet_form_3.*','job_project_worksheet_form_4.*','job.title as job_name','job_project_worksheet_form_1.id as job_project_worksheet_id')->join('job','job_project_worksheet_form_1.job_id','job.id')->leftJoin('job_project_worksheet_form_2', 'job_project_worksheet_form_2.job_project_worksheet_form_1_id','job_project_worksheet_form_1.id')->leftJoin('job_project_worksheet_form_3', 'job_project_worksheet_form_3.job_project_worksheet_form_1_id','job_project_worksheet_form_1.id')->leftJoin('job_project_worksheet_form_4', 'job_project_worksheet_form_4.job_project_worksheet_form_1_id','job_project_worksheet_form_1.id')->where('job_project_worksheet_form_1.id',$request->job_form_id)->first();
                if($data == null){
                    $status = false;
                    $message = 'No Form Found';
                    $code = 400;
                    $data = (object)[];
                    $flag = 1;
                }
                else {
                    $flag = 0;
                    $data = (object)(make_null($data));
                    $responseArr = array();
                    foreach ($data as $key => $value) {
                        $responseArr[$key] = (string) (($value !='0000-00-00' && $value !='00:00:00') ? $value : '');
                    }
                    $data = $responseArr;
                }
            }
        }
        return response()->json(['flag' => $flag,'data' => $data,'status'=>$status,'message'=>$message,'code'=> $code,'action'=>'list']);
    }

}
