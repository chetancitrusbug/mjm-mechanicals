
@extends('layouts.backend')
@section('title',trans('job.view_form'))
@section('content')
<div class="panel panel-default">
    <div class="panel-heading"> Project Worksheet </div>
    {!! Form::model($jobProjectWorksheetForm, [ 'method' => 'PATCH', 'url' => ['/admin/jobProjectWorksheetForm', $jobProjectWorksheetForm->job_project_worksheet_id], 'class' => 'form-horizontal','id'=>'jobProjectWorksheetForm', 'enctype'=>'multipart/form-data' ]) !!}
    <a href="{{ url('/admin/job/' . $job->id . '/edit') }}" title="Back to Forms list"><button type="button" class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true" ></i>Back</button></a>
    {!! Form::hidden('job_id', $job->id, ['class' => 'form-control input-100']) !!}
    <div class="panel-body">

        <div class="grid-box-1 form-rowpw01 clearfix">

            <div class="row">

                <div class="worksheet-box1 clearfix">

                    <div class="col-md-6 col-sm-12 clearfix">

                        <div class="leftrowws1 clearfix">
                            <div class="form-group clearfix">
                                <p class="height-auto">Customer Name</p>
                                {!! Form::text('customer_name', null, ['class' => 'form-control input-100']) !!}
                            </div>
                            <div class="form-group clearfix">
                                <p class="height-auto">Address</p>
                                {!! Form::textarea('address', null, ['class' => 'form-control input-100']) !!}
                            </div>
                            <div class="form-group clearfix">
                                <p class="height-auto">Phone</p>
                                {!! Form::text('phone', null, ['class' => 'form-control input-100']) !!}
                            </div>
                            <div class="form-group clearfix">
                                <p class="height-auto">Job #</p>
                                {!! Form::text('job', null, ['class' => 'form-control input-100']) !!}
                            </div>
                        </div>

                        <div class="leftrowws2 clearfix">
                            <div class="thumb-ws01 clearfix">
                                <img src="{!! asset('assets/images/ws-1.png') !!}" class="img-responsive img-ws-1">
                            </div>
                        </div>

                        <div class="leftrowws3 clearfix">
                            <h3>Do you need? </h3>

                            <div class="main-box-check clearfix">
                                <div class="row-h1 clearfix">
                                    <div class="ws-width60 clearfix">
                                        <p>Fan switch on furnace</p>
                                    </div>
                                    <div class="ws-width40 clearfix">
                                        <div class="p-blk1 clearfix">
                                            {!! Form::radio('fan_switch_on_furnace', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                            <p class="ws-fleft-01"> Yes </p>
                                        </div>
                                        <div class="p-blk1 clearfix">
                                            {!! Form::radio('fan_switch_on_furnace', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                            <p class="ws-fleft-01"> No </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row-h1 clearfix">
                                    <div class="ws-width60 clearfix">
                                        <p>Fan switch on t-stat </p>
                                    </div>
                                    <div class="ws-width40 clearfix">
                                        <div class="p-blk1 clearfix">
                                            {!! Form::radio('fan_switch_on_tstat', '1', null, ['class' => 'form-radio radio-ws1']) !!}

                                            <p class="ws-fleft-01"> Yes </p>
                                        </div>
                                        <div class="p-blk1 clearfix">
                                            {!! Form::radio('fan_switch_on_tstat', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                            <p class="ws-fleft-01"> No </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row-h1 clearfix">
                                    <div class="ws-width60 clearfix">
                                        <p>Fan center </p>
                                    </div>
                                    <div class="ws-width40 clearfix">
                                        <div class="p-blk1 clearfix">
                                            {!! Form::radio('fan_center', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                            <p class="ws-fleft-01"> Yes </p>
                                        </div>
                                        <div class="p-blk1 clearfix">
                                            {!! Form::radio('fan_center', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                            <p class="ws-fleft-01"> No </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row-h1 clearfix">
                                    <div class="ws-width60 clearfix">
                                        <p>Condensate pump </p>
                                    </div>
                                    <div class="ws-width40 clearfix">
                                        <div class="p-blk1 clearfix">
                                            {!! Form::radio('condensate_pump', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                            <p class="ws-fleft-01"> Yes </p>
                                        </div>
                                        <div class="p-blk1 clearfix">
                                            {!! Form::radio('condensate_pump', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                            <p class="ws-fleft-01"> No </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row-h1 clearfix">
                                    <div class="ws-width60 clearfix">
                                        <p>Concrete pad</p>
                                    </div>
                                    <div class="ws-width40 clearfix">
                                        <div class="p-blk1 clearfix">
                                            {!! Form::radio('concrete_pad', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                            <p class="ws-fleft-01"> Yes </p>
                                        </div>
                                        <div class="p-blk1 clearfix">
                                            {!! Form::radio('concrete_pad', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                            <p class="ws-fleft-01"> No </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row-h1 clearfix">
                                    <div class="ws-width60 clearfix">
                                        <p>Lineset
                                            {!! Form::text('linesetFt_value', null, ['class' => 'fc-custom01']) !!}
                                        Ft.</p>
                                    </div>
                                    <div class="ws-width40 clearfix">
                                        <div class="p-blk1 clearfix">
                                            {!! Form::radio('linesetFt', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                            <p class="ws-fleft-01"> Yes </p>
                                        </div>
                                        <div class="p-blk1 clearfix">
                                            {!! Form::radio('linesetFt', '0', null, ['class' => 'form-radio radio-ws1']) !!}

                                            <p class="ws-fleft-01"> No </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row-h1 clearfix">
                                    <div class="ws-width60 clearfix">
                                        <p>Insulate ducts </p>
                                    </div>
                                    <div class="ws-width40 clearfix">
                                        <div class="p-blk1 clearfix">
                                            {!! Form::radio('insulate_ducts', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                            <p class="ws-fleft-01"> Yes </p>
                                        </div>
                                        <div class="p-blk1 clearfix">
                                            {!! Form::radio('insulate_ducts', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                            <p class="ws-fleft-01"> No </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row-h1 clearfix">
                                    <div class="ws-width60 clearfix">
                                        <p>Chimney Liner </p>
                                    </div>
                                    <div class="ws-width40 clearfix">
                                        <div class="p-blk1 clearfix">
                                            {!! Form::radio('chimney_fliner', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                            <p class="ws-fleft-01"> Yes </p>
                                        </div>
                                        <div class="p-blk1 clearfix">
                                            {!! Form::radio('chimney_fliner', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                            <p class="ws-fleft-01"> No </p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row-h1 clearfix">
                                    <div class="ws-width60 clearfix">
                                        <p>Transition Fittings</p>
                                    </div>
                                    <div class="ws-width40 clearfix">
                                        <div class="p-blk1 clearfix">
                                            {!! Form::radio('transition_fittings', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                            <p class="ws-fleft-01"> Yes </p>
                                        </div>
                                        <div class="p-blk1 clearfix">
                                            {!! Form::radio('transition_fittings', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                            <p class="ws-fleft-01"> No </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="leftrowws4 clearfix">
                            <div class="thumb-ws01 clearfix">
                                <img src="{!! asset('assets/images/ws-3.png') !!}" class="img-responsive img-ws-1">
                            </div>
                        </div>

                    </div>
                    <!--  End of col-first1  -->

                    <div class="col-md-6 col-sm-12 clearfix">

                        <div class="leftrowws5 clearfix">
                            <h3>Existing Equipment: </h3>
                            <div class="main-box-check clearfix">

                                <div class="row-h2 clearfix">

                                    <div class="ws-width50 clearfix">
                                        <div class="ws-width30 clearfix">
                                            <p class="padding-p1">Brand</p>
                                        </div>
                                        <div class="ws-width70 clearfix">
                                            {!! Form::text('brand', null, ['class' => 'fc-100']) !!}
                                        </div>
                                    </div>

                                    <div class="ws-width50 second-half clearfix">
                                        <div class="ws-width30 clearfix">
                                            <p class="padding-p1">Fuel</p>
                                        </div>
                                        <div class="ws-width70 clearfix">
                                            {!! Form::text('fuel', null, ['class' => 'fc-100']) !!}
                                        </div>
                                    </div>

                                    <div class="ws-width50 mt-10 clearfix">
                                        <div class="ws-width30 clearfix">
                                            <p class="padding-p1">Model</p>
                                        </div>
                                        <div class="ws-width70 clearfix">
                                            {!! Form::text('model', null, ['class' => 'fc-100']) !!}
                                        </div>
                                    </div>
                                    <div class="ws-width50 mt-10 second-half clearfix">
                                        <div class="ws-width30 clearfix">
                                            <p class="padding-p1">BTU</p>
                                        </div>
                                        <div class="ws-width70 clearfix">
                                            {!! Form::text('BTU', null, ['class' => 'fc-100']) !!}
                                        </div>
                                    </div>

                                    <div class="ws-width100 mt-10 clearfix">
                                        <div class="ws-width15 clearfix">
                                            <p class="padding-p1">Location</p>
                                        </div>
                                        <div class="ws-width85 clearfix">
                                            {!! Form::text('location', null, ['class' => 'fc-100']) !!}
                                        </div>
                                    </div>

                                    <div class="ws-width100 mt-10 clearfix">
                                        <div class="ws-width15 clearfix">
                                            <p class="padding-p1">Stat Type</p>
                                        </div>
                                        <div class="ws-width85 clearfix">
                                            {!! Form::text('stat_type', null, ['class' => 'fc-100']) !!}
                                        </div>
                                    </div>

                                    <div class="ws-width100 mt-10 clearfix">
                                        <div class="ws-width15 clearfix">
                                            <p class="padding-p1"># of Stat Wires:</p>
                                        </div>
                                        <div class="ws-width85 clearfix">
                                            <div class="pl-50">
                                                <div class="list-0001 clearfix">
                                                    {!! Form::checkbox('of_stat_wires', '2', ((isset($jobProjectWorksheetForm->of_stat_wires) && $jobProjectWorksheetForm->of_stat_wires == 2) ? true : false), ['class' => 'form-radio radio-ws1']) !!}
                                                    <p class="f0left-01">2</p>
                                                </div>
                                                <div class="list-0001 clearfix">
                                                    {!! Form::checkbox('of_stat_wires', '3', ((isset($jobProjectWorksheetForm->of_stat_wires) && $jobProjectWorksheetForm->of_stat_wires == 3) ? true : false), ['class' => 'form-radio radio-ws1']) !!}
                                                    <p class="f0left-01">3</p>
                                                </div>
                                                <div class="list-0001 clearfix">
                                                    {!! Form::checkbox('of_stat_wires', '4', ((isset($jobProjectWorksheetForm->of_stat_wires) && $jobProjectWorksheetForm->of_stat_wires == 4) ? true : false), ['class' => 'form-radio radio-ws1']) !!}
                                                    <p class="f0left-01">4</p>
                                                </div>
                                                <div class="list-0001 clearfix">
                                                    {!! Form::checkbox('of_stat_wires', '5', ((isset($jobProjectWorksheetForm->of_stat_wires) && $jobProjectWorksheetForm->of_stat_wires == 5) ? true : false), ['class' => 'form-radio radio-ws1']) !!}
                                                    <p class="f0left-01">5 </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="ws-full mt-10 clearfix">
                                        <p class="padding-p1">Blower Type:</p>
                                    </div>

                                    <div class="ws-width50 mt-5 clearfix">
                                        <div class="ws-width30 clearfix">
                                            <p class="padding-p1 fw-400">Direct</p>
                                        </div>
                                        <div class="ws-width70 clearfix">
                                            {!! Form::text('blower_type_direct', null, ['class' => 'fc-100']) !!}
                                        </div>
                                    </div>

                                    <div class="ws-width50 mt-5 second-half clearfix">
                                        <div class="ws-width30 clearfix">
                                            <p class="padding-p1 fw-400">Belt</p>
                                        </div>
                                        <div class="ws-width70 clearfix">
                                            {!! Form::text('blower_Type_belt', null, ['class' => 'fc-100']) !!}
                                        </div>
                                    </div>

                                    <div class="ws-width100 mt-10 clearfix">
                                        <div class="ws-width15 clearfix">
                                            <p class="padding-p1">Motor Size:</p>
                                        </div>
                                        <div class="ws-width85 clearfix">
                                            <div class="pl-50">
                                                <div class="list-0001 clearfix">
                                                    {!! Form::checkbox('motor_size', '1/6', ((isset($jobProjectWorksheetForm->motor_size) && $jobProjectWorksheetForm->motor_size == '1/6') ? true : false), ['class' => 'form-radio radio-ws1']) !!}
                                                    <p class="f0left-01">1/6</p>
                                                </div>
                                                <div class="list-0001 clearfix">
                                                    {!! Form::checkbox('motor_size', '1/4', ((isset($jobProjectWorksheetForm->motor_size) && $jobProjectWorksheetForm->motor_size == '1/4') ? true : false), ['class' => 'form-radio radio-ws1']) !!}
                                                    <p class="f0left-01">1/4</p>
                                                </div>
                                                <div class="list-0001 clearfix">
                                                    {!! Form::checkbox('motor_size', '1/3',  ((isset($jobProjectWorksheetForm->motor_size) && $jobProjectWorksheetForm->motor_size == '1/3') ? true : false), ['class' => 'form-radio radio-ws1']) !!}
                                                    <p class="f0left-01">1/3</p>
                                                </div>
                                                <div class="list-0001 clearfix">
                                                    {!! Form::checkbox('motor_size', '1/2', ((isset($jobProjectWorksheetForm->motor_size) && $jobProjectWorksheetForm->motor_size == '1/2') ? true : false), ['class' => 'form-radio radio-ws1']) !!}
                                                    <p class="f0left-01">1/2</p>
                                                </div>
                                                <div class="list-0001 clearfix">
                                                    {!! Form::checkbox('motor_size', '3/4', ((isset($jobProjectWorksheetForm->motor_size) && $jobProjectWorksheetForm->motor_size == '3/4') ? true : false), ['class' => 'form-radio radio-ws1']) !!}
                                                    <p class="f0left-01">3/4</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="ws-full mt-10 clearfix">
                                        <p class="padding-p1">Motor:</p>
                                    </div>

                                    <div class="ws-width50 mt-5 clearfix">
                                        <div class="ws-width30 clearfix">
                                            <p class="padding-p1 fw-400">Speed</p>
                                        </div>
                                        <div class="ws-width70 clearfix">
                                            {!! Form::text('motor_speed', null, ['class' => 'fc-100']) !!}
                                        </div>
                                    </div>

                                    <div class="ws-width50 mt-5 second-half clearfix">
                                        <div class="ws-width30 clearfix">
                                            <p class="padding-p1 fw-400">Volt </p>
                                        </div>
                                        <div class="ws-width70 clearfix">
                                            {!! Form::text('motor_volt', null, ['class' => 'fc-100']) !!}
                                        </div>
                                    </div>

                                    <div class="ws-width50 mt-5 clearfix">
                                        <div class="ws-width30 clearfix">
                                            <p class="padding-p1 fw-400">Brand</p>
                                        </div>
                                        <div class="ws-width70 clearfix">
                                            {!! Form::text('motor_brand', null, ['class' => 'fc-100']) !!}
                                        </div>
                                    </div>

                                    <div class="ws-full mt-10 clearfix">
                                        <p class="padding-p1">Existing Vent:</p>
                                    </div>

                                    <div class="ws-width50 mt-5 clearfix">
                                        <div class="ws-width30 clearfix">
                                            <p class="padding-p1 fw-400">Size</p>
                                        </div>
                                        <div class="ws-width70 clearfix">
                                            {!! Form::text('existing_vent_size', null, ['class' => 'fc-100']) !!}
                                        </div>
                                    </div>

                                    <div class="ws-width50 mt-5 second-half clearfix">
                                        <div class="ws-width30 clearfix">
                                            <p class="padding-p1 fw-400">Brand</p>
                                        </div>
                                        <div class="ws-width70 clearfix">
                                            {!! Form::text('existing_vent_brand', null, ['class' => 'fc-100']) !!}

                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="leftrowws5 leftrowws05-2 clearfix">
                            <h3>Existing Electrical: </h3>
                            <div class="main-box-check clearfix">

                                <div class="row-h2 clearfix">

                                    <div class="ws-width100 mt-10 clearfix">
                                        <div class="ws-width35 clearfix">
                                            <p class="padding-p1">Location of Panel </p>
                                        </div>
                                        <div class="ws-width65 clearfix">
                                            {!! Form::text('location_of_panel', null, ['class' => 'fc-100']) !!}
                                        </div>
                                    </div>

                                    <div class="ws-width100 mt-10 clearfix">
                                        <div class="ws-width35 clearfix">
                                            <p class="padding-p1">Brand of Panel</p>
                                        </div>
                                        <div class="ws-width65 clearfix">
                                            {!! Form::text('brand_of_panel', null, ['class' => 'fc-100']) !!}
                                        </div>
                                    </div>

                                    <div class="ws-width100 mt-10 clearfix">
                                        <div class="ws-width35 clearfix">
                                            <p class="padding-p1">Spare Circuits?</p>
                                        </div>
                                        <div class="ws-width65 clearfix">
                                            {!! Form::text('spare_circuits', null, ['class' => 'fc-100']) !!}
                                        </div>
                                    </div>

                                    <div class="ws-width100 mt-10 clearfix">
                                        <div class="ws-width35 clearfix">
                                            <p class="padding-p1">Electrician? </p>
                                        </div>
                                        <div class="ws-width65 clearfix">
                                            {!! Form::text('electrician', null, ['class' => 'fc-100']) !!}
                                        </div>
                                    </div>

                                    <div class="ws-width100 mt-10 clearfix">
                                        <div class="ws-width35 clearfix">
                                            <p class="padding-p1">New T-Stat Wire?</p>
                                        </div>
                                        <div class="ws-width65 clearfix">
                                            <div class="">
                                                <div class="list-0001 clearfix">
                                                    {!! Form::radio('new_tstat_wire', '1', null, ['class' => 'form-radio radio-ws1']) !!}

                                                    <p class="f0left-01">YES</p>
                                                </div>
                                                <div class="list-0001 clearfix">
                                                    {!! Form::radio('new_tstat_wire', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                                    <p class="f0left-01">NO</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="ws-full mt-10 clearfix">
                                        <p class="padding-p1">Distance to: </p>
                                    </div>

                                    <div class="ws-width100 mt-5 clearfix">
                                        <div class="ws-width35 clearfix">
                                            <p class="padding-p1 fw-400">Furnace </p>
                                        </div>
                                        <div class="ws-width65 clearfix">
                                            {!! Form::text('distance_to_furnace', null, ['class' => 'fc-100']) !!}
                                        </div>
                                    </div>
                                    <div class="ws-width100 mt-5 clearfix">
                                        <div class="ws-width35 clearfix">
                                            <p class="padding-p1 fw-400">A/C Unit </p>
                                        </div>
                                        <div class="ws-width65 clearfix">
                                            {!! Form::text('AC_unit', null, ['class' => 'fc-100']) !!}
                                        </div>
                                    </div>
                                    <div class="ws-width100 mt-5 clearfix">
                                        <div class="ws-width35 clearfix">
                                            <p class="padding-p1 fw-400">Light in crawlspace </p>
                                        </div>
                                        <div class="ws-width65 clearfix">
                                            {!! Form::text('light_in_crawlspace', null, ['class' => 'fc-100']) !!}
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="leftrowws6 clearfix">
                            <div class="thumb-ws01 clearfix">
                                <img src="{!! asset('assets/images/ws-2.png') !!}" class="img-responsive img-ws-3">
                            </div>
                        </div>

                    </div>

                </div>

            </div>

            <hr>

            <div class="row">
                <div class="worksheet-box2 clearfix">

                    <div class="fullboxright1 clearfix">
                        <div class="col-md-4 col-sm-6 clearfix">
                            <div class="img-thumb1 clearfix">
                                <img src="{!! asset('assets/images/logo-pn.png') !!}" class="img-responsive mjm-logo01">
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 order-2 clearfix">
                            <div class="form-group clearfix">
                                <p class="height-auto">Sold Date </p>
                                {!! Form::text('sold_date', null, [ 'id' =>'sold_date', 'class' => 'form-control input-100']) !!}
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 order-1 clearfix">
                            <div class="form-group clearfix">
                                <p class="height-auto">Install Date </p>
                                {!! Form::text('install_date', null, ['id' =>'install_date','class' => 'form-control input-100']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="wssecond-02 clearfix">
                        <div class="col-md-6 col-sm-12">

                            <div class="boxrow-ws01 clearfix">
                                <div class="form-group clearfix">
                                    <p class="height-auto">Quoted By </p>
                                    {!! Form::text('quoted_by', null, ['class' => 'form-control input-100']) !!}
                                </div>
                                <div class="form-group clearfix">
                                    <p class="height-auto">Job Number </p>
                                    {!! Form::text('job_number', null, ['class' => 'form-control input-100']) !!}
                                </div>
                                <div class="form-group clearfix">
                                    <p class="height-auto">Apt. Date </p>
                                    {!! Form::text('apt_date', null, ['class' => 'form-control input-100']) !!}
                                </div>
                                <div class="form-group clearfix">
                                    <p class="height-auto">Time </p>
                                    {!! Form::text('time', null, ['class' => 'form-control input-100']) !!}
                                </div>
                            </div>

                            <div class="boxrow-ws02 clearfix">
                                <h3 class="h3-title1">Interested In</h3>
                                <div class="rowblock01 clearfix">
                                    <div class="width50">
                                        {!! Form::checkbox('furnace', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Furnace</p></span>
                                    </div>
                                    <div class="width50">
                                        {!! Form::checkbox('water_heater', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Water Heater </p></span>
                                    </div>
                                    <div class="width50">
                                        {!! Form::checkbox('air_conditioning', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Air Conditioning </p></span>
                                    </div>
                                    <div class="width50">
                                        {!! Form::checkbox('ductwork', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Ductwork </p></span>
                                    </div>
                                    <div class="width50">
                                        {!! Form::checkbox('fireplace_other', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Fireplace/Other </p></span>
                                    </div>
                                    <h2 class="h2-title01">Source:</h2>
                                    <div class="width50">
                                        {!! Form::checkbox('source_yellow_pages', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Yellow Pages </p></span>
                                    </div>
                                    <div class="width50">
                                        {!! Form::checkbox('source_radio', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Radio </p></span>
                                    </div>
                                    <div class="width50">
                                        {!! Form::checkbox('source_tv', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">TV</p></span>
                                    </div>
                                    <div class="width50">
                                        {!! Form::checkbox('source_referral', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Referral </p></span>
                                    </div>
                                    <div class="width50">
                                        {!! Form::checkbox('source_home_show', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Home Show </p></span>
                                    </div>
                                    <div class="width50">
                                        {!! Form::checkbox('source_internet_other', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Internet/Other </p></span>
                                    </div>
                                </div>

                            </div>

                            <div class="boxrow-ws02 boxrow-ws03 clearfix">
                                <h3 class="h3-title1">Entry Arrangements/Special Instructions </h3>
                                <div class="rowblock01 clearfix">

                                    <div class="width100">
                                        {!! Form::textarea('entry_arrangements_special_instructions', null) !!}
                                    </div>

                                    <div class="width50">
                                        {!! Form::checkbox('see_our_access_notes', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">See Our Access Notes </p></span>
                                    </div>
                                    <div class="width50">
                                        {!! Form::checkbox('send_letter', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Send Letter </p></span>
                                    </div>

                                </div>
                            </div>

                            <div class="boxrow-ws02 boxrow-ws04 clearfix">
                                <h3 class="h3-title1">Existing System </h3>
                                <div class="rowblock01 clearfix">

                                    <div class="width50">
                                        {!! Form::checkbox('gas_pipe_size', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Gas pipe size </p></span>
                                    </div>
                                    <div class="width50">
                                        {!! Form::checkbox('fa_electric', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">FA Electric </p></span>
                                    </div>

                                    <div class="width50">
                                        {!! Form::checkbox('conv_oil', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Conv. Oil</p></span>
                                    </div>
                                    <div class="width50">
                                        {!! Form::checkbox('oil', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Oil</p></span>
                                    </div>
                                    <div class="width50">
                                        {!! Form::checkbox('lo_boy', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Lo-Boy </p></span>
                                    </div>
                                    <div class="width50">
                                        {!! Form::checkbox('hi_boy', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Hi-Boy </p></span>
                                    </div>
                                    <div class="width50">
                                        {!! Form::checkbox('horiz', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Horiz </p></span>
                                    </div>
                                    <div class="width50">
                                        {!! Form::checkbox('boiler', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Boiler</p></span>
                                    </div>
                                    <div class="width50">
                                        {!! Form::checkbox('baseboard', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Baseboard </p></span>
                                    </div>
                                </div>

                                <div class="boxrow-ws04-2 clearfix">
                                    <div class="col-md-6 col-sm-6 p-zero clearfix">

                                        <h2 class="h2-title01 clearfix">Existing Equip.</h2>
                                        <div class="rowblock01 clearfix">
                                            <div class="width50">
                                                {!! Form::checkbox('existing_equip_leave', '1', null, ['class' => 'form-radio']) !!}
                                                <span class="pl-10 f-600"> <p class="f01left-01">Leave </p></span>
                                            </div>
                                            <div class="width50">
                                                {!! Form::checkbox('existing_equip_remove', '1', null, ['class' => 'form-radio']) !!}
                                                <span class="pl-10 f-600"> <p class="f01left-01">Remove </p></span>
                                            </div>
                                        </div>
                                        <h2 class="h2-title01 clearfix">Extras: </h2>
                                        <div class="rowblock01 clearfix">
                                            <div class="width100">
                                                {!! Form::checkbox('extras_difficult_access', '1', null, ['class' => 'form-radio']) !!}
                                                <span class="pl-10 f-600"> <p class="f01left-01">Difficult Access </p></span>
                                            </div>
                                            <div class="width100">
                                                {!! Form::checkbox('extras_dismantle_equipment', '1', null, ['class' => 'form-radio']) !!}
                                                <span class="pl-10 f-600"> <p class="f01left-01">Dismantle Equipment </p></span>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6 col-sm-6 p-zero clearfix">

                                        <div class="boxrow-ws01 boxrow-ws01-2 clearfix">
                                            <div class="form-group clearfix">
                                                <p class="height-auto">BTU Gross</p>
                                                {!! Form::text('btu_gross', null, ['class' => 'form-control input-50']) !!}
                                            </div>
                                            <div class="form-group clearfix">
                                                <p class="height-auto">BTU Net </p>
                                                {!! Form::text('btu_net', null, ['class' => 'form-control input-50']) !!}
                                            </div>
                                            <div class="form-group clearfix">
                                                <p class="height-auto style-i">Make</p>
                                            </div>
                                            <div class="form-group clearfix">
                                                <p class="height-auto style-i">Model</p>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="boxrow-ws02 boxrow-ws05 clearfix">
                                <h3 class="h3-title1">Venting</h3>
                                <div class="boxrow-ws04-2 clearfix">


                                    <div class="col-md-4 col-sm-12 p-zero clearfix">
                                        <div class="form-group clearfix">
                                            <p class="height-auto">Roof Type, Pitch</p>
                                            {!! Form::textarea('roof_type_pitch', null,['rows'=>5]) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-12 clearfix">
                                        <div class="table-lt">
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-responsive">
                                                    <thead>
                                                        <tr>
                                                            <th class="col-md-3 text-center">&nbsp;</th>
                                                            <th class="col-md-3 text-center">Length </th>
                                                            <th class="col-md-3 text-center">Diameter </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody class="text-center">
                                                        <tr>
                                                            <td>
                                                                <p>C-Vent</p>
                                                            </td>
                                                            <td>{!! Form::text('c_vent_length', null, ['class' => 'form-control input-box-1']) !!}</td>
                                                            <td>{!! Form::text('c_vent_diameter', null, ['class' => 'form-control input-box-1']) !!}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p>B-Vent</p>
                                                            </td>
                                                            <td>{!! Form::text('b_vent_length', null, ['class' => 'form-control input-box-1']) !!}</td>
                                                            <td>{!! Form::text('b_vent_diameter', null, ['class' => 'form-control input-box-1']) !!}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <p>PVC</p>
                                                            </td>
                                                            <td>{!! Form::text('pvc_length', null, ['class' => 'form-control input-box-1']) !!}</td>
                                                            <td>{!! Form::text('pvc_diameter', null, ['class' => 'form-control input-box-1']) !!}</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="col-md-12 col-sm-12 p-zero clearfix">
                                        <div class="boxrow-ws05-1 clearfix">
                                            <div class="form-group clearfix">
                                                <p class="height-auto">Termination Location: </p>
                                                {!! Form::text('termination_location', null, ['class' => 'form-control input-50']) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 p-zero clearfix">
                                            <h2 class="h2-title01 clearfix">Vent to: </h2>
                                            <div class="rowblock01 rowblock501 clearfix">
                                                <div class="width50">
                                                    {!! Form::checkbox('vent_to_lined_chimney', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="">Lined Chimney </p></span>
                                                </div>
                                                <div class="width50">
                                                    {!! Form::checkbox('vent_to_masonry_unlined', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Masonry Unlined </p></span>
                                                </div>
                                                <div class="width50">
                                                    {!! Form::checkbox('vent_to_sidewall', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Sidewall </p></span>
                                                </div>
                                                <div class="width50">
                                                    {!! Form::checkbox('vent_to_other', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Other </p></span>
                                                </div>
                                                <div class="width50">
                                                    {!! Form::checkbox('vent_to_power', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Power </p></span>
                                                </div>
                                                <div class="width50">
                                                    {!! Form::checkbox('vent_to_new_roof_jack', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">New Roof Jack </p></span>
                                                </div>
                                                <div class="width50">
                                                    {!! Form::checkbox('vent_to_install_liner', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Install Liner </p></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="main-box-check main-box-check5-02 clearfix">
                                            <div class="row-h1 clearfix">
                                                <div class="ws-width50 clearfix">
                                                    <p>Induce Fan Draft Assist?</p>
                                                </div>
                                                <div class="ws-width50 clearfix">
                                                    <div class="p-blk1 clearfix">
                                                        {!! Form::radio('induce_fan_draft_assist', '1', null, ['class' => 'form-radio radio-ws1']) !!}

                                                        <p class="ws-fleft-01"> Yes </p>
                                                    </div>
                                                    <div class="p-blk1 clearfix">
                                                        {!! Form::radio('induce_fan_draft_assist', '2', null, ['class' => 'form-radio radio-ws1']) !!}
                                                        <p class="ws-fleft-01"> No </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row-h1 clearfix">
                                                <div class="ws-width50 clearfix">
                                                    <p>Combined?</p>
                                                </div>
                                                <div class="ws-width50 clearfix">
                                                    <div class="p-blk1 clearfix">
                                                        <p class="ws-fleft-01"> Yes </p>
                                                        {!! Form::radio('combined', '1', null, ['class' => 'form-radio radio-ws1']) !!}

                                                    </div>
                                                    <div class="p-blk1 clearfix">
                                                        <p class="ws-fleft-01"> No </p>
                                                        {!! Form::radio('combined', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 p-zero clearfix">
                                            <div class="table-lt2 clearfix">
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-responsive">
                                                        <thead>
                                                            <tr>
                                                                <th class="col-md-3 text-center">HWT</th>
                                                                <th class="col-md-3 text-center">Draft </th>
                                                                <th class="col-md-3 text-center">Size </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="text-center">
                                                            <tr>
                                                                <td>
                                                                    <p>Others </p>
                                                                </td>
                                                                <td>
                                                                    <p class="f-500">Water/Fan</p>
                                                                </td>
                                                                <td>
                                                                    {!! Form::text('others_water_fan_1', null, ['class' => 'form-control input-box-1']) !!}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <p>Others </p>
                                                                </td>
                                                                <td>
                                                                    <p class="f-500">Water/Fan</p>
                                                                </td>
                                                                <td>{!! Form::text('others_water_fan_2', null, ['class' => 'form-control input-box-1']) !!}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <p>Combined </p>
                                                                </td>
                                                                <td>
                                                                    <p class="f-500">Water/Fan</p>
                                                                </td>
                                                                <td>{!! Form::text('combined_size', null, ['class' => 'form-control input-box-1']) !!}</td>
                                                            </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            <div class="boxrow-ws02 boxrow-ws06 clearfix">
                                <h3 class="h3-title1">Ducts </h3>

                                <div class="main-box-check main-box-check6 clearfix">

                                    <div class="row-h1 clearfix">
                                        <div class="ws-width50 clearfix">
                                            <p>Insulate runs? </p>
                                        </div>
                                        <div class="ws-width50 clearfix">
                                            <div class="p-blk1 clearfix">
                                                {!! Form::radio('insulate_runs', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                                <p class="ws-fleft-01"> Yes </p>
                                            </div>
                                            <div class="p-blk1 clearfix">
                                                {!! Form::radio('insulate_runs', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                                <p class="ws-fleft-01"> No </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-h1 clearfix">
                                        <div class="ws-width50 clearfix">
                                            <p>Insulate plenums? </p>
                                        </div>
                                        <div class="ws-width50 clearfix">
                                            <div class="p-blk1 clearfix">
                                                <p class="ws-fleft-01"> Yes </p>
                                                {!! Form::radio('insulate_plenums', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                            </div>
                                            <div class="p-blk1 clearfix">
                                                <p class="ws-fleft-01"> No </p>
                                                {!! Form::radio('insulate_plenums', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-h1 clearfix">
                                        <div class="ws-width50 clearfix">
                                            <p>Insulate existing runs? </p>
                                        </div>
                                        <div class="ws-width50 clearfix">
                                            <div class="p-blk1 clearfix">
                                                {!! Form::radio('insulate_runs', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                                <p class="ws-fleft-01"> Yes </p>
                                            </div>
                                            <div class="p-blk1 clearfix">
                                                {!! Form::radio('insulate_runs', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                                <p class="ws-fleft-01"> No </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-h1 clearfix">
                                        <div class="ws-width50 clearfix">
                                            <p>Insulate trunk? </p>
                                        </div>
                                        <div class="ws-width50 clearfix">
                                            <div class="p-blk1 clearfix">
                                                <p class="ws-fleft-01"> Yes </p>
                                                {!! Form::radio('insulate_trunk', '1', null, ['class' => 'form-radio radio-ws1']) !!}

                                            </div>
                                            <div class="p-blk1 clearfix">
                                                <p class="ws-fleft-01"> No </p>
                                                {!! Form::radio('insulate_trunk', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row-h1 clearfix">
                                        <div class="ws-width50 clearfix">
                                            <p>Add balancing damper? </p>
                                        </div>
                                        <div class="ws-width50 clearfix">
                                            <div class="p-blk1 clearfix">
                                                {!! Form::radio('add_balancing_damper', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                                <p class="ws-fleft-01"> Yes </p>
                                            </div>
                                            <div class="p-blk1 clearfix">
                                                {!! Form::radio('add_balancing_damper', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                                <p class="ws-fleft-01"> No </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="boxrow-ws01 boxrow-ws601 clearfix">
                                        <div class="form-group clearfix">
                                            <p class="height-auto"># of new W/A runs </p>
                                            {!! Form::text('of_new_wa_runs', null, ['class' => 'form-control input-50']) !!}
                                        </div>
                                        <div class="form-group clearfix">
                                            <p class="height-auto"># of new R/A runs </p>
                                            {!! Form::text('of_new_ra_runs', null, ['class' => 'form-control input-50']) !!}
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="boxrow-ws02 boxrow-ws07 clearfix">
                                <h3 class="h3-title1">Located & marked with Owner </h3>
                                <div class="rowblock01 clearfix">
                                    <div class="width50">
                                        {!! Form::checkbox('registers', '1', null, ['class' => 'form-radio']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Registers </p></span>
                                    </div>
                                    <div class="width50">
                                        {!! Form::checkbox('chimney_terminations', '1', null, ['class' => 'form-radio ']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Chimney terminations </p></span>
                                    </div>
                                    <div class="width50">
                                        {!! Form::checkbox('meter_location', '1', null, ['class' => 'form-radio ']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Meter location </p></span>
                                    </div>
                                    <div class="width50">
                                        {!! Form::checkbox('vent_paths', '1', null, ['class' => 'form-radio ']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Vent paths </p></span>
                                    </div>
                                    <div class="width50">
                                        {!! Form::checkbox('rair_grilles', '1', null, ['class' => 'form-radio ']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">R-Air grilles </p></span>
                                    </div>
                                    <div class="width50">
                                        {!! Form::checkbox('gas_pipe_route', '1', null, ['class' => 'form-radio ']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Gas Pipe route </p></span>
                                    </div>
                                    <h2 class="h2-title01">Special routings:</h2>
                                    <div class="width50">
                                        {!! Form::checkbox('special_routings_curb_pipe', '1', null, ['class' => 'form-radio ']) !!}
                                        <span class="pl-10 f-600"> <p class="f01left-01">Curb/Pipe </p></span>
                                    </div>

                                </div>

                            </div>

                        </div>

                        <!-- End of Second Page half section  -->

                        <div class="col-md-6 col-sm-12">

                            <div class="boxrow-ws02 boxrow-ws08 clearfix">
                                <h3 class="h3-title1">Client Information </h3>

                                <div class="main-box-check main-box-check8 clearfix">
                                    <div class="boxrow-ws01 boxrow-ws601 clearfix">
                                        <div class="form-group clearfix">
                                            <p class="height-auto">Customer Name </p>
                                            {!! Form::text('client_information_customer_name', null, ['class' => 'form-control input-35']) !!}
                                        </div>
                                        <div class="form-group clearfix">
                                            <p class="height-auto">Business Name </p>
                                            {!! Form::text('client_information_business_name', null, ['class' => 'form-control input-35']) !!}
                                        </div>
                                        <div class="form-group clearfix">
                                            <p class="height-auto">Service Address </p>
                                            {!! Form::text('client_information_service_address', null, ['class' => 'form-control input-35']) !!}
                                        </div>
                                        <div class="form-group clearfix">
                                            <p class="height-auto">City/State/Zip </p>
                                            {!! Form::text('client_information_city_state_zip', null, ['class' => 'form-control input-35']) !!}
                                        </div>
                                        <div class="form-group clearfix">
                                            <p class="height-auto">Map/Grid </p>
                                            {!! Form::text('client_information_map_grid', null, ['class' => 'form-control input-35']) !!}
                                        </div>
                                        <div class="form-group clearfix">
                                            <p class="height-auto">Mailing Address </p>
                                            {!! Form::text('client_information_mailing_address', null, ['class' => 'form-control input-35']) !!}
                                        </div>
                                        <div class="form-group clearfix">
                                            <p class="height-auto">Home Phone </p>
                                            {!! Form::text('client_information_home_phone', null, ['class' => 'form-control input-35']) !!}
                                        </div>
                                        <div class="form-group clearfix">
                                            <p class="height-auto">Work </p>
                                            {!! Form::text('client_information_work', null, ['class' => 'form-control input-35']) !!}
                                        </div>
                                        <div class="form-group clearfix">
                                            <p class="height-auto">Email Address </p>
                                            {!! Form::text('client_information_email_address', null, ['class' => 'form-control input-35']) !!}
                                        </div>
                                        <div class="form-group clearfix">
                                            <p class="height-auto">House Size </p>
                                            {!! Form::text('client_information_house_size', null, ['class' => 'form-control input-35']) !!}
                                        </div>
                                        <div class="form-group clearfix">
                                            <p class="height-auto">Age </p>
                                            {!! Form::text('client_information_age', null, ['class' => 'form-control input-35']) !!}
                                        </div>

                                        <div class="rowblock01 rowblock08 clearfix">
                                            <div class="width33">
                                                <span class="pl-10 f-600"> <p class="f01left-01">Main</p></span>
                                                {!! Form::checkbox('client_information_main_upper_lower', 'main', null, ['class' => 'form-radio']) !!}
                                            </div>
                                            <div class="width33">
                                                <span class="pl-10 f-600"> <p class="f01left-01">Upper </p></span>
                                                {!! Form::checkbox('client_information_main_upper_lower', 'upper', null, ['class' => 'form-radio']) !!}
                                            </div>
                                            <div class="width33">
                                                <span class="pl-10 f-600"> <p class="f01left-01">Lower </p></span>
                                                {!! Form::checkbox('client_information_main_upper_lower', 'lower', null, ['class' => 'form-radio']) !!}
                                            </div>
                                        </div>

                                        <div class="rowblock01 rowblock08 rowblock08-2 clearfix">
                                            <div class="width33">
                                                <span class="pl-10 f-600"> <p class="f01left-01">Meter: </p></span>
                                            </div>
                                            <div class="width33">
                                                <span class="pl-10 f-600"> <p class="f01left-01">Existing </p></span>
                                                {!! Form::checkbox('client_information_meter_existing_ordered', 'existing', null, ['class' => 'form-radio']) !!}
                                            </div>
                                            <div class="width33">
                                                <span class="pl-10 f-600"> <p class="f01left-01">Ordered </p></span>
                                                {!! Form::checkbox('client_information_meter_existing_ordered', 'ordered', null, ['class' => 'form-radio']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group clearfix">
                                            <p class="height-auto">Date </p>
                                            {!! Form::text('client_information_date', null, ['class' => 'form-control input-35']) !!}
                                        </div>
                                        <div class="form-group clearfix">
                                            <p class="height-auto">Meter Set Date </p>
                                            {!! Form::text('client_information_meter_set_date', null, ['class' => 'form-control input-35']) !!}
                                        </div>


                                    </div>
                                </div>
                            </div>

                            <div class="boxrow-ws02 boxrow-ws09 clearfix">
                                <h3 class="h3-title1">Plenums & Air Quality </h3>

                                <div class="main-box-check main-box-check8 clearfix">
                                    <div class="boxrow-ws01 boxrow-ws601 clearfix">
                                        <div class="rowblock01 rowblock08 rowblock09 clearfix">
                                            <div class="width33">
                                                <span class="pl-10 f-600"> <p class="f01left-01 pl-0">Warm Air</p></span>
                                            </div>
                                            <div class="width33">
                                                {!! Form::checkbox('warm_air_new_transition', 'new', null, ['class' => 'form-radio']) !!}
                                                <span class="pl-10 f-600"> <p class="f01left-01">New </p></span>

                                            </div>
                                            <div class="width33">
                                                {!! Form::checkbox('warm_air_new_transition', 'transition', null, ['class' => 'form-radio']) !!}
                                                <span class="pl-10 f-600"> <p class="f01left-01">Transition </p></span>

                                            </div>
                                        </div>

                                        <div class="rowblock01 rowblock08 rowblock09 clearfix">
                                            <div class="width33">
                                                <span class="pl-10 f-600"> <p class="f01left-01 pl-0">Return Air </p></span>
                                            </div>
                                            <div class="width33">
                                                {!! Form::checkbox('return_air_new_transition', 'new', null, ['class' => 'form-radio']) !!}
                                                <span class="pl-10 f-600"> <p class="f01left-01">New </p></span>
                                            </div>
                                            <div class="width33">
                                                {!! Form::checkbox('return_air_new_transition', 'transition', null, ['class' => 'form-radio']) !!}
                                                <span class="pl-10 f-600"> <p class="f01left-01">Transition </p></span>
                                            </div>
                                            <div class="width33">
                                                {!! Form::checkbox('elevate_furnace', 'transition', null, ['class' => 'form-radio']) !!}
                                                <span class="pl-10 f-600"> <p class="f01left-01">Elevate furnace </p></span>
                                            </div>
                                        </div>

                                        <div class="rowblock01 rowblock08 rowblock09 clearfix">
                                            <div class="width33">
                                                <span class="pl-10 f-600"> <p class="f01left-01 pl-0">Duct Cleaning</p></span>
                                            </div>
                                            <div class="width33">
                                                {!! Form::radio('duct_cleaning', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                                <span class="pl-10 f-600"> <p class="f01left-01">Yes </p></span>
                                            </div>
                                            <div class="width33">
                                                {!! Form::radio('duct_cleaning', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                                <span class="pl-10 f-600"> <p class="f01left-01">No </p></span>
                                            </div>
                                        </div>

                                        <div class="rowblock01 rowblock08 rowblock09 clearfix">
                                            <div class="width33">
                                                <span class="pl-10 f-600"> <p class="f01left-01 pl-0">EAC </p></span>
                                            </div>
                                            <div class="width33">
                                                {!! Form::radio('eac', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                                <span class="pl-10 f-600"> <p class="f01left-01">Yes </p></span>
                                            </div>
                                            <div class="width33">
                                                {!! Form::radio('eac', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                                <span class="pl-10 f-600"> <p class="f01left-01">No </p></span>
                                            </div>
                                        </div>

                                        <div class="form-group form-group-input1 clearfix">
                                            <p class="height-auto">Existing </p>
                                            {!! Form::text('existing', null, ['class' => 'form-control input-70']) !!}
                                        </div>

                                        <div class="rowblock01 rowblock08 rowblock09 clearfix">
                                            <div class="width33">
                                                <span class="pl-10 f-600"> <p class="f01left-01 pl-0">Media Air Cleaner </p></span>
                                            </div>
                                            <div class="width33">
                                                {!! Form::radio('media_air_cleaner', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                                <span class="pl-10 f-600"> <p class="f01left-01">Yes </p></span>
                                            </div>
                                            <div class="width33">
                                                {!! Form::radio('media_air_cleaner', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                                <span class="pl-10 f-600"> <p class="f01left-01">No </p></span>
                                            </div>
                                        </div>

                                        <div class="rowblock01 rowblock08 rowblock09 clearfix">
                                            <div class="width33">
                                                <span class="pl-10 f-600"> <p class="f01left-01 pl-0">Humidifier </p></span>
                                            </div>
                                            <div class="width33">
                                                {!! Form::radio('humidifier', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                                <span class="pl-10 f-600"> <p class="f01left-01">Yes </p></span>
                                            </div>
                                            <div class="width33">
                                                {!! Form::radio('humidifier', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                                <span class="pl-10 f-600"> <p class="f01left-01">No </p></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="boxrow-ws02 boxrow-ws010 clearfix">
                                <h3 class="h3-title1">Gas Piping </h3>

                                <div class="main-box-check main-box-check8 clearfix">
                                    <div class="boxrow-ws01 boxrow-ws601 clearfix">

                                        <div class="form-group form-group-input1 clearfix">
                                            <p class="height-auto">Length </p>
                                            {!! Form::text('gas_piping_length', null, ['class' => 'form-control input-70']) !!}
                                        </div>

                                        <div class="form-group form-group-input1 clearfix">
                                            <p class="height-auto">Diameter </p>
                                            {!! Form::text('gas_piping_diameter', null, ['class' => 'form-control input-70']) !!}
                                        </div>

                                        <div class="rowblock01 rowblock08 rowblock09 clearfix">
                                            <div class="width33">
                                                {!! Form::checkbox('see_gas_piping_sheet', '1', null, ['class' => 'form-radio']) !!}
                                                <span class="pl-10 f-600"> <p class="f01left-01">See Gas Piping Sheet </p></span>
                                            </div>
                                            <div class="width33">
                                                <span class="pl-10 f-600"> <p class="f01left-01">Get from BDSR </p></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="boxrow-ws02 boxrow-ws011 clearfix">
                                <h3 class="h3-title1">Wiring & Controls </h3>

                                <div class="main-box-check main-box-check8 clearfix">
                                    <div class="boxrow-ws01 boxrow-ws601 clearfix">
                                        <div class="rowblock01 rowblock08 rowblock09 clearfix">

                                            <div class="col-md-6 col-sm-12 p-zero clearfix">
                                                <div class="width100">
                                                    {!! Form::checkbox('provide_separate_circuit', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Provide separate circuit </p></span>
                                                </div>
                                                <div class="width100">
                                                    {!! Form::checkbox('use_existing_circuit', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Use existing circuit </p></span>
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-sm-12 p-zero clearfix">
                                                <div class="border-c01 clearfix">
                                                    <div class="width100">
                                                        <span class="pl-10 f-600"> <p class="f01left-01 pl-0">Abandon Circuit? </p></span>
                                                    </div>
                                                    <div class="width33">
                                                        {!! Form::radio('abandon_circuit', '1', null, ['class' => 'form-radio radio-ws1']) !!}

                                                        <span class="pl-10 f-600"> <p class="f01left-01">Yes </p></span>
                                                    </div>
                                                    <div class="width33">
                                                        {!! Form::radio('abandon_circuit', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                                        <span class="pl-10 f-600"> <p class="f01left-01">No </p></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12 col-sm-12 p-zero cboth clearfix">

                                                <div class="width50">
                                                    {!! Form::checkbox('relocate_thermostat', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Relocate thermostat </p></span>
                                                </div>
                                                <div class="width50">
                                                    <p class="f01left-01">Extg #Tstat Wires </p>
                                                </div>
                                                <div class="w-100c clearfix">
                                                    <div class="width33">
                                                        {!! Form::checkbox('size_of_service', '1', null, ['class' => 'form-radio']) !!}
                                                        <span class="pl-10 f-600"> <p class="f01left-01">Size of service </p></span>
                                                    </div>
                                                    <div class="width33">
                                                        {!! Form::checkbox('size_of_service', '200 amp', null, ['class' => 'form-radio']) !!}
                                                        <span class="pl-10 f-600"> <p class="f01left-01">200 amp </p></span>
                                                    </div>
                                                    <div class="width33">
                                                        {!! Form::checkbox('size_of_service', 'other', null, ['class' => 'form-radio']) !!}
                                                        <span class="pl-10 f-600"> <p class="f01left-01">Other</p></span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group form-group-input1 mb0 clearfix">
                                            <p class="height-auto">Location: </p>
                                            {!! Form::text('location', null, ['class' => 'form-control input-70']) !!}
                                        </div>

                                        <div class="rowblock01 rowblock08 rowblock09 clearfix">
                                            <div class="width33">
                                                <span class="pl-10 f-600"> <p class="f01left-01 pl-0">Need stat wire Electrician needed? </p></span>
                                            </div>
                                            <div class="width33">
                                                {!! Form::radio('need_stat_wire_electrician_needed', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                                <span class="pl-10 f-600"> <p class="f01left-01">Yes </p></span>
                                            </div>
                                            <div class="width33">
                                                {!! Form::radio('need_stat_wire_electrician_needed', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                                <span class="pl-10 f-600"> <p class="f01left-01">No </p></span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="boxrow-ws02 boxrow-ws012 clearfix">
                                <h3 class="h3-title1">Miscellaneous </h3>

                                <div class="main-box-check main-box-check8 clearfix">
                                    <div class="boxrow-ws01 boxrow-ws601 clearfix">

                                        <div class="rowblock01 rowblock08 rowblock09 clearfix">

                                            <div class="w-100c clearfix">
                                                <div class="width33">
                                                    {!! Form::checkbox('install_condensate_pump', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Install Condensate Pump</p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::checkbox('floor_drain', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Floor Drain </p></span>
                                                </div>
                                            </div>

                                            <div class="form-group form-group-input02 mb0 clearfix">
                                                <p class="height-auto style-i">Condensate Pump Termination Location:</p>
                                                {!! Form::text('condensate_pump_termination_location', null, ['class' => 'form-control input-70w']) !!}
                                            </div>

                                            <div class="w-100c clearfix">
                                                <div class="width33">
                                                    {!! Form::checkbox('hot_water_tank_needed', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Hot Water Tank needed* <span class="f400 pl-30"> *details on back </span></p>
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="w-100c mt-10 clearfix">
                                                <div class="width33">
                                                    {!! Form::checkbox('outside_french_drain', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Outside French Drain </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::checkbox('provide_cac_option', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Provide CAC Option </p></span>
                                                </div>
                                            </div>

                                            <div class="w-100c mt-10 clearfix">
                                                <div class="width100">
                                                    {!! Form::checkbox('service_light_w_switch_at_entry_to_furnace_room', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Service Light w/ switch at entry to furnace room </p></span>
                                                </div>
                                            </div>

                                            <div class="w-100c clearfix">
                                                <div class="width100">
                                                    {!! Form::checkbox('miscellaneous_other', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Other </p></span>
                                                </div>
                                            </div>

                                            <div class="w-100c clearfix">
                                                <div class="width33">
                                                    {!! Form::checkbox('provide_combustion_air', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">provide combustion air </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::checkbox('drill_concrete', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Drill Concrete </p></span>
                                                </div>
                                            </div>

                                            <div class="w-100c mt-10 clearfix">
                                                <div class="width100">
                                                    {!! Form::checkbox('bring_extension_ladder', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Bring extension ladder  </p></span>
                                                </div>
                                            </div>

                                            <div class="form-group form-group-input1 clearfix">
                                                <p class="height-auto">Height: </p>
                                                {!! Form::text('miscellaneous_height', null, ['class' => 'form-control input-70']) !!}
                                            </div>

                                        </div>

                                        <div class="main-box-check main-box-checkw12 border-top1 clearfix">

                                            <div class="rowblock01 rowblock08 rowblock09 clearfix">
                                                <div class="width33">
                                                    <span class="pl-10 f-600"> <p class="f01left-01 pl-0">Asbestos Survey? </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::radio('asbestos_survey', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Yes </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::radio('asbestos_survey', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">No </p></span>
                                                </div>
                                            </div>
                                            <div class="rowblock01 rowblock08 rowblock09 clearfix">
                                                <div class="width33">
                                                    <span class="pl-10 f-600"> <p class="f01left-01 pl-0">Mold Survey? </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::radio('mold_survey', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Yes </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::radio('mold_survey', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">No </p></span>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="boxrow-ws02 boxrow-ws013 clearfix">
                                <h3 class="h3-title1">Permits </h3>

                                <div class="main-box-check main-box-check8 clearfix">
                                    <div class="boxrow-ws01 boxrow-ws601 clearfix">

                                        <div class="main-box-check main-box-checkw12 clearfix">

                                            <div class="mb-15 clearfix">
                                                <div class="form-group form-group-input1 form-group-input75 clearfix">
                                                    <p class="height-auto">Date: </p>
                                                    {!! Form::text('permits_date', null, ['class' => 'form-control input-70']) !!}
                                                </div>
                                                <div class="form-group form-group-input1 form-group-input25 clearfix">
                                                    <p class="height-auto">Order Date: </p>
                                                </div>
                                            </div>

                                            <div class="rowblock01 rowblock08 rowblock09 rowblock013 clearfix">
                                                <div class="width33">
                                                    <span class="pl-10 f-600"> <p class="f01left-01 pl-0">Gas Piping </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::radio('permit_gas_piping', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Yes </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::radio('permit_gas_piping', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">No </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::text('permit_order_date_gas_piping', null, ['class' => 'form-control form-control100']) !!}
                                                </div>
                                            </div>

                                            <div class="rowblock01 rowblock08 rowblock09 rowblock013 clearfix">
                                                <div class="width33">
                                                    <span class="pl-10 f-600"> <p class="f01left-01 pl-0">Mechanical </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::radio('permit_mechanical', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Yes </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::radio('permit_mechanical', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">No </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::text('permit_order_date_mechanical', null, ['class' => 'form-control form-control100']) !!}
                                                </div>
                                            </div>

                                            <div class="rowblock01 rowblock08 rowblock09 rowblock013 clearfix">
                                                <div class="width33">
                                                    <span class="pl-10 f-600"> <p class="f01left-01 pl-0">Plumbing </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::radio('permit_plumbing', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Yes </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::radio('permit_plumbing', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">No </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::text('permit_order_date_plumbing', null, ['class' => 'form-control form-control100']) !!}
                                                </div>
                                            </div>

                                            <div class="rowblock01 rowblock08 rowblock09 rowblock013 clearfix">
                                                <div class="width33">
                                                    <span class="pl-10 f-600"> <p class="f01left-01 pl-0">Electrical </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::radio('permit_electrical', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Yes </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::radio('permit_electrical', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">No </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::text('permit_order_date_electrical', null, ['class' => 'form-control form-control100']) !!}
                                                </div>
                                            </div>

                                            <div class="rowblock01 rowblock08 rowblock09 rowblock013 clearfix">
                                                <div class="width33">
                                                    <span class="pl-10 f-600"> <p class="f01left-01 pl-0">Boiler </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::radio('permit_boiler', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Yes </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::radio('permit_boiler', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">No </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::text('permit_order_date_boiler', null, ['class' => 'form-control form-control100']) !!}
                                                </div>
                                            </div>

                                            <div class="form-group form-group-input02 mb0 clearfix">
                                                <p class="height-auto">Projected Hours: </p>
                                                {!! Form::text('permit_projected_hours', null, ['class' => 'form-control input-70w']) !!}
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>

            <hr>

            <div class="row">
                <div class="worksheet-box2 clearfix">

                    <div class="fullboxright1 clearfix">
                        <div class="col-md-4 col-sm-6 clearfix">
                            <div class="img-thumb1 clearfix">
                                <img src="http://13.127.235.254/dev/laravel/mjm-mechanicals/public/assets/images/logo-pn.png" class="img-responsive mjm-logo01">
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6 order-2 clearfix">
                            <div class="center-align-content clearfix">
                                <h5>Project Worksheet</h5>
                            </div>
                        </div>

                    </div>

                    <div class="wssecond-03 clearfix">

                        <div class="box3-row1 clearfix">
                            <div class="col-md-12 col-sm-12 clearfix">
                                <div class="table-full1 table-full50 clearfix">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-responsive">
                                            <thead>
                                                <tr>
                                                    <th class="width-220 minth">ITEM</th>
                                                    <th class="width-120 minth">Del </th>
                                                    <th class="width-120 minth">USD </th>
                                                    <th class="width-220 minth">ITEM</th>
                                                    <th class="width-120 minth">Del </th>
                                                    <th class="width-120 minth">USD </th>
                                                </tr>
                                            </thead>
                                            <tbody class="text-center">
                                                <tr>
                                                    <td>
                                                        <p>GALV PIPE </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>R-8 FLEX DCT </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r8_flex_dct_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r8_flex_dct_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>3"</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_3_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_3_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>4"</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r8_flex_dct_4_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r8_flex_dct_4_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>4" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_4_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_4_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>5" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r8_flex_dct_5_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r8_flex_dct_5_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>5" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_5_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_5_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>6" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r8_flex_dct_6_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r8_flex_dct_6_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>6" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_6_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_6_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>7" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r8_flex_dct_7_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r8_flex_dct_7_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>7" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_7_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_7_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>8" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r8_flex_dct_8_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r8_flex_dct_8_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>8" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_8_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_8_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>9" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r8_flex_dct_9_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r8_flex_dct_9_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>10" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_10_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_10_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>12" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r8_flex_dct_12_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r8_flex_dct_12_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>12" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_12_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_12_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>14" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r8_flex_dct_14_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r8_flex_dct_14_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>13" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_13_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_13_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>16" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r8_flex_dct_16_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r8_flex_dct_16_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>14" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_14_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_14_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>5" Saddle</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_saddle_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_saddle_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>16" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_16_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('galv_pipe_16_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>6" Saddle</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_saddle_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_saddle_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>ELBOWS </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>7" Saddle</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('7_saddle_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('7_saddle_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>3" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_3_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_3_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>8" Saddle</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('8_saddle_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('8_saddle_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>4" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_4_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_4_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>6" Oval BT 90</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_oval_bt_90_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_oval_bt_90_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>5" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_5_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_5_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>6" Oval BT Torp</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_oval_bt_torp_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_oval_bt_torp_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>6" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_6_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_6_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>6" Oval BT 45-6</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_oval_bt_45_6_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_oval_bt_45_6_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>7" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_7_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_7_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>6" Oval BT St</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_oval_bt_st_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_oval_bt_st_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>8" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_8_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_8_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>6" Oval Cap</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_oval_cap_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_oval_cap_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>9" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_9_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_9_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>6" Oval Pipe</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_oval_pipe_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_oval_pipe_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>10" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_10_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_10_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>7" Oval BT 90</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('7_oval_bt_90_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('7_oval_bt_90_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>12" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_12_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_12_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>7" Oval BT Torp</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('7_oval_bt_torp_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('7_oval_bt_torp_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>13" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_13_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_13_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>7" Oval BT 45-7</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('7_oval_bt_45_7_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('7_oval_bt_45_7_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>14" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_14_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_14_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>7" Oval BT St</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('7_oval_bt_st_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('7_oval_bt_st_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>16" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_16_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('elbows_16_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>7" Oval Cap</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('7_oval_cap_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('7_oval_cap_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>FRESH AIR </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('fresh_air_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('fresh_air_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>7" Oval Pipe</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('7_oval_pipe_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('7_oval_pipe_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>6" Damper </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_damper_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_damper_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>8" Oval Bt 90</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('8_oval_bt_90_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('8_oval_bt_90_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>7" Damper</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('7_damper_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('7_damper_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>8" Oval BT Torp</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('8_oval_bt_torp_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('8_oval_bt_torp_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>8" Damper</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('8_damper_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('8_damper_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>8" Oval BT 45-8</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('8_oval_bt_45_8_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('8_oval_bt_45_8_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>Timer </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('timer_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('timer_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>8" Oval BT St</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('8_oval_bt_st_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('8_oval_bt_st_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>&nbsp; </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('textbox_1', null, ['disabled'=>'disabled','class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('textbox_2', null, ['disabled'=>'disabled','class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>8" Oval Cap</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('8_oval_cap_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('8_oval_cap_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>&nbsp; </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('textbox_3', null, ['disabled'=>'disabled','class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('textbox_4', null, ['disabled'=>'disabled','class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>8" Oval Pipe</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('8_oval_pipe_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('8_oval_pipe_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>&nbsp; </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('textbox_5', null, ['disabled'=>'disabled','class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('textbox_6', null, ['disabled'=>'disabled','class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>&nbsp;</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('textbox_7', null, ['disabled'=>'disabled','class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('textbox_8', null, ['disabled'=>'disabled','class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>&nbsp; </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('textbox_9', null, ['disabled'=>'disabled','class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('textbox_10', null, ['disabled'=>'disabled','class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>&nbsp;</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('textbox_11', null, ['disabled'=>'disabled','class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('textbox_12', null, ['disabled'=>'disabled','class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="table-full1 table-full2 table-full50 clearfix">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-responsive">
                                            <thead>
                                                <tr>
                                                    <th class="width-220 minth">ITEM</th>
                                                    <th class="width-120 minth">Del </th>
                                                    <th class="width-120 minth">USD </th>
                                                    <th class="width-220 minth">ITEM</th>
                                                    <th class="width-120 minth">Del </th>
                                                    <th class="width-120 minth">USD </th>
                                                </tr>
                                            </thead>
                                            <tbody class="text-center">
                                                <tr>
                                                    <td>
                                                        <p>R-4 FLEX DCT</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r4_flex_dct_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r4_flex_dct_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>FLR REGS</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('flr_regs_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('flr_regs_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>4"</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r4_flex_dct_4_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r4_flex_dct_4_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>2-1/4 * 10</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('flr_regs_2_1_4_10_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('flr_regs_2_1_4_10_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>5" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r4_flex_dct_5_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r4_flex_dct_5_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>2-1/4 * 12</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('flr_regs_2_1_4_12_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('flr_regs_2_1_4_12_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>6" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r4_flex_dct_6_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r4_flex_dct_6_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>4 * 10 </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('flr_regs_4_10_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('flr_regs_4_10_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>7" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r4_flex_dct_7_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r4_flex_dct_7_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>4 * 12 </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('flr_regs_4_12_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('flr_regs_4_12_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>8" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r4_flex_dct_8_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r4_flex_dct_8_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>4 * 14</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('flr_regs_4_14_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('flr_regs_4_14_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>9" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r4_flex_dct_9_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r4_flex_dct_9_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>6 * 10 </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('flr_regs_6_10_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('flr_regs_6_10_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>12" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r4_flex_dct_12_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r4_flex_dct_12_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>10 * 4 </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('flr_regs_10_4_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('flr_regs_10_4_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>14" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r4_flex_dct_14_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r4_flex_dct_14_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>12 * 4 </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('flr_regs_12_4_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('flr_regs_12_4_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>16" </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r4_flex_dct_16_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('r4_flex_dct_16_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>14 * 4 </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('flr_regs_14_4_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('flr_regs_14_4_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>W/A BOOTS</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('wa_boots_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('wa_boots_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>R/A GRILLE </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('ra_grille_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('ra_grille_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>4 * 10 ST </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_10_st_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_10_st_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>8 * 30</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('ra_grille_8_30_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('ra_grille_8_30_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p> Torp </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_10_st_torp_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_10_st_torp_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>10 * 30 </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('ra_grille_10_30_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('ra_grille_10_30_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>90 </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_10_st_90_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_10_st_90_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>17 * 30 </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('ra_grille_17_30_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('ra_grille_17_30_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>2 * 10ST</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('2_10_st_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('2_10_st_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>10 * 14 </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('ra_grille_10_14_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('ra_grille_10_14_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>Torp</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('2_10_st_torp_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('2_10_st_torp_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>14 * 14 </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('ra_grille_14_14_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('ra_grille_14_14_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>90 </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('2_10_st_90_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('2_10_st_90_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>20 * 14 </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('ra_grille_20_14_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('ra_grille_20_14_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>4 * 12 * 7 ST </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_12_7_st_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_12_7_st_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>20 * 20</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('ra_grille_20_20_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('ra_grille_20_20_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>Torp </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_12_7_st_torp_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_12_7_st_torp_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>20 * 24 </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('ra_grille_20_24_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('ra_grille_20_24_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>90 </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_12_7_st_90_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_12_7_st_90_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>24 * 30 </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('ra_grille_24_30_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('ra_grille_24_30_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>HWT 20 * 20 </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('hwt_20_20_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('hwt_20_20_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>TAPE ETC.</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('tape_etc_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('tape_etc_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>HWT 25 * 25 </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('hwt_25_25_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('hwt_25_25_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>2" 333 Tape </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('2_333_tape_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('2_333_tape_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>Base 14 * 28 * 18</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('base_14_28_18_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('base_14_28_18_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>2" Foil Tape</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('2_foil_tape_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('2_foil_tape_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>Base 14 * 28 * 24</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('base_14_28_24_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('base_14_28_24_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>3" DuctWrap</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_ductWrap_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_ductWrap_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>Base 17 * 28 * 18</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('base_17_28_18_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('base_17_28_18_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>3" Fabric Tape</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_fabric_tape_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_fabric_tape_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>Plen 13 * 22 * 35</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('plen_13_22_35_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('plen_13_22_35_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>Caddy Strap</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('caddy_strap_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('caddy_strap_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>Plen 16 * 22 * 35</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('plen_16_22_35_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('plen_16_22_35_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>Duro Strap</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('duro_strap_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('duro_strap_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>D Cleat Light</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('d_cleat_light_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('d_cleat_light_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>Pandt Strap</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('pandt_strap_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('pandt_strap_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>D Cleat 24g</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('d_cleat_24g_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('d_cleat_24g_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>CaulK - Silicone</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('caulk_silicone_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('caulk_silicone_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>S Cleat Light</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('s_cleat_light_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('s_cleat_light_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>Toe Kicks</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('toe_kicks_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('toe_kicks_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>S Cleat 24g</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('s_cleat_24g_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('s_cleat_24g_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>&nbsp;</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('textbox_13', null, ['disabled'=>'disabled','class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('textbox_14', null, ['disabled'=>'disabled','class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>Stat wire 18-5</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('stat_wire_18_5_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('stat_wire_18_5_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>&nbsp;</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('textbox_15', null, ['disabled'=>'disabled','class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('textbox_16', null, ['disabled'=>'disabled','class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>Stat wire 18-8</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('stat_wire_18_8_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('stat_wire_18_8_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>&nbsp;</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('textbox_17', null, ['disabled'=>'disabled','class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('textbox_18', null, ['disabled'=>'disabled','class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>Literature Rack</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('literature_rack_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('literature_rack_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        <p>&nbsp;</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('textbox_19', null, ['disabled'=>'disabled','class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('textbox_20', null, ['disabled'=>'disabled','class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="table-full1 table-full2 table-full3 clearfix">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-responsive">
                                            <thead>
                                                <tr>
                                                    <th class="width-220 minth">ITEM</th>
                                                    <th class="width-120 minth">Del </th>
                                                    <th class="width-120 minth">USD </th>
                                                    <th class="width-120 minth">Del </th>
                                                    <th class="width-120 minth">USD </th>
                                                    <th class="width-120 minth">Del </th>
                                                    <th class="width-120 minth">USD </th>
                                                    <th class="width-120 minth">Del </th>
                                                    <th class="width-120 minth">USD </th>
                                                </tr>
                                            </thead>
                                            <tbody class="text-center">

                                                <tr>
                                                    <td>
                                                        <p>&nbsp;</p>
                                                    </td>
                                                    <td colspan="2" class="text-center">
                                                        <p>3"</p>
                                                    </td>
                                                    <td colspan="2" class="text-center">
                                                        <p>4"</p>
                                                    </td>
                                                    <td colspan="2" class="text-center">
                                                        <p>5"</p>
                                                    </td>
                                                    <td colspan="2" class="text-center">
                                                        <p>6"</p>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td>
                                                        <p>B-VENT</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_b_vent_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_b_vent_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_b_vent_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_b_vent_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_b_vent_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_b_vent_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_b_vent_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_b_vent_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>90 Elbows</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_90_elbows_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_90_elbows_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_90_elbows_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_90_elbows_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_90_elbows_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_90_elbows_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_90_elbows_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_90_elbows_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>45 Elbows</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_45_elbows_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_45_elbows_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_45_elbows_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_45_elbows_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>

                                                    <td>
                                                        {!! Form::text('5_45_elbows_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_45_elbows_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_45_elbows_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_45_elbows_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>5' Length</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_5_length_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_5_length_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_5_length_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_5_length_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_5_length_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_5_length_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_5_length_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_5_length_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>3' Length</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_3_length_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_3_length_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_3_length_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_3_length_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_3_length_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_3_length_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_3_length_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_3_length_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>2' Length</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_2_length_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_2_length_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_2_length_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_2_length_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_2_length_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_2_length_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_2_length_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_2_length_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>1' Length</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_1_length_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_1_length_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_1_length_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_1_length_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_1_length_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_1_length_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_1_length_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_1_length_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>1' Adjustable</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_1_adjustable_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_1_adjustable_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_1_adjustable_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_1_adjustable_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_1_adjustable_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_1_adjustable_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_1_adjustable_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_1_adjustable_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>TEE</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_tee_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_tee_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_tee_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_tee_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_tee_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_tee_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_tee_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_tee_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>WYES</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_wyes_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_wyes_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_wyes_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_wyes_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_wyes_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_wyes_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_wyes_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_wyes_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>Increasers</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_increasers_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_increasers_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_increasers_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_increasers_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_increasers_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_increasers_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_increasers_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_increasers_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>4/12 Flash</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_4_12_flash_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_4_12_flash_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_4_12_flash_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_4_12_flash_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_4_12_flash_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_4_12_flash_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_4_12_flash_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_4_12_flash_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>5/12 Flash</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_5_12_flash_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_5_12_flash_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_5_12_flash_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_5_12_flash_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_5_12_flash_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_5_12_flash_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_5_12_flash_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_5_12_flash_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>6/12 Flash</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_6_12_flash_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_6_12_flash_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_6_12_flash_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_6_12_flash_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_6_12_flash_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_6_12_flash_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_6_12_flash_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_6_12_flash_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>7/12 Flash</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_7_12_flash_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_7_12_flash_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_7_12_flash_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_7_12_flash_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_7_12_flash_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_7_12_flash_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_7_12_flash_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_7_12_flash_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>Reducer</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_reducer_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_reducer_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_reducer_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_reducer_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_reducer_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_reducer_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_reducer_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_reducer_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>CAP</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_cap_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_cap_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_cap_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_cap_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_cap_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_cap_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_cap_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_cap_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p>COLLAR</p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_collar_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('3_collar_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_collar_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('4_collar_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_collar_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('5_collar_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_collar_del', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('6_collar_usd', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div class="boxrow-ws02 clearfix">
                                    <h3 class="h3-title1">Furnace Type </h3>

                                    <div class="main-box-check main-box-check8 clearfix">
                                        <div class="boxrow-ws01 boxrow-ws601 clearfix">
                                            <div class="rowblock01 rowblock08 rowblock09 clearfix">

                                                <div class="col-md-12 col-sm-12 p-zero cboth clearfix">

                                                    <div class="w-100c clearfix">
                                                        <div class="width33">
                                                            {!! Form::checkbox('furnace_type_up_flow', '1', null, ['class' => 'form-radio']) !!}
                                                            <span class="pl-10 f-600"> <p class="f01left-01">Up Flow</p></span>
                                                        </div>
                                                        <div class="width33">
                                                            {!! Form::checkbox('furnace_type_counter_flow', '1', null, ['class' => 'form-radio']) !!}
                                                            <span class="pl-10 f-600"> <p class="f01left-01">Counter Flow </p></span>
                                                        </div>
                                                        <div class="width33">
                                                            {!! Form::checkbox('furnace_type_condensing', '1', null, ['class' => 'form-radio']) !!}
                                                            <span class="pl-10 f-600"> <p class="f01left-01">Condensing </p></span>
                                                        </div>
                                                        <div class="width33">
                                                            {!! Form::checkbox('furnace_type_horizontal', '1', null, ['class' => 'form-radio']) !!}
                                                            <span class="pl-10 f-600"> <p class="f01left-01">Horizontal </p></span>
                                                        </div>
                                                    </div>

                                                    <div class="form-group teaxtarea001 clearfix">
                                                        <p class="height-auto">Notes:</p>
                                                        {!! Form::textarea('furnace_type_notes', null,['class'=>'',"rows"=>'3']) !!}
                                                    </div>


                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="boxrow-ws02 clearfix">
                                    <h3 class="h3-title1">Equipment </h3>
                                    <div class="box03-1 box31 clearfix">
                                        <div class="col-md-12 col-sm-12 p-zero clearfix">
                                            <div class="col-md-4 col-sm-4 clearfix">
                                                <div class="form-group form-group-input02 mb0 clearfix">
                                                    <p class="height-auto">Furnace </p>
                                                    {!! Form::text('equipment_furnace', null, ['class' => 'form-control input-70w']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 clearfix">
                                                <div class="form-group form-group-input02 mb0 clearfix">
                                                    <p class="height-auto">Make </p>
                                                    {!! Form::text('equipment_make', null, ['class' => 'form-control input-70w']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 clearfix">
                                                <div class="form-group form-group-input02 mb0 clearfix">
                                                    <p class="height-auto">Model </p>
                                                    {!! Form::text('equipment_model', null, ['class' => 'form-control input-70w']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 clearfix">
                                            <div class="form-group form-group-input02 mb0 clearfix">
                                                <p class="height-auto">Serial # </p>
                                                {!! Form::text('equipment_serial_1', null, ['class' => 'form-control input-70w']) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 p-zero clearfix">
                                            <div class="col-md-4 col-sm-4 clearfix">
                                                <div class="form-group form-group-input02 mb0 clearfix">
                                                    <p class="height-auto">EAC/Filter </p>
                                                    {!! Form::text('equipment_eac_filter', null, ['class' => 'form-control input-70w']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 clearfix">
                                                <div class="form-group form-group-input02 mb0 clearfix">
                                                    <p class="height-auto">Size </p>
                                                    {!! Form::text('equipment_eac_filte_size', null, ['class' => 'form-control input-70w']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 p-zero clearfix">
                                            <div class="col-md-4 col-sm-4 clearfix">
                                                <div class="form-group form-group-input02 mb0 clearfix">
                                                    <p class="height-auto">Thermostat </p>
                                                    {!! Form::text('equipment_thermostat', null, ['class' => 'form-control input-70w']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 clearfix">
                                                <div class="form-group form-group-input02 mb0 clearfix">
                                                    <p class="height-auto">Make </p>
                                                    {!! Form::text('equipment_thermostat_make', null, ['class' => 'form-control input-70w']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 clearfix">
                                                <div class="form-group form-group-input02 mb0 clearfix">
                                                    <p class="height-auto">Model </p>
                                                    {!! Form::text('equipment_thermostat_model', null, ['class' => 'form-control input-70w']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 p-zero clearfix">
                                            <div class="col-md-4 col-sm-4 clearfix">
                                                <div class="form-group form-group-input02 mb0 clearfix">
                                                    <p class="height-auto">Cond. Unit </p>
                                                    {!! Form::text('equipment_cond_unit', null, ['class' => 'form-control input-70w']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 clearfix">
                                                <div class="form-group form-group-input02 mb0 clearfix">
                                                    <p class="height-auto">Make </p>
                                                    {!! Form::text('equipment_cond_unit_make', null, ['class' => 'form-control input-70w']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 clearfix">
                                                <div class="form-group form-group-input02 mb0 clearfix">
                                                    <p class="height-auto">Model </p>
                                                    {!! Form::text('equipment_cond_unit_model', null, ['class' => 'form-control input-70w']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 clearfix">
                                            <div class="form-group form-group-input02 mb0 clearfix">
                                                <p class="height-auto">Serial # </p>
                                                {!! Form::text('equipment_serial_2', null, ['class' => 'form-control input-70w']) !!}
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 p-zero clearfix">
                                            <div class="col-md-4 col-sm-4 clearfix">
                                                <div class="form-group form-group-input02 mb0 clearfix">
                                                    <p class="height-auto">Evap Coil </p>
                                                    {!! Form::text('equipment_evap_coil', null, ['class' => 'form-control input-70w']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 clearfix">
                                                <div class="form-group form-group-input02 mb0 clearfix">
                                                    <p class="height-auto">Make </p>
                                                    {!! Form::text('equipment_evap_coi_make', null, ['class' => 'form-control input-70w']) !!}
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 clearfix">
                                                <div class="form-group form-group-input02 mb0 clearfix">
                                                    <p class="height-auto">Model </p>
                                                    {!! Form::text('equipment_evap_coi_model', null, ['class' => 'form-control input-70w']) !!}
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="boxrow-ws02 clearfix">
                                    <h3 class="h3-title1">Hot Water Tank </h3>
                                    <div class="box31 box32 clearfix">

                                        <div class="col-md-12 col-sm-12 clearfix">
                                            <div class="form-group form-group-input02 mb0 clearfix">
                                                <p class="height-auto">Size </p>
                                                {!! Form::text('hot_water_tank_size', null, ['class' => 'form-control input-70w']) !!}
                                            </div>
                                        </div>

                                        <div class="col-md-12 col-sm-12 clearfix">

                                            <div class="rowblock01 rowblock08 rowblock09 clearfix">
                                                <div class="width100">
                                                    <span class="pl-10 f-600"> <p class="f01left-01 pl-0">Type: </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::checkbox('hot_water_tank_type_standard_atmosphere', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Standard Atmosphere </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::checkbox('hot_water_tank_type_direct_vent', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Direct Vent </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::checkbox('hot_water_tank_type_induced_vent', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Induced Vent </p></span>
                                                </div>
                                            </div>

                                            <div class="main-box-check main-box-checkw12 clearfix">
                                                <div class="rowblock01 rowblock08 rowblock09 clearfix">
                                                    <div class="width33">
                                                        <span class="pl-10 f-600"> <p class="f01left-01 pl-0">Stand Required </p></span>
                                                    </div>
                                                    <div class="width33">
                                                        {!! Form::radio('hot_water_tank_stand_required', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                                        <span class="pl-10 f-600"> <p class="f01left-01">Yes </p></span>
                                                    </div>
                                                    <div class="width33">
                                                        {!! Form::radio('hot_water_tank_stand_required', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                                        <span class="pl-10 f-600"> <p class="f01left-01">No </p></span>
                                                    </div>
                                                </div>
                                                <div class="rowblock01 rowblock08 rowblock09 clearfix">
                                                    <div class="width33">
                                                        <span class="pl-10 f-600"> <p class="f01left-01 pl-0">Expansion Tank </p></span>
                                                    </div>
                                                    <div class="width33">
                                                        {!! Form::radio('hot_water_tank_expansion_tank', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                                        <span class="pl-10 f-600"> <p class="f01left-01">Yes </p></span>
                                                    </div>
                                                    <div class="width33">
                                                        {!! Form::radio('hot_water_tank_expansion_tank', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                                        <span class="pl-10 f-600"> <p class="f01left-01">No </p></span>
                                                    </div>
                                                </div>
                                                <div class="rowblock01 rowblock08 rowblock09 clearfix">
                                                    <div class="width33">
                                                        <span class="pl-10 f-600"> <p class="f01left-01 pl-0">PRV </p></span>
                                                    </div>
                                                    <div class="width33">
                                                        {!! Form::radio('hot_water_tank_prv', '1', null, ['class' => 'form-radio radio-ws1']) !!}
                                                        <span class="pl-10 f-600"> <p class="f01left-01">Yes </p></span>
                                                    </div>
                                                    <div class="width33">
                                                        {!! Form::radio('hot_water_tank_prv', '0', null, ['class' => 'form-radio radio-ws1']) !!}
                                                        <span class="pl-10 f-600"> <p class="f01left-01">No </p></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="rowblock01 rowblock08 rowblock09 clearfix">
                                                <div class="width33">
                                                    {!! Form::checkbox('hot_water_tank_t_p_drain', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">T & P Drain </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::checkbox('hot_water_tank_run_to_outside', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Run to outside </p></span>
                                                </div>
                                            </div>

                                            <div class="rowblock01 rowblock08 rowblock09 clearfix">
                                                <div class="width33">
                                                    {!! Form::checkbox('hot_water_tank_floor', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Floor </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::checkbox('hot_water_tank_exist', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Exist </p></span>
                                                </div>
                                                <div class="width33">
                                                    {!! Form::checkbox('hot_water_tank_pan', '1', null, ['class' => 'form-radio']) !!}
                                                    <span class="pl-10 f-600"> <p class="f01left-01">Pan </p></span>
                                                </div>
                                            </div>

                                        </div>



                                    </div>
                                </div>

                                <div class="table-full1 table-full4 clearfix">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-responsive">
                                            <thead>
                                                <tr>
                                                    <th class="width-220 minth">Equip.</th>
                                                    <th class="width-120 minth">Make </th>
                                                    <th class="width-120 minth">Modal </th>
                                                </tr>
                                            </thead>
                                            <tbody class="text-center">
                                                <tr>
                                                    <td>
                                                        <p> Water Htr. </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('water_htr_make', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('water_htr_model', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p> Serial# </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('serial_3_make', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('serial_3_model', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p> Fireplace </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('fireplace_make', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('fireplace_model', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p> Rooftop Unit </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('rooftop_unit_make', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('rooftop_unit_model', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p> Serial # </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('serial_4_make', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('serial_4_model', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p> Wall Heater </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('wall_heater_make', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('wall_heater_model', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p> Other </p>
                                                    </td>
                                                    <td>
                                                        {!! Form::text('other_make', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                    <td>
                                                        {!! Form::text('other_model', null, ['class' => 'form-control input-box-1']) !!}
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>



        </div>
        <div class="grid-box-1 clearfix">
            <div class=" pull-left">
                {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</div>
<!-- End of box-div -->
@endsection
@push('script-head')
<script type="text/javascript">
    $(function () {
            $('input[name="quoted_by"]').datetimepicker({ format: 'YYYY-MM-DD' });
            $('input[name="sold_date"]').datetimepicker({ format: 'YYYY-MM-DD' });
            $('input[name="install_date"]').datetimepicker({ format: 'YYYY-MM-DD' });
            $('input[name="apt_date"]').datetimepicker({ format: 'YYYY-MM-DD' });
            $('input[name="client_information_date"]').datetimepicker({format: 'YYYY-MM-DD' });
            $('input[name="client_information_meter_set_date"]').datetimepicker({ format: 'YYYY-MM-DD' });
            $('input[name="permits_date"]').datetimepicker({ format: 'YYYY-MM-DD' });
            $('input[name="permit_order_date_mechanical"]').datetimepicker({ format: 'YYYY-MM-DD' });
            $('input[name="permit_order_date_plumbing"]').datetimepicker({ format: 'YYYY-MM-DD' });
            $('input[name="permit_order_date_gas_piping"]').datetimepicker({ format: 'YYYY-MM-DD' });
            $('input[name="permit_order_date_electrical"]').datetimepicker({ format: 'YYYY-MM-DD' });
            $('input[name="permit_order_date_boiler"]').datetimepicker({ format: 'YYYY-MM-DD' });
            $('input[name="time"]').datetimepicker({ format: 'HH:mm:ss' });
        });


        $('input[name="of_stat_wires"]').on('change', function() {
            $('input[name="of_stat_wires"]').not(this).prop('checked', false);
        });

        $('input[name="motor_size"]').on('change', function() {
            $('input[name="motor_size"]').not(this).prop('checked', false);
        });
</script>


@endpush
