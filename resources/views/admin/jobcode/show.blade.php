@extends('layouts.backend')

@section('title',trans('jobcode.view_jobcode'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('jobcode.jobcode')</div>
                <div class="panel-body">

                    <a href="{{ URL::previous() }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('jobcode.back')
                        </button>
                    </a>
                    @if(Auth::user()->can('access.user.edit'))
                    <a href="{{ url('/admin/jobcode/' . $jobcode->id . '/edit') }}" title="Edit jobcode">
                        <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                            @lang('jobcode.edit_jobcode')
                        </button>
                    </a>
                    @endif

                    <br/>
                    <br/>


                    <div class="table-responsive">
                        <table class="table table-borderless">
                            <tbody>

                            <tr>
                                <td>@lang('jobcode.id')</td>
                                <td>{{ $jobcode->id }}</td>
                            </tr>


                            <tr>
                                <td>@lang('jobcode.job_code')</td>
                                <td>{{ $jobcode->job_code }}</td>
                            </tr>

                            <tr>
                                <td>@lang('jobcode.description')</td>
                                <td>{{ $jobcode->description }}</td>
                            </tr>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection