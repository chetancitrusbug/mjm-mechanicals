<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobProjrctWorksheet2 extends Model
{
    use SoftDeletes;

    public $table='job_project_worksheet_form_2';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'job_project_worksheet_form_1_id', 'sold_date', 'install_date', 'quoted_by', 'job_number', 'apt_date', 'time', 'furnace', 'water_heater', 'air_conditioning', 'ductwork', 'fireplace_other', 'source_yellow_pages', 'source_radio', 'source_tv', 'source_referral', 'source_home_show', 'source_internet_other', 'entry_arrangements_special_instructions', 'see_our_access_notes', 'send_letter', 'gas_pipe_size', 'fa_electric', 'conv_oil', 'oil', 'lo_boy', 'hi_boy', 'horiz', 'boiler', 'baseboard', 'existing_equip_leave', 'existing_equip_remove', 'extras_difficult_access', 'extras_dismantle_equipment', 'btu_gross', 'btu_net', 'make', 'model', 'roof_type_pitch', 'c_vent_length', 'c_vent_diameter', 'b_vent_length', 'b_vent_diameter', 'pvc_length', 'pvc_diameter', 'termination_location', 'vent_to_lined_chimney', 'vent_to_sidewall', 'vent_to_power', 'vent_to_install_liner', 'vent_to_masonry_unlined', 'vent_to_other', 'vent_to_new_roof_jack', 'induce_fan_draft_assist', 'combined', 'others_water_fan_1', 'others_size_1', 'others_water_fan_2', 'others_size_2', 'combined_water_fan', 'combined_size', 'insulate_runs', 'insulate_plenums', 'insulate_existing_runs', 'insulate_trunk', 'add_balancing_damper', 'of_new_wa_runs', 'of_new_ra_runs', 'registers', 'chimney_terminations', 'meter_location', 'vent_paths', 'rair_grilles', 'gas_pipe_route', 'special_routings_curb_pipe', 'client_information_customer_name', 'client_information_business_name', 'client_information_service_address', 'client_information_city_state_zip', 'client_information_map_grid', 'client_information_mailing_address', 'client_information_home_phone', 'client_information_work', 'client_information_email_address', 'client_information_house_size', 'client_information_age', 'client_information_main_upper_lower', 'client_information_meter_existing_ordered', 'client_information_date', 'client_information_meter_set_date', 'warm_air_new_transition', 'return_air_new_transition', 'elevate_furnace', 'duct_cleaning', 'eac', 'existing', 'media_air_cleaner', 'humidifier', 'gas_piping_length', 'gas_piping_diameter', 'see_gas_piping_sheet', 'provide_separate_circuit', 'use_existing_circuit', 'relocate_thermostat', 'abandon_circuit', 'size_of_service', 'location', 'need_stat_wire_electrician_needed', 'install_condensate_pump', 'floor_drain', 'condensate_pump_termination_location', 'hot_water_tank_needed', 'outside_french_drain', 'provide_cac_option', 'service_light_w_switch_at_entry_to_furnace_room', 'miscellaneous_other', 'provide_combustion_air', 'drill_concrete', 'bring_extension_ladder', 'miscellaneous_height', 'asbestos_survey', 'mold_survey', 'permits_date', 'permit_order_date_gas_piping', 'permit_gas_piping', 'permit_order_date_mechanical','permit_mechanical','permit_order_date_plumbing','permit_plumbing','permit_order_date_electrical','permit_electrical','permit_order_date_boiler','permit_boiler','permit_projected_hours','status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
