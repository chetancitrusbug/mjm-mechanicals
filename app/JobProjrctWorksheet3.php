<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobProjrctWorksheet3 extends Model
{
    use SoftDeletes;

    public $table='job_project_worksheet_form_3';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'job_project_worksheet_form_1_id', 'galv_pipe_del', 'galv_pipe_3_del', 'galv_pipe_4_del', 'galv_pipe_5_del', 'galv_pipe_6_del', 'galv_pipe_7_del', 'galv_pipe_8_del', 'galv_pipe_10_del', 'galv_pipe_12_del', 'galv_pipe_13_del', 'galv_pipe_14_del', 'galv_pipe_16_del', 'r8_flex_dct_del', 'r8_flex_dct_4_del', 'r8_flex_dct_5_del', 'r8_flex_dct_6_del', 'r8_flex_dct_7_del', 'r8_flex_dct_8_del', 'r8_flex_dct_9_del', 'r8_flex_dct_12_del', 'r8_flex_dct_14_del', 'r8_flex_dct_16_del', 'elbows_del', 'elbows_3_del', 'elbows_4_del', 'elbows_5_del', 'elbows_6_del', 'elbows_7_del', 'elbows_8_del', 'elbows_9_del', 'elbows_10_del', 'elbows_12_del', 'elbows_13_del', 'elbows_14_del', 'elbows_16_del', 'fresh_air_del', '6_damper_del', '7_damper_del', '8_damper_del', 'timer_del', '5_saddle_del', '6_saddle_del', '7_saddle_del', '8_saddle_del', '6_oval_bt_90_del', '6_oval_bt_torp_del', '6_oval_bt_45_6_del', '6_oval_bt_st_del', '6_oval_cap_del', '6_oval_pipe_del', '7_oval_bt_90_del', '7_oval_bt_torp_del', '7_oval_bt_45_7_del', '7_oval_bt_st_del', '7_oval_cap_del', '7_oval_pipe_del', '8_oval_bt_90_del', '8_oval_bt_torp_del', '8_oval_bt_45_8_del', '8_oval_bt_st_del', '8_oval_cap_del', '8_oval_pipe_del', 'r4_flex_dct_del', 'r4_flex_dct_4_del', 'r4_flex_dct_5_del', 'r4_flex_dct_6_del', 'r4_flex_dct_7_del', 'r4_flex_dct_8_del', 'r4_flex_dct_9_del', 'r4_flex_dct_12_del', 'r4_flex_dct_14_del', 'r4_flex_dct_16_del', 'flr_regs_del', 'flr_regs_2_1_4_10_del', 'flr_regs_2_1_4_12_del', 'flr_regs_4_12_del', 'flr_regs_4_14_del', 'flr_regs_6_10_del', 'flr_regs_10_4_del', 'flr_regs_12_4_del', 'flr_regs_14_4_del', 'ra_grille_8_30_del', 'ra_grille_10_30_del', 'ra_grille_17_30_del', 'ra_grille_10_14_del', 'ra_grille_14_14_del', 'ra_grille_20_14_del', 'ra_grille_20_20_del', 'ra_grille_20_24_del', 'ra_grille_24_30_del', 'wa_boots_del', '4_10_st_del', '4_10_st_torp_del', '4_10_st_90_del', '2_10_st_del', '2_10_st_torp_del', '2_10_st_90_del', '4_12_7_st_del', '4_12_7_st_torp_del', '4_12_7_st_90_del', 'hwt_20_20_del', 'hwt_25_25_del', 'base_14_28_18_del', 'base_14_28_24_del', 'base_17_28_18_del', 'plen_13_22_35_del', 'plen_16_22_35_del', 'd_cleat_light_del', 'd_cleat_24g_del', 's_cleat_light_del', 's_cleat_24g_del', 'stat_wire_18_5_del', 'stat_wire_18_8_del', 'literature_rack_del', 'tape_etc_del', '2_333_tape_del', '2_foil_tape_del', '3_ductWrap_del', '3_fabric_tape_del', 'caddy_strap_del', 'duro_strap_del', 'pandt_strap_del', 'caulk_silicone_del', 'toe_kicks_del', 'galv_pipe_usd', 'galv_pipe_3_usd', 'galv_pipe_4_usd', 'galv_pipe_5_usd', 'galv_pipe_6_usd', 'galv_pipe_7_usd', 'galv_pipe_8_usd', 'galv_pipe_10_usd', 'galv_pipe_12_usd', 'galv_pipe_13_usd', 'galv_pipe_14_usd', 'galv_pipe_16_usd', 'r8_flex_dct_usd', 'r8_flex_dct_4_usd', 'r8_flex_dct_5_usd', 'r8_flex_dct_6_usd', 'r8_flex_dct_7_usd', 'r8_flex_dct_8_usd', 'r8_flex_dct_9_usd', 'r8_flex_dct_12_usd', 'r8_flex_dct_14_usd', 'r8_flex_dct_16_usd', 'elbows_usd', 'elbows_3_usd', 'elbows_4_usd', 'elbows_5_usd', 'elbows_6_usd', 'elbows_7_usd', 'elbows_8_usd', 'elbows_9_usd', 'elbows_10_usd', 'elbows_12_usd', 'elbows_13_usd', 'elbows_14_usd', 'elbows_16_usd', 'fresh_air_usd', '6_damper_usd', '7_damper_usd', '8_damper_usd', 'timer_usd', '5_saddle_usd', '6_saddle_usd', '7_saddle_usd', '8_saddle_usd', '6_oval_bt_90_usd', '6_oval_bt_torp_usd', '6_oval_bt_45_6_usd', '6_oval_bt_st_usd', '6_oval_cap_usd', '6_oval_pipe_usd', '7_oval_bt_90_usd', '7_oval_bt_torp_usd', '7_oval_bt_45_7_usd', '7_oval_bt_st_usd', '7_oval_cap_usd', '7_oval_pipe_usd', '8_oval_bt_90_usd', '8_oval_bt_torp_usd', '8_oval_bt_45_8_usd', '8_oval_bt_st_usd', '8_oval_cap_usd', '8_oval_pipe_usd', 'r4_flex_dct_usd', 'r4_flex_dct_4_usd', 'r4_flex_dct_5_usd', 'r4_flex_dct_6_usd', 'r4_flex_dct_7_usd', 'r4_flex_dct_8_usd', 'r4_flex_dct_9_usd', 'r4_flex_dct_12_usd', 'r4_flex_dct_14_usd', 'r4_flex_dct_16_usd', 'flr_regs_usd', 'flr_regs_2_1_4_10_usd', 'flr_regs_2_1_4_12_usd', 'flr_regs_4_12_usd', 'flr_regs_4_14_usd', 'flr_regs_6_10_usd', 'flr_regs_10_4_usd', 'flr_regs_12_4_usd', 'flr_regs_14_4_usd', 'ra_grille_8_30_usd', 'ra_grille_10_30_usd', 'ra_grille_17_30_usd', 'ra_grille_10_14_usd', 'ra_grille_14_14_usd', 'ra_grille_20_14_usd', 'ra_grille_20_20_usd', 'ra_grille_20_24_usd', 'ra_grille_24_30_usd', 'wa_boots_usd', '4_10_st_usd', '4_10_st_torp_usd', '4_10_st_90_usd', '2_10_st_usd', '2_10_st_torp_usd', '2_10_st_90_usd', '4_12_7_st_usd', '4_12_7_st_torp_usd', '4_12_7_st_90_usd', 'hwt_20_20_usd', 'hwt_25_25_usd', 'base_14_28_18_usd', 'base_14_28_24_usd', 'base_17_28_18_usd', 'plen_13_22_35_usd', 'plen_16_22_35_usd', 'd_cleat_light_usd', 'd_cleat_24g_usd', 's_cleat_light_usd', 's_cleat_24g_usd', 'stat_wire_18_5_usd', 'stat_wire_18_8_usd', 'literature_rack_usd', 'tape_etc_usd', '2_333_tape_usd', '2_foil_tape_usd', '3_ductWrap_usd', '3_fabric_tape_usd', 'caddy_strap_usd', 'duro_strap_usd', 'pandt_strap_usd', 'caulk_silicone_usd', 'toe_kicks_usd','status','flr_regs_4_10_del','flr_regs_4_10_usd','ra_grille_usd','ra_grille_del'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
