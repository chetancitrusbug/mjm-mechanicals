<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\EmailController;
use App\Job;
use App\User;
use App\Jobassignemployee;
use App\Jobtask;
use App\Folder;
use App\Foldervideo;
use App\Folderdocument;
use App\Video;
use App\Document;
use App\Jobcarddetail;
use App\Jobcard;
use App\Foldernotes;
use App\Taskfolder;
use App\JobGeothermalForm;
use App\JobFurnaceForm;
use App\JobInspectionForm;
use App\JobChecklistForm;
use App\JobMaintenanceForm;
use App\JobSystemRepairForm;
use App\JobLogs;
use App\JobPlumbingForm;
use App\JobInvoice;
use App\JobInvoiceOtherField;
use App\JobWeeklyVehicleSafetyChecklistForm;
use App\JobProjrctWorksheet1;
use App\JobProjrctWorksheet2;
use App\JobProjrctWorksheet3;
use App\JobProjrctWorksheet4;
use App\JobImage;
use App\JobImageFolder;
use App\Jobcode;
use App\Forms;
use App\JobForms;
use DB;
use Mail;

class JobsController extends Controller
{
    public function __construct()
    {
        $this->mail_function = new EmailController();
    }

    public function jobCreate(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Job Added Successfully';
        $status = 'true';
        $error = '';
        $rules = array(
            'job_number' => 'required',
            'name' => 'sometimes',
            'description' => 'sometimes',
            'client_id' => 'sometimes',
            'duedate' => 'required|date|date_format:Y-m-d',
            'user_id' => 'required',
            'notes' => 'sometimes',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 200;
            $msgArr =  $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
			return response()->json(['data' => (object)$data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        } else {

           $jobcode = Jobcode::where('id', $request->job_number)->where('status', 'active')->first();


            if(!$jobcode){
                $status = false;
                $messages = 'Job number not Found';
                $code = 400;
            }
            if ($status != false) {

                $jobdata = Job::where('job_number', $request->job_number)->first();
                if ($jobdata == null) {

                    $data = $request->all();
                    $data['job_number'] = (($data['job_number'] != null) ? $data['job_number'] : '');
                    $data['title'] = (($data['name'] != null) ? $data['name'] : '');
                    $data['description'] = (($data['description'] != null) ? $data['description'] : '');
                    $data['client_id'] = (($data['client_id'] != null) ? $data['client_id'] : '');
                    $data['duedate'] = (($data['duedate'] != null) ? $data['duedate'] : '');
                    $data['notes'] = (($data['notes'] != null) ? $data['notes'] : '');
                    $data['status'] = 'active';
                    $data['job_status'] = 'pending';
                    $job = Job::create($data);
                    $job->created_by=$job->id;
                    $job->save();

                    if ($job) {
                        if(!empty($request->input('user_id'))){
                            $jobassignemp = new Jobassignemployee();
                            $jobassignemp->job_id = $job->id;
                            $jobassignemp->job_employee_id = $data['user_id'];
                            $jobassignemp->save();
                        }
                        $data = Job::find($job->id);
                        $messages = 'Job Added Successfully';
                        $status = true;
                        $code = 200;
                    } else {
                        $status = false;
                        $code = \Config::get('constants.responce_code.bad_request');
                        $messages = 'Something went wrong ! Please try again later';
                    }

                } else {
                    $status = false;
                    $messages = 'Job already added with this job number Please enter another job number';
                    $code = 200;
                }
            }
        }
        return response()->json(['data' => (object)$data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function checkInOutJob(Request $request){
        $data = [];
        $message = "Job logs Saved";
        $status = true;
        $code = 200;

        $rules = array(
            'user_id'=>'required|integer',
            'job_id'=>'required|integer',
            'in_out_flag'=>'required|integer',
            'date_time'=> 'required|date|date_format:Y-m-d H:i:s'
        );


        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $status = false;
            $code = 400;
            $flag = 0;
            $msgArr = $validator->messages()->toArray();
            $message = reset($msgArr)[0];
        } else {

            $user_id = $request->user_id;
            $job_id = $request->job_id;
            $date_time = rawurldecode($request->date_time);
            $in_out_flag = $request->in_out_flag;
            //dd($date_time);
            $users= User::where('users.id',$user_id)->first();
            $job= Job::where('job.id',$job_id)->first();


            if(!$users){
                $status = false;
                $message = 'No Users Found';
                $code = 400;
            }

            if(!$job){
                $status = false;
                $message = 'No Job Found';
                $code = 400;
            }

            if($status){
                if ($request->in_out_flag == 0) {
                    $jobLogs = JobLogs::where('job_id', $job_id)->where('user_id', $user_id)->where('end_datetime','=', null)->first();
                    if ($jobLogs != null) {
                        $status = false;
                        $message = 'Already Check In';
                        $code = 400;
                    }else {
                        $jobLogs = new JobLogs;
                        $jobLogs->job_id = $job_id;
                        $jobLogs->user_id = $user_id;
                        $jobLogs->start_datetime = $date_time;
                        $jobLogs->save();
                        $data = $jobLogs;
                    }
                }
                else {
                    $jobLogs = JobLogs::where('job_id', $job_id)->where('user_id', $user_id)->first();
                    if($jobLogs != null)
                    {
                        $jobLogs = JobLogs::where('job_id', $job_id)->where('user_id', $user_id)->where('end_datetime', '=', null)->first();
                        if ($jobLogs != null) {
                            $jobLogs->end_datetime = $date_time;
                            $jobLogs->save();
                            $data = $jobLogs;
                        } else {
                            $status = false;
                            $message = 'Already Check Out';
                            $code = 400;
                        }
                    }
                    else {
                        $status = false;
                        $message = 'No Job Log Found';
                        $code = 400;
                    }
                }
            }
        }
        return response()->json(['data'=>(object)$data, 'status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function joblist(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $page = $request->page;
        $users= User::where('users.id',$id)->first();
        if($users){
            $joblistdata = Job::join('jobassignemployee','job.id','=','jobassignemployee.job_id')
            ->leftJoin('clients','job.client_id','=','clients.id')
            ->where('jobassignemployee.job_employee_id',$id)
            ->where('job.job_status','!=','markasdone')
            ->select('job.id as relation_id','job.title as title','job.description as description','job.duedate as duedate','job.notes as notes','job.status as status','job.deleted_at as deleted_at','job.created_by as created_by','job.created_at as created_at','job.updated_at as updated_at','job.client_id as client_id','job.job_status as job_status','job.job_number as job_number','job.start_time as start_time','job.end_time as end_time','jobassignemployee.job_id as job_id','jobassignemployee.job_employee_id as job_employee_id','clients.name as client_name');

            if ($request->search != ''){
                $search = $request->search;
                $where_filter = "(job.title LIKE '%$search%' OR job.job_number LIKE  '%$search%' OR job.duedate LIKE  '%$search%' OR clients.name LIKE  '%$search%')";
                $joblistdata = $joblistdata->whereRaw($where_filter);
            }
            $joblistdata  = $joblistdata->paginate(10);

            if(count($joblistdata)>0){
                $data=$joblistdata;
            }
            else{
                $status = false;
                $message = 'No Jobs Found';
                $code = 200;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 200;
        }

        return response()->json(['status'=>$status, 'data'=>(object)$data,'message'=>$message,'code'=> $code]);
    }

    public function jobdetail(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $users= User::where('users.id',$id)->first();
        if($users){
             $jobdata = Job::with('clientName')
             ->where('id',$job_id)
             ->select('job.id as job_id','job.title as title','job.description as description','job.duedate as duedate','job.notes as notes','job.status as status','job.deleted_at as deleted_at','job.created_by as created_by','job.created_at as created_at','job.updated_at as updated_at','job.client_id as client_id','job.job_status as job_status','job.job_number as job_number','job.start_time as start_time','job.end_time as end_time')
            ->get();
            if(count($jobdata)>0){
                $jobcode = Jobcode::where('id', $jobdata[0]->job_number)->where('status', 'active')->first();
                if (isset($jobcode->job_code) && $jobcode->job_code != null) {
                    $jobdata[0]->job_number = $jobcode->job_code;
                }
                else {
                    $jobdata[0]->job_number = '';
                }
                $jobLogs = JobLogs::where('job_id', $job_id)->where('user_id', $id)->where('start_datetime','!=', null)->where('end_datetime','=', null)->first();
                    if ($jobLogs != null) {
                        $jobdata[0]->jonInOutStatus = 1;
                    }
                    else {
                        $jobdata[0]->jonInOutStatus = 0;
                    }
                 $data=$jobdata;
                 $message = 'Success';
            }
            else{
                $status = false;
                $message = 'No Jobs Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }

     public function jobtasklist(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
		$folder_id = $request->folder_id;
        $users= User::where('users.id',$id)->first();
        if($users){

			$taskfolder = Taskfolder::find($folder_id);
			$taskfolder['folder_id']=$taskfolder->id;
            $tasklistdata = Jobassignemployee::
			 where('jobassignemployee.job_employee_id',$id)
            ->where('jobassignemployee.job_id',$job_id)
			//->with('JobtaskList')
			->with(['JobtaskList' => function ($qs) use ($folder_id){
                     $qs

					   ->where('jobtask.folder_id',$folder_id);
                   }])
			->with(['checkemployeeJob' => function ($q) use ($id){
                     $q
                       ->where('employeejob.employee_id', $id);
                   }])
            ->select('jobassignemployee.id as relation_id','jobassignemployee.job_id as job_id','jobassignemployee.job_employee_id as job_employee_id','jobassignemployee.status as status','jobassignemployee.created_at as created_at','jobassignemployee.updated_at as updated_at')
            ->get();

			 $finalArray = array();
            if(count($tasklistdata)>0){
				// $data=$tasklistdata;
                // $message = 'Success';
				foreach($tasklistdata as $p_key => $p_val){
                    $finalArray['JobtaskList'][$p_key] = $p_val;
					$finalArray['folder'] = $taskfolder;
                    $message = 'Success';
                }
            }
            else{
                $status = false;
                $message = 'No Job Task Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$finalArray,'message'=>$message,'code'=> $code]);
    }

    public function jobfolderlist(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $users= User::where('users.id',$id)->first();
        if($users){

        $folderlistdata = Jobassignemployee::with('JobfolderList')
            ->where('jobassignemployee.job_employee_id',$id)
            ->where('jobassignemployee.job_id',$job_id)
            ->select('jobassignemployee.id as relation_id','jobassignemployee.job_id as job_id','jobassignemployee.job_employee_id as job_employee_id','jobassignemployee.status as status','jobassignemployee.created_at as created_at','jobassignemployee.updated_at as updated_at')
            ->get();
            if(count($folderlistdata)>0){
                 $data=$folderlistdata;
                 $message = 'Success';

            }
            else{
                $status = false;
                $message = 'Job is not Assign';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }

	public function jobtaskfolderlist(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $users= User::where('users.id',$id)->first();
        if($users){

        $folderlistdata = Jobassignemployee::with('JobtaskfolderList')
            ->where('jobassignemployee.job_employee_id',$id)
            ->where('jobassignemployee.job_id',$job_id)
            ->select('jobassignemployee.id as relation_id','jobassignemployee.job_id as job_id','jobassignemployee.job_employee_id as job_employee_id','jobassignemployee.status as status','jobassignemployee.created_at as created_at','jobassignemployee.updated_at as updated_at')
            ->get();
            if(count($folderlistdata)>0){
                 $data=$folderlistdata;
                 $message = 'Success';

            }
            else{
                $status = false;
                $message = 'Job is not Assign';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }

    public function foldervideodetail(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $folder_id = $request->folder_id;
        $users= User::where('users.id',$id)->first();
        if($users){
          $folder = Folder::find($folder_id);
           $folder['folder_id']= $folder->id;
          $foldervideolistdata = Foldervideo::join('videos','videos.id','=','foldervideo.video_id')
          ->where('videos.status','=','active')
          ->where('foldervideo.folder_id',$folder_id)
          ->where('foldervideo.job_id',$job_id)
          ->select('foldervideo.id as relation_id','foldervideo.folder_id as folder_id','foldervideo.job_id as job_id','foldervideo.video_id as video_id','foldervideo.status as status','foldervideo.user_type as user_type','foldervideo.employee_id as employee_id','videos.title as title','videos.description as description','videos.video_url as video_url','videos.created_by as created_by')
          ->get();
          $finalArray = array();
            if(count($foldervideolistdata)>0){
                foreach($foldervideolistdata as $p_key => $p_val){
                    $finalArray['video_detail'][$p_key] = $p_val;
                    $finalArray['video_detail'][$p_key]['video_url']=url('/Video'.'/'.$p_val['video_url']);
					//$finalArray['folder'] = $folder;
                    $message = 'Success';
                }
            }
            else{
                $status = false;
                $message = 'No Folders Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$finalArray,'message'=>$message,'code'=> $code]);
    }

    public function folderdocumentdetail(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $folder_id = $request->folder_id;
        $users= User::where('users.id',$id)->first();
        if($users){
          $folder = Folder::find($folder_id);
          $folder['folder_id']=$folder->id;
          $folderdocumentlistdata = Folderdocument::join('documents','documents.id','=','folderdocument.document_id')
          ->where('documents.status','=','active')
          ->where('folderdocument.folder_id',$folder_id)
          ->where('folderdocument.job_id',$job_id)
           ->select('folderdocument.id as relation_id','folderdocument.folder_id as folder_id','folderdocument.job_id as job_id','folderdocument.document_id as document_id','folderdocument.status as status','folderdocument.user_type as user_type','folderdocument.employee_id as employee_id','documents.title as title','documents.description as description','documents.document_url as document_url','documents.created_by as created_by')
          ->get();
          $finalArray = array();
            if(count($folderdocumentlistdata)>0){
                foreach($folderdocumentlistdata as $p_key => $p_val){
                    $finalArray['document_detail'][$p_key] = $p_val;
                    $finalArray['document_detail'][$p_key]['document_url']=url('/Document'.'/'.$p_val['document_url']);
					//$finalArray['folder'] = $folder;
                    $message = 'Success';
                }
            }
            else{
                $status = false;
                $message = 'No Folders Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$finalArray,'message'=>$message,'code'=> $code]);
    }

    public function videodetail(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $folder_id = $request->folder_id;
        $video_id = $request->video_id;
        $users= User::where('users.id',$id)->first();
        if($users){
          $folder = Folder::find($folder_id);
          // $folder['folder_id']= $folder->id;
          $foldervideolistdata = Video::where('id',$video_id)
          ->select('videos.id as relation_id','videos.title as title','videos.description as description','videos.video_url as video_url','videos.status as status','videos.deleted_at as deleted_at','videos.user_type as user_type')
          ->get();

          $finalArray = array();
            if((count($foldervideolistdata) > 0)){
                foreach($foldervideolistdata as $p_key => $p_val){
                    $finalArray['video_detail'][$p_key] = $p_val;
                    $finalArray['video_detail'][$p_key]['video_url'] = url('/Video'.'/'.$p_val['video_url']);
                    //$finalArray['folder'] = $folder;
                    $message = 'Success';
                }

            }
            else{
                $status = false;
                $message = 'No Videos Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$finalArray,'message'=>$message,'code'=> $code]);
    }

     public function documentdetail(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $folder_id = $request->folder_id;
        $document_id = $request->document_id;
        $users= User::where('users.id',$id)->first();
        if($users){
          $folder = Folder::find($folder_id);
          $folder['folder_id']=$folder->id;
          $documentdetaildata = Document::where('id',$document_id)
          ->select('documents.id as relation_id','documents.title as title','documents.description as description','documents.document_url as document_url','documents.status as status','documents.deleted_at as deleted_at','documents.user_type as user_type')
          ->get();
          $finalArray = array();
            if(count($documentdetaildata)>0){
                foreach($documentdetaildata as $p_key => $p_val){
                    $finalArray['document_detail'][$p_key] = $p_val;
                    $finalArray['document_detail'][$p_key]['document_url']=url('/Document'.'/'.$p_val['document_url']);
                    //$finalArray['folder'] = $folder;
                    $message = 'Success';
                }

            }
            else{
                $status = false;
                $message = 'No Document Found';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$finalArray,'message'=>$message,'code'=> $code]);
    }
    public function createjobcard(Request $request){
        $data = [];
        $message = "Job card Saved";
        $status = true;
        $code = 200;

        $user_id = $request->user_id;
        $job_id = $request->job_id;
        $job_title = $request->job_title;
        $data = $request->data;
        $signature_img=$request->image;
        $second_client_name=$request->second_client_name;
        $users= User::where('users.id',$user_id)->first();
        $job= Job::where('job.id',$job_id)->first();


        if(!$users){
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }else if($user_id==''){
            $status = false;
            $message = 'Enter User Id';
            $code = 400;
        }
        if(!$job){
            $status = false;
            $message = 'No Job Found';
            $code = 400;
        }else if($job_id==''){
            $status = false;
            $message = 'Enter Job Id';
            $code = 400;
        }
        if($signature_img==''){
            $status = false;
            $message = 'Select Image';
            $code = 400;
        }
        if($data==''){
            $status = false;
            $message = 'Enter Data';
            $code = 400;
        }
        if($job_title==''){
            $status = false;
            $message = 'Enter Job Title';
            $code = 400;
        }
        $jobcarddata=json_decode($data);

        if($status){
            $total_amount=0;
            $jobcard = new Jobcard;
            $jobcard->job_id=$job_id;
            $jobcard->job_title=$job_title;
            $jobcard->user_id=$user_id;
            $jobcard->second_client_name=$second_client_name;
            if ($request->image) {
                $job_card_signature_image = $request->image;
                $filename = uniqid(time()) . '.' . $job_card_signature_image->getClientOriginalExtension();
                $job_card_signature_image->move(public_path('CardsSignature'), $filename);
                $jobcard['job_card_signature_image'] = $filename;

            }

            $jobcard->save();


            foreach($jobcarddata->data as $val){
                if($val->name!='' && $val->qty!='' && $val->price!=''){
                    $data = new Jobcarddetail;

                    $data['job_card_id'] = $jobcard->id;
                    $data['job_card_name'] = $val->name;
                    $data['job_card_qty'] = $val->qty;
                    $data['job_card_price'] = $val->price;
                    $data['job_card_status'] = 'active';
                    $data['total']=($val->qty)*( $val->price);
                    $total_amount=$total_amount+$val->qty*$val->price;
                    $data->save();
                }
            }

            $jobcard->total_amount=$total_amount;
            $jobcard->save();
            $jobcarddetail = Jobcarddetail::where('job_card_id',$jobcard->id)->get();
            $emails = ['mjmmechanical@frontier.com'];
            Mail::send('admin.emails.changeordermail', compact('jobcard', 'job', 'jobcarddetail'), function ($message) use ($job,$emails) {
                $message->to($emails)->subject('MJM APP CHANGE ORDER (#'.$job->job_number.')');
            });
        }
        return response()->json(['status'=>$status,'message'=>$message,'code'=> $code]);
    }
    public function jobcardlist(Request $request){
        $data = [];
        $message = "Success";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $users= User::where('users.id',$id)->first();
        $job= Job::where('job.id',$job_id)->first();
		$jobcardexist= Jobcard::where('jobcard.job_id',$job_id)->first();

        if($users){
            if($job){
				if($jobcardexist){

					$jobcard = Jobcard::with('jobdetailList')->where('job_id',$job_id)->where('user_id',$id)->select('jobcard.id as job_card_id','jobcard.job_id as job_id','jobcard.job_title as job_title','jobcard.user_id as user_id','jobcard.total_amount as total_amount','jobcard.job_card_signature_image as job_card_signature_image','jobcard.status as status','jobcard.second_client_name as second_client_name','jobcard.id as id')->get();
					$finalArray = array();
					$data['job_card']=$jobcard;
				}
				else{
					$status = false;
					$message = 'No Job Card Found';
					$code = 400;
				}
			}
            else{
            $status = false;
            $message = 'No Job Found';
            $code = 400;
        }
        }else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }

     public function jobcarddetail(Request $request){
        $data = [];
        $message = "Success";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $job_card_id = $request->job_card_id;
        $users= User::where('users.id',$id)->first();
        $job= Job::where('job.id',$job_id)->first();

        if($users){
            if($job){
                $jobcard = Jobcard::with('jobdetailList')->where('id',$job_card_id)->where('user_id',$id)->select('jobcard.id as job_card_id','jobcard.job_id as job_id','jobcard.job_title as job_title','jobcard.user_id as user_id','jobcard.total_amount as total_amount','jobcard.job_card_signature_image as job_card_signature_image','jobcard.status as status','jobcard.second_client_name as second_client_name','jobcard.id as id')->get();
				$data['job_card']=$jobcard;
            }
            else{
            $status = false;
            $message = 'No Job Found';
            $code = 400;
        }
        }else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }
    public function addimage(Request $request){
        $data = [];
        $message = "Folder Created";
        $status = true;
        $code = 200;

        $user_id = $request->user_id;
        $job_id = $request->job_id;
        $folder_name = $request->folder_name;
        $folder_img=$request->image;
        $users= User::where('users.id',$user_id)->first();
        $job= Job::where('job.id',$job_id)->first();


        if(!$users){
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }else if($user_id==''){
            $status = false;
            $message = 'Enter User Id';
            $code = 400;
        }
        if(!$job){
            $status = false;
            $message = 'No Job Found';
            $code = 400;
        }else if($job_id==''){
            $status = false;
            $message = 'Enter Job Id';
            $code = 400;
        }
        if($folder_name==''){
            $status = false;
            $message = 'Enter Folder Name';
            $code = 400;
        }
        if($folder_img==''){
            $status = false;
            $message = 'Upload Folder Image';
            $code = 400;
        }

        if($status){
            $folder = new Folder;
            $folder->job_id=$job_id;
            $folder->folder_name=$folder_name;
            $folder->user_type='employee';
            $folder->employee_id=$user_id;
            if ($request->image) {
                $folder_image = $request->image;
                $filename = uniqid(time()) . '.' . $folder_image->getClientOriginalExtension();
                $folder_image->move(public_path('FolderImage'), $filename);
                $folder['folder_img'] = url('FolderImage').'/'.$filename;
            }

            $folder->save();

            $folder->created_by=$folder->id;
            $folder->save();
        }
        return response()->json(['status'=>$status,'message'=>$message,'code'=> $code]);
    }


     public function adddocument(Request $request){
        $data = [];
        $message = "Document Uploaded";
        $status = true;
        $code = 200;

        $user_id = $request->user_id;
        $job_id = $request->job_id;
        $folder_id = $request->folder_id;
        $document_title = $request->document_title;
        $document_description = $request->document_description;
        $document_img=$request->image;
        $users= User::where('users.id',$user_id)->first();
        $job= Job::where('job.id',$job_id)->first();
        $folder= Folder::where('folder.id',$folder_id)->first();


        if(!$users){
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }else if($user_id==''){
            $status = false;
            $message = 'Enter User Id';
            $code = 400;
        }
        if(!$job){
            $status = false;
            $message = 'No Job Found';
            $code = 400;
        }else if($job_id==''){
            $status = false;
            $message = 'Enter Job Id';
            $code = 400;
        }
        if(!$folder){
            $status = false;
            $message = 'No Folder Found';
            $code = 400;
        }else if($folder==''){
            $status = false;
            $message = 'Enter Folder Id';
            $code = 400;
        }
        if($document_title==''){
            $status = false;
            $message = 'Enter Document title';
            $code = 400;
        }
        if($document_img==''){
            $status = false;
            $message = 'Upload Document';
            $code = 400;
        }

        if($status){
            $document = new Document;
            $document->title=$document_title;
            $document->description=$document_description;
            $document->user_type='employee';
            if ($request->image) {
                $document_image = $request->image;
                $filename = uniqid(time()) . '.' . $document_image->getClientOriginalExtension();
                $document_image->move(public_path('Document'), $filename);
                $document['document_url'] = $filename;
            }
            $document->save();

            $folderdocument = new Folderdocument;
            $folderdocument->folder_id=$folder_id;
            $folderdocument->job_id=$job_id;
            $folderdocument->document_id=$document->id;
            $folderdocument->user_type='employee';
            $folderdocument->employee_id=$user_id;
            $folderdocument->save();

        }
        return response()->json(['status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function addvideo(Request $request){
        $data = [];
        $message = "Video Uploaded";
        $status = true;
        $code = 200;

        $user_id = $request->user_id;
        $job_id = $request->job_id;
        $folder_id = $request->folder_id;
        $video_title = $request->video_title;
        $video_description = $request->video_description;
        $video_img=$request->video;
        $users= User::where('users.id',$user_id)->first();
        $job= Job::where('job.id',$job_id)->first();
        $folder= Folder::where('folder.id',$folder_id)->first();


        if(!$users){
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }else if($user_id==''){
            $status = false;
            $message = 'Enter User Id';
            $code = 400;
        }
        if(!$job){
            $status = false;
            $message = 'No Job Found';
            $code = 400;
        }else if($job_id==''){
            $status = false;
            $message = 'Enter Job Id';
            $code = 400;
        }
        if(!$folder){
            $status = false;
            $message = 'No Folder Found';
            $code = 400;
        }else if($folder==''){
            $status = false;
            $message = 'Enter Folder Id';
            $code = 400;
        }
        if($video_title==''){
            $status = false;
            $message = 'Enter Video title';
            $code = 400;
        }
        if($video_img==''){
            $status = false;
            $message = 'Upload Video';
            $code = 400;
        }

        if($status){
            $video = new Video;
            $video->title=$video_title;
            $video->description=$video_description;
            $video->user_type='employee';
            if ($request->video) {
                $video_image = $request->video;
                $filename = uniqid(time()) . '.' . $video_image->getClientOriginalExtension();
                $video_image->move(public_path('Video'), $filename);
                $video['video_url'] = $filename;
            }
            $video->save();

            $foldervideo = new Foldervideo;
            $foldervideo->folder_id=$folder_id;
            $foldervideo->job_id=$job_id;
            $foldervideo->video_id=$video->id;
            $foldervideo->user_type='employee';
            $foldervideo->employee_id=$user_id;
            $foldervideo->save();

        }
        return response()->json(['status'=>$status,'message'=>$message,'code'=> $code]);
    }
    public function updatejobtask(Request $request){
        $data = [];
        $message = "Job Task Updated";
        $status = true;
        $code = 200;

        $user_id = $request->user_id;
        $jobtask_id = $request->jobtask_id;
        $jobtask_status = $request->jobtask_status;
        $users= User::where('users.id',$user_id)->first();

        if(!$users){
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }else if($user_id==''){
            $status = false;
            $message = 'Enter User Id';
            $code = 400;
        }
        if($jobtask_id==''){
            $status = false;
            $message = 'Enter Job Task Id';
            $code = 400;
        }
        if($jobtask_status==''){
            $status = false;
            $message = 'Enter Job Task Status';
            $code = 400;
        }
       $jobtask = Jobtask::find($jobtask_id);
        if($status){
            if($users){
                if($jobtask){
                    $jobtask->jobtask_status=$jobtask_status;
                    $jobtask->save();
                }else{
                    $status = false;
                    $message = 'No Job Task Found';
                    $code = 400;
                }
            }
            else{
                    $status = false;
                    $message = 'No User Found';
                    $code = 400;
                }
        }
        return response()->json(['status'=>$status,'message'=>$message,'code'=> $code]);
    }
     public function markasdone(Request $request){
        $data = [];
        $message = "Job Status Updated";
        $status = true;
        $code = 200;
        $subject="Job Status";
        $user_id = $request->user_id;
        $job_id = $request->job_id;
        $users= User::where('users.id',$user_id)->first();

        if(!$users){
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }else if($user_id==''){
            $status = false;
            $message = 'Enter User Id';
            $code = 400;
        }
        if($job_id==''){
            $status = false;
            $message = 'Enter Job Id';
            $code = 400;
        }

       $job = Job::find($job_id);
        if($status){
            if($users){
                if($job){
                    $job->job_status='markasdone';
                    $job->save();
					$this->mail_function->sendMailJobAction($job_id);
                }else{
                    $status = false;
                    $message = 'No Job Found';
                    $code = 400;
                }
            }
            else{
                    $status = false;
                    $message = 'No User Found';
                    $code = 400;
                }
        }


        return response()->json(['status'=>$status,'message'=>$message,'code'=> $code]);
    }
    public function addnotes(Request $request){
        $data = [];
        $message = "Added Successfully";
        $status = true;
        $code = 200;

        $user_id = $request->user_id;
        $job_id = $request->job_id;
        $folder_id = $request->folder_id;
        $notes = $request->notes;
        $users= User::where('users.id',$user_id)->first();
        $job= Job::where('job.id',$job_id)->first();


        if(!$users){
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }else if($user_id==''){
            $status = false;
            $message = 'Enter User Id';
            $code = 400;
        }
        if(!$job){
            $status = false;
            $message = 'No Job Found';
            $code = 400;
        }else if($job_id==''){
            $status = false;
            $message = 'Enter Job Id';
            $code = 400;
        }
        if($notes==''){
            $status = false;
            $message = 'Enter Notes';
            $code = 400;
        }

        if($status){
            $foldernotes = new Foldernotes;
            $foldernotes->job_id=$job_id;
            $foldernotes->user_type='employee';
            $foldernotes->employee_id=$user_id;
            $foldernotes->notes=$notes;
            $foldernotes->save();
        }
        return response()->json(['status'=>$status,'message'=>$message,'code'=> $code]);
    }

     public function notesfolderlist(Request $request){
        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $job_id = $request->job_id;
        $users= User::where('users.id',$id)->first();
        if($users){

        $foldernoteslistdata = Foldernotes::where('status','active')->where('job_id',$job_id)->select('id as notes_id','job_id as job_id','notes as notes','user_type as user_type','employee_id as employee_id','status as status','deleted_at as deleted_at')->get();
            if(count($foldernoteslistdata)>0){
                 $data=$foldernoteslistdata;
                 $message = 'Success';

            }
            else{
                $status = false;
                $message = 'Job is not Assign';
                $code = 400;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }

        return response()->json(['status'=>$status, 'data'=>$data,'message'=>$message,'code'=> $code]);
    }

	public function editnotes(Request $request){
        $data = [];
        $message = "Updated Successfully";
        $status = true;
        $code = 200;

        $user_id = $request->user_id;
        $job_id = $request->job_id;
        $note_id=$request->note_id;
        $notes = $request->notes;
        $users= User::where('users.id',$user_id)->first();
        $job= Job::where('job.id',$job_id)->first();
        $foldernotes= Foldernotes::where('foldernotes.id',$note_id)->first();


        if(!$users){
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }else if($user_id==''){
            $status = false;
            $message = 'Enter User Id';
            $code = 400;
        }
        if(!$job){
            $status = false;
            $message = 'No Job Found';
            $code = 400;
        }else if($job_id==''){
            $status = false;
            $message = 'Enter Job Id';
            $code = 400;
        }
        if(!$foldernotes){
            $status = false;
            $message = 'No Notes Found';
            $code = 400;
        }else if($foldernotes==''){
            $status = false;
            $message = 'Enter Notes Id';
            $code = 400;
        }
        if($notes==''){
            $status = false;
            $message = 'Enter Notes';
            $code = 400;
        }

        if($status){
            //$foldernotes = new Foldernotes;
            $foldernotes->job_id=$job_id;
            $foldernotes->user_type='employee';
            $foldernotes->employee_id=$user_id;
            $foldernotes->notes=$notes;
            $foldernotes->save();
        }
        return response()->json(['status'=>$status,'message'=>$message,'code'=> $code]);
    }

    public function deletenotes(Request $request){
        $data = [];
        $message = "Deleted Successfully";
        $status = true;
        $code = 200;

        $user_id = $request->user_id;
        $note_id=$request->note_id;
        $users= User::where('users.id',$user_id)->first();
        $foldernotes= Foldernotes::where('foldernotes.id',$note_id)->first();


        if(!$users){
            $status = false;
            $message = 'No Users Found';
            $code = 400;
        }else if($user_id==''){
            $status = false;
            $message = 'Enter User Id';
            $code = 400;
        }
        if(!$foldernotes){
            $status = false;
            $message = 'No Notes Found';
            $code = 400;
        }else if($foldernotes==''){
            $status = false;
            $message = 'Enter Notes Id';
            $code = 400;
        }

        if($status){
            $foldernotes->delete();
        }
        return response()->json(['status'=>$status,'message'=>$message,'code'=> $code]);
    }

	public function jobsDoneList(Request $request){


        $data = [];
        $message = "";
        $status = true;
        $code = 200;

        $id = $request->user_id;
        $page = $request->page;
        $users= User::where('users.id',$id)->first();
        if($users){
            $joblistdata = Job::join('jobassignemployee','job.id','=','jobassignemployee.job_id')
            ->leftJoin('clients','job.client_id','=','clients.id')
            ->where('jobassignemployee.job_employee_id',$id)
            ->where('job.job_status','markasdone')
            ->select('job.id as relation_id','job.title as title','job.description as description','job.duedate as duedate','job.notes as notes','job.status as status','job.deleted_at as deleted_at','job.created_by as created_by','job.created_at as created_at','job.updated_at as updated_at','job.client_id as client_id','job.job_status as job_status','job.job_number as job_number','job.start_time as start_time','job.end_time as end_time','jobassignemployee.job_id as job_id','jobassignemployee.job_employee_id as job_employee_id','clients.name as client_name');

            if ($request->search != ''){
                $search = $request->search;
                $where_filter = "(job.title LIKE '%$search%' OR job.job_number LIKE  '%$search%' OR job.duedate LIKE  '%$search%' OR clients.name LIKE  '%$search%' )";
                $joblistdata = $joblistdata->whereRaw($where_filter);

                //$joblistdata->where('job.title', 'like', $search.'%');
            }
            $joblistdata  = $joblistdata->paginate(10);

            if(count($joblistdata)>0){
                $data=$joblistdata;
            }
            else{
                $status = false;
                $message = 'No Jobs Found';
                $code = 200;
            }
        }
        else{
            $status = false;
            $message = 'No Users Found';
            $code = 200;
        }

        return response()->json(['status'=>$status, 'data'=>(object)$data,'message'=>$message,'code'=> $code]);
    }

    public function addJobImage(Request $request){
        $data = array();
        $code = 200;
        $messages = 'Added Successfully';
        $status = 'true';
        $error = '';
        $rules = array(
            'image_title' => 'sometimes',
            'image_description' => 'sometimes',
            'folder_id' => 'required',
            'job_id' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png|max:100000',
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 200;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {

            $job = Job::where('job.id', $request->job_id)->first();
            $folder = Folder::where('job_id', $request->job_id)->where('id', $request->folder_id)->first();

            if (!$job) {
                $status = false;
                $messages = 'No job Found';
                $code = 400;
            } else if (!$folder) {
                $status = false;
                $messages = 'No folder Found';
                $code = 400;
            }


            if ($status) {

                $data = $request->all();
                $data['title'] = (($data['image_title'] != null) ? $data['image_title'] : '');
                $data['description'] = (($data['image_description'] != null) ? $data['image_description'] : '');

                if ($request->image) {
                    $folder_image = $request->image;
                    $filename = uniqid(time()) . '.' . $folder_image->getClientOriginalExtension();
                    $folder_image->move(public_path('jobimages'), $filename);
                    $data['image_url'] = 'jobimages/' . $filename;
                }
                $formdata = JobImage::create($data);
                if ($formdata) {
                    $data = JobImage::find($formdata->id);


                    $imagedata['image_id'] = $formdata->id;
                    $imagedata['job_id'] = $request->job_id;
                    $imagedata['folder_id'] = $request->folder_id;
                    JobImageFolder::create($imagedata);

                    $messages = 'Added Successfully';
                    $status = true;
                    $code = 200;
                } else {
                    $status = false;
                    $code = \Config::get('constants.responce_code.bad_request');
                    $messages = 'Something went wrong ! Please try again later';
                }
            }
        }
        return response()->json(['data' => (object)$data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function jobImagesList(Request $request){
        $data['images'] = [];
        $code = 200;
        $messages = 'Listed Successfully';
        $status = 'true';
        $error = '';
        $rules = array(
            'job_id' => 'required'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 200;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {

                $job = Job::where('job.id', $request->job_id)->first();

                if (!$job) {
                    $status = false;
                    $messages = 'No job Found';
                    $code = 400;
                }

                if ($status) {
                    $jobImage = JobImage::select('jobimages.*','jobimages.id as job_image_id')->join('folderimages','folderimages.image_id','jobimages.id')->where('folderimages.job_id', $request->job_id)->where('jobimages.status', 'active')->get();
                    foreach ($jobImage as $key => $value) {
                        $path= public_path().'/'.$value->image_url;

                        if(file_exists($path))
                        {
                            $jobImage[$key]->image_url = url($value->image_url);
                        }
                        else
                        {
                            $jobImage[$key]->image_url = '';
                        }
                    }
                    $data['images'] = $jobImage;
                }

        }
        return response()->json(['data' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function jobImageDetail(Request $request){
        $data = (object)[];
        $code = 200;
        $messages = 'Get Successfully';
        $status = 'true';
        $error = '';
        $rules = array(
            'job_image_id' => 'required'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 200;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {

                $jobImage = JobImage::where('id', $request->job_image_id)->first();

                if (!$jobImage) {
                    $status = false;
                    $messages = 'No Image Found';
                    $code = 400;
                }

                if ($status) {

                        $path= public_path().'/'.$jobImage->image_url;

                        if(file_exists($path))
                        {
                            $jobImage->image_url = url($jobImage->image_url);
                        }
                        else
                        {
                            $jobImage->image_url = '';
                        }

                    $data = $jobImage;
                }

        }
        return response()->json(['data' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function jobImageDelete(Request $request){
        $data = (object)[];
        $code = 200;
        $messages = 'Deleted Successfully';
        $status = 'true';
        $error = '';
        $rules = array(
            'job_image_id' => 'required'
        );

        $validator = \Validator::make($request->all(), $rules, []);
        if ($validator->fails()) {
            $validation = $validator;
            $status = false;
            $code = 200;
            $msgArr = $validator->messages()->toArray();
            $messages = reset($msgArr)[0];
        } else {

                $jobImage = JobImage::where('id', $request->job_image_id)->first();

                if (!$jobImage) {
                    $status = false;
                    $messages = 'No Image Found';
                    $code = 400;
                }

                if ($status) {
                    $jobImage->delete();
                }

        }
        return response()->json(['data' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }

    public function jobCodesList(Request $request){
        $data['codes'] = [];
        $code = 200;
        $messages = 'Listed Successfully';
        $status = 'true';

        $jobs = Job::select('job_number')->where('status', 'active')->pluck('job_number')->toArray();

        $jobcode = Jobcode::select('job_code.*')->whereNotIn('job_code.id',$jobs)->where('job_code.status', 'active')->get();


        if ($jobcode != null) {
            $data['codes'] = $jobcode;
        }


        return response()->json(['data' => $data, 'code' => $code, 'message' => $messages, 'status' => $status]);
        exit;
    }




}


