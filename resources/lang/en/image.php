<?php

return [

	'add_image' => 'Add image',
    'add_new_image' => 'Add New image',
    'back' => 'Back',
    'edit_image' => 'Edit image',
    'update' => 'Update',
    'title' => 'Title',
	'images' => 'images',
    'id' => 'Id',
	'status' => 'Status',
	'actions' => 'Actions',
	'active' => 'Active',
    'inactive' => 'Inactive',
    'blocked' => 'Blocked',
	'view' => 'View',
	'image' => 'image',
	'create' => 'Create',
	'image_url' => 'image Url',
	'description' => 'Description',
	'changeimage'=>'Change image',
	'addimage'=>'image',
	'view_image'=>'View image',
];