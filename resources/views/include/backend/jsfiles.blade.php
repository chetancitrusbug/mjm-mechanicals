<!-- / jquery [required] -->
<script src="{!! asset('assets/javascripts/jquery/jquery.min.js') !!}" type="text/javascript"></script>
<!-- / jquery mobile (for touch events) -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<script src="{!! asset('assets/javascripts/jquery/jquery.mobile.custom.min.js') !!}" type="text/javascript"></script>
<!-- / jquery migrate (for compatibility with new jquery) [required] -->
<script src="{!! asset('assets/javascripts/jquery/jquery-migrate.min.js') !!}" type="text/javascript"></script>
<!-- / jquery ui -->
<script src="{!! asset('assets/javascripts/jquery/jquery-ui.min.js') !!}" type="text/javascript"></script>
<!-- / jQuery UI Touch Punch -->
<script src="{!! asset('assets/javascripts/plugins/jquery_ui_touch_punch/jquery.ui.touch-punch.min.js') !!}"
        type="text/javascript"></script>
<!-- / bootstrap [required] -->
<script src="{!! asset('assets/javascripts/bootstrap/bootstrap.js') !!}" type="text/javascript"></script>


<script src="{!! asset('js/bootstrap-datetimepicker.min.js') !!}"
        type="text/javascript"></script>

<script src="{!! asset('assets/javascripts/plugins/select2/select2.js') !!}" type="text/javascript"></script>
<!-- / modernizr -->


<script src="{!! asset('assets/javascripts/plugins/modernizr/modernizr.min.js') !!}" type="text/javascript"></script>
<!-- / retina -->
<script src="{!! asset('assets/javascripts/plugins/retina/retina.js') !!}" type="text/javascript"></script>
<!-- / theme file [required] -->
<script src="{!! asset('assets/javascripts/theme.js') !!}" type="text/javascript"></script>
<!-- / demo file [not required!] -->
<script src="{!! asset('assets/javascripts/demo.js') !!}" type="text/javascript"></script>



{{-- Data Table--}}
<script src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="//cdn.datatables.net/responsive/2.2.0/js/dataTables.responsive.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js" ></script>

<script src="{!! asset('assets/javascripts/plugins/validate/jquery.validate.min.js')!!}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/datejs/1.0/date.min.js"></script>


<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.js"></script>


<script type="text/javascript">

$(document).ready(function() {
function changeTab(){
                var taskactiveurl = window.location.href;
                var taskque= taskactiveurl.substring(taskactiveurl.indexOf('?'));
                var taskhash= taskactiveurl.substring(taskactiveurl.indexOf('#'));
                var split = taskhash.split("?");
                if(split[0] != null){
                        subfolderurl = split[0];
                }
                subfolderurl = subfolderurl.replace("#", "");
                //console.log(subfolderurl);
                if(taskhash == '#folderactive'){
                        $("ul li.jobfolderadds").addClass("active");
                        $("div #folder").addClass(" active in");
                        $( "ul.subfolderul li:first-child" ).addClass("active" );
                        $( "div.subfolderdiv:first" ).removeClass("fade" );
                        $( "div.subfolderdiv:first" ).addClass("active" );
                        $("ul li.jobtaskadds").removeClass("active");
                        $("div #task").removeClass("active in");
                        $("ul li.jobadds").removeClass("active");
                        $("div #job").removeClass(" active in");
						$("ul li.job_details_edit").removeClass("active");
                        $("div #jobdetails").removeClass(" active in");
						$("ul li.order_form_list").removeClass("active");
                        $("div #order_form").removeClass(" active in");
                        $("ul li.jobnotesadds").removeClass("active");
                        $("div #notes").removeClass("active in");
                }else if(taskhash == '#taskfolderactive'){
					console.log('here');
                        $("ul li.jobtaskadds").addClass("active");
                        $("div #task").addClass("active in");
                        $( "ul.subtaskfolderul li:first-child" ).addClass("active" );
                        $( "div.subtaskfolderdiv:first" ).removeClass("fade" );
                        $( "div.subtaskfolderdiv:first" ).addClass("active" );
                        $("ul li.jobadds").removeClass("active");
                        $("div #job").removeClass(" active in");
						$("ul li.job_details_edit").removeClass("active");
                        $("div #jobdetails").removeClass(" active in");
						$("ul li.order_form_list").removeClass("active");
                        $("div #order_form").removeClass(" active in");
                        $("ul li.jobfolderadds").removeClass("active");
                        $("div #folder").removeClass("active in");
                } else  if(taskhash == '#tasklist'){
                        $("ul li.jobtaskadds").addClass("active");
                        $("div #task").addClass(" active in");
                        $("ul li.jobfolderadds").removeClass("active");
                        $("div #folder").removeClass(" active in");
						$("ul li.jobadds").removeClass("active");
                        $("div #job").removeClass(" active in");
						$("ul li.job_details_edit").removeClass("active");
                        $("div #jobdetails").removeClass(" active in");
						$("ul li.order_form_list").removeClass("active");
                        $("div #order_form").removeClass(" active in");
                        $("ul li.jobnotesadds").removeClass("active");
                        $("div #notes").removeClass("active in");
                        $( "ul.subtaskfolderul li" ).removeClass("active");
						$( "div.subtaskfolderdiv" ).removeClass("active");
                        $( "ul.subtaskfolderul li:first-child" ).addClass("active" );
                        $( "div.subtaskfolderdiv:first" ).removeClass("fade" );
                        $( "div.subtaskfolderdiv:first" ).addClass("active" );
                }else  if(taskhash == '#taskactive'){
                        $("ul li.jobtaskadds").addClass("active");
                        $("div #task").addClass(" active in");
                        $("ul li.jobfolderadds").removeClass("active");
                        $("div #folder").removeClass(" active in");
                        $("ul li.jobadds").removeClass("active");
                        $("div #job").removeClass(" active in");
                        $("ul li.job_details_edit").removeClass("active");
                        $("div #jobdetails").removeClass(" active in");
						$("ul li.order_form_list").removeClass("active");
                        $("div #order_form").removeClass(" active in");
                        $("ul li.jobnotesadds").removeClass("active");
                        $("div #notes").removeClass("active in");
                }else if(taskhash == '#orderlist'){
                        $("ul li.order_form_list").addClass("active");
                        $("div #order_form").addClass(" active in");
                        $("ul li.jobadds").addClass("active");
                        $("div #job").addClass(" active in");
                        $("ul li.job_details_edit").removeClass("active");
                        $("div #jobdetails").removeClass(" active");
                        $("ul li.jobnotesadds").removeClass("active");
                        $("div #notes").removeClass("active in");
                }
                 else if(taskque == '?folderactive'){
                        $("ul li.jobfolderadds").addClass("active");
                        $("div #folder").addClass(" active in");
                        if(subfolderurl){
							$("li.sub_folder_id_"+subfolderurl).addClass("active");
							$("div #jobdetails"+subfolderurl).addClass("active in");
                        }
                 }
                else if(taskque == '?taskfolderactive'){

                        $("ul li.jobtaskadds").addClass("active");
                        $("div #task").addClass(" active in");
                        if(subfolderurl){

							$("li.subtask_folder_id_"+subfolderurl).addClass("active");
							$("div #jobtaskdetails"+subfolderurl).addClass("active in");
                        }
                }else if(taskhash == '#noteslist'){
                        $("ul li.order_form_list").removeClass("active");
                        $("div #order_form").removeClass(" active in");
                        $("ul li.jobadds").removeClass("active");
                        $("div #job").removeClass(" active in");
                         $("ul li.job_details_edit").removeClass("active");
                        $("div #jobdetails").removeClass(" active");
                        $("ul li.jobnotesadds").addClass("active");
                        $("div #notes").addClass("active in");
                } else{
                        $("ul li.jobadds").addClass("active");
                        $("div #job").addClass(" active in");
                        $( "ul.subfolderul li:first-child" ).addClass("active" );
                        $( "div.subfolderdiv:first" ).removeClass("fade" );
                        $( "div.subfolderdiv:first" ).addClass("active" );
						$("ul li.job_details_edit").addClass("active");
                        $("div #jobdetails").addClass(" active in");
						$("ul li.jobnotesadds").removeClass("active");
                        $("div #notes").removeClass("active in");
                }

        }
        changeTab();
        $("li.jobtaskadds a").click(function() {
                var taskurl= window.location.href.split("#")[0];// Returns full URL
                window.location.href=taskurl+'#tasklist';
                changeTab();
                if(subfolderurl){
							$("li.sub_folder_id_"+subfolderurl).removeClass("active");
							$("div #jobdetails"+subfolderurl).removeClass("active in");
                }
                console.log('here');
				$("ul li.jobfolderadds").removeClass("active");
                        $("div #folder").removeClass(" active in");
						$( "ul.subtaskfolderul li" ).removeClass("active" );
						$( "div.subtaslfolderdiv" ).removeClass("active" );
                        $( "ul.subtaskfolderul li:first-child" ).addClass("active" );
                        $( "div.subtaskfolderdiv:first" ).removeClass("fade" );
                        $( "div.subtaskfolderdiv:first" ).addClass("active" );
                        $("ul li.jobtaskadds").addClass("active");
                        $("div #task").addClass("active in");
                        $("ul li.jobadds").removeClass("active");
                        $("div #job").removeClass(" active in");
						$("ul li.job_details_edit").removeClass("active");
                        $("div #jobdetails").removeClass(" active in");
						$("ul li.order_form_list").removeClass("active");
                        $("div #order_form").removeClass(" active in");
                        $("ul li.jobnotesadds").removeClass("active");
                        $("div #notes").removeClass("active in");
        });


		$(".jobcard_back").click(function() {
				var url = '{{ URL::previous() }}';
				window.location.href=url+'#orderlist';
		});
		$(".jobadds").click(function() {
				var taskurl= window.location.href.split("#")[0];
				window.location.href=taskurl+'#joblist';
				//alert('here');
				$("ul li.jobadds").addClass("active");
                $("div #job").addClass(" active in");
                $("ul li.jobtaskadds").removeClass("active");
                $("div #task").removeClass(" active in");
                $("ul li.jobfolderadds").removeClass("active");
                $("div #folder").removeClass(" active in");
                $( "ul.subfolderul li" ).removeClass("active" );
                $( "div.subfolderdiv" ).removeClass("active" );
                $("ul li.jobnotesadds").removeClass("active");
                $("div #notes").removeClass("active in");
                $("ul li.job_details_edit").addClass("active");
                $("div #jobdetails").addClass("active in");
                $("div #forms").removeClass("active");
                $("ul li.jobformsadds").removeClass("active");
        });

        $(".jobformsadds").click(function() {
            var taskurl= window.location.href.split("#")[0];// Returns full URL
            window.location.href=taskurl+'#forms';

                // $("ul li.jobadds").removeClass("active");
                // $("div #job").removeClass(" active in");
                // $("ul li.jobtaskadds").removeClass("active");
                // $("div #task").removeClass(" active in");
                // $("ul li.jobfolderadds").removeClass("active");
                // $("div #folder").removeClass(" active in");
                // $( "ul.subfolderul li" ).removeClass("active" );
                // $( "div.subfolderdiv" ).removeClass("active" );
                // $("ul li.job_details_edit").removeClass("active");
                // $("div #jobdetails").removeClass("active in");
                // $("div #notes").removeClass("active in");
                // $("div #forms").addClass("active");
                // $("ul li.jobformsadds").addClass("active");
        });

        $("li.jobfolderadds").click(function() {
			  var taskurl= window.location.href.split("#")[0];// Returns full URL
			   window.location.href=taskurl+'#folderlist';
			  	if(subfolderurl){
                    // $("li .sub_folder_id_"+subfolderurl).removeClass("active");
                    // $("div #jobdetails"+subfolderurl).removeClass("active in");
                }
				$("ul li.jobfolderadds").addClass("active");
                $("div #folder").addClass(" active in");
                $( "ul.subfolderul li" ).removeClass("active" );
                $( "div.subfolderdiv" ).removeClass("active" );
                $( "ul.subfolderul li:first-child" ).addClass("active" );
                $( "div.subfolderdiv:first" ).removeClass("fade" );
                $( "div.subfolderdiv:first" ).addClass("active" );
                $("ul li.jobtaskadds").removeClass("active");
                $("div #task").removeClass("active in");
                $("ul li.jobadds").removeClass("active");
                $("div #job").removeClass(" active in");
                $("ul li.job_details_edit").removeClass("active");
                $("div #jobdetails").removeClass(" active in");
                $("ul li.order_form_list").removeClass("active");
                $("div #order_form").removeClass(" active in");
                $("ul li.jobnotesadds").removeClass("active");
                $("div #notes").removeClass("active in");
                $("div #forms").removeClass("active");
                $("ul li.jobformsadds").removeClass("active");
				//changeTab();

		});
        $(".jobnotesadds").click(function() {
				//var url = '{{ URL::previous() }}';
				//window.location.href=url+'#noteslist';
                 var taskurl= window.location.href.split("#")[0];// Returns full URL
			   window.location.href=taskurl+'#noteslist';
				//alert('here');
				 $("ul li.jobadds").removeClass("active");
                        $("div #job").removeClass(" active in");
						$("ul li.jobtaskadds").removeClass("active");
                        $("div #task").removeClass(" active in");
                        $("ul li.jobfolderadds").removeClass("active");
                        $("div #folder").removeClass(" active in");
						$( "ul.subfolderul li" ).removeClass("active" );
						$( "div.subfolderdiv" ).removeClass("active" );
						$("ul li.job_details_edit").removeClass("active");
                        $("div #jobdetails").removeClass("active in");
                        $("div #notes").addClass("active in");
		});
		$(".job_details_edit").click(function() {
				var taskurl= window.location.href.split("#")[0];// Returns full URL
				window.location.href=taskurl;

		});
		$(".order_form_list").click(function() {
				var taskurl= window.location.href.split("#")[0];// Returns full URL
				window.location.href=taskurl+'#orderlist';
				changeTab();

		});

$('.summernote_class_content').summernote({
lang: 'fr-FR',
        height: 300,
        toolbar : [
        ['style', ['bold', 'italic', 'underline', 'clear']],
        ['font', ['fontsize']],
        ['color', ['color']],
        ['para', ['ul', 'ol', 'paragraph']],
        ['link', ['link']],
        //['picture', ['picture']]
        ],
        onImageUpload: function(files, editor, welEditable) {
        for (var i = files.lenght - 1; i >= 0; i--) {
        sendFile(files[i], this);
        }
        }
});
function sendFile(file, el) {
var form_data = new FormData();
form_data.append('file', file);
$.ajax ({
data: form_data,
        type: "POST",
        url: "../up.php",
        cache: false,
        contentType: false,
        processData: false,
        success: function(url) {
        $(el).summernote('editor.insertImage', url);
        }
})
}

});





        </script>


