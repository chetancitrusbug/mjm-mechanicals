<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class JobImage extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'jobimages';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description','image_url','status'];
    protected $appends =['image_full_url'];
    public function getImageFullUrlAttribute()
    {
        $path= public_path().'/'.$this->image_url;

        if(file_exists($path))
        {
            return url(''.$this->image_url);
        }
        return '';
    }
}
