<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'status_dropdown' => [
        'new' => 'New',
        'open' => 'Open',
        'pending' => 'Pending',
        'on-hold' => 'On-hold',
        'solved' => 'Solved',
        'closed' => 'Closed'
    ],
    'label'=>[
        'dashboard'=>'Dashboard',
        'all_tickets'=>'All Tickets',
    ],
];
