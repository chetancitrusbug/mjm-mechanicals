<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobChecklistForm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_checklist_form', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('job_id');
            $table->integer('user_id');
            $table->string('all_register_open')->nullable();
            $table->string('install_toe_kicks')->nullable();
            $table->string('install_thermostat')->nullable();
            $table->string('furnace_cleaned')->nullable();
            $table->string('place_new_filter')->nullable();
            $table->string('place_new_filter_size')->nullable();
            $table->string('place_new_filter_type')->nullable();
            $table->string('set_ac_and_level')->nullable();
            $table->string('pad_size')->nullable();
            $table->string('basement_rack')->nullable();
            $table->string('weigh_in_charge')->nullable();
            $table->string('pre_charge')->nullable();
            $table->string('total_added')->nullable();
            $table->string('line_set_length')->nullable();
            $table->string('inside_of_ac_panel')->nullable();
            $table->string('install_start_assist')->nullable();
            $table->string('spp6')->nullable();
            $table->string('factory_kit')->nullable();
            $table->string('verify_fireplace_valve')->nullable();
            $table->string('startup_furnace')->nullable();
            $table->string('gas_pressure_lo')->nullable();
            $table->string('gas_pressure_hi')->nullable();
            $table->string('fan_speed')->nullable();
            $table->string('temp_rise')->nullable();
            $table->string('ac_hp_fan_speed')->nullable();
            $table->string('stickers_on_equipment')->nullable();
            $table->string('drain_installed')->nullable();
            $table->string('floor_drain')->nullable();
            $table->string('sump_pit')->nullable();
            $table->string('condo_pump')->nullable();
            $table->string('flue_pipes')->nullable();
            $table->string('flue_pipes_size')->nullable();
            $table->string('flue_pipes_side_wall')->nullable();
            $table->string('flue_pipes_roof')->nullable();
            $table->string('flue_pipes_2pipe')->nullable();
            $table->string('flue_pipes_concentric')->nullable();
            $table->string('stat_wire_size')->nullable();
            $table->string('thermostat')->nullable();
            $table->string('ac_hp')->nullable();
            $table->string('humidifier')->nullable();
            $table->string('by_pass')->nullable();
            $table->string('powered')->nullable();
            $table->string('water_line_installed')->nullable();
            $table->string('tested')->nullable();
            $table->string('control_type_stat')->nullable();
            $table->string('manufacture_control')->nullable();
            $table->string('brand')->nullable();
            $table->string('fresh_air_damper')->nullable();
            $table->string('ac_disconnect_installed')->nullable();
            $table->string('status')->default('active')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_checklist_form');
    }
}
