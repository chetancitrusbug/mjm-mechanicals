<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRaGrillJobProjectWorksheetForm3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_project_worksheet_form_3', function (Blueprint $table) {
            $table->string('ra_grille_del',50)->nullable()->after('flr_regs_14_4_del');
            $table->string('ra_grille_usd',50)->nullable()->after('flr_regs_14_4_usd');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job_project_worksheet_form_3', function (Blueprint $table) {
            $table->dropColumn('ra_grille_del');
            $table->dropColumn('ra_grille_usd');
        });
    }
}
