<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobFurnaceForm extends Model
{
    //
    use SoftDeletes;

    public $table='job_furnace_form';
   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id','job_id','user_id','date','customer_name','address','phone','email','system_age','staying_warm_enough','temrature_consistent','dry_inside_home','have_any_weird_smells','furnace_boiler_1','brand_1','model_1','serial_1','furnace_boiler_2','brand_2','model_2','serial_2','furnace_boiler_3','brand_3','model_3','serial_3','filter_size_1','quantity_1','accessories_1','filter_size_2','quantity_2','accessories_2','filter_size_3','quantity_3','accessories_3','gas_type','blower','ignition','stages','draft_meter','efficiency','blower_amps_r','blower_amps_a','blower_capacitor_r','blower_capacitor_a','draft_motor_amps_r','draft_motor_amps_a','draft_motor_capacitor_r','draft_motor_capacitor_a','ignitor_ohms_r','ignitor_ohms_a','temprature_rise_rated','temprature_rise_actual','temprature_rise_degrees','flame_signal_ua','gas_pressure_r','gas_pressure_a','thermostat_setpoint','temp_humidity','tstat_type','motor_bearings','blower_wheel','heat_exchanger','burners','ignitor','limits','notes','tech','occupant','bottom_date','status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function getDateAttribute($value)
    {
        if($value != ""){
            return \Carbon\Carbon::parse($value)->format('d-m-Y');
        }
        return $value;
    }

    public function setDateAttribute($value)
    {
        if($value != ""){
            $this->attributes['date'] = \Carbon\Carbon::parse($value)->format('Y-m-d');
        }
    }

    public function getBottomDateAttribute($value)
    {
        if($value != ""){
            return \Carbon\Carbon::parse($value)->format('d-m-Y');
        }
        return $value;
    }

    public function setBottomDateAttribute($value)
    {
        if($value != ""){
            $this->attributes['bottom_date'] = \Carbon\Carbon::parse($value)->format('Y-m-d');
        }
    }
}
