@extends('layouts.backend')

@section('title',trans('material.edit_material'))
@section('pageTitle',trans('material.edit_material'))


@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('material.edit_material')</div>
                <div class="panel-body">
                    <a href="{{ url('/admin/material') }}" title="Back">
                        <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> @lang('material.back')
                        </button>
                    </a>
                    <br/>
                    <br/>

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model($material, [
                        'method' => 'PATCH',
                        'url' => ['/admin/material', $material->id],
                        'class' => 'form-horizontal',
                        'id'=>'formmaterial',
                        'enctype'=>'multipart/form-data'
                    ]) !!}


                    @include ('admin.material.form', ['submitButtonText' => trans('material.update')])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
