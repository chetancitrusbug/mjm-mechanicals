<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('street1');
            $table->string('street2')->nullable();
            $table->string('country');
            $table->string('province');
            $table->string('city');
            $table->string('email')->unique();
            $table->string('phone');
		    $table->string('status')->default('active')->nullable();

            $table->softDeletes();

            $table->integer('created_by')->default(0);
			$table->integer('updated_by')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropIfExists('clients');
    }
}
